//
//  AppDelegate.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 13..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ESFramework/ESFramework.h>
#import <UserNotifications/UserNotifications.h>
#import "RootViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RootViewController *rootViewController;

@end

