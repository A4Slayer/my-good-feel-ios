//
//  EventDetailViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 14..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "EventDetailViewController.h"

@interface EventDetailViewController ()

@end

@implementation EventDetailViewController

- (void)updateView
{
    [_loginView removeFromSuperview];
    _loginView = nil;
    
    [_imageScrollView removeFromSuperview];
    _imageScrollView = nil;

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"cookie"]) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
        NSDictionary *cookieProperties = @{
                                           NSHTTPCookieDomain: @".stiscloudbonds.com",
                                           NSHTTPCookiePath: @"/",
                                           NSHTTPCookieName: @".ASPXAUTH",
                                           NSHTTPCookieValue: [[NSUserDefaults standardUserDefaults] objectForKey:@"cookie"]
                                           };
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:[NSHTTPCookie cookieWithProperties:cookieProperties]];
    }
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_dataDictionary[@"MoveUrl"]]]];
    [_webView setFrame:self.view.bounds];

}

- (void)popupSignView:(UIButton *)button
{
    EventSignViewController *eventSignViewController = [[EventSignViewController alloc] initWithNibName:nil bundle:nil pClass:self type:(int)button.tag];
    
    UINavigationController *eventSignNavigationController = [[UINavigationController alloc] initWithRootViewController:eventSignViewController];
    [eventSignNavigationController.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:eventSignNavigationController animated:YES completion:^{
        //
    }];
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadEvent:(NSString *)eventNo
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_EVENT_GET_DETAIL]];

    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@", eventNo];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            
            [[DataSingleton sharedSingletonClass] setBadgeString:responseData[@"data"][@"ReadCnt"]];
            
            NSDictionary *dict = @{@"MoveUrl" : responseData[@"data"][@"MoveUrl"]};
            self->_dataDictionary = dict;
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"cookie"]) {
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
                NSDictionary *cookieProperties = @{
                                                   NSHTTPCookieDomain: [NSURL URLWithString:[responseData[@"data"][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]].host,
                                                   NSHTTPCookiePath: @"/",
                                                   NSHTTPCookieName: @".ASPXAUTH",
                                                   NSHTTPCookieValue: [[NSUserDefaults standardUserDefaults] objectForKey:@"cookie"]
                                                   };
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:[NSHTTPCookie cookieWithProperties:cookieProperties]];
            }
            
            self->_webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT)];
            [self->_webView setDelegate:self];
            [self->_webView setScalesPageToFit:YES];
            [self.view addSubview:self->_webView];
            
            if ([responseData[@"data"][@"EventType"] intValue] == 101) {
                if ([[[DataSingleton sharedSingletonClass] userInfo][@"UserType"] intValue] == 102) {
                    
                    self->_loginView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - kTOP_HEIGHT - CGHeightFromIP6P(256 / 3) - kIPHONE_X_BOTTOM_MARGIN, kSCREEN_WIDTH, CGHeightFromIP6P(256 / 3) + kIPHONE_X_BOTTOM_MARGIN)];
                    [self->_loginView setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
                    [self->_loginView.layer setShadowPath:[UIBezierPath bezierPathWithRect:self->_loginView.bounds].CGPath];
                    [self->_loginView.layer setShadowColor:[UIColor blackColor].CGColor];
                    [self->_loginView.layer setShadowOpacity:0.25];
                    [self->_loginView.layer setShadowOffset:CGSizeMake(0, 0)];
                    [self->_loginView.layer setShadowRadius:10];
                    [self.view addSubview:self->_loginView];
                    
                    UIButton *loginButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(48 / 3), CGHeightFromIP6P(42 / 2), CGWidthFromIP6P(549 / 3), CGHeightFromIP6P(173 / 3))];
                    [loginButton setImage:[UIImage imageNamed:@"event_button_login"] forState:UIControlStateNormal];
                    [loginButton addTarget:self action:@selector(popupSignView:) forControlEvents:UIControlEventTouchUpInside];
                    [loginButton setTag:0];
                    [self->_loginView addSubview:loginButton];
                    
                    UIButton *signUpButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(645 / 3), CGHeightFromIP6P(42 / 2), CGWidthFromIP6P(549 / 3), CGHeightFromIP6P(173 / 3))];
                    [signUpButton setImage:[UIImage imageNamed:@"event_button_signup"] forState:UIControlStateNormal];
                    [signUpButton addTarget:self action:@selector(popupSignView:) forControlEvents:UIControlEventTouchUpInside];
                    [signUpButton setTag:1];
                    [self->_loginView addSubview:signUpButton];
                    
                    self->_imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT - self->_loginView.frame.size.height)];
                    [self->_imageScrollView setBackgroundColor:[UIColor lightGrayColor]];
                    [self.view addSubview:self->_imageScrollView];
                    
                    UIImageView *detailImageView = [[UIImageView alloc] init];
                    [self->_imageScrollView addSubview:detailImageView];
                    
                    [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[responseData[@"data"][@"EventDetailPath"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                                 options:0
                                                                progress:nil
                                                               completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                                   if (!error) {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [detailImageView setImage:image];
                                                                           float ratio = kSCREEN_WIDTH / detailImageView.image.size.width;
                                                                           [detailImageView setFrame:CGRectMake(0, 0, kSCREEN_WIDTH, (int)detailImageView.image.size.height * ratio)];
                                                                           [self->_imageScrollView setContentSize:CGSizeMake(kSCREEN_WIDTH, detailImageView.frame.size.height)];
                                                                       });
                                                                   } else {
                                                                   }
                                                               }];

                } else {
                    [self->_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[responseData[@"data"][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]]];
                    
                    self->_loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    [self->_loadingIndicatorView setCenter:CGPointMake(self->_webView.frame.size.width / 2, self->_webView.frame.size.height / 2)];
                    [self->_webView addSubview:self->_loadingIndicatorView];
                    
                    [self->_loadingIndicatorView startAnimating];
                }
            } else if ([responseData[@"data"][@"EventType"] intValue] == 102) {
                [self->_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[responseData[@"data"][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]]];
                
                self->_loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                [self->_loadingIndicatorView setCenter:CGPointMake(self->_webView.frame.size.width / 2, self->_webView.frame.size.height / 2)];
                [self->_webView addSubview:self->_loadingIndicatorView];
                
                [self->_loadingIndicatorView startAnimating];
            }

        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass eventNo:(NSString *)eventNo
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xefeff4)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"이벤트"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x000000)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
        [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
        
        [self loadEvent:eventNo];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateView) name:@"UPDATE_EVENT_DETAIL" object:nil];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (_loadingIndicatorView) {
        [_loadingIndicatorView stopAnimating];
        [_loadingIndicatorView removeFromSuperview];
        _loadingIndicatorView = nil;
    }
}


- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[request.URL scheme] isEqualToString:@"elle"]) {
        [self popViewController];
    }
    
    return YES;
}

@end
