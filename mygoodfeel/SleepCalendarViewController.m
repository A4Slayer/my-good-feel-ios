//
//  SleepCalendarViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 12. 4..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SleepCalendarViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface SleepCalendarViewController ()

@end

@implementation SleepCalendarViewController

- (void)bannerRollingThread
{
    if (self) {
        if (_bannerScrollView.contentSize.width > _bannerScrollView.frame.size.width) {
            if (_bannerScrollView.contentOffset.x < _bannerScrollView.contentSize.width - _bannerScrollView.frame.size.width) {
                [_bannerScrollView setContentOffset:CGPointMake(_bannerScrollView.contentOffset.x + _bannerScrollView.frame.size.width, _bannerScrollView.contentOffset.y) animated:YES];
            } else {
                [_bannerScrollView setContentOffset:CGPointMake(0, _bannerScrollView.contentOffset.y) animated:YES];
            }
            
        }
        [self performSelector:@selector(bannerRollingThread) withObject:nil afterDelay:8.0f];
    }
}

- (void)touchBanner:(UIButton *)button
{
    [MSAnalytics trackEvent:@"Banner clicked" withProperties:@{
                                                               @"PageId" : @"SleepInfoMonthly",
                                                               @"BannerNo" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerNo"] intValue]],
                                                               @"BannerPublishGroupCd" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerPublishGroupCd"] intValue]],
                                                               @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                               @"All" : [NSString stringWithFormat:@"%@|%@|%@|%@",
                                                                         @"SleepInfoMonthly",
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerNo"] intValue]],
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerPublishGroupCd"] intValue]],
                                                                         [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                         ]
                                                               }];
    
    if ([_dataDictionary[@"Banner"][button.tag][@"MoveType"] intValue] == 101) {
        [(RootViewController *)_pClass changeToEventViewController];
        EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_dataDictionary[@"Banner"][button.tag][@"IndexNo"]];
        UINavigationController *eventNavigationController = (UINavigationController *)[[(RootViewController *)_pClass drawerViewController] centerViewController];
        [eventNavigationController pushViewController:eventDetailViewController animated:YES];
    } else if ([_dataDictionary[@"Banner"][button.tag][@"MoveType"] intValue] == 102) {
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][button.tag][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] options:@{} completionHandler:^(BOOL success) {
                exit(0);
            }];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][button.tag][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
        }
    }
}
- (void)resignPicker
{
    [_monthScrollView setContentOffset:CGPointMake((_monthScrollView.frame.size.width * 12 * [_datePickerView selectedRowInComponent:0]) + _monthScrollView.frame.size.width * [_datePickerView selectedRowInComponent:1], 0) animated:YES];
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self->_blackMaskView setAlpha:0];
        [self->_dateSelectView setFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 215 + 44)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)prevMonth
{
    if ((int)_monthScrollView.contentOffset.x % (int)_monthScrollView.frame.size.width == 0) {
        if ((int)_monthScrollView.contentOffset.x > 0) {
            [_monthScrollView setContentOffset:CGPointMake((int)_monthScrollView.contentOffset.x - (int)_monthScrollView.frame.size.width, (int)_monthScrollView.contentOffset.y) animated:YES];
        }
    }
}

- (void)nextMonth
{
    if ((int)_monthScrollView.contentOffset.x % (int)_monthScrollView.frame.size.width == 0) {
        if ((int)_monthScrollView.contentOffset.x < (int)_monthScrollView.contentSize.width - (int)_monthScrollView.frame.size.width) {
            [_monthScrollView setContentOffset:CGPointMake((int)_monthScrollView.contentOffset.x + (int)_monthScrollView.frame.size.width, (int)_monthScrollView.contentOffset.y) animated:YES];
        }
    }
}

- (void)removeGuideView:(UIButton *)guideButton
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [guideButton setAlpha:0];
    } completion:^(BOOL finished) {
        [guideButton removeFromSuperview];
    }];
    
}

- (void)becomePicker
{
    if (![_blackMaskView superview]) {
        [self.navigationController.view addSubview:_blackMaskView];
        [self.navigationController.view addSubview:_dateSelectView];
    }
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self->_blackMaskView setAlpha:0.7];
        [self->_dateSelectView setFrame:CGRectMake(0, kSCREEN_HEIGHT - 215 - 44, kSCREEN_WIDTH, 215 + 44)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)drawGraph
{
    // 16 to 21
//    int firstDay = 29;
//    int lastDay = 30;
//    int maxDay = 30;
//
//    firstDay--;
//    //        // start day percent
//    //        firstDay / (float)maxDay;
//    //
//    //        // period percent
//    //        (lastDay - firstDay) / (float)maxDay;
//
//    UIView *periodView = [[UIView alloc] initWithFrame:CGRectMake(_graphView.frame.size.width * (firstDay / (float)maxDay), 0, _graphView.frame.size.width * ((lastDay - firstDay) / (float)maxDay), _graphView.frame.size.height)];
//    [periodView setBackgroundColor:UIColorFromRGB(0xea979c)];
//    [periodView setAlpha:0.53f];
//    [_graphView addSubview:periodView];
    
    for (UIView *sView in [_contentView subviews]) {
        if (!(sView == _sleepTimeButton || sView == _sleepScoreButton)) {
            [sView removeFromSuperview];
        }
    }
    
    if (isSleepTime) {
        [_avrSleepTimeLabel setText:@"평균 수면 시간"];
        [_prevValueLabel setText:_dataDictionary[@"AVGSleepTime"][@"BeforeSleepTiem"]];
        [_currValueLabel setText:_dataDictionary[@"AVGSleepTime"][@"CurrentSleepTiem"]];
    } else {
        [_avrSleepTimeLabel setText:@"평균 수면 점수"];
        [_prevValueLabel setText:_dataDictionary[@"AVGSleepScore"][@"BeforeSleepScore"]];
        [_currValueLabel setText:_dataDictionary[@"AVGSleepScore"][@"CurrentSleepScore"]];
    }
    
    if (!_sleepArray) {
        // 더미 뷰 하나 붙이고
        UIImageView *sensorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(364) / 3, CGHeightFromIP6P(364) / 3)];
        [sensorImageView setImage:[UIImage imageNamed:@"connectsensor_image_sensor"]];
        [sensorImageView setCenter:CGPointMake(_contentView.frame.size.width / 2, _contentView.frame.size.height / 2)];
        [_contentView addSubview:sensorImageView];
        
        UILabel *guideLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, sensorImageView.frame.origin.y + sensorImageView.frame.size.height + CGHeightFromIP6P(24), _contentView.frame.size.width, 10)];
        [guideLabel setFont:LightWithSize(15)];
        [guideLabel setTextColor:UIColorFromRGB(0x000000)];
        [guideLabel setText:@"센서를 사용하여\n수면 정보를 확인하실 수 있습니다."];
        [guideLabel setNumberOfLines:0];
        [guideLabel sizeToFit];
        [guideLabel setTextAlignment:NSTextAlignmentCenter];
        [guideLabel setFrame:CGRectMake(guideLabel.frame.origin.x, guideLabel.frame.origin.y, _contentView.frame.size.width, guideLabel.frame.size.height)];
        [_contentView addSubview:guideLabel];

        
        return;
    }
    
    NSString *monthString = [[NSString alloc] initWithFormat:@"%@-%02lu-05", _yearLabel.text, (long)(_monthScrollView.contentOffset.x / _monthScrollView.frame.size.width) % 12 + 1];       // 타임존 문제로 1일이 아닌 여유있게 5일로 설정
    NSDateFormatter *monthDateFormatter = [[NSDateFormatter alloc] init];
    [monthDateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[monthDateFormatter dateFromString:monthString]]; // Get necessary date components

    // set last of month
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    NSDate *tDateMonth = [calendar dateFromComponents:comps];
    [monthDateFormatter setDateFormat:@"dd"];
    
    int lastDay = [[monthDateFormatter stringFromDate:tDateMonth] intValue];
    
    UIImageView *graphView = [[UIImageView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(204 / 3), CGHeightFromIP6P(300 / 3), CGWidthFromIP6P(992 / 3), CGHeightFromIP6P(562 / 3))];
    [graphView setBackgroundColor:[UIColor whiteColor]];
    [_contentView addSubview:graphView];
    
    UIGraphicsBeginImageContext(graphView.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [graphView.image drawInRect:CGRectMake(0, 0, graphView.frame.size.width, graphView.frame.size.height)];
    CGContextSetLineWidth(context, k2PX + k2PX);

    
    int maxScore = 0;
    
    for (int i = 0; i < [_sleepArray count]; i++) {
        SleepSummaryComplete *sleepSummary = _sleepArray[i];
        
        if (isSleepTime) {
            
            int validSleepCount = 0;
            for (int j = 0; j < [sleepSummary.hypnoTypeGraph count]; j++) {
                if ([sleepSummary.hypnoTypeGraph[j] intValue] != -1) {
                    validSleepCount++;
                }
            }
            if (maxScore < validSleepCount) {
                maxScore = validSleepCount;
            }
        } else {
            if (maxScore < (int)[sleepSummary getSleepParams].scoreTotalSleepTime) {
                maxScore = (int)[sleepSummary getSleepParams].scoreTotalSleepTime;
            }
        }
        
    }
    
    CGPoint lastPoint;
    
    SleepSummaryComplete *firstSleepSummary = _sleepArray[0];
    
    int sleepDay = [[monthDateFormatter stringFromDate:[[NSDate alloc] initWithTimeIntervalSince1970:[firstSleepSummary getSleepParams].timestamp]] intValue];

    if (sleepDay == 1) {
        lastPoint = CGPointMake(0, graphView.frame.size.height - ( [firstSleepSummary getSleepParams].scoreTotalSleepTime / maxScore ) * graphView.frame.size.height);
    } else {
        lastPoint = CGPointMake(0, graphView.frame.size.height);
    }
    
    for (int i = 1; i < lastDay; i++) {
        BOOL isFound = NO;
        
        // 생리 기간 표시
        if ([self->_dataDictionary[@"MonthlyMenstrual"][i - 1][@"MenstrualDayStatus"] isEqualToString:@"Y"] || [self->_dataDictionary[@"MonthlyMenstrual"][i - 1][@"MenstrualDayStatus"] isEqualToString:@"S"]) {

            UIView *menstrualView = [[UIView alloc] initWithFrame:CGRectMake(lastPoint.x, 0, (float)graphView.frame.size.width / lastDay, graphView.frame.size.height)];
            [menstrualView setBackgroundColor:UIColorFromRGB(0xea979c)];
            [menstrualView setAlpha:0.53];
            [graphView addSubview:menstrualView];

        }

        for (int j = 0; j < [_sleepArray count]; j++) {
            SleepSummaryComplete *sleepSummary = _sleepArray[j];
            
            int sleepDay = [[monthDateFormatter stringFromDate:[[NSDate alloc] initWithTimeIntervalSince1970:[sleepSummary getSleepParams].timestamp]] intValue];
            
            if (sleepDay == i + 1) {
                isFound = YES;
                if (!isSleepTime) {

                    CGContextMoveToPoint(context, lastPoint.x, lastPoint.y);
                    CGContextAddLineToPoint(context, i * ((float)graphView.frame.size.width / lastDay), graphView.frame.size.height - ( (float)[sleepSummary getSleepParams].scoreTotalSleepTime / maxScore ) * graphView.frame.size.height );
                    CGContextSetStrokeColorWithColor(context, UIColorFromRGB(0xd1a6cd).CGColor);
                    CGContextStrokePath(context);
                    
                    lastPoint = CGPointMake(i * ((float)graphView.frame.size.width / lastDay), graphView.frame.size.height - ( (float)[sleepSummary getSleepParams].scoreTotalSleepTime / maxScore ) * graphView.frame.size.height);

                } else {
                    
                    int validSleepCount = 0;
                    for (int k = 0; k < [sleepSummary.hypnoTypeGraph count]; k++) {
                        if ([sleepSummary.hypnoTypeGraph[k] intValue] != -1) {
                            validSleepCount++;
                        }
                    }
                    
                    CGContextMoveToPoint(context, lastPoint.x, lastPoint.y);
                    CGContextAddLineToPoint(context, i * ((float)graphView.frame.size.width / lastDay), graphView.frame.size.height - ( (float)validSleepCount / maxScore ) * graphView.frame.size.height );
                    CGContextSetStrokeColorWithColor(context, UIColorFromRGB(0xd1a6cd).CGColor);
                    CGContextStrokePath(context);
                    
                    lastPoint = CGPointMake(i * ((float)graphView.frame.size.width / lastDay), graphView.frame.size.height - ( (float)validSleepCount / maxScore ) * graphView.frame.size.height);
                }
                break;
            }
            
//            NSLog(@"sleepSummary : %@", sleepSummary);
//
//            NSLog(@"sleep score: %@", [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].scoreTotalSleepTime]);
//            NSLog(@"sleep timestamp: %@", [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].timestamp]);
//            NSLog(@"sleep date: %@", [[NSDate alloc] initWithTimeIntervalSince1970:[sleepSummary getSleepParams].timestamp]);
        }
        
        if (!isFound) {
            CGContextMoveToPoint(context, lastPoint.x, lastPoint.y);
            CGContextAddLineToPoint(context, i * ((float)graphView.frame.size.width / lastDay), graphView.frame.size.height );
            CGContextSetStrokeColorWithColor(context, UIColorFromRGB(0xd1a6cd).CGColor);
            CGContextStrokePath(context);

            lastPoint = CGPointMake(i * ((float)graphView.frame.size.width / lastDay), graphView.frame.size.height );
        }

    }

    graphView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    
    UILabel *highScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(46 / 3), CGHeightFromIP6P(300 / 3), CGWidthFromIP6P(158 / 3), CGHeightFromIP6P(44 / 3))];
    [highScoreLabel setTextColor:UIColorFromRGB(0x717171)];
    [highScoreLabel setFont:MediumWithSize(CGHeightFromIP6P(14))];
    [highScoreLabel setText:[NSString stringWithFormat:@"%d점", maxScore]];
    if (isSleepTime) {
        [highScoreLabel setText:[NSString stringWithFormat:@"%dH", maxScore / 2 / 60]];
    }
    [highScoreLabel sizeToFit];
    [_contentView addSubview:highScoreLabel];
    
    UILabel *lowScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(46 / 3), CGHeightFromIP6P(818 / 3), CGWidthFromIP6P(158 / 3), CGHeightFromIP6P(44 / 3))];
    [lowScoreLabel setTextColor:UIColorFromRGB(0x717171)];
    [lowScoreLabel setFont:MediumWithSize(CGHeightFromIP6P(14))];
    [lowScoreLabel setText:@"0점"];
    if (isSleepTime) {
        [lowScoreLabel setText:@"0H"];
    }
    [lowScoreLabel sizeToFit];
    [_contentView addSubview:lowScoreLabel];
    
    
    UILabel *firstDayLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(233 / 3), CGHeightFromIP6P(933 / 3), CGWidthFromIP6P(100), CGHeightFromIP6P(44))];
    [firstDayLabel setTextColor:UIColorFromRGB(0xc1c1c1)];
    [firstDayLabel setFont:MediumWithSize(CGHeightFromIP6P(14))];
    [firstDayLabel setText:@"1일"];
    [firstDayLabel sizeToFit];
    [_contentView addSubview:firstDayLabel];
    
    UILabel *midDayLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(204 / 3), firstDayLabel.frame.origin.y, CGWidthFromIP6P(992 / 3), firstDayLabel.frame.size.height)];
    [midDayLabel setTextAlignment:NSTextAlignmentCenter];
    [midDayLabel setTextColor:UIColorFromRGB(0xc1c1c1)];
    [midDayLabel setFont:MediumWithSize(CGHeightFromIP6P(14))];
    [midDayLabel setText:@"15일"];
    [_contentView addSubview:midDayLabel];
    
    UILabel* lastDayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(933 / 3), CGWidthFromIP6P(100), CGHeightFromIP6P(44))];
    [lastDayLabel setTextColor:UIColorFromRGB(0xc1c1c1)];
    [lastDayLabel setFont:MediumWithSize(CGHeightFromIP6P(14))];
    [lastDayLabel setText:[NSString stringWithFormat:@"%d일", lastDay]];
    [lastDayLabel sizeToFit];
    [lastDayLabel setFrame:CGRectMake(CGWidthFromIP6P(1196 / 3) - lastDayLabel.frame.size.width, lastDayLabel.frame.origin.y, lastDayLabel.frame.size.width, lastDayLabel.frame.size.height)];
    [_contentView addSubview:lastDayLabel];

}

- (void)getMonthlySleepData
{
    if (IS_HIDE_SLEEP_PART) {
        return;
    }
    NSString *monthString = [[NSString alloc] initWithFormat:@"%@-%02lu-05", _yearLabel.text, (long)(_monthScrollView.contentOffset.x / _monthScrollView.frame.size.width) % 12 + 1];       // 타임존 문제로 1일이 아닌 여유있게 5일로 설정

    NSDateFormatter *monthDateFormatter = [[NSDateFormatter alloc] init];
    [monthDateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    _sleepArray = [[HistoryManager sharedInstance] getMonthSummariesForDate:[monthDateFormatter dateFromString:monthString] forUser:[[UserManager sharedInstance] getUser].email filterShortSessions:NO];
    
    [self drawGraph];
}

- (void)getMonthlySleepDiary
{
    if (IS_HIDE_SLEEP_PART) {
        return;
    }
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SLEEPDIARY_GET_MONTH]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@-%02lu-01", _yearLabel.text, (long)(_monthScrollView.contentOffset.x / _monthScrollView.frame.size.width) % 12 + 1];
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserStatus"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        [self->_datePickerView selectRow:(long)(self->_monthScrollView.contentOffset.x / self->_monthScrollView.frame.size.width / 12) inComponent:0 animated:NO];
        [self->_datePickerView selectRow:(long)(self->_monthScrollView.contentOffset.x / self->_monthScrollView.frame.size.width) % 12 inComponent:1 animated:NO];
        
        self->_dataDictionary = responseData[@"data"];
        [self getMonthlySleepData];
        
        for (UIButton *bannerButton in [self->_bannerScrollView subviews]) {
            if ([bannerButton isKindOfClass:[UIButton class]]) {
                [bannerButton removeFromSuperview];
            }
        }
        
        for (int i = 0; i < [self->_dataDictionary[@"Banner"] count]; i++) {
            UIButton *bannerButton = [[UIButton alloc] initWithFrame:CGRectMake(self->_bannerScrollView.frame.size.width * i, 0, self->_bannerScrollView.frame.size.width, self->_bannerScrollView.frame.size.height)];
            [bannerButton setTag:i];
            [bannerButton addTarget:self action:@selector(touchBanner:) forControlEvents:UIControlEventTouchUpInside];
            [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[self->_dataDictionary[@"Banner"][i][@"BannerPath"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                         options:0
                                                        progress:nil
                                                       completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                           if (!error) {
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [bannerButton setImage:image forState:UIControlStateNormal];
                                                               });
                                                           } else {
                                                           }
                                                       }];
            
            [self->_bannerScrollView addSubview:bannerButton];
        }
        [self->_bannerScrollView setContentSize:CGSizeMake(self->_bannerScrollView.frame.size.width * [self->_dataDictionary[@"Banner"] count], self->_bannerScrollView.frame.size.height)];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];
}

- (void)presentSleepCalendarGuide
{
    SleepCalendarGuideViewController *sleepCalendarGuideViewController = [[SleepCalendarGuideViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *sleepCalendarGuideNavigationController = [[UINavigationController alloc] initWithRootViewController:sleepCalendarGuideViewController];
    [sleepCalendarGuideNavigationController.navigationBar setTranslucent:NO];
    [sleepCalendarGuideNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                              forBarPosition:UIBarPositionAny
                                                                  barMetrics:UIBarMetricsDefault];
    [sleepCalendarGuideNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [sleepCalendarGuideNavigationController.navigationBar setTitleTextAttributes:@{
                                                                                   NSFontAttributeName: SemiBoldWithSize(18),
                                                                                   NSForegroundColorAttributeName: UIColorFromRGB(0xf05971)
                                                                                   }];
    [sleepCalendarGuideNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffeff1)] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController presentViewController:sleepCalendarGuideNavigationController animated:YES completion:^{
        //
    }];
}

- (void)changeCalendarTab:(UIButton *)button
{
    [_sleepScoreButton setImage:[UIImage imageNamed:@"sleepcalendar_button_sleepscore"] forState:UIControlStateNormal];
    [_sleepTimeButton setImage:[UIImage imageNamed:@"sleepcalendar_button_sleeptime"] forState:UIControlStateNormal];

    if (button == _sleepScoreButton) {
        isSleepTime = NO;
        [_sleepScoreButton setImage:[UIImage imageNamed:@"sleepcalendar_button_sleepscore_h"] forState:UIControlStateNormal];
    } else if (button == _sleepTimeButton) {
        isSleepTime = YES;
        [_sleepTimeButton setImage:[UIImage imageNamed:@"sleepcalendar_button_sleeptime_h"] forState:UIControlStateNormal];
    }
    [self drawGraph];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    return [self initWithNibName:nibNameOrNil bundle:nibBundleOrNil pClass:pClass date:[NSDate date]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass date:(NSDate *)date
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        isSleepTime = NO;
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.view.bounds;
        gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [self.view.layer insertSublayer:gradient atIndex:0];
        
        
        NSMutableAttributedString *attrString;
        CNSTextAttachment *imageAttachment;
        
        UIButton *titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleButton setBackgroundColor:[UIColor clearColor]];
        [titleButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleButton addTarget:self action:@selector(presentSleepCalendarGuide) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setTitleView:titleButton];
        
        if (@available(iOS 9.0, *)) {
            [titleButton setTitle:@"수면 캘린더 " forState:UIControlStateNormal];
            [titleButton.titleLabel setFont:SemiBoldWithSize(19)];
            [titleButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
            [titleButton setImage:[UIImage imageNamed:@"cyclecalendar_button_qmark"] forState:UIControlStateNormal];
            [titleButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
            [titleButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 3.5 / 2, 0)];
        } else {
            attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"수면 캘린더 "]
                                                                attributes:@{ NSFontAttributeName : SemiBoldWithSize(19),
                                                                              NSForegroundColorAttributeName : [UIColor whiteColor] }];
            
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -3.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_button_qmark"]];
            [attrString appendAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            
            [titleButton setAttributedTitle:attrString forState:UIControlStateNormal];
        }
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
        
        UIButton *cycleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"sleepcalendar_button_cycle"].size.width, [UIImage imageNamed:@"sleepcalendar_button_cycle"].size.height)];
        [cycleButton setImage:[UIImage imageNamed:@"sleepcalendar_button_cycle"] forState:UIControlStateNormal];
        [cycleButton addTarget:(RootViewController *)_pClass action:@selector(changeToSleepDayInfoViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cycleButton]];
        
        
        UIView *yearBGView = [[UIView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, CGHeightFromIP6P(16))];
        [yearBGView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:yearBGView];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                       fromDate:date];
        
        _yearLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, CGHeightFromIP6P(16))];
        [_yearLabel setText:[NSString stringWithFormat:@"%ld", [components year]]];
        [_yearLabel setBackgroundColor:[UIColor clearColor]];
        [_yearLabel setTextColor:[UIColor whiteColor]];
        [_yearLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(17))];
        [_yearLabel setTextAlignment:NSTextAlignmentCenter];
        [self.view addSubview:_yearLabel];
        
        NSDateFormatter *monthDateFormatter = [[NSDateFormatter alloc] init];
        [monthDateFormatter setDateFormat:@"M"];
        
        
        _monthScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _yearLabel.frame.origin.y + _yearLabel.frame.size.height, (int)(kSCREEN_WIDTH / 2 - CGWidthFromIP6P(30)), CGHeightFromIP6P(71))];
        [_monthScrollView setDelegate:self];
        [_monthScrollView setClipsToBounds:NO];
        [_monthScrollView setCenter:CGPointMake(self.view.frame.size.width / 2, _monthScrollView.center.y)];
        [_monthScrollView setBackgroundColor:[UIColor clearColor]];
        [_monthScrollView setShowsVerticalScrollIndicator:NO];
        [_monthScrollView setShowsHorizontalScrollIndicator:NO];
        [_monthScrollView setPagingEnabled:YES];
        [_monthScrollView setContentSize:CGSizeMake(_monthScrollView.frame.size.width * 12 * 21, _monthScrollView.frame.size.height)];
        [self.view addSubview:_monthScrollView];
        
        UIButton *prevMonthButton = [[UIButton alloc] initWithFrame:CGRectMake(0, _yearLabel.frame.origin.y + _yearLabel.frame.size.height, kSCREEN_WIDTH / 4, _monthScrollView.frame.size.height)];
        [prevMonthButton setBackgroundColor:[UIColor clearColor]];
        [prevMonthButton addTarget:self action:@selector(prevMonth) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:prevMonthButton];
        
        UIButton *nextMonthButton = [[UIButton alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH / 4 * 3, _yearLabel.frame.origin.y + _yearLabel.frame.size.height, kSCREEN_WIDTH / 4, _monthScrollView.frame.size.height)];
        [nextMonthButton setBackgroundColor:[UIColor clearColor]];
        [nextMonthButton addTarget:self action:@selector(nextMonth) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextMonthButton];
        
        _monthItemArray = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < 12 * 21; i++) {
            
            UIView *monthBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(55), CGHeightFromIP6P(55))];
            [monthBGView setBackgroundColor:[UIColor whiteColor]];
            [monthBGView.layer setCornerRadius:monthBGView.frame.size.width / 2];
            [monthBGView setClipsToBounds:YES];
            [monthBGView setAlpha:0];
            [_monthScrollView addSubview:monthBGView];
            
            UIButton *monthButton = [[UIButton alloc] initWithFrame:CGRectMake(i * _monthScrollView.frame.size.width, 0, _monthScrollView.frame.size.width, CGHeightFromIP6P(70))];
            [monthButton.titleLabel setFont:MediumWithSize(CGHeightFromIP6P(20))];
            [monthButton setTitle:[NSString stringWithFormat:@"%d월", i % 12 + 1] forState:UIControlStateNormal];
            [monthButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [monthButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
            [monthButton setBackgroundColor:[UIColor clearColor]];
            [monthButton addTarget:self action:@selector(becomePicker) forControlEvents:UIControlEventTouchUpInside];
            [_monthScrollView addSubview:monthButton];
            
            [monthBGView setCenter:CGPointMake(monthButton.center.x, monthButton.center.y - 1)];
            
            [_monthItemArray addObject:@{
                                         @"view" : monthBGView,
                                         @"button" : monthButton
                                         }];
        }
        

        NSDate *currDate = [NSDate date];
        NSDateComponents *currComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                           fromDate:currDate];
        
        [_monthScrollView setContentOffset:CGPointMake(([[monthDateFormatter stringFromDate:date] intValue] + (12 * ([components year] + 10 - [currComponents year])) - 1) * _monthScrollView.frame.size.width, 0) animated:YES];
        
        
        gradient = [CAGradientLayer layer];
        gradient.frame = yearBGView.bounds;
        gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [yearBGView.layer insertSublayer:gradient atIndex:0];
        
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _monthScrollView.frame.origin.y + _monthScrollView.frame.size.height, kSCREEN_WIDTH, kSCREEN_HEIGHT - (_monthScrollView.frame.origin.y + _monthScrollView.frame.size.height))];
        [_mainScrollView setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        [self.view addSubview:_mainScrollView];
        
        if (IS_HIDE_SLEEP_PART) {
            float ratio = kSCREEN_WIDTH / [UIImage imageNamed:@"sleepcalendar_image_dummy_cover"].size.width;
            
            UIImageView *dummyCoverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, ratio * [UIImage imageNamed:@"sleepcalendar_image_dummy_cover"].size.height)];
            [dummyCoverImageView setImage:[UIImage imageNamed:@"sleepcalendar_image_dummy_cover"]];
            [_mainScrollView addSubview:dummyCoverImageView];
        } else {

            _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, CGHeightFromIP6P(1055 / 3))];
            [_contentView setBackgroundColor:[UIColor whiteColor]];
            [_mainScrollView addSubview:_contentView];
            
            
            _sleepScoreButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P((33 + 8) / 3), CGHeightFromIP6P(80 / 3), CGWidthFromIP6P(588 / 3), CGHeightFromIP6P(156 / 3))];
            [_sleepScoreButton setImage:[UIImage imageNamed:@"sleepcalendar_button_sleepscore_h"] forState:UIControlStateNormal];
            [_sleepScoreButton setImage:[UIImage imageNamed:@"sleepcalendar_button_sleepscore_h"] forState:UIControlStateHighlighted];
            [_sleepScoreButton addTarget:self action:@selector(changeCalendarTab:) forControlEvents:UIControlEventTouchUpInside];
            [_contentView addSubview:_sleepScoreButton];
            
            _sleepTimeButton = [[UIButton alloc] initWithFrame:CGRectMake(_sleepScoreButton.frame.origin.x + _sleepScoreButton.frame.size.width - CGWidthFromIP6P(7 / 3) - CGWidthFromIP6P(8 / 3) - k3PX, _sleepScoreButton.frame.origin.y, CGWidthFromIP6P(588 / 3), CGHeightFromIP6P(156 / 3))];
            [_sleepTimeButton setImage:[UIImage imageNamed:@"sleepcalendar_button_sleeptime"] forState:UIControlStateNormal];
            [_sleepTimeButton setImage:[UIImage imageNamed:@"sleepcalendar_button_sleeptime_h"] forState:UIControlStateHighlighted];
            [_sleepTimeButton addTarget:self action:@selector(changeCalendarTab:) forControlEvents:UIControlEventTouchUpInside];
            [_contentView addSubview:_sleepTimeButton];
            
            
            UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, (int)(_contentView.frame.origin.y + _contentView.frame.size.height), kSCREEN_WIDTH, k1PX)];
            [separatorView setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
            [_mainScrollView addSubview:separatorView];
            
            UIView *iconGuideBGView = [[UIView alloc] initWithFrame:CGRectMake(0, separatorView.frame.origin.y + separatorView.frame.size.height, kSCREEN_WIDTH, CGHeightFromIP6P(130 / 3))];
            [iconGuideBGView setBackgroundColor:[UIColor whiteColor]];
            [_mainScrollView addSubview:iconGuideBGView];
            
            UILabel *pyrexiaGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(25 / 3), kSCREEN_WIDTH, CGHeightFromIP6P(81 / 3))];
            [pyrexiaGuideLabel setBackgroundColor:[UIColor clearColor]];
            [pyrexiaGuideLabel setTextAlignment:NSTextAlignmentCenter];
            [iconGuideBGView addSubview:pyrexiaGuideLabel];
            
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -2.0)];
            [imageAttachment setImage:[UIImage imageNamed:@"sleepcalendar_image_color"]];
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 기간"]
                                                                               attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(14)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x717171) }]];
            [pyrexiaGuideLabel setAttributedText:attrString];
            
            _bannerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, iconGuideBGView.frame.origin.y + iconGuideBGView.frame.size.height, kSCREEN_WIDTH, CGHeightFromIP6P(260 / 3))];
            [_bannerScrollView setBackgroundColor:UIColorFromRGB(0xe1ded4)];
            [_bannerScrollView setPagingEnabled:YES];
            [_bannerScrollView setBounces:NO];
            [_bannerScrollView setShowsVerticalScrollIndicator:NO];
            [_bannerScrollView setShowsHorizontalScrollIndicator:NO];
            [_bannerScrollView setScrollEnabled:NO];
            [_mainScrollView addSubview:_bannerScrollView];
            
            [self performSelector:@selector(bannerRollingThread) withObject:nil afterDelay:8.0f];
            
            // 50 78
            
            _avrSleepTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), _bannerScrollView.frame.origin.y + _bannerScrollView.frame.size.height + CGHeightFromIP6P(78 / 3), CGWidthFromIP6P(230 / 3), CGHeightFromIP6P(48 / 3))];
            [_avrSleepTimeLabel setBackgroundColor:[UIColor clearColor]];
            [_avrSleepTimeLabel setTextColor:[UIColor blackColor]];
            [_avrSleepTimeLabel setText:@"평균 수면 점수"];
            [_avrSleepTimeLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(18))];
            [_avrSleepTimeLabel sizeToFit];
            [_mainScrollView addSubview:_avrSleepTimeLabel];
            
            
            UIView *whiteRoundBGView = [[UIView alloc] initWithFrame:CGRectMake(_avrSleepTimeLabel.frame.origin.x, _avrSleepTimeLabel.frame.origin.y + _avrSleepTimeLabel.frame.size.height + CGHeightFromIP6P(29 /3), CGWidthFromIP6P(564 / 3), CGHeightFromIP6P(160 / 3))];
            [whiteRoundBGView setBackgroundColor:[UIColor whiteColor]];
            [whiteRoundBGView.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
            [whiteRoundBGView setClipsToBounds:YES];
            [_mainScrollView addSubview:whiteRoundBGView];
            
            UILabel *prevLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(31 / 3), CGWidthFromIP6P(170 / 3), CGHeightFromIP6P(38 / 3))];
            [prevLabel setBackgroundColor:[UIColor clearColor]];
            [prevLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
            [prevLabel setText:@"저번 달"];
            [prevLabel setTextColor:[UIColor blackColor]];
            [whiteRoundBGView addSubview:prevLabel];
            
            _prevValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(81 / 3), whiteRoundBGView.frame.size.width - prevLabel.frame.origin.x * 2, CGHeightFromIP6P(58 / 3))];
            [_prevValueLabel setBackgroundColor:[UIColor clearColor]];
            [_prevValueLabel setFont:MediumWithSize(CGHeightFromIP6P(19))];
            [_prevValueLabel setTextAlignment:NSTextAlignmentRight];
            [_prevValueLabel setTextColor:[UIColor blackColor]];
            [whiteRoundBGView addSubview:_prevValueLabel];
            
            whiteRoundBGView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(633 / 3), whiteRoundBGView.frame.origin.y, CGWidthFromIP6P(564 / 3), CGHeightFromIP6P(160 / 3))];
            [whiteRoundBGView setBackgroundColor:[UIColor whiteColor]];
            [whiteRoundBGView.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
            [whiteRoundBGView setClipsToBounds:YES];
            [_mainScrollView addSubview:whiteRoundBGView];
            
            UILabel *currLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(31 / 3), CGWidthFromIP6P(170 / 3), CGHeightFromIP6P(38 / 3))];
            [currLabel setBackgroundColor:[UIColor clearColor]];
            [currLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(15))];
            [currLabel setText:@"이번 달"];
            [currLabel setTextColor:UIColorFromRGB(0xc382bd)];
            [whiteRoundBGView addSubview:currLabel];
            
            _currValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(81 / 3), whiteRoundBGView.frame.size.width - prevLabel.frame.origin.x * 2, CGHeightFromIP6P(58 / 3))];
            [_currValueLabel setBackgroundColor:[UIColor clearColor]];
            [_currValueLabel setFont:BoldWithSize(CGHeightFromIP6P(19))];
            [_currValueLabel setTextAlignment:NSTextAlignmentRight];
            [_currValueLabel setTextColor:UIColorFromRGB(0xc382bd)];
            [whiteRoundBGView addSubview:_currValueLabel];
            
            
            _blackMaskView = [[UIView alloc] initWithFrame:self.view.bounds];
            [_blackMaskView setBackgroundColor:[UIColor blackColor]];
            [_blackMaskView setAlpha:0];
            
            _dateSelectView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 44 + 215)];
            [_dateSelectView setBackgroundColor:[UIColor clearColor]];
            
            UIView *pickerResignView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(414), 44)];
            [pickerResignView setBackgroundColor:[UIColor clearColor]];
            
            UIView *pickerBlackMaskView = [[UIView alloc] initWithFrame:pickerResignView.bounds];
            [pickerBlackMaskView setBackgroundColor:[UIColor blackColor]];
            [pickerBlackMaskView setAlpha:0.8];
            [pickerResignView addSubview:pickerBlackMaskView];
            
            UIButton *pickerResignButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(414) - CGWidthFromIP6P(40 + 4 + 4), 3, CGWidthFromIP6P(40), 44 - 3 - 3)];
            [pickerResignButton.titleLabel setFont:RegularWithSize(14)];
            [pickerResignButton setTitle:@"완료" forState:UIControlStateNormal];
            [pickerResignButton setBackgroundColor:[UIColor clearColor]];
            [pickerResignButton addTarget:self action:@selector(resignPicker) forControlEvents:UIControlEventTouchUpInside];
            [pickerResignView addSubview:pickerResignButton];
            
            [_dateSelectView addSubview:pickerResignView];
            
            
            
            
            _datePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, kSCREEN_WIDTH, 215)];
            [_datePickerView setBackgroundColor:[UIColor whiteColor]];
            [_datePickerView setShowsSelectionIndicator:YES];
            [_datePickerView setDelegate:self];
            [_datePickerView setDataSource:self];
            [_dateSelectView addSubview:_datePickerView];
            
            
            UILabel *yearLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
            [yearLabel setFont:RegularWithSize(21)];
            [yearLabel setText:@"년"];
            [yearLabel setTextColor:UIColorFromRGB(0x333333)];
            [yearLabel sizeToFit];
            [yearLabel setCenter:CGPointMake(_datePickerView.frame.size.width / 2 + CGWidthFromIP6P(5), _datePickerView.frame.size.height / 2 + 1)];
            [_datePickerView addSubview:yearLabel];
            
            
            UILabel *monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
            [monthLabel setFont:RegularWithSize(21)];
            [monthLabel setText:@"월"];
            [monthLabel setTextColor:UIColorFromRGB(0x333333)];
            [monthLabel sizeToFit];
            [monthLabel setCenter:CGPointMake(kSCREEN_WIDTH - CGWidthFromIP6P(118), _datePickerView.frame.size.height / 2 + 1)];
            [_datePickerView addSubview:monthLabel];
            
            [self getMonthlySleepDiary];
            
        }
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// UIPickerView Delegate & DataSource
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        if (component == 0) {
            return 100;
        } else if (component == 1) {
            return 80;
        }
    }
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        return 50;
    }
    
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        if (component == 0) {
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                           fromDate:[NSDate date]];
            
            return [NSString stringWithFormat:@"%ld", (long)row + ([components year] - 10)];
        } else if(component == 1) {
            return [NSString stringWithFormat:@"%ld", (long)row + 1];
        }
    }
    return nil;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        if (component == 0) {
            return 21;
        } else if(component == 1) {
            return 12;
        }
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView == _datePickerView) {
        return 2;
    }
    return 0;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == _monthScrollView) {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                       fromDate:[NSDate date]];
        [_yearLabel setText:[NSString stringWithFormat:@"%lu", (long)(scrollView.contentOffset.x / scrollView.frame.size.width / 12 + 0.04) + [components year] - 10]];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _monthScrollView) {
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            for (int i = 0; i < [self->_monthItemArray count]; i++) {
                UIView *bgView = self->_monthItemArray[i][@"view"];
                UIButton *itemButton = self->_monthItemArray[i][@"button"];
                if (i == (int)(scrollView.contentOffset.x / scrollView.frame.size.width)) {
                    [bgView setAlpha:1];
                    [itemButton setTitleColor:UIColorFromRGB(0xf9626e) forState:UIControlStateNormal];
                } else {
                    [bgView setAlpha:0];
                    [itemButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
                }
            }
        } completion:^(BOOL finished) {
            [self performSelectorOnMainThread:@selector(getMonthlySleepDiary) withObject:nil waitUntilDone:NO];
        }];
    }
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (scrollView == _monthScrollView) {
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            for (int i = 0; i < [self->_monthItemArray count]; i++) {
                UIView *bgView = self->_monthItemArray[i][@"view"];
                UIButton *itemButton = self->_monthItemArray[i][@"button"];
                if (i == (int)(scrollView.contentOffset.x / scrollView.frame.size.width)) {
                    [bgView setAlpha:1];
                    [itemButton setTitleColor:UIColorFromRGB(0xf9626e) forState:UIControlStateNormal];
                } else {
                    [bgView setAlpha:0];
                    [itemButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
                }
            }
        } completion:^(BOOL finished) {
            [self performSelectorOnMainThread:@selector(getMonthlySleepDiary) withObject:nil waitUntilDone:NO];
        }];
    }
}

@end
