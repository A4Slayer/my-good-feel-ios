//
//  PreferenceUserInfoViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 17/05/2019.
//  Copyright © 2019 appknot. All rights reserved.
//

#import "PreferenceUserInfoViewController.h"

@interface PreferenceUserInfoViewController ()

@end

@implementation PreferenceUserInfoViewController

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"사용자 정보"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x3c3c3e)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
        [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT) style:UITableViewStyleGrouped];
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setTableFooterView:[[UIView alloc] init]];
        [self.view addSubview:_mainTableView];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark -
#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 38;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *rtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 38)];
    [rtnView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 15, 200, 13)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:UIColorFromRGB(0x6d6d72)];
    [titleLabel setFont:[UIFont systemFontOfSize:12]];
    [titleLabel setText:@"내 정보"];
    [rtnView addSubview:titleLabel];
    
    return rtnView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [NSString stringWithFormat:@"Cell_%ld_%ld", (long)indexPath.section, (long)indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor whiteColor]];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
        [titleLabel setTextColor:[UIColor blackColor]];
        [cell.contentView addSubview:titleLabel];

        UILabel *valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x + titleLabel.frame.size.width + 30, 0, kSCREEN_WIDTH - 16 - (titleLabel.frame.origin.x + titleLabel.frame.size.width + 30), 44)];
        [valueLabel setTextColor:[UIColor grayColor]];
        [valueLabel setTextAlignment:NSTextAlignmentRight];
        [cell.contentView addSubview:valueLabel];
        
        if (indexPath.row == 0) {
            [titleLabel setText:@"User No"];
            [valueLabel setText:[[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
        } else if (indexPath.row == 1) {
            [titleLabel setText:@"User ID"];
            [valueLabel setText:[[DataSingleton sharedSingletonClass] userInfo][@"UserId"]];
        } else if (indexPath.row == 2) {
            [titleLabel setText:@"Mac address"];
        }

        [titleLabel sizeToFit];
        [titleLabel setFrame:CGRectMake(16, 0, titleLabel.frame.size.width, 44)];
        [valueLabel setFrame:CGRectMake(titleLabel.frame.origin.x + titleLabel.frame.size.width + 30, 0, kSCREEN_WIDTH - 16 - (titleLabel.frame.origin.x + titleLabel.frame.size.width + 30), 44)];

    }
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
