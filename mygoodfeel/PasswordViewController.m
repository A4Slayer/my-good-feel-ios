//
//  PasswordViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 17..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "PasswordViewController.h"

@interface PasswordViewController ()

@end

@implementation PasswordViewController

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xefeff4)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"앱 잠금"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x000000)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
        [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
        
        UIButton *cycleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_cycle"].size.width, [UIImage imageNamed:@"common_button_cycle"].size.height)];
        [cycleButton setImage:[UIImage imageNamed:@"common_button_cycle"] forState:UIControlStateNormal];
        [cycleButton setAlpha:0];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cycleButton]];
        
        _mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [self.view addSubview:_mainTableView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)changeUsingPassword:(UISwitch *)passwordSwitch
{
    if (passwordSwitch.isOn) {
        [_bioAuthSwitch setEnabled:YES];
        PasswordSettingViewController *passwordSettingViewController = [[PasswordSettingViewController alloc] initWithNibName:nil bundle:nil pClass:self];
        
        [self.navigationController presentViewController:passwordSettingViewController animated:YES completion:^{
        }];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"app_password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_bioAuthSwitch setOn:NO animated:YES];
        [_bioAuthSwitch setEnabled:NO];
        [_mainTableView reloadData];
    }
}

- (void)changeUsingBio:(UISwitch *)bioSwitch
{
    if (bioSwitch.isOn) {
        [[DataSingleton sharedSingletonClass] setBioProcessing:@"1"];
        [[DataSingleton sharedSingletonClass] attachLoadingView];
        
        LAContext *laContext = [[LAContext alloc] init];
        NSError *error;
        
        if ([laContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            if (error != NULL) {
                NSLog(@"can evaluate error: %@", error);
                [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"생체 정보 인증을 사용할 수 없습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                [bioSwitch setOn:NO];
                [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[DataSingleton sharedSingletonClass] detachLoadingView];
                });
            } else {
                
                NSString *reasonString = @"Touch ID 등록";
                BOOL isSupport = NO;
                
                if (@available(iOS 11.0.1, *)) {
                    if (laContext.biometryType == LABiometryTypeFaceID) {
                        reasonString = @"Face ID 등록";
                        isSupport = YES;
                    } else if (laContext.biometryType == LABiometryTypeTouchID) {
                        isSupport = YES;
                    } else {
                        [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[DataSingleton sharedSingletonClass] detachLoadingView];
                        });
                    }
                } else {
                    // Fallback on earlier versions
                    isSupport = NO;
                    [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[DataSingleton sharedSingletonClass] detachLoadingView];
                    });
                }
                
                if (isSupport) {
                    [laContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:reasonString reply:^(BOOL success, NSError * _Nullable error) {
                        
                        if (error != NULL) {
                            [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"인증 실패 횟수 초과로 인해 생체 정보 인증을 사용할 수 없습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                            [bioSwitch setOn:NO];
                            [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [[DataSingleton sharedSingletonClass] detachLoadingView];
                            });
                        } else if (success) {
                            if (success) {
                                // 설정되었습니다
                                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"app_password_bio"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [[DataSingleton sharedSingletonClass] detachLoadingView];
                                    [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"설정되었습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                                });
                                [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                            } else {
                                [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"생체 정보 등록에 실패했습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                                [bioSwitch setOn:NO];
                                [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [[DataSingleton sharedSingletonClass] detachLoadingView];
                                });
                            }
                        } else {
                            [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"생체 정보 등록에 실패했습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                            [bioSwitch setOn:NO];
                            [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [[DataSingleton sharedSingletonClass] detachLoadingView];
                            });
                        }
                    }];
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"생체 정보 인증을 사용할 수 없습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                    [bioSwitch setOn:NO];
                    [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[DataSingleton sharedSingletonClass] detachLoadingView];
                    });
                }
            }
        } else {
            [bioSwitch setOn:NO];
            if (error.code == -8) {
                [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"인증 실패 횟수 초과로 인해 생체 정보 인증을 사용할 수 없습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[DataSingleton sharedSingletonClass] detachLoadingView];
                });
            } else {
                [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"생체 정보 인증을 사용할 수 없습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                [[DataSingleton sharedSingletonClass] setBioProcessing:@"0"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[DataSingleton sharedSingletonClass] detachLoadingView];
                });
            }
        }
        
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"app_password_bio"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark -
#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            PasswordChangeViewController *passwordChangeViewController = [[PasswordChangeViewController alloc] initWithNibName:nil bundle:nil pClass:self];
            
            [self.navigationController presentViewController:passwordChangeViewController animated:YES completion:^{
            }];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"app_password"]) {
        return 2;
    } else {
        return 1;
    }
    return 1;
}

#pragma mark -
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 38;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section
{
    return 38;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *rtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 38)];
        [rtnView setBackgroundColor:[UIColor clearColor]];
        [rtnView setClipsToBounds:YES];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 10, 200, 13)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextColor:UIColorFromRGB(0x6d6d72)];
        [titleLabel setFont:[UIFont systemFontOfSize:12]];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"app_password"]) {
            [titleLabel setText:@"생체 정보로 PIN 번호 인증을 대체합니다."];
        } else {
            [titleLabel setText:@"앱 진입 시 암호를 묻지 않습니다."];
        }
        
        [rtnView addSubview:titleLabel];
        
        return rtnView;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"app_password"]) {
            return 2;
        } else {
            return 1;
        }
    } else if (section == 1) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"app_password"]) {
            return 1;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [NSString stringWithFormat:@"Cell_%ld_%ld", indexPath.section, indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"앱 잠금"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];
                
                _passwordSwitch = [[UISwitch alloc] init];
                [_passwordSwitch setCenter:CGPointMake(kSCREEN_WIDTH - _passwordSwitch.frame.size.width / 2 - CGWidthFromIP6P(18), 44 / 2)];
                [_passwordSwitch addTarget:self action:@selector(changeUsingPassword:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:_passwordSwitch];
                
                if ([[NSUserDefaults standardUserDefaults] objectForKey:@"app_password"]) {
                    [_passwordSwitch setOn:YES];
                }
                
            } else if (indexPath.row == 1) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"생체 정보로 인증"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];
                
                _bioAuthSwitch = [[UISwitch alloc] init];
                [_bioAuthSwitch setCenter:CGPointMake(kSCREEN_WIDTH - _bioAuthSwitch.frame.size.width / 2 - CGWidthFromIP6P(18), 44 / 2)];
                [_bioAuthSwitch addTarget:self action:@selector(changeUsingBio:) forControlEvents:UIControlEventTouchUpInside];
                [_bioAuthSwitch setOn:NO];
                [_bioAuthSwitch setEnabled:NO];
                
                if ([_passwordSwitch isOn]) {
                    [_bioAuthSwitch setEnabled:YES];
                }
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"app_password_bio"] intValue] == 1) {
                    [_bioAuthSwitch setOn:YES];
                }

                [cell.contentView addSubview:_bioAuthSwitch];
            }
        } else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"PIN 번호 변경"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];

                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
        }
    }
        
    return cell;
}
    
@end
