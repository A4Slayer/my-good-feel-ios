//
//  PasswordChangeViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 23..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "PasswordChangeViewController.h"
#import "PasswordViewController.h"

@interface PasswordChangeViewController ()

@end

@implementation PasswordChangeViewController

- (void)drawPasswordIndicator
{
    [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter3View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter4View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    
    if (passwordIndex == 1) {
        [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xff9699)];
    } else if (passwordIndex == 2) {
        [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xff9699)];
    } else if (passwordIndex == 3) {
        [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter3View setBackgroundColor:UIColorFromRGB(0xff9699)];
    } else if (passwordIndex == 4) {
        [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter3View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter4View setBackgroundColor:UIColorFromRGB(0xff9699)];
        
        if (changeStep == 0) {
            
            NSString *passwordString = [NSString stringWithFormat:@"%@%@%@%@", _passwordArray[0], _passwordArray[1], _passwordArray[2], _passwordArray[3]];
            if ([passwordString isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"app_password"]]) {
                changeStep = 1;
                passwordIndex = 0;
                [_descLabel setText:@"새로운 PIN 번호 4자리를 입력해주세요."];
                [_descLabel sizeToFit];
                [_descLabel setFrame:CGRectMake(0, _descLabel.frame.origin.y, kSCREEN_WIDTH, _descLabel.frame.size.height)];
            } else {
                passwordIndex = 0;
                [[[UIAlertView alloc] initWithTitle:@"" message:@"비밀번호가 일치하지 않습니다" delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
            }
            
            [self drawPasswordIndicator];
        } else if (changeStep == 1) {
            
            changeStep = 2;
            passwordIndex = 0;
            [_descLabel setText:@"다시 한번 입력해주세요"];
            [_descLabel sizeToFit];
            [_descLabel setFrame:CGRectMake(0, _descLabel.frame.origin.y, kSCREEN_WIDTH, _descLabel.frame.size.height)];
            
            [self drawPasswordIndicator];
        } else if (changeStep == 2) {
            NSString *passwordString = [NSString stringWithFormat:@"%@%@%@%@", _passwordArray[0], _passwordArray[1], _passwordArray[2], _passwordArray[3]];
            NSString *passwordConfirmString = [NSString stringWithFormat:@"%@%@%@%@", _passwordConfirmArray[0], _passwordConfirmArray[1], _passwordConfirmArray[2], _passwordConfirmArray[3]];
            
            if ([passwordString isEqualToString:passwordConfirmString]) {
                // 등록 완료
                [[NSUserDefaults standardUserDefaults] setObject:passwordConfirmString forKey:@"app_password"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[(PasswordViewController *)_pClass mainTableView] reloadData];
                
                [self dismissViewControllerAnimated:YES completion:^{
                    //
                }];
            } else {
                // 비밀번호 다름 오류
                [[[UIAlertView alloc] initWithTitle:@"" message:@"비밀번호가 일치하지 않습니다" delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                passwordIndex = 0;
                [self drawPasswordIndicator];
            }
        }
    }
}

- (void)touchNumber:(UIButton *)button
{
    if (button.tag == 100) {
        [self dismissViewControllerAnimated:YES completion:^{
            //
        }];
    } else if (button.tag == -1) {
        passwordIndex--;
        if (passwordIndex < 0) {
            passwordIndex = 0;
        }
        [self drawPasswordIndicator];
    } else {
        if (changeStep == 0 || changeStep == 1) {
            [_passwordArray setObject:[NSString stringWithFormat:@"%ld", (long)button.tag] atIndexedSubscript:passwordIndex];
        } else {
            [_passwordConfirmArray setObject:[NSString stringWithFormat:@"%ld", (long)button.tag] atIndexedSubscript:passwordIndex];
        }
        passwordIndex++;
        
        [self drawPasswordIndicator];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        changeStep = 0;
        passwordIndex = 0;
        _passwordArray = [[NSMutableArray alloc] init];
        _passwordConfirmArray = [[NSMutableArray alloc] init];
        [self.view setBackgroundColor:UIColorFromRGB(0xffffff)];
        
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(64), kSCREEN_WIDTH, CGHeightFromIP6P(22))];
        [titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(21))];
        [titleLabel setTextColor:UIColorFromRGB(0x474747)];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"앱 잠금 암호 입력"];
        [titleLabel sizeToFit];
        [titleLabel setFrame:CGRectMake(0, titleLabel.frame.origin.y, kSCREEN_WIDTH, titleLabel.frame.size.height)];
        [self.view addSubview:titleLabel];
        
        _descLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.frame.origin.y + titleLabel.frame.size.height + CGHeightFromIP6P(4), kSCREEN_WIDTH, CGHeightFromIP6P(17))];
        [_descLabel setFont:MediumWithSize(CGHeightFromIP6P(16))];
        [_descLabel setTextColor:UIColorFromRGB(0x474747)];
        [_descLabel setTextAlignment:NSTextAlignmentCenter];
        [_descLabel setText:@"기존 PIN 번호 4자리를 입력해주세요"];
        [_descLabel sizeToFit];
        [_descLabel setFrame:CGRectMake(0, _descLabel.frame.origin.y, kSCREEN_WIDTH, _descLabel.frame.size.height)];
        [self.view addSubview:_descLabel];
        
        
        _passwordCharacter1View = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(409 / 3), _descLabel.frame.origin.y + _descLabel.frame.size.height + CGHeightFromIP6P(59), CGWidthFromIP6P(42 / 3), CGHeightFromIP6P(42 / 3))];
        [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
        [_passwordCharacter1View.layer setCornerRadius:_passwordCharacter1View.frame.size.width / 2];
        [_passwordCharacter1View setClipsToBounds:YES];
        [self.view addSubview:_passwordCharacter1View];
        
        _passwordCharacter2View = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(537 / 3), _descLabel.frame.origin.y + _descLabel.frame.size.height + CGHeightFromIP6P(59), CGWidthFromIP6P(42 / 3), CGHeightFromIP6P(42 / 3))];
        [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
        [_passwordCharacter2View.layer setCornerRadius:_passwordCharacter1View.frame.size.width / 2];
        [_passwordCharacter2View setClipsToBounds:YES];
        [self.view addSubview:_passwordCharacter2View];
        
        _passwordCharacter3View = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(664 / 3), _descLabel.frame.origin.y + _descLabel.frame.size.height + CGHeightFromIP6P(59), CGWidthFromIP6P(42 / 3), CGHeightFromIP6P(42 / 3))];
        [_passwordCharacter3View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
        [_passwordCharacter3View.layer setCornerRadius:_passwordCharacter1View.frame.size.width / 2];
        [_passwordCharacter3View setClipsToBounds:YES];
        [self.view addSubview:_passwordCharacter3View];
        
        _passwordCharacter4View = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(792 / 3), _descLabel.frame.origin.y + _descLabel.frame.size.height + CGHeightFromIP6P(59), CGWidthFromIP6P(42 / 3), CGHeightFromIP6P(42 / 3))];
        [_passwordCharacter4View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
        [_passwordCharacter4View.layer setCornerRadius:_passwordCharacter1View.frame.size.width / 2];
        [_passwordCharacter4View setClipsToBounds:YES];
        [self.view addSubview:_passwordCharacter4View];
        
        
        float additionalSpacing = kSCREEN_HEIGHT * 0.023;
        
        
        UIButton *number1Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(154 / 3), CGHeightFromIP6P(254), CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number1Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number1Button setTitle:@"1" forState:UIControlStateNormal];
        [number1Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number1Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number1Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number1Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number1Button setTag:1];
        [number1Button.layer setCornerRadius:number1Button.frame.size.width / 2];
        [number1Button setClipsToBounds:YES];
        [number1Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number1Button];
        
        UIButton *number2Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(495 / 3), CGHeightFromIP6P(254), CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number2Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number2Button setTitle:@"2" forState:UIControlStateNormal];
        [number2Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number2Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number2Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number2Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number2Button setTag:2];
        [number2Button.layer setCornerRadius:number2Button.frame.size.width / 2];
        [number2Button setClipsToBounds:YES];
        [number2Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number2Button];
        
        UIButton *number3Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(839 / 3), CGHeightFromIP6P(254), CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number3Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number3Button setTitle:@"3" forState:UIControlStateNormal];
        [number3Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number3Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number3Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number3Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number3Button setTag:3];
        [number3Button.layer setCornerRadius:number3Button.frame.size.width / 2];
        [number3Button setClipsToBounds:YES];
        [number3Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number3Button];
        
        
        UIButton *number4Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(154 / 3), number1Button.frame.origin.y + number1Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number4Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number4Button setTitle:@"4" forState:UIControlStateNormal];
        [number4Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number4Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number4Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number4Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number4Button setTag:4];
        [number4Button.layer setCornerRadius:number4Button.frame.size.width / 2];
        [number4Button setClipsToBounds:YES];
        [number4Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number4Button];
        
        UIButton *number5Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(495 / 3), number1Button.frame.origin.y + number1Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number5Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number5Button setTitle:@"5" forState:UIControlStateNormal];
        [number5Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number5Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number5Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number5Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number5Button setTag:5];
        [number5Button.layer setCornerRadius:number5Button.frame.size.width / 2];
        [number5Button setClipsToBounds:YES];
        [number5Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number5Button];
        
        UIButton *number6Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(839 / 3), number1Button.frame.origin.y + number1Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number6Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number6Button setTitle:@"6" forState:UIControlStateNormal];
        [number6Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number6Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number6Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number6Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number6Button setTag:6];
        [number6Button.layer setCornerRadius:number6Button.frame.size.width / 2];
        [number6Button setClipsToBounds:YES];
        [number6Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number6Button];
        
        
        UIButton *number7Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(154 / 3), number4Button.frame.origin.y + number4Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number7Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number7Button setTitle:@"7" forState:UIControlStateNormal];
        [number7Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number7Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number7Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number7Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number7Button setTag:7];
        [number7Button.layer setCornerRadius:number7Button.frame.size.width / 2];
        [number7Button setClipsToBounds:YES];
        [number7Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number7Button];
        
        UIButton *number8Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(495 / 3), number4Button.frame.origin.y + number4Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number8Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number8Button setTitle:@"8" forState:UIControlStateNormal];
        [number8Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number8Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number8Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number8Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number8Button setTag:8];
        [number8Button.layer setCornerRadius:number8Button.frame.size.width / 2];
        [number8Button setClipsToBounds:YES];
        [number8Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number8Button];
        
        UIButton *number9Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(839 / 3), number4Button.frame.origin.y + number4Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number9Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number9Button setTitle:@"9" forState:UIControlStateNormal];
        [number9Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number9Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number9Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number9Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number9Button setTag:9];
        [number9Button.layer setCornerRadius:number9Button.frame.size.width / 2];
        [number9Button setClipsToBounds:YES];
        [number9Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number9Button];
        
        
        UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(154 / 3), number7Button.frame.origin.y + number7Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [cancelButton.titleLabel setFont:MediumWithSize(CGHeightFromIP6P(18))];
        [cancelButton setTitle:@"취소" forState:UIControlStateNormal];
        [cancelButton setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [cancelButton setTag:100];
        [cancelButton setClipsToBounds:YES];
        [cancelButton setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:cancelButton];
        
        UIButton *number0Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(495 / 3), number7Button.frame.origin.y + number7Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [number0Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
        [number0Button setTitle:@"0" forState:UIControlStateNormal];
        [number0Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [number0Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
        [number0Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
        [number0Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [number0Button setTag:0];
        [number0Button.layer setCornerRadius:number0Button.frame.size.width / 2];
        [number0Button setClipsToBounds:YES];
        [number0Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:number0Button];
        
        UIButton *removeButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(839 / 3), number7Button.frame.origin.y + number7Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
        [removeButton.titleLabel setFont:MediumWithSize(CGHeightFromIP6P(18))];
        [removeButton setTitle:@"지움" forState:UIControlStateNormal];
        [removeButton setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
        [removeButton addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
        [removeButton setTag:-1];
        [removeButton setClipsToBounds:YES];
        [removeButton setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.view addSubview:removeButton];
        
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
