//
//  MenstruationingViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenstruationingViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UIWebView *webView;

@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UIButton *forwardButton;
@property (strong, nonatomic) UIButton *shareButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
