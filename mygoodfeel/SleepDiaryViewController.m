//
//  SleepDiaryViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 12. 7..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SleepDiaryViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;


@interface SleepDiaryViewController ()

@end

@implementation SleepDiaryViewController

- (void)setDailyStatus
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SLEEPDAILY_SET_STATUS]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    
    if (isCaffeine) {
        [urlString appendFormat:@"/Y"];
    } else {
        [urlString appendFormat:@"/N"];
    }
    
    if (isAlcohol) {
        [urlString appendFormat:@"/Y"];
    } else {
        [urlString appendFormat:@"/N"];
    }
    
    if (isExercise) {
        [urlString appendFormat:@"/Y"];
    } else {
        [urlString appendFormat:@"/N"];
    }
    
    if (isOvereat) {
        [urlString appendFormat:@"/Y"];
    } else {
        [urlString appendFormat:@"/N"];
    }
    
    if (isStressed) {
        [urlString appendFormat:@"/Y"];
    } else {
        [urlString appendFormat:@"/N"];
    }
    
    if (isOvernight) {
        [urlString appendFormat:@"/Y"];
    } else {
        [urlString appendFormat:@"/N"];
    }

    
    [urlString appendFormat:@"/%@", _dateString];               // date
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    NSLog(@"param: %@", urlString);
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [self dismissSelfView];
        [[[UIAlertView alloc] initWithTitle:nil message:@"저장되었습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
    
}

- (void)getDailyStatus
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SLEEPDAILY_GET_STATUS]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@", _dateString];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"data"][@"CoffeeYN"] isEqualToString:@"Y"]) {
            self->isCaffeine = YES;
            [self->_caffeineCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
        if ([responseData[@"data"][@"DrinkYN"] isEqualToString:@"Y"]) {
            self->isAlcohol = YES;
            [self->_alcoholCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
        if ([responseData[@"data"][@"HealthYN"] isEqualToString:@"Y"]) {
            self->isExercise = YES;
            [self->_exerciseCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
        if ([responseData[@"data"][@"OverEatYN"] isEqualToString:@"Y"]) {
            self->isOvereat = YES;
            [self->_overeatCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
        if ([responseData[@"data"][@"StressYN"] isEqualToString:@"Y"]) {
            self->isStressed = YES;
            [self->_stressedCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
        if ([responseData[@"data"][@"OverNightYN"] isEqualToString:@"Y"]) {
            self->isOvernight = YES;
            [self->_overnightCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (void)dismissSelfView
{
//    [(CycleDayInfoViewController *)_pClass getCycleDailyInfo];
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}
    
- (void)toggleCheckButton:(UIButton *)button
{
    if (button == _caffeineCheckButton) {
        isCaffeine = !isCaffeine;
        [_caffeineCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        if (isCaffeine) {
            [_caffeineCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
    } else if (button == _alcoholCheckButton) {
        isAlcohol = !isAlcohol;
        [_alcoholCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        if (isAlcohol) {
            [_alcoholCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
    } else if (button == _exerciseCheckButton) {
        isExercise = !isExercise;
        [_exerciseCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        if (isExercise) {
            [_exerciseCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
    } else if (button == _overeatCheckButton) {
        isOvereat = !isOvereat;
        [_overeatCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        if (isOvereat) {
            [_overeatCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
    } else if (button == _stressedCheckButton) {
        isStressed = !isStressed;
        [_stressedCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        if (isStressed) {
            [_stressedCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
    } else if (button == _overnightCheckButton) {
        isOvernight = !isOvernight;
        [_overnightCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        if (isOvernight) {
            [_overnightCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass dateString:(NSString *)dateString
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _dateString = dateString;
        _pClass = pClass;
        
        isCaffeine = NO;
        isAlcohol = NO;
        isExercise = NO;
        isOvereat = NO;
        isStressed = NO;
        isOvernight = NO;

        [self.view setBackgroundColor:UIColorFromRGB(0xefeff4)];
        
        [self setTitle:@"수면 다이어리"];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_close_pink"].size.width, [UIImage imageNamed:@"common_button_close_pink"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"common_button_close_pink"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];
        
        UIButton *saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
        [saveButton setTitleColor:UIColorFromRGB(0xf16774) forState:UIControlStateNormal];
        [saveButton setTitleColor:UIColorFromRGB(0x883444) forState:UIControlStateHighlighted];
        [saveButton.titleLabel setFont:MediumWithSize(17)];
        [saveButton setTitle:@"저장" forState:UIControlStateNormal];
        [saveButton sizeToFit];
        [saveButton addTarget:self action:@selector(setDailyStatus) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:saveButton]];
        
        UILabel *checkGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, CGHeightFromIP6P(114 / 3))];
        [checkGuideLabel setBackgroundColor:[UIColor clearColor]];
        [checkGuideLabel setTextColor:UIColorFromRGB(0x87878f)];
        [checkGuideLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
        [checkGuideLabel setText:@"      해당하는 것에 체크해주세요. 수면 측정에에 도움이 됩니다."];
        [self.view addSubview:checkGuideLabel];
        
        
        UIView *itemListView = [[UIView alloc] initWithFrame:CGRectMake(0, checkGuideLabel.frame.size.height, kSCREEN_HEIGHT, 1000 / 3 + k3PX + k3PX)];
        [itemListView setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:itemListView];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(50 / 3, 200 / 3, kSCREEN_WIDTH - 50 / 3, k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xe4e4e4)];
        [itemListView addSubview:separatorView];
        
        separatorView = [[UIView alloc] initWithFrame:CGRectMake(50 / 3, 400 / 3 + k1PX, kSCREEN_WIDTH - 50 / 3, k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xe4e4e4)];
        [itemListView addSubview:separatorView];
        
        separatorView = [[UIView alloc] initWithFrame:CGRectMake(50 / 3, 600 / 3 + k2PX, kSCREEN_WIDTH - 50 / 3, k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xe4e4e4)];
        [itemListView addSubview:separatorView];
        
        separatorView = [[UIView alloc] initWithFrame:CGRectMake(50 / 3, 800 / 3 + k2PX, kSCREEN_WIDTH - 50 / 3, k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xe4e4e4)];
        [itemListView addSubview:separatorView];
        


        UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 200 / 3)];
        [cellView setBackgroundColor:[UIColor clearColor]];
        [itemListView addSubview:cellView];
        
        UIImageView *cellIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sleepdiary_image_caffeine"]];
        [cellIconImageView setCenter:CGPointMake(57 / 3 + cellIconImageView.frame.size.width / 2, 200 / 3 / 2)];
        [cellView addSubview:cellIconImageView];
        
        UILabel *cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(160 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"카페인을 섭취했어요"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        

        _caffeineCheckButton = [[UIButton alloc] initWithFrame:CGRectMake(cellView.frame.size.width - 44 - CGWidthFromIP6P(16), cellView.frame.size.height / 2 - 44 / 2, 44, 44)];
        [_caffeineCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_caffeineCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_caffeineCheckButton addTarget:self action:@selector(toggleCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_caffeineCheckButton];

        
        cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 200 / 3 + k1PX, kSCREEN_WIDTH, 200 / 3)];
        [cellView setBackgroundColor:[UIColor clearColor]];
        [itemListView addSubview:cellView];
        
        cellIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sleepdiary_image_alcohol"]];
        [cellIconImageView setCenter:CGPointMake(57 / 3 + cellIconImageView.frame.size.width / 2, 200 / 3 / 2)];
        [cellView addSubview:cellIconImageView];
        
        cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(160 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"술 마셨어요"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        
        _alcoholCheckButton = [[UIButton alloc] initWithFrame:CGRectMake(cellView.frame.size.width - 44 - CGWidthFromIP6P(16), cellView.frame.size.height / 2 - 44 / 2, 44, 44)];
        [_alcoholCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_alcoholCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_alcoholCheckButton addTarget:self action:@selector(toggleCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_alcoholCheckButton];

        
        
        cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 400 / 3 + k2PX, kSCREEN_WIDTH, 200 / 3)];
        [cellView setBackgroundColor:[UIColor clearColor]];
        [itemListView addSubview:cellView];
        
        cellIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sleepdiary_image_exercise"]];
        [cellIconImageView setCenter:CGPointMake(57 / 3 + cellIconImageView.frame.size.width / 2, 200 / 3 / 2)];
        [cellView addSubview:cellIconImageView];
        
        cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(160 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"운동했어요"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        
        _exerciseCheckButton = [[UIButton alloc] initWithFrame:CGRectMake(cellView.frame.size.width - 44 - CGWidthFromIP6P(16), cellView.frame.size.height / 2 - 44 / 2, 44, 44)];
        [_exerciseCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_exerciseCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_exerciseCheckButton addTarget:self action:@selector(toggleCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_exerciseCheckButton];

        
        cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 600 / 3 + k2PX, kSCREEN_WIDTH, 200 / 3)];
        [cellView setBackgroundColor:[UIColor clearColor]];
        [itemListView addSubview:cellView];
        
        cellIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sleepdiary_image_overeat"]];
        [cellIconImageView setCenter:CGPointMake(57 / 3 + cellIconImageView.frame.size.width / 2, 200 / 3 / 2)];
        [cellView addSubview:cellIconImageView];
        
        cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(160 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"과식했어요"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        
        _overeatCheckButton = [[UIButton alloc] initWithFrame:CGRectMake(cellView.frame.size.width - 44 - CGWidthFromIP6P(16), cellView.frame.size.height / 2 - 44 / 2, 44, 44)];
        [_overeatCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_overeatCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_overeatCheckButton addTarget:self action:@selector(toggleCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_overeatCheckButton];

        
        cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 800 / 3 + k2PX, kSCREEN_WIDTH, 200 / 3)];
        [cellView setBackgroundColor:[UIColor clearColor]];
        [itemListView addSubview:cellView];
        
        cellIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sleepdiary_image_stressed"]];
        [cellIconImageView setCenter:CGPointMake(57 / 3 + cellIconImageView.frame.size.width / 2, 200 / 3 / 2)];
        [cellView addSubview:cellIconImageView];
        
        cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(160 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"스트레스가 심했어요"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        
        _stressedCheckButton = [[UIButton alloc] initWithFrame:CGRectMake(cellView.frame.size.width - 44 - CGWidthFromIP6P(16), cellView.frame.size.height / 2 - 44 / 2, 44, 44)];
        [_stressedCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_stressedCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_stressedCheckButton addTarget:self action:@selector(toggleCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_stressedCheckButton];

        
        cellView = [[UIView alloc] initWithFrame:CGRectMake(0, itemListView.frame.origin.y + 1100 / 3 + k2PX, kSCREEN_WIDTH, 200 / 3)];
        [cellView setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:cellView];
        
        cellIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sleepdiary_image_overnight"]];
        [cellIconImageView setCenter:CGPointMake(57 / 3 + cellIconImageView.frame.size.width / 2, 200 / 3 / 2)];
        [cellView addSubview:cellIconImageView];
        
        cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(160 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"입는 오버나이트를 입었어요"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        
        _overnightCheckButton = [[UIButton alloc] initWithFrame:CGRectMake(cellView.frame.size.width - 44 - CGWidthFromIP6P(16), cellView.frame.size.height / 2 - 44 / 2, 44, 44)];
        [_overnightCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_overnightCheckButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_overnightCheckButton addTarget:self action:@selector(toggleCheckButton:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_overnightCheckButton];

        
        
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"CycleInfoDiary",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"CycleInfoDiary",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];
        
        [self getDailyStatus];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
