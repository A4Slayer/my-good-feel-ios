//
//  PreferenceSensorNotificaionViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 4. 16..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "PreferenceSensorNotificaionViewController.h"

@interface PreferenceSensorNotificaionViewController ()

@end

@implementation PreferenceSensorNotificaionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"알림 설정"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x3c3c3e)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
        [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT) style:UITableViewStyleGrouped];
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setTableFooterView:[[UIView alloc] init]];
        [self.view addSubview:_mainTableView];
        
    }
    return self;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 44;
    } else if (indexPath.section == 1) {
        return 270;
    } else if (indexPath.section == 2) {
        return 270;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 38;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *rtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 38)];
    [rtnView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 15, 200, 13)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:UIColorFromRGB(0x6d6d72)];
    [titleLabel setFont:[UIFont systemFontOfSize:12]];
    
    if (section == 0) {
        [titleLabel setText:@"알림 설정"];
    } else if (section == 1) {
        [titleLabel setText:@"이상 심박수 알림"];
    } else if (section == 2) {
        [titleLabel setText:@"이상 호흡주기 알림"];
    }
    
    [rtnView addSubview:titleLabel];

    return rtnView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (void)changeSlide:(UISlider *)slider
{
    if (slider == _overHRSlider) {
        int hrValue = slider.value * 60 + 50;
        [_overHRLabel setText:[NSString stringWithFormat:@"심박수가 %dbpm을 초과할 경우 알립니다.", hrValue]];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", hrValue] forKey:@"hrmax"];

        
        int hrMinValue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"hrmin"] intValue];

        if (hrValue <= hrMinValue) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", hrValue - 1] forKey:@"hrmin"];
            [_underHRSlider setValue:([[[NSUserDefaults standardUserDefaults] objectForKey:@"hrmin"] intValue] - 35) / 35.0];
            [_underHRLabel setText:[NSString stringWithFormat:@"심박수가 %dbpm을 미달할 경우 알립니다.", hrValue - 1]];
        }
        
    } else if (slider == _underHRSlider) {
        int hrValue = slider.value * 35 + 35;
        [_underHRLabel setText:[NSString stringWithFormat:@"심박수가 %dbpm을 미달할 경우 알립니다.", hrValue]];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", hrValue] forKey:@"hrmin"];

        int hrMaxValue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"hrmax"] intValue];
        
        if (hrValue >= hrMaxValue) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", hrValue + 1] forKey:@"hrmax"];
            [_overHRSlider setValue:([[[NSUserDefaults standardUserDefaults] objectForKey:@"hrmax"] intValue] - 50) / 60.0];
            [_overHRLabel setText:[NSString stringWithFormat:@"심박수가 %dbpm을 초과할 경우 알립니다.", hrValue + 1]];
        }

    } else if (slider == _overRRSlider) {
        int rrValue = slider.value * 20 + 12;
        [_overRRLabel setText:[NSString stringWithFormat:@"호흡주기가 %dbpm을 초과할 경우 알립니다.", rrValue]];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", rrValue] forKey:@"rrmax"];

        int rrMinValue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"rrmin"] intValue];
        
        if (rrValue <= rrMinValue) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", rrValue - 1] forKey:@"rrmin"];
            [_underRRSlider setValue:([[[NSUserDefaults standardUserDefaults] objectForKey:@"rrmin"] intValue] - 7) / 13.0];
            [_underRRLabel setText:[NSString stringWithFormat:@"호흡주기가 %dbpm을 미달할 경우 알립니다.", rrValue - 1]];
        }

    } else if (slider == _underRRSlider) {
        int rrValue = slider.value * 13 + 7;
        [_underRRLabel setText:[NSString stringWithFormat:@"호흡주기가 %dbpm을 미달할 경우 알립니다.", rrValue]];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", rrValue] forKey:@"rrmin"];
        
        int rrMaxValue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"rrmax"] intValue];
        
        if (rrValue >= rrMaxValue) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", rrValue + 1] forKey:@"rrmax"];
            [_overRRSlider setValue:([[[NSUserDefaults standardUserDefaults] objectForKey:@"rrmax"] intValue] - 12) / 20.0];
            [_overRRLabel setText:[NSString stringWithFormat:@"호흡주기가 %dbpm을 초과할 경우 알립니다.", rrValue + 1]];
        }

    }

    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)changeSwitch:(UISwitch *)alertSwitch
{
    if (alertSwitch.isOn) {
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"sleep_alert"];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"sleep_alert"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [NSString stringWithFormat:@"Cell_%ld_%ld", (long)indexPath.section, (long)indexPath.row];
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            [cell setBackgroundColor:[UIColor whiteColor]];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
            [titleLabel setText:@"알림"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:titleLabel];
            
            if (_notificationSwitch) {
                [_notificationSwitch removeFromSuperview];
                _notificationSwitch = nil;
            }
            
            _notificationSwitch = [[UISwitch alloc] init];
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"sleep_alert"] isEqualToString:@"1"]) {
                [_notificationSwitch setOn:YES];
            } else {
                [_notificationSwitch setOn:NO];
            }
            
            [_notificationSwitch setCenter:CGPointMake(kSCREEN_WIDTH - _notificationSwitch.frame.size.width / 2 - CGWidthFromIP6P(18), 44 / 2)];
            [_notificationSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
            [cell.contentView addSubview:_notificationSwitch];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        }
        return cell;
    } else if (indexPath.section == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            [cell setBackgroundColor:[UIColor whiteColor]];
            
            // 이상 심박수 알림
            _overHRLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, kSCREEN_WIDTH - 32, 44)];
            [_overHRLabel setText:[NSString stringWithFormat:@"심박수가 %@bpm을 초과할 경우 알립니다.", [[NSUserDefaults standardUserDefaults] objectForKey:@"hrmax"]]];
            [_overHRLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:_overHRLabel];
            

            UILabel *hrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 44 + 10, 100, 100)];
            [hrValueLabel setText:@"50"];
            [hrValueLabel setTextColor:[UIColor lightGrayColor]];
            [hrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [hrValueLabel sizeToFit];
            [cell.contentView addSubview:hrValueLabel];
            
            hrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 44 + 10, 100, 100)];
            [hrValueLabel setText:@"80"];
            [hrValueLabel setTextColor:[UIColor lightGrayColor]];
            [hrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [hrValueLabel sizeToFit];
            [hrValueLabel setCenter:CGPointMake(kSCREEN_WIDTH / 2, hrValueLabel.center.y)];
            [cell.contentView addSubview:hrValueLabel];
            
            hrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 44 + 10, 100, 100)];
            [hrValueLabel setText:@"110"];
            [hrValueLabel setTextColor:[UIColor lightGrayColor]];
            [hrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [hrValueLabel sizeToFit];
            [hrValueLabel setFrame:CGRectMake(kSCREEN_WIDTH - 16 - hrValueLabel.frame.size.width, hrValueLabel.frame.origin.y, hrValueLabel.frame.size.width, hrValueLabel.frame.size.height)];
            [cell.contentView addSubview:hrValueLabel];

            
            if (_overHRSlider) {
                [_overHRSlider removeFromSuperview];
            }
            
            _overHRSlider = [[UISlider alloc] initWithFrame:CGRectMake(16, 88, kSCREEN_WIDTH - 32, 20)];
            [_overHRSlider setValue:([[[NSUserDefaults standardUserDefaults] objectForKey:@"hrmax"] intValue] - 50) / 60.0];
            [_overHRSlider addTarget:self action:@selector(changeSlide:) forControlEvents:UIControlEventValueChanged];
            [cell.contentView addSubview:_overHRSlider];

            _underHRLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 130, kSCREEN_WIDTH - 32, 44)];
            [_underHRLabel setText:[NSString stringWithFormat:@"심박수가 %@bpm을 미달할 경우 알립니다.", [[NSUserDefaults standardUserDefaults] objectForKey:@"hrmin"]]];
            [_underHRLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:_underHRLabel];

            
            hrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 174 + 10, 100, 100)];
            [hrValueLabel setText:@"35"];
            [hrValueLabel setTextColor:[UIColor lightGrayColor]];
            [hrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [hrValueLabel sizeToFit];
            [cell.contentView addSubview:hrValueLabel];
            
            hrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 174 + 10, 100, 100)];
            [hrValueLabel setText:@"53"];
            [hrValueLabel setTextColor:[UIColor lightGrayColor]];
            [hrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [hrValueLabel sizeToFit];
            [hrValueLabel setCenter:CGPointMake(kSCREEN_WIDTH / 2, hrValueLabel.center.y)];
            [cell.contentView addSubview:hrValueLabel];
            
            hrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 174 + 10, 100, 100)];
            [hrValueLabel setText:@"70"];
            [hrValueLabel setTextColor:[UIColor lightGrayColor]];
            [hrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [hrValueLabel sizeToFit];
            [hrValueLabel setFrame:CGRectMake(kSCREEN_WIDTH - 16 - hrValueLabel.frame.size.width, hrValueLabel.frame.origin.y, hrValueLabel.frame.size.width, hrValueLabel.frame.size.height)];
            [cell.contentView addSubview:hrValueLabel];

            if (_underHRSlider) {
                [_underHRSlider removeFromSuperview];
            }
            
            _underHRSlider = [[UISlider alloc] initWithFrame:CGRectMake(16, 218, kSCREEN_WIDTH - 32, 20)];
            [_underHRSlider setValue:([[[NSUserDefaults standardUserDefaults] objectForKey:@"hrmin"] intValue] - 35) / 35.0];
            [_underHRSlider addTarget:self action:@selector(changeSlide:) forControlEvents:UIControlEventValueChanged];
            [cell.contentView addSubview:_underHRSlider];

            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        return cell;
    } else if (indexPath.section == 2) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            [cell setBackgroundColor:[UIColor whiteColor]];
            
            _overRRLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, kSCREEN_WIDTH - 32, 44)];
            [_overRRLabel setText:[NSString stringWithFormat:@"호흡주기가 %@bpm을 초과할 경우 알립니다.", [[NSUserDefaults standardUserDefaults] objectForKey:@"rrmax"]]];
            [_overRRLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:_overRRLabel];
            
            
            UILabel *rrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 44 + 10, 100, 100)];
            [rrValueLabel setText:@"12"];
            [rrValueLabel setTextColor:[UIColor lightGrayColor]];
            [rrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [rrValueLabel sizeToFit];
            [cell.contentView addSubview:rrValueLabel];
            
            rrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 44 + 10, 100, 100)];
            [rrValueLabel setText:@"22"];
            [rrValueLabel setTextColor:[UIColor lightGrayColor]];
            [rrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [rrValueLabel sizeToFit];
            [rrValueLabel setCenter:CGPointMake(kSCREEN_WIDTH / 2, rrValueLabel.center.y)];
            [cell.contentView addSubview:rrValueLabel];
            
            rrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 44 + 10, 100, 100)];
            [rrValueLabel setText:@"32"];
            [rrValueLabel setTextColor:[UIColor lightGrayColor]];
            [rrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [rrValueLabel sizeToFit];
            [rrValueLabel setFrame:CGRectMake(kSCREEN_WIDTH - 16 - rrValueLabel.frame.size.width, rrValueLabel.frame.origin.y, rrValueLabel.frame.size.width, rrValueLabel.frame.size.height)];
            [cell.contentView addSubview:rrValueLabel];
            

            if (_overRRSlider) {
                [_overRRSlider removeFromSuperview];
            }

            _overRRSlider = [[UISlider alloc] initWithFrame:CGRectMake(16, 88, kSCREEN_WIDTH - 32, 20)];
            [_overRRSlider setValue:([[[NSUserDefaults standardUserDefaults] objectForKey:@"rrmax"] intValue] - 12) / 20.0];
            [_overRRSlider addTarget:self action:@selector(changeSlide:) forControlEvents:UIControlEventValueChanged];
            [cell.contentView addSubview:_overRRSlider];
            
            _underRRLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 130, kSCREEN_WIDTH - 32, 44)];
            [_underRRLabel setText:[NSString stringWithFormat:@"호흡주기가 %@bpm을 미달할 경우 알립니다.", [[NSUserDefaults standardUserDefaults] objectForKey:@"rrmin"]]];
            [_underRRLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:_underRRLabel];
            
            
            rrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 174 + 10, 100, 100)];
            [rrValueLabel setText:@"7"];
            [rrValueLabel setTextColor:[UIColor lightGrayColor]];
            [rrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [rrValueLabel sizeToFit];
            [cell.contentView addSubview:rrValueLabel];
            
            rrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 174 + 10, 100, 100)];
            [rrValueLabel setText:@"14"];
            [rrValueLabel setTextColor:[UIColor lightGrayColor]];
            [rrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [rrValueLabel sizeToFit];
            [rrValueLabel setCenter:CGPointMake(kSCREEN_WIDTH / 2, rrValueLabel.center.y)];
            [cell.contentView addSubview:rrValueLabel];
            
            rrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 174 + 10, 100, 100)];
            [rrValueLabel setText:@"20"];
            [rrValueLabel setTextColor:[UIColor lightGrayColor]];
            [rrValueLabel setFont:[UIFont systemFontOfSize:14]];
            [rrValueLabel sizeToFit];
            [rrValueLabel setFrame:CGRectMake(kSCREEN_WIDTH - 16 - rrValueLabel.frame.size.width, rrValueLabel.frame.origin.y, rrValueLabel.frame.size.width, rrValueLabel.frame.size.height)];
            [cell.contentView addSubview:rrValueLabel];
            
            
            if (_underRRSlider) {
                [_underRRSlider removeFromSuperview];
            }

            _underRRSlider = [[UISlider alloc] initWithFrame:CGRectMake(16, 218, kSCREEN_WIDTH - 32, 20)];
            [_underRRSlider setValue:([[[NSUserDefaults standardUserDefaults] objectForKey:@"rrmin"] intValue] - 7) / 13.0];
            [_underRRSlider addTarget:self action:@selector(changeSlide:) forControlEvents:UIControlEventValueChanged];
            [cell.contentView addSubview:_underRRSlider];

            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        return cell;
    } else {
        NSString *cellId = [NSString stringWithFormat:@"DEFAULT"];
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        return cell;
    }
    
    return nil;
}

@end
