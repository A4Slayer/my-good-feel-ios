//
//  PreferenceUserInfoViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 17/05/2019.
//  Copyright © 2019 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"

@interface PreferenceUserInfoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) UITableView *mainTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@end
