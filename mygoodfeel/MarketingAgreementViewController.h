//
//  MarketingAgreementViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"

@interface MarketingAgreementViewController : UIViewController

@property (strong, nonatomic) id pClass;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
