//
//  CycleCalendarGuideViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 17..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "CycleCalendarGuideViewController.h"

@interface CycleCalendarGuideViewController ()

@end

@implementation CycleCalendarGuideViewController

- (void)dismissSelfView
{
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        [self setTitle:@"주기 캘린더 읽는 법"];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_close_pink"].size.width, [UIImage imageNamed:@"common_button_close_pink"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"common_button_close_pink"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];


        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT)];
        [_mainScrollView setBackgroundColor:UIColorFromRGB(0xffffff)];
        [self.view addSubview:_mainScrollView];

        UILabel *menstruationStartDayLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(101 / 3), CGWidthFromIP6P(353 / 3), CGHeightFromIP6P(89 / 3))];
        [menstruationStartDayLabel setTextColor:UIColorFromRGB(0x434343)];
        [menstruationStartDayLabel setBackgroundColor:UIColorFromRGB(0xffb6b8)];
        [menstruationStartDayLabel setTextAlignment:NSTextAlignmentCenter];
        [menstruationStartDayLabel setFont:MediumWithSize(CGHeightFromIP6P(16))];
        [menstruationStartDayLabel setText:@"생리 시작일"];
        [_mainScrollView addSubview:menstruationStartDayLabel];

        UILabel *availablePregnancyLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(101 / 3), CGWidthFromIP6P(353 / 3), CGHeightFromIP6P(89 / 3))];
        [availablePregnancyLabel setTextColor:UIColorFromRGB(0x434343)];
        [availablePregnancyLabel setBackgroundColor:UIColorFromRGB(0xf0e3f6)];
        [availablePregnancyLabel setTextAlignment:NSTextAlignmentCenter];
        [availablePregnancyLabel setFont:MediumWithSize(CGHeightFromIP6P(16))];
        [availablePregnancyLabel setText:@"예상 가임기"];
        [_mainScrollView addSubview:availablePregnancyLabel];

        UILabel *menstruationPeriodLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(241 / 3), CGWidthFromIP6P(353 / 3), CGHeightFromIP6P(89 / 3))];
        [menstruationPeriodLabel setTextColor:UIColorFromRGB(0x434343)];
        [menstruationPeriodLabel setBackgroundColor:UIColorFromRGB(0xffccce)];
        [menstruationPeriodLabel setTextAlignment:NSTextAlignmentCenter];
        [menstruationPeriodLabel setFont:MediumWithSize(CGHeightFromIP6P(16))];
        [menstruationPeriodLabel setText:@"생리 기간"];
        [_mainScrollView addSubview:menstruationPeriodLabel];

        UILabel *availableMenstruationPeriodLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(241 / 3), CGWidthFromIP6P(353 / 3), CGHeightFromIP6P(89 / 3))];
        [availableMenstruationPeriodLabel setTextColor:UIColorFromRGB(0x434343)];
        [availableMenstruationPeriodLabel setBackgroundColor:UIColorFromRGB(0xffe3e4)];
        [availableMenstruationPeriodLabel setTextAlignment:NSTextAlignmentCenter];
        [availableMenstruationPeriodLabel setFont:MediumWithSize(CGHeightFromIP6P(16))];
        [availableMenstruationPeriodLabel setText:@"예상 생리 기간"];
        [_mainScrollView addSubview:availableMenstruationPeriodLabel];

        
        UILabel *todayIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(445 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [todayIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:todayIconLabel];
        CNSTextAttachment *imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_today"]];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  오늘의 날짜"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [todayIconLabel setAttributedText:attrString];
        

        UILabel *bloodNoneIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(445 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodNoneIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodNoneIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_none"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 양 없음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodNoneIconLabel setAttributedText:attrString];

        
        UILabel *bloodLessIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(607 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodLessIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodLessIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_less"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 양 적음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodLessIconLabel setAttributedText:attrString];

        
        UILabel *loveIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(607 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [loveIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:loveIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -8)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_love"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  사랑"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [loveIconLabel setAttributedText:attrString];

        
        UILabel *bloodNormalIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(769 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodNormalIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodNormalIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_normal"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 양 보통"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodNormalIconLabel setAttributedText:attrString];

        
        UILabel *pyrexiaIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(769 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [pyrexiaIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:pyrexiaIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_pyrexia"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  발열 있음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [pyrexiaIconLabel setAttributedText:attrString];

        
        UILabel *bloodMuchIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(931 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodMuchIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodMuchIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_much"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 양 많음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodMuchIconLabel setAttributedText:attrString];
        
        
        


        
        UILabel *calcGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), bloodMuchIconLabel.frame.origin.y + bloodMuchIconLabel.frame.size.height + CGHeightFromIP6P(20), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(240 / 3)))];
        [calcGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [calcGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [calcGuideLabel setClipsToBounds:YES];
        [calcGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [calcGuideLabel setNumberOfLines:0];
        [_mainScrollView addSubview:calcGuideLabel];
        
        attrString = [[NSMutableAttributedString alloc] initWithString:@"MY좋은느낌 생리 기간 계산\n"
                                                            attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(20)),
                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x000000) }];
        [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"\n"
                                                                                  attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(5)) }]];
        [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"MY좋은느낌은 두 가지 주기 계산법을 적용하고 있습니다."
                                                                                  attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(14)),
                                                                                                NSForegroundColorAttributeName : UIColorFromRGB(0x949494) }]];
        [calcGuideLabel setAttributedText:attrString];
        

        UILabel *normalCycleTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), calcGuideLabel.frame.origin.y + calcGuideLabel.frame.size.height + CGHeightFromIP6P(14), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(70 / 3)))];
        [normalCycleTitleLabel setBackgroundColor:[UIColor clearColor]];
        [normalCycleTitleLabel setTextColor:UIColorFromRGB(0xff6b86)];
        [normalCycleTitleLabel setFont:BoldWithSize(CGHeightFromIP6P(17))];
        [normalCycleTitleLabel setText:@"· 26일~32일의 생리 주기를 가진 고객"];
        [normalCycleTitleLabel sizeToFit];
        [_mainScrollView addSubview:normalCycleTitleLabel];
        
        UILabel *normalCycleDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(56 / 3), ceilf(normalCycleTitleLabel.frame.origin.y + normalCycleTitleLabel.frame.size.height + CGHeightFromIP6P(4)), kSCREEN_WIDTH - CGWidthFromIP6P(56 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [normalCycleDescLabel setNumberOfLines:0];
        [normalCycleDescLabel setBackgroundColor:[UIColor clearColor]];
        [normalCycleDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [normalCycleDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [normalCycleDescLabel setText:@"조지타운 대학교 생식건강연구소에서 개발한 표준일 피임법을 적용하며 생리 시작일부터 세어 처음 7일 동안은 안전기이며 8일부터 19일까지는 가임기, 20째 날부터 다음 예정 생리 시작일 전날까지 다시 안전기로 계산하는 방법입니다."];
        [normalCycleDescLabel sizeToFit];
        [_mainScrollView addSubview:normalCycleDescLabel];

        UILabel *abnormalCycleTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(normalCycleDescLabel.frame.origin.y + normalCycleDescLabel.frame.size.height + CGHeightFromIP6P(20)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(70 / 3)))];
        [abnormalCycleTitleLabel setBackgroundColor:[UIColor clearColor]];
        [abnormalCycleTitleLabel setTextColor:UIColorFromRGB(0xff6b86)];
        [abnormalCycleTitleLabel setFont:BoldWithSize(CGHeightFromIP6P(17))];
        [abnormalCycleTitleLabel setText:@"· 25일 이하 또는 33일 이상의 주기를 가진 고객"];
        [abnormalCycleTitleLabel sizeToFit];
        [_mainScrollView addSubview:abnormalCycleTitleLabel];
        
        UILabel *abnormalCycleDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(56 / 3), ceilf(abnormalCycleTitleLabel.frame.origin.y + abnormalCycleTitleLabel.frame.size.height + CGHeightFromIP6P(4)), kSCREEN_WIDTH - CGWidthFromIP6P(56 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [abnormalCycleDescLabel setNumberOfLines:0];
        [abnormalCycleDescLabel setBackgroundColor:[UIColor clearColor]];
        [abnormalCycleDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [abnormalCycleDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [abnormalCycleDescLabel setText:@"대한산부인과의사회에서 적용하는 계산법을 적용하며 첫 예상 생리 시작일은 생리가 끝난 다음날부터 28일 후로 예측하며 이후 3개월의 평균치로 다음 주기를 예상합니다."];
        [abnormalCycleDescLabel sizeToFit];
        [_mainScrollView addSubview:abnormalCycleDescLabel];

        
        UILabel *availableGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(abnormalCycleDescLabel.frame.origin.y + abnormalCycleDescLabel.frame.size.height + CGHeightFromIP6P(22)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [availableGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [availableGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [availableGuideLabel setClipsToBounds:YES];
        [availableGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [availableGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [availableGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [availableGuideLabel setText:@"가임기 및 배란일"];
        [_mainScrollView addSubview:availableGuideLabel];

        UILabel *availableDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(availableGuideLabel.frame.origin.y + availableGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [availableDescLabel setNumberOfLines:0];
        [availableDescLabel setBackgroundColor:[UIColor clearColor]];
        [availableDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [availableDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [availableDescLabel setText:@"MY좋은느낌은 예상 가임기를 최대 6일로 계산하고 있으며, 배란일은 가임기 시작일로부터 3~5일째 날입니다. 정확한 배란일 측정은 배란 테스트기를 활용하시면 정확히 파악하실 수 있습니다."];
        [availableDescLabel sizeToFit];
        [_mainScrollView addSubview:availableDescLabel];

        [_mainScrollView setContentSize:CGSizeMake(_mainScrollView.frame.size.width, availableDescLabel.frame.origin.y + availableDescLabel.frame.size.height + kBOTTOM_HEIGHT + CGHeightFromIP6P(16))];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
