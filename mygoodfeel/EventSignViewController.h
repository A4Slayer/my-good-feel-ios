//
//  EventSignViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 15..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNKeychain.h"
#import "AFNetworking.h"

@interface EventSignViewController : UIViewController <UIWebViewDelegate>
{
    BOOL isSignUp;
}

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UIWebView *webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass type:(int)type;

@end
