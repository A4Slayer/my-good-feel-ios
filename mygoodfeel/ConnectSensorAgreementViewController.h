//
//  ConnectSensorAgreementViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 4. 16..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"

@interface ConnectSensorAgreementViewController : UIViewController
{
    BOOL isCheckedAgreement;
}

@property (strong, nonatomic) id pClass;

@property (strong, nonatomic) UIScrollView *mainScrollView;

@property (strong, nonatomic) UIButton *agreementButton;
@property (strong, nonatomic) UIButton *agreeButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
