//
//  EventViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 14..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDetailViewController.h"
#import "AKSDWebImageManager.h"

@interface EventViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UITableView *mainTableView;
@property (strong, nonatomic) NSDictionary *eventDictionary;

@property (strong, nonatomic) NSMutableArray *cellHeightArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
