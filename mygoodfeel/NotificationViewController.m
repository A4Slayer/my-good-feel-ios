//
//  NotificationViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 6..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "NotificationViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface NotificationViewController ()

@end

@implementation NotificationViewController

- (void)requestNotification
{
    if (isLoading) {
        return;
    }
    
    isLoading = YES;
    
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_NOTIFICATION_GET_INFO]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@", _lastPageStr];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self->isLoading = NO;
        
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            if ([responseData[@"data"][@"PushInfo"] count]) {
                [self->_notificationArray addObjectsFromArray:responseData[@"data"][@"PushInfo"]];
                self->_lastPageStr = responseData[@"data"][@"PushInfo"][ [responseData[@"data"][@"PushInfo"] count] - 1][@"DisplayOrder"];
            } else {
                self->isMore = NO;
                
                if ([self->_notificationArray count] == 0) {
                    UIImageView *emptyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P([UIImage imageNamed:@"notification_image_empty"].size.width), CGHeightFromIP6P([UIImage imageNamed:@"notification_image_empty"].size.height))];
                    [emptyImageView setImage:[UIImage imageNamed:@"notification_image_empty"]];
                    [emptyImageView setCenter:CGPointMake(self->_mainTableView.frame.size.width / 2, self->_mainTableView.frame.size.height / 2 - CGHeightFromIP6P(40))];
                    [self->_mainTableView addSubview:emptyImageView];
                    
                    UILabel *emptyLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(16), emptyImageView.frame.origin.y + emptyImageView.frame.size.height + CGHeightFromIP6P(26), kSCREEN_WIDTH - CGWidthFromIP6P(32), 10)];
                    [emptyLabel setBackgroundColor:[UIColor clearColor]];
                    [emptyLabel setTextAlignment:NSTextAlignmentCenter];
                    [emptyLabel setText:@"새로운 소식이 없습니다."];
                    [emptyLabel setFont:MediumWithSize(CGHeightFromIP6P(17))];
                    [emptyLabel setTextColor:UIColorFromRGB(0xbcbcbc)];
                    [emptyLabel sizeToFit];
                    [emptyLabel setFrame:CGRectMake(CGWidthFromIP6P(16), emptyImageView.frame.origin.y + emptyImageView.frame.size.height + CGHeightFromIP6P(26), kSCREEN_WIDTH - CGWidthFromIP6P(32), emptyLabel.frame.size.height)];
                    [self->_mainTableView addSubview:emptyLabel];
                    // ui label
                }
                
            }
            [self->_mainTableView reloadData];
        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self->isLoading = NO;
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        isLoading = NO;
        isMore = YES;
        _pClass = pClass;
        _notificationArray = [[NSMutableArray alloc] init];
        _lastPageStr = @"0";
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"알림"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x000000)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu_black"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu_black"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
        
        UIButton *cycleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_cycle"].size.width, [UIImage imageNamed:@"common_button_cycle"].size.height)];
        [cycleButton setImage:[UIImage imageNamed:@"common_button_cycle"] forState:UIControlStateNormal];
        [cycleButton setAlpha:0];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cycleButton]];
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT) style:UITableViewStylePlain];
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setTableFooterView:[[UIView alloc] init]];
        [_mainTableView setSeparatorColor:UIColorFromRGB(0xf1f1f5)];
        [self.view addSubview:_mainTableView];
        
        //        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
        //                                                                 @"PageId" : @"Event",
        //                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
        //                                                                 @"UserStatus" : [[DataSingleton sharedSingletonClass] userInfo][@"UserStatus"],
        //                                                                 @"All" : [NSString stringWithFormat:@"%@|%@|%@",
        //                                                                           @"Event",
        //                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
        //                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserStatus"]
        //                                                                           ]
        //                                                                 }];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_notificationArray[indexPath.row][@"MessageCd"] intValue] == 102) {
        EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_notificationArray[indexPath.row][@"TargetIndexNo"]];
        [self.navigationController pushViewController:eventDetailViewController animated:YES];
    } else if ([_notificationArray[indexPath.row][@"MessageCd"] intValue] == 103) {
        NoticeDetailViewController *noticeDetailViewController = [[NoticeDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self boardNo:_notificationArray[indexPath.row][@"TargetIndexNo"]];
        [self.navigationController pushViewController:noticeDetailViewController animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CGHeightFromIP6P(77);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_notificationArray count] + isMore;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= [_notificationArray count]) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LOADING_CELL"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LOADING_CELL"];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell.contentView setBackgroundColor:[UIColor clearColor]];
        }
        [self requestNotification];
        return cell;
    } else {
        NSString *cellId = [NSString stringWithFormat:@"Cell_Notification_%d", [_notificationArray[indexPath.row][@"MessageSeq"] intValue]];
        NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];

        if (!cell) {
            cell = [[NotificationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId dataDictionary:_notificationArray[indexPath.row]];
        }

        return cell;
    }

    return nil;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([_mainTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_mainTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_mainTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_mainTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
