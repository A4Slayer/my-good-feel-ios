//
//  DailyReportViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "DailyReportViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface DailyReportViewController ()

@end

@implementation DailyReportViewController

- (void)bannerRollingThread
{
    if (self) {
        if (_bannerScrollView.contentSize.width > _bannerScrollView.frame.size.width) {
            if (_bannerScrollView.contentOffset.x < _bannerScrollView.contentSize.width - _bannerScrollView.frame.size.width) {
                [_bannerScrollView setContentOffset:CGPointMake(_bannerScrollView.contentOffset.x + _bannerScrollView.frame.size.width, _bannerScrollView.contentOffset.y) animated:YES];
            } else {
                [_bannerScrollView setContentOffset:CGPointMake(0, _bannerScrollView.contentOffset.y) animated:YES];
            }
        }
        
        if (_leftEventScrollView.contentSize.width > _leftEventScrollView.frame.size.width) {
            if (_leftEventScrollView.contentOffset.x < _leftEventScrollView.contentSize.width - _leftEventScrollView.frame.size.width) {
                [_leftEventScrollView setContentOffset:CGPointMake(_leftEventScrollView.contentOffset.x + _leftEventScrollView.frame.size.width, _leftEventScrollView.contentOffset.y) animated:YES];
            } else {
                [_leftEventScrollView setContentOffset:CGPointMake(0, _leftEventScrollView.contentOffset.y) animated:YES];
            }
        }
        
        if (_rightEventScrollView.contentSize.width > _rightEventScrollView.frame.size.width) {
            if (_rightEventScrollView.contentOffset.x < _rightEventScrollView.contentSize.width - _rightEventScrollView.frame.size.width) {
                [_rightEventScrollView setContentOffset:CGPointMake(_rightEventScrollView.contentOffset.x + _rightEventScrollView.frame.size.width, _rightEventScrollView.contentOffset.y) animated:YES];
            } else {
                [_rightEventScrollView setContentOffset:CGPointMake(0, _rightEventScrollView.contentOffset.y) animated:YES];
            }
        }
        
        [self performSelector:@selector(bannerRollingThread) withObject:nil afterDelay:8.0f];
    }
}

- (void)touchBanner:(UIButton *)button
{
    [MSAnalytics trackEvent:@"Banner clicked" withProperties:@{
                                                               @"PageId" : @"DailyReport",
                                                               @"BannerNo" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerNo"] intValue]],
                                                               @"BannerPublishGroupCd" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerPublishGroupCd"] intValue]],
                                                               @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                               @"All" : [NSString stringWithFormat:@"%@|%@|%@|%@",
                                                                         @"DailyReport",
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerNo"] intValue]],
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerPublishGroupCd"] intValue]],
                                                                         [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                         ]
                                                               }];

    if ([_dataDictionary[@"Banner"][button.tag][@"MoveType"] intValue] == 101) {
        [(RootViewController *)_pClass changeToEventViewController];
        EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_dataDictionary[@"Banner"][button.tag][@"IndexNo"]];
        UINavigationController *eventNavigationController = (UINavigationController *)[[(RootViewController *)_pClass drawerViewController] centerViewController];
        [eventNavigationController pushViewController:eventDetailViewController animated:YES];
//        EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_dataDictionary[@"Banner"][button.tag][@"IndexNo"]];
//        [self.navigationController pushViewController:eventDetailViewController animated:YES];
    } else if ([_dataDictionary[@"Banner"][button.tag][@"MoveType"] intValue] == 102) {
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][button.tag][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] options:@{} completionHandler:^(BOOL success) {
                exit(0);
            }];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][button.tag][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
        }
    }

}


- (void)getDailyReport
{
    if ([_lastDateString isEqualToString:[NSString stringWithFormat:@"%@-%@-%02ld", [_dateString substringWithRange:NSMakeRange(0, 4)], [_dateString substringWithRange:NSMakeRange(5, 2)], _dayCarousel.currentItemIndex + 1]]) {
        return;
    }
    
    _lastDateString = [NSString stringWithFormat:@"%@-%@-%02ld", [_dateString substringWithRange:NSMakeRange(0, 4)], [_dateString substringWithRange:NSMakeRange(5, 2)], _dayCarousel.currentItemIndex + 1];
    
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_DAILYREPORT_GET_INFO]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@", _lastDateString];
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserStatus"]];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                   fromDate:[NSDate date]];
    
    [_datePickerView selectRow:[[_dateString substringWithRange:NSMakeRange(0, 4)] intValue] - ([components year] - 10) inComponent:0 animated:NO];
    [_datePickerView selectRow:[[_dateString substringWithRange:NSMakeRange(5, 2)] intValue] - 1 inComponent:1 animated:NO];

    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        self->_dataDictionary = responseData[@"data"];

        NSMutableAttributedString *attrString;
        CNSTextAttachment *imageAttachment;

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:[dateFormatter dateFromString:[NSString stringWithFormat:@"%@-%@-%02ld", [self->_dateString substringWithRange:NSMakeRange(0, 4)], [self->_dateString substringWithRange:NSMakeRange(5, 2)], self->_dayCarousel.currentItemIndex + 1]]];

        NSArray *weekdayArray = @[ @"", @"일", @"월", @"화", @"수", @"목", @"금", @"토" ];

        [self->_weekdayLabel setText:[NSString stringWithFormat:@"%@요일", weekdayArray[[components weekday]]]];

        if ([self->_dataDictionary[@"IconCd"] intValue] == 101) {
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_menstruation_none"]];
        } else if ([self->_dataDictionary[@"IconCd"] intValue] == 102) {
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_menstruation_normal"]];
        } else if ([self->_dataDictionary[@"IconCd"] intValue] == 103) {
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_menstruation_much"]];
        } else if ([self->_dataDictionary[@"IconCd"] intValue] == 104) {
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_menstruation_pre"]];
        } else if ([self->_dataDictionary[@"IconCd"] intValue] == 105) {
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_menstruation_available"]];
        } else if ([self->_dataDictionary[@"IconCd"] intValue] == 201) {
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_menstruation_less"]];
        }
        
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", self->_dataDictionary[@"DailyMessage"]]
                                                                                  attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(40)),
                                                                                                NSForegroundColorAttributeName : UIColorFromRGB(0xf0606e) }]];
        [self->_statusLabel setAttributedText:attrString];

        NSString *weightChangeString = @"- ";
        
        if (![self->_dataDictionary[@"WeightChange"] isEqualToString:@"0"]) {
            weightChangeString = self->_dataDictionary[@"WeightChange"];
        }
        
        if (IS_HIDE_SLEEP_PART) {
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSMutableAttributedString alloc] initWithString:weightChangeString
                                                                                                                            attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(27)),
                                                                                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" kg"]
                                                                                      attributes:@{ NSFontAttributeName : LightWithSize(CGHeightFromIP6P(14)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x838383) }]];
            [self->_weightValueLabel setAttributedText:attrString];
        } else {
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSMutableAttributedString alloc] initWithString:weightChangeString
                                                                                                                            attributes:@{ NSFontAttributeName : BoldWithSize(CGHeightFromIP6P(35)),
                                                                                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" kg"]
                                                                                      attributes:@{ NSFontAttributeName : LightWithSize(CGHeightFromIP6P(16)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x838383) }]];
            [self->_weightValueLabel setAttributedText:attrString];
            [self->_weightValueLabel sizeToFit];
            
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSMutableAttributedString alloc] initWithString:weightChangeString
                                                                                                                            attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(27)),
                                                                                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" kg"]
                                                                                      attributes:@{ NSFontAttributeName : LightWithSize(CGHeightFromIP6P(14)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x838383) }]];
            [self->_weightValue2Label setAttributedText:attrString];
        }

        for (UIView *sView in [self->_bannerScrollView subviews]) {
            [sView removeFromSuperview];
        }
        for (UIView *sView in [self->_leftEventScrollView subviews]) {
            [sView removeFromSuperview];
        }
        for (UIView *sView in [self->_rightEventScrollView subviews]) {
            [sView removeFromSuperview];
        }

        self->_healthBannerArray = [[NSMutableArray alloc] init];
        int bannerIndex = 0;
        int leftEventIndex = 0;
        int rightEventIndex = 0;

        for (int i = 0; i < [self->_dataDictionary[@"Banner"] count]; i++) {
            NSDictionary *bannerDictionary = self->_dataDictionary[@"Banner"][i];
            if ([bannerDictionary[@"BannerPublishGroupCd"] intValue] == 104) {
                UIButton *bannerButton = [[UIButton alloc] initWithFrame:CGRectMake(bannerIndex * self->_bannerScrollView.frame.size.width, 0, self->_bannerScrollView.frame.size.width, self->_bannerScrollView.frame.size.height)];
                [bannerButton setTag:i];
                [bannerButton addTarget:self action:@selector(touchBanner:) forControlEvents:UIControlEventTouchUpInside];
                [self->_bannerScrollView addSubview:bannerButton];
                bannerIndex++;
                
                UIActivityIndicatorView *loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                [loadingIndicatorView setCenter:CGPointMake(bannerButton.frame.size.width / 2, bannerButton.frame.size.height / 2)];
                [bannerButton addSubview:loadingIndicatorView];
                [loadingIndicatorView startAnimating];
                
                [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[bannerDictionary[@"BannerPath"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                             options:0
                                                            progress:nil
                                                           completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                               if (!error) {
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [loadingIndicatorView stopAnimating];
                                                                       [loadingIndicatorView removeFromSuperview];
                                                                       [bannerButton setImage:image forState:UIControlStateNormal];
                                                                   });
                                                               } else {
                                                               }
                                                           }];

            } else if ([bannerDictionary[@"BannerPublishGroupCd"] intValue] == 105) {
                [self->_healthBannerArray addObject:@{
                                                      @"idx" : [NSString stringWithFormat:@"%d", i],
                                                      @"banner" : bannerDictionary
                                                      }];
            } else if ([bannerDictionary[@"BannerPublishGroupCd"] intValue] == 106) {
                
                if ([bannerDictionary[@"DisplayOrder"] intValue] % 2 == 1) {
                    // left
                    UIButton *eventButton = [[UIButton alloc] initWithFrame:CGRectMake(self->_leftEventScrollView.frame.size.width * leftEventIndex, 0, self->_leftEventScrollView.frame.size.width, self->_leftEventScrollView.frame.size.height)];
                    [eventButton setBackgroundColor:[UIColor clearColor]];
                    [eventButton setTag:i];
                    [eventButton addTarget:self action:@selector(touchBanner:) forControlEvents:UIControlEventTouchUpInside];
                    [self->_leftEventScrollView addSubview:eventButton];
                    
                    UIImageView *eventImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self->_leftEventScrollView.frame.size.width, self->_leftEventScrollView.frame.size.height)];
                    [eventImageView.layer setCornerRadius:CGWidthFromIP6P(9 / 3)];
                    [eventImageView setClipsToBounds:YES];
                    [eventImageView setBackgroundColor:[UIColor grayColor]];
                    [eventButton addSubview:eventImageView];

                    UIActivityIndicatorView *loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                    [loadingIndicatorView setCenter:CGPointMake(eventImageView.frame.size.width / 2, eventImageView.frame.size.height / 2)];
                    [eventImageView addSubview:loadingIndicatorView];
                    [loadingIndicatorView startAnimating];

                    [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[bannerDictionary[@"BannerPath"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                                 options:0
                                                                progress:nil
                                                               completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                                   if (!error) {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [loadingIndicatorView stopAnimating];
                                                                           [loadingIndicatorView removeFromSuperview];
                                                                           [eventImageView setImage:image];
                                                                           [eventImageView setBackgroundColor:[UIColor clearColor]];
                                                                       });
                                                                   } else {
                                                                   }
                                                               }];
                    
                    leftEventIndex++;
                } else {
                    UIButton *eventButton = [[UIButton alloc] initWithFrame:CGRectMake(self->_rightEventScrollView.frame.size.width * rightEventIndex, 0, self->_rightEventScrollView.frame.size.width, self->_rightEventScrollView.frame.size.height)];
                    [eventButton setBackgroundColor:[UIColor clearColor]];
                    [eventButton setTag:i];
                    [eventButton addTarget:self action:@selector(touchBanner:) forControlEvents:UIControlEventTouchUpInside];
                    [self->_rightEventScrollView addSubview:eventButton];
                    
                    UIImageView *eventImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self->_rightEventScrollView.frame.size.width, self->_rightEventScrollView.frame.size.height)];
                    [eventImageView.layer setCornerRadius:CGWidthFromIP6P(9 / 3)];
                    [eventImageView setClipsToBounds:YES];
                    [eventImageView setBackgroundColor:[UIColor grayColor]];
                    [eventButton addSubview:eventImageView];

                    UIActivityIndicatorView *loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                    [loadingIndicatorView setCenter:CGPointMake(eventImageView.frame.size.width / 2, eventImageView.frame.size.height / 2)];
                    [eventImageView addSubview:loadingIndicatorView];
                    [loadingIndicatorView startAnimating];

                    [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[bannerDictionary[@"BannerPath"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                                 options:0
                                                                progress:nil
                                                               completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                                   if (!error) {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [loadingIndicatorView stopAnimating];
                                                                           [loadingIndicatorView removeFromSuperview];
                                                                           [eventImageView setImage:image];
                                                                           [eventImageView setBackgroundColor:[UIColor clearColor]];
                                                                       });
                                                                   } else {
                                                                   }
                                                               }];
                    
                    rightEventIndex++;
                }
            }
        }
        
        [self->_bannerScrollView setContentOffset:CGPointMake(0, 0)];
        [self->_leftEventScrollView setContentOffset:CGPointMake(0, 0)];
        [self->_rightEventScrollView setContentOffset:CGPointMake(0, 0)];

        [self->_bannerScrollView setContentSize:CGSizeMake(self->_bannerScrollView.frame.size.width * bannerIndex, self->_bannerScrollView.frame.size.height)];
        [self->_leftEventScrollView setContentSize:CGSizeMake(self->_leftEventScrollView.frame.size.width * leftEventIndex, self->_leftEventScrollView.frame.size.height)];
        [self->_rightEventScrollView setContentSize:CGSizeMake(self->_rightEventScrollView.frame.size.width * rightEventIndex, self->_rightEventScrollView.frame.size.height)];
        [self->_healthCarousel reloadData];

        [self->_healthPageControl setNumberOfPages:[self->_healthBannerArray count]];
        [self->_healthPageControl setCurrentPage:0];
        
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"dailyreport_guide"] intValue] == 0) {
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"dailyreport_guide"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            self->_guideScrollView = [[UIScrollView alloc] initWithFrame:[[UIApplication sharedApplication] keyWindow].bounds];
            [self->_guideScrollView setContentSize:CGSizeMake(self->_mainScrollView.contentSize.width, self->_mainScrollView.contentSize.height - kSTATUSBAR_HEIGHT)];
            [self->_guideScrollView setDelegate:self];
            [self->_guideScrollView setBounces:NO];
            [[[UIApplication sharedApplication] keyWindow] addSubview:self->_guideScrollView];
            
            UIButton *guideButton = [[UIButton alloc] initWithFrame:CGRectMake(0, -kSTATUSBAR_HEIGHT, self->_guideScrollView.contentSize.width, self->_guideScrollView.contentSize.height)];

            if (IS_35INCH) {
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_4"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_4"] forState:UIControlStateHighlighted];
            } else if (IS_IPHONE5S) {
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_5s"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_5s"] forState:UIControlStateHighlighted];
            } else if (IS_IPHONE8) {
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_8"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_8"] forState:UIControlStateHighlighted];
            } else if (IS_IPHONE8P) {
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_8p"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_8p"] forState:UIControlStateHighlighted];
            } else if (IS_IPHONEX) {
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_x"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"dailyreport_image_guide_x"] forState:UIControlStateHighlighted];
            }

            UIImageView *cancelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cyclecalendar_image_close"]];
            [cancelImageView setFrame:CGRectMake(16, 4 + kSTATUSBAR_HEIGHT, cancelImageView.frame.size.width, cancelImageView.frame.size.height)];
            [guideButton addSubview:cancelImageView];

            [guideButton addTarget:self action:@selector(removeGuideView:) forControlEvents:UIControlEventTouchUpInside];
            [self->_guideScrollView addSubview:guideButton];

            UIView *bottomMarginView = [[UIView alloc] initWithFrame:CGRectMake(0, guideButton.frame.origin.y + guideButton.frame.size.height, kSCREEN_WIDTH, kSTATUSBAR_HEIGHT + kBOTTOM_HEIGHT)];
            [bottomMarginView setBackgroundColor:UIColorFromRGB(0x6d6d6e)];
            [self->_guideScrollView addSubview:bottomMarginView];
            

        }
        
        
        // 수면 파트
        
        if (IS_HIDE_SLEEP_PART) {
            //
        } else {
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyy-MM-dd";
            
            NSArray *sleepArray = [[HistoryManager sharedInstance] getSleepSegmentsForDate:[formatter dateFromString:self->_lastDateString] forUser:[[UserManager sharedInstance] getUser].email];
            if ([sleepArray count]) {
                [self->_sleepView setAlpha:1];
                [self->_noSleepView setAlpha:0];
                
                SleepSummaryComplete *sleepSummary = sleepArray[[sleepArray count] - 1];
                
                int validSleepCount = 0;
                int deepSleepCount = 0;
                
                for (int i = 0; i < [sleepSummary.hypnoTypeGraph count]; i++) {
                    if ([sleepSummary.hypnoTypeGraph[i] intValue] != -1) {
                        validSleepCount++;
                        
                        if ([sleepSummary.hypnoTypeGraph[i] intValue] == 4) {
                            deepSleepCount++;
                        }
                    }
                }
                
                validSleepCount = validSleepCount / 2;
                deepSleepCount = deepSleepCount / 2;
                [self->_sleepTimeValueLabel setText:[NSString stringWithFormat:@"%d시간 %d분", validSleepCount / 60, validSleepCount % 60]];
                [self->_sleepTimeValueLabel sizeToFit];
                
                NSMutableString *deepSleepValueString = [[NSMutableString alloc] init];
                
                if (deepSleepCount >= 60) {
                    [deepSleepValueString appendFormat:@"%d시간", deepSleepCount / 60];
                }
                [deepSleepValueString appendFormat:@" %d분", deepSleepCount % 60];
                
                [self->_deepSleepTimeValueLabel setText:[NSString stringWithFormat:@"%@", [deepSleepValueString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]];
                [self->_deepSleepTimeValueLabel sizeToFit];
                
                attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d ", (int)[sleepSummary getSleepParams].scoreTotalSleepTime]
                                                                                                                                attributes:@{ NSFontAttributeName : BoldWithSize(CGHeightFromIP6P(35)),
                                                                                                                                              NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
                [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" 점"]
                                                                                          attributes:@{ NSFontAttributeName : LightWithSize(CGHeightFromIP6P(16)),
                                                                                                        NSForegroundColorAttributeName : UIColorFromRGB(0x838383) }]];
                [self->_sleepValueLabel setAttributedText:attrString];
                [self->_sleepValueLabel sizeToFit];
                
                
                int startTimestamp = (int)[sleepSummary getSleepParams].timestamp - (60 * 60 * 9);
                int endTimestamp = startTimestamp + validSleepCount * 60;
                
                NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:startTimestamp];
                NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:endTimestamp];
                
                NSDateFormatter *currentDateFormatter = [[NSDateFormatter alloc] init];
                [currentDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                [currentDateFormatter setDateFormat:@"HH:mm a"];
                
                [self->_startTimeLabel setText:[currentDateFormatter stringFromDate:startDate]];
                [self->_startTimeLabel sizeToFit];
                [self->_endTimeLabel setText:[currentDateFormatter stringFromDate:endDate]];
                
                float lastX = 0;
                
                for (UIView *sView in [self->_spectrumView subviews]) {
                    [sView removeFromSuperview];
                }
                
                for (int i = 0; i < [sleepSummary.hypnoTypeGraph count]; i++) {
                    UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(lastX, 0, self->_spectrumView.frame.size.width / (float)[sleepSummary.hypnoTypeGraph count], self->_spectrumView.frame.size.height)];
                    [self->_spectrumView addSubview:colorView];
                    
                    if ([sleepSummary.hypnoTypeGraph[i] intValue] == 0) {
                        [colorView setBackgroundColor:UIColorFromRGB(0xffbfba)];
                    } else if ([sleepSummary.hypnoTypeGraph[i] intValue] == 1) {
                        [colorView setBackgroundColor:UIColorFromRGB(0xfdef67)];
                    } else if ([sleepSummary.hypnoTypeGraph[i] intValue] == 2) {
                        [colorView setBackgroundColor:UIColorFromRGB(0xa4e581)];
                    } else if ([sleepSummary.hypnoTypeGraph[i] intValue] == 3) {
                        [colorView setBackgroundColor:UIColorFromRGB(0xa4e581)];
                    } else if ([sleepSummary.hypnoTypeGraph[i] intValue] == 4) {
                        [colorView setBackgroundColor:UIColorFromRGB(0xdaade9)];
                    } else {
                        [colorView setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
                    }
                    
                    lastX = lastX + colorView.frame.size.width;
                    //
                }
                
                imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5)];
                
                if ([sleepSummary getSleepParams].avgStressCategory == 1) {
                    [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_stress_less"]];
                    attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 낮음"]
                                                                                       attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(20)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x000000) }]];
                } else if ([sleepSummary getSleepParams].avgStressCategory == 2) {
                    [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_stress_normal"]];
                    attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 보통"]
                                                                                       attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(20)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x000000) }]];
                } else if ([sleepSummary getSleepParams].avgStressCategory == 3) {
                    [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_stress_high"]];
                    attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 높음"]
                                                                                       attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(20)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x000000) }]];
                } else {
                    attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"알 수 없음"]
                                                                                       attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(20)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x000000) }]];
                }
                
                [self->_sleepStressValueLabel setAttributedText:attrString];
                
                
                int hrCount = 0;
                float hrTotal = 0;
                
                for (int i = 0; i < [sleepSummary.hrGraph count]; i++) {
                    if ([sleepSummary.hrGraph[i] floatValue] > 0) {
                        hrCount++;
                        hrTotal += [sleepSummary.hrGraph[i] floatValue];
                    }
                }
                
                int rrCount = 0;
                float rrTotal = 0;
                
                for (int i = 0; i < [sleepSummary.rrGraph count]; i++) {
                    if ([sleepSummary.rrGraph[i] floatValue] > 0) {
                        rrCount++;
                        rrTotal += [sleepSummary.rrGraph[i] floatValue];
                    }
                }
                
                imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5)];
                [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_rr"]];
                attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d", (int)(rrTotal / rrCount)]
                                                                                   attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(20)),
                                                                                                 NSForegroundColorAttributeName : UIColorFromRGB(0x000000) }]];
                [self->_rrValueLabel setAttributedText:attrString];
                
                imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5)];
                [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_hr"]];
                attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d", (int)(hrTotal / hrCount)]
                                                                                   attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(20)),
                                                                                                 NSForegroundColorAttributeName : UIColorFromRGB(0x000000) }]];
                [self->_hrValueLabel setAttributedText:attrString];
                
                
            } else {
                [self->_sleepView setAlpha:0];
                [self->_noSleepView setAlpha:1];
                // 수면 정보 없음
            }

        }
        

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (void)presentDailyReportGuide
{
    DailyReportGuideViewController *dailyReportGuideViewController = [[DailyReportGuideViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *dailyReportGuideNavigationController = [[UINavigationController alloc] initWithRootViewController:dailyReportGuideViewController];
    [dailyReportGuideNavigationController.navigationBar setTranslucent:NO];
    [dailyReportGuideNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                             forBarPosition:UIBarPositionAny
                                                                 barMetrics:UIBarMetricsDefault];
    [dailyReportGuideNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [dailyReportGuideNavigationController.navigationBar setTitleTextAttributes:@{
                                                                                  NSFontAttributeName: SemiBoldWithSize(18),
                                                                                  NSForegroundColorAttributeName: UIColorFromRGB(0xf05971)
                                                                                  }];
    [dailyReportGuideNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffeff1)] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController presentViewController:dailyReportGuideNavigationController animated:YES completion:^{
        //
    }];
}

- (void)becomePicker
{
    if (![_blackMaskView superview]) {
        [self.navigationController.view addSubview:_blackMaskView];
        [self.navigationController.view addSubview:_dateSelectView];
    }
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self->_blackMaskView setAlpha:0.7];
        [self->_dateSelectView setFrame:CGRectMake(0, kSCREEN_HEIGHT - 215 - 44, kSCREEN_WIDTH, 215 + 44)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)resignPicker
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                   fromDate:[NSDate date]];

    _dateString = [NSString stringWithFormat:@"%04ld-%02ld", [components year] - 10 + [_datePickerView selectedRowInComponent:0], [_datePickerView selectedRowInComponent:1] + 1];
    [_dateButton setTitle:[NSString stringWithFormat:@"%@년 %d월", [_dateString substringWithRange:NSMakeRange(0, 4)], [[_dateString substringWithRange:NSMakeRange(5, 2)] intValue]] forState:UIControlStateNormal];
    [_dayCarousel setCurrentItemIndex:0];
    [_dayCarousel reloadData];
    [self getDailyReport];

    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self->_blackMaskView setAlpha:0];
        [self->_dateSelectView setFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 215 + 44)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        _lastDateString = @"";
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM"];
        _dateString = [dateFormatter stringFromDate:[NSDate date]];

        NSMutableAttributedString *attrString;
        CNSTextAttachment *imageAttachment;
        
        UIButton *titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleButton setBackgroundColor:[UIColor clearColor]];
        [titleButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleButton addTarget:self action:@selector(presentDailyReportGuide) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setTitleView:titleButton];
        
        if (@available(iOS 9.0, *)) {
            [titleButton setTitle:@"일간 리포트 " forState:UIControlStateNormal];
            [titleButton.titleLabel setFont:SemiBoldWithSize(19)];
            [titleButton setTitleColor:UIColorFromRGB(0x3c3c3e) forState:UIControlStateNormal];
            [titleButton setImage:[UIImage imageNamed:@"cycledayinfo_button_qmark"] forState:UIControlStateNormal];
            [titleButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
            [titleButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 3.5 / 2, 0)];
        } else {
            attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"일간 리포트 "]
                                                                attributes:@{ NSFontAttributeName : SemiBoldWithSize(19),
                                                                              NSForegroundColorAttributeName : [UIColor whiteColor] }];
            
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -3.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_button_qmark"]];
            [attrString appendAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            
            [titleButton setAttributedTitle:attrString forState:UIControlStateNormal];
        }
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu_black"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu_black"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
        
        UIButton *dummyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cycledayinfo_button_calendar"].size.width, [UIImage imageNamed:@"cycledayinfo_button_calendar"].size.height)];
        [dummyButton setImage:[UIImage imageNamed:@"cycledayinfo_button_calendar"] forState:UIControlStateNormal];
        [dummyButton addTarget:self action:@selector(changeToMainViewController) forControlEvents:UIControlEventTouchUpInside];
        [dummyButton setAlpha:0];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:dummyButton]];
        

        _mainScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        [_mainScrollView setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView setContentSize:CGSizeMake(kSCREEN_WIDTH, 1600)];
        [self.view addSubview:_mainScrollView];
        

        _dateButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(284 / 3), CGHeightFromIP6P(15), CGWidthFromIP6P(674 / 3), CGHeightFromIP6P(148 / 3))];
        [_dateButton setBackgroundColor:[UIColor whiteColor]];
        [_dateButton.layer setCornerRadius:_dateButton.frame.size.height / 2];
        [_dateButton setClipsToBounds:YES];
        [_dateButton setTitleColor:UIColorFromRGB(0xf16774) forState:UIControlStateNormal];
        [_dateButton.titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(19))];
        [_dateButton setTitle:[NSString stringWithFormat:@"%@년 %d월", [_dateString substringWithRange:NSMakeRange(0, 4)], [[_dateString substringWithRange:NSMakeRange(5, 2)] intValue]] forState:UIControlStateNormal];
        [_dateButton setTitleEdgeInsets:UIEdgeInsetsMake(CGHeightFromIP6P(2), 0, 0, 0)];
        [_dateButton addTarget:self action:@selector(becomePicker) forControlEvents:UIControlEventTouchUpInside];
        [_mainScrollView addSubview:_dateButton];
        
        UIImageView *optionImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dailyreport_image_option"]];
        [optionImageView setCenter:CGPointMake(_dateButton.frame.size.width - CGWidthFromIP6P(23), _dateButton.frame.size.height / 2)];
        [_dateButton addSubview:optionImageView];
        
        
        _dayCarousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, _dateButton.frame.origin.y + _dateButton.frame.size.height + CGHeightFromIP6P(20), kSCREEN_WIDTH, CGHeightFromIP6P(70))];
        [_dayCarousel setType:iCarouselTypeLinear];
        [_dayCarousel setBackgroundColor:[UIColor clearColor]];
        [_dayCarousel setBounces:YES];
        [_dayCarousel setDelegate:self];
        [_dayCarousel setDataSource:self];
        [_mainScrollView addSubview:_dayCarousel];
        
        [dateFormatter setDateFormat:@"d"];
        [_dayCarousel setCurrentItemIndex:[[dateFormatter stringFromDate:[NSDate date]] intValue] - 1];
        
        
        UIView *whiteBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [whiteBGView setBackgroundColor:[UIColor whiteColor]];
        [_mainScrollView addSubview:whiteBGView];

        
        UIImageView *cardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(1174 / 3), CGHeightFromIP6P(363 / 3))];
        [cardImageView setImage:[UIImage imageNamed:@"dailyreport_image_status_bg"]];
        [cardImageView setCenter:CGPointMake(self.view.frame.size.width / 2, _dayCarousel.frame.origin.y + _dayCarousel.frame.size.height + cardImageView.frame.size.height / 2 + CGHeightFromIP6P(20))];
        [_mainScrollView addSubview:cardImageView];
        [whiteBGView setFrame:CGRectMake(0, cardImageView.center.y, kSCREEN_WIDTH, _mainScrollView.contentSize.height - cardImageView.center.y)];
        
        _weekdayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(18), cardImageView.frame.size.width, CGHeightFromIP6P(12))];
        [_weekdayLabel setFont:MediumWithSize(CGHeightFromIP6P(12))];
        [_weekdayLabel setTextAlignment:NSTextAlignmentCenter];
        [_weekdayLabel setBackgroundColor:[UIColor clearColor]];
        [_weekdayLabel setTextColor:UIColorFromRGB(0xf86a8b)];
        [_weekdayLabel setText:@""];
        [cardImageView addSubview:_weekdayLabel];
        
        _statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(44), cardImageView.frame.size.width, CGHeightFromIP6P(44))];
        [_statusLabel setTextAlignment:NSTextAlignmentCenter];
        [_statusLabel setBackgroundColor:[UIColor clearColor]];

        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5.5)];

        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" "]
                                                                                  attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(40)),
                                                                                                NSForegroundColorAttributeName : UIColorFromRGB(0xf0606e) }]];
        [_statusLabel setAttributedText:attrString];
        [cardImageView addSubview:_statusLabel];
 
        _descLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(90), cardImageView.frame.size.width, CGHeightFromIP6P(13))];
        [_descLabel setFont:RegularWithSize(CGHeightFromIP6P(12))];
        [_descLabel setTextAlignment:NSTextAlignmentCenter];
        [_descLabel setBackgroundColor:[UIColor clearColor]];
        [_descLabel setTextColor:UIColorFromRGB(0x8f7b7d)];
        [cardImageView addSubview:_descLabel];
        
        UIImageView *spectrumImageView;
        ////////////
        
        if (IS_HIDE_SLEEP_PART) {
            
            UIView *weightShadowView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(48 / 3), cardImageView.frame.origin.y + cardImageView.frame.size.height + CGHeightFromIP6P(6), CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(147 / 3))];
            [weightShadowView.layer setShadowPath:[UIBezierPath bezierPathWithRect:weightShadowView.bounds].CGPath];
            [weightShadowView.layer setShadowColor:[UIColor blackColor].CGColor];
            [weightShadowView.layer setShadowOffset:CGSizeMake(0, 0)];
            [weightShadowView.layer setShadowRadius:3];
            [weightShadowView.layer setShadowOpacity:0.15];
            [_mainScrollView addSubview:weightShadowView];
            
            UIView *weightBGView = [[UIView alloc] initWithFrame:weightShadowView.frame];
            [weightBGView.layer setCornerRadius:CGWidthFromIP6P(18 / 3)];
            [weightBGView setClipsToBounds:YES];
            [weightBGView setBackgroundColor:[UIColor whiteColor]];
            [_mainScrollView addSubview:weightBGView];
            
            UILabel *changeWeightLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(15), 2, CGWidthFromIP6P(130), weightBGView.frame.size.height)];
            [changeWeightLabel setBackgroundColor:[UIColor clearColor]];
            
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -2.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_icon_weight"]];
            
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  체중 변화"]
                                                                                      attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x5c5c5c) }]];
            [changeWeightLabel setAttributedText:attrString];
            [weightBGView addSubview:changeWeightLabel];
            
            
            _weightValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(130), 2, CGWidthFromIP6P(240), weightBGView.frame.size.height)];
            [_weightValueLabel setTextAlignment:NSTextAlignmentRight];
            [_weightValueLabel setBackgroundColor:[UIColor clearColor]];
            
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"- "]
                                                                                                                            attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(27)),
                                                                                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" kg"]
                                                                                      attributes:@{ NSFontAttributeName : LightWithSize(CGHeightFromIP6P(14)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x838383) }]];
            [_weightValueLabel setAttributedText:attrString];
            
            [weightBGView addSubview:_weightValueLabel];
            
            
            
            spectrumImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(1174 / 3), CGHeightFromIP6P(639 / 3))];
            [spectrumImageView setImage:[UIImage imageNamed:@"dailyreport_image_spectrum"]];
            [spectrumImageView setCenter:CGPointMake(self.view.frame.size.width / 2, weightBGView.frame.origin.y + weightBGView.frame.size.height + spectrumImageView.frame.size.height / 2 + CGHeightFromIP6P(25))];
            [_mainScrollView addSubview:spectrumImageView];

        } else {
            
            _sleepView = [[UIView alloc] initWithFrame:CGRectMake(0, cardImageView.frame.origin.y + cardImageView.frame.size.height + CGHeightFromIP6P(26), kSCREEN_WIDTH, CGHeightFromIP6P(410))];
            [_sleepView setBackgroundColor:[UIColor whiteColor]];
            [_sleepView setAlpha:0];
            [_mainScrollView addSubview:_sleepView];
            
            UIImageView *sleepIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(88) / 3, 0, CGWidthFromIP6P(53) / 3, CGHeightFromIP6P(53) / 3)];
            [sleepIconImageView setImage:[UIImage imageNamed:@"dailyreport_image_sleep"]];
            [_sleepView addSubview:sleepIconImageView];
            
            UILabel *sleepTimeGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(sleepIconImageView.frame.origin.x + sleepIconImageView.frame.size.width + CGWidthFromIP6P(4), sleepIconImageView.frame.origin.y, 100, 100)];
            [sleepTimeGuideLabel setFont:LightWithSize(sleepIconImageView.frame.size.height)];
            [sleepTimeGuideLabel setText:@"수면 시간"];
            [sleepTimeGuideLabel setTextColor:UIColorFromRGB(0xb65b8a)];
            [sleepTimeGuideLabel sizeToFit];
            [_sleepView addSubview:sleepTimeGuideLabel];
            
            _sleepTimeValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(sleepIconImageView.frame.origin.x, sleepTimeGuideLabel.frame.origin.y + sleepTimeGuideLabel.frame.size.height + CGHeightFromIP6P(13), 100, 100)];
            [_sleepTimeValueLabel setFont:BoldWithSize(CGHeightFromIP6P(31))];
            [_sleepTimeValueLabel setText:@"-"];
            [_sleepTimeValueLabel setTextColor:UIColorFromRGB(0xb65b8a)];
            [_sleepTimeValueLabel sizeToFit];
            [_sleepView addSubview:_sleepTimeValueLabel];
            
            UILabel *deepSleepTimeGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(_sleepTimeValueLabel.frame.origin.x, _sleepTimeValueLabel.frame.origin.y + _sleepTimeValueLabel.frame.size.height, 100, 100)];
            [deepSleepTimeGuideLabel setFont:MediumWithSize(CGHeightFromIP6P(15))];
            [deepSleepTimeGuideLabel setText:@"깊은 수면"];
            [deepSleepTimeGuideLabel setTextColor:UIColorFromRGB(0xb4b4b4)];
            [deepSleepTimeGuideLabel sizeToFit];
            [_sleepView addSubview:deepSleepTimeGuideLabel];
            
            _deepSleepTimeValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(deepSleepTimeGuideLabel.frame.origin.x + deepSleepTimeGuideLabel.frame.size.width + CGWidthFromIP6P(4), deepSleepTimeGuideLabel.frame.origin.y, 100, 100)];
            [_deepSleepTimeValueLabel setFont:MediumWithSize(CGHeightFromIP6P(15))];
            [_deepSleepTimeValueLabel setText:@"-"];
            [_deepSleepTimeValueLabel setTextColor:UIColorFromRGB(0x525252)];
            [_deepSleepTimeValueLabel sizeToFit];
            [_sleepView addSubview:_deepSleepTimeValueLabel];
            
            
            UILabel *sleepScoreGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH / 2, sleepTimeGuideLabel.frame.origin.y, 100, 100)];
            [sleepScoreGuideLabel setFont:LightWithSize(sleepIconImageView.frame.size.height)];
            [sleepScoreGuideLabel setText:@"수면 점수"];
            [sleepScoreGuideLabel setTextColor:UIColorFromRGB(0xb4b4b4)];
            [sleepScoreGuideLabel sizeToFit];
            [_sleepView addSubview:sleepScoreGuideLabel];
            
            _sleepValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(sleepScoreGuideLabel.frame.origin.x, sleepScoreGuideLabel.frame.origin.y + sleepScoreGuideLabel.frame.size.height + CGHeightFromIP6P(13), 100, 100)];
            [_sleepValueLabel setBackgroundColor:[UIColor clearColor]];
            
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"- "]
                                                                                                                            attributes:@{ NSFontAttributeName : BoldWithSize(CGHeightFromIP6P(30)),
                                                                                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" 점"]
                                                                                      attributes:@{ NSFontAttributeName : LightWithSize(CGHeightFromIP6P(16)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x838383) }]];
            [_sleepValueLabel setAttributedText:attrString];
            [_sleepValueLabel sizeToFit];
            [_sleepView addSubview:_sleepValueLabel];
            
            
            UILabel *changeWeightGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(905) / 3, sleepTimeGuideLabel.frame.origin.y, 100, 100)];
            [changeWeightGuideLabel setFont:LightWithSize(sleepIconImageView.frame.size.height)];
            [changeWeightGuideLabel setText:@"체중 변화"];
            [changeWeightGuideLabel setTextColor:UIColorFromRGB(0xb4b4b4)];
            [changeWeightGuideLabel sizeToFit];
            [_sleepView addSubview:changeWeightGuideLabel];
            
            _weightValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(changeWeightGuideLabel.frame.origin.x, changeWeightGuideLabel.frame.origin.y + changeWeightGuideLabel.frame.size.height + CGHeightFromIP6P(13), 100, 100)];
            [_weightValueLabel setBackgroundColor:[UIColor clearColor]];
            
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"- "]
                                                                                                                            attributes:@{ NSFontAttributeName : BoldWithSize(CGHeightFromIP6P(30)),
                                                                                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" kg"]
                                                                                      attributes:@{ NSFontAttributeName : LightWithSize(CGHeightFromIP6P(16)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x838383) }]];
            [_weightValueLabel setAttributedText:attrString];
            [_weightValueLabel sizeToFit];
            [_sleepView addSubview:_weightValueLabel];
            
            
            UIView *spectrumShadowView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(48 / 3), deepSleepTimeGuideLabel.frame.origin.y + deepSleepTimeGuideLabel.frame.size.height + CGHeightFromIP6P(30), CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(600 / 3))];
            [spectrumShadowView.layer setShadowPath:[UIBezierPath bezierPathWithRect:spectrumShadowView.bounds].CGPath];
            [spectrumShadowView.layer setShadowColor:[UIColor blackColor].CGColor];
            [spectrumShadowView.layer setShadowOffset:CGSizeMake(0, 0)];
            [spectrumShadowView.layer setShadowRadius:3];
            [spectrumShadowView.layer setShadowOpacity:0.15];
            [_sleepView addSubview:spectrumShadowView];
            
            UIView *spectrumBGView = [[UIView alloc] initWithFrame:spectrumShadowView.frame];
            [spectrumBGView.layer setCornerRadius:CGWidthFromIP6P(18 / 3)];
            [spectrumBGView setClipsToBounds:YES];
            [spectrumBGView setBackgroundColor:[UIColor whiteColor]];
            [_sleepView addSubview:spectrumBGView];
            
            
            
            UILabel *spectrumLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), CGHeightFromIP6P(72 / 3), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [spectrumLabel setFont:MediumWithSize(16)];
            [spectrumLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
            [spectrumLabel setText:@"수면 깊이 스펙트럼"];
            [spectrumLabel sizeToFit];
            [spectrumBGView addSubview:spectrumLabel];
            
            _startTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(spectrumLabel.frame.origin.x, spectrumLabel.frame.origin.y + spectrumLabel.frame.size.height + CGHeightFromIP6P(26), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [_startTimeLabel setFont:MediumWithSize(13)];
            [_startTimeLabel setTextColor:UIColorFromRGB(0x3b3b3b)];
            [_startTimeLabel setText:@"-"];
            [_startTimeLabel sizeToFit];
            [spectrumBGView addSubview:_startTimeLabel];
            
            
            _endTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_startTimeLabel.frame.origin.x, _startTimeLabel.frame.origin.y, CGWidthFromIP6P(348), _startTimeLabel.frame.size.height)];
            [_endTimeLabel setTextAlignment:NSTextAlignmentRight];
            [_endTimeLabel setFont:MediumWithSize(13)];
            [_endTimeLabel setTextColor:UIColorFromRGB(0x3b3b3b)];
            [_endTimeLabel setText:@"-"];
            [spectrumBGView addSubview:_endTimeLabel];
            
            _spectrumView = [[UIView alloc] initWithFrame:CGRectMake(_startTimeLabel.frame.origin.x, _startTimeLabel.frame.origin.y + _startTimeLabel.frame.size.height + CGHeightFromIP6P(9), CGWidthFromIP6P(348), CGHeightFromIP6P(32))];
            [_spectrumView setBackgroundColor:[UIColor lightGrayColor]];
            [spectrumBGView addSubview:_spectrumView];
            
            UIView *dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), _spectrumView.frame.origin.y + _spectrumView.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
            [dotView setBackgroundColor:UIColorFromRGB(0xffbfba)];
            [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
            [dotView setClipsToBounds:YES];
            [spectrumBGView addSubview:dotView];
            
            UILabel *sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
            [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
            [sleepStatusLabel setTextColor:UIColorFromRGB(0x010101)];
            [sleepStatusLabel setText:@"깨어남"];
            [sleepStatusLabel sizeToFit];
            [spectrumBGView addSubview:sleepStatusLabel];
            
            
            dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(344 / 3), _spectrumView.frame.origin.y + _spectrumView.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
            [dotView setBackgroundColor:UIColorFromRGB(0xfdef67)];
            [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
            [dotView setClipsToBounds:YES];
            [spectrumBGView addSubview:dotView];
            
            sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
            [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
            [sleepStatusLabel setTextColor:UIColorFromRGB(0x010101)];
            [sleepStatusLabel setText:@"뒤척임"];
            [sleepStatusLabel sizeToFit];
            [spectrumBGView addSubview:sleepStatusLabel];
            
            
            dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(608 / 3), _spectrumView.frame.origin.y + _spectrumView.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
            [dotView setBackgroundColor:UIColorFromRGB(0xa4e581)];
            [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
            [dotView setClipsToBounds:YES];
            [spectrumBGView addSubview:dotView];
            
            sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
            [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
            [sleepStatusLabel setTextColor:UIColorFromRGB(0x010101)];
            [sleepStatusLabel setText:@"얕은잠"];
            [sleepStatusLabel sizeToFit];
            [spectrumBGView addSubview:sleepStatusLabel];
            
            
            dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(872 / 3), _spectrumView.frame.origin.y + _spectrumView.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
            [dotView setBackgroundColor:UIColorFromRGB(0xdaade9)];
            [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
            [dotView setClipsToBounds:YES];
            [spectrumBGView addSubview:dotView];
            
            sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
            [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
            [sleepStatusLabel setTextColor:UIColorFromRGB(0x010101)];
            [sleepStatusLabel setText:@"깊은잠"];
            [sleepStatusLabel sizeToFit];
            [spectrumBGView addSubview:sleepStatusLabel];
            
            
            UILabel *sleepStressGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(108) / 3, spectrumBGView.frame.origin.y + spectrumBGView.frame.size.height + CGHeightFromIP6P(30), 0, 0)];
            [sleepStressGuideLabel setFont:MediumWithSize(CGHeightFromIP6P(17))];
            [sleepStressGuideLabel setText:@"수면 스트레스"];
            [sleepStressGuideLabel setTextColor:UIColorFromRGB(0x747474)];
            [sleepStressGuideLabel sizeToFit];
            [_sleepView addSubview:sleepStressGuideLabel];
            
            _sleepStressValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(sleepStressGuideLabel.frame.origin.x, sleepStressGuideLabel.frame.origin.y + sleepStressGuideLabel.frame.size.height + CGHeightFromIP6P(14), sleepStressGuideLabel.frame.size.width, CGHeightFromIP6P(25))];
            [_sleepStressValueLabel setTextAlignment:NSTextAlignmentCenter];
            [_sleepView addSubview:_sleepStressValueLabel];
            
            UILabel *rrGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(490) / 3, spectrumBGView.frame.origin.y + spectrumBGView.frame.size.height + CGHeightFromIP6P(30), 0, 0)];
            [rrGuideLabel setFont:MediumWithSize(CGHeightFromIP6P(17))];
            [rrGuideLabel setText:@"평균 호흡주기"];
            [rrGuideLabel setTextColor:UIColorFromRGB(0x747474)];
            [rrGuideLabel sizeToFit];
            [_sleepView addSubview:rrGuideLabel];
            
            _rrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(rrGuideLabel.frame.origin.x, rrGuideLabel.frame.origin.y + rrGuideLabel.frame.size.height + CGHeightFromIP6P(14), rrGuideLabel.frame.size.width, CGHeightFromIP6P(25))];
            [_rrValueLabel setTextAlignment:NSTextAlignmentCenter];
            [_sleepView addSubview:_rrValueLabel];
            
            UILabel *hrGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(889) / 3, spectrumBGView.frame.origin.y + spectrumBGView.frame.size.height + CGHeightFromIP6P(30), 0, 0)];
            [hrGuideLabel setFont:MediumWithSize(CGHeightFromIP6P(17))];
            [hrGuideLabel setText:@"평균 심박수"];
            [hrGuideLabel setTextColor:UIColorFromRGB(0x747474)];
            [hrGuideLabel sizeToFit];
            [_sleepView addSubview:hrGuideLabel];
            
            _hrValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(hrGuideLabel.frame.origin.x, hrGuideLabel.frame.origin.y + hrGuideLabel.frame.size.height + CGHeightFromIP6P(14), hrGuideLabel.frame.size.width, CGHeightFromIP6P(25))];
            [_hrValueLabel setTextAlignment:NSTextAlignmentCenter];
            [_sleepView addSubview:_hrValueLabel];
            
            
            UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(428) / 3, sleepStressGuideLabel.frame.origin.y, k1PX, CGHeightFromIP6P(54))];
            [separatorView setBackgroundColor:UIColorFromRGB(0xe0e0e0)];
            [_sleepView addSubview:separatorView];
            
            separatorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(808) / 3, sleepStressGuideLabel.frame.origin.y, k1PX, CGHeightFromIP6P(54))];
            [separatorView setBackgroundColor:UIColorFromRGB(0xe0e0e0)];
            [_sleepView addSubview:separatorView];
            
            
            ////////////
            
            _noSleepView = [[UIView alloc] initWithFrame:CGRectMake(0, cardImageView.frame.origin.y + cardImageView.frame.size.height + CGHeightFromIP6P(26), kSCREEN_WIDTH, CGHeightFromIP6P(410))];
            [_noSleepView setBackgroundColor:[UIColor whiteColor]];
            [_mainScrollView addSubview:_noSleepView];
            
            
            UIView *weightShadowView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(48 / 3), 0, CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(147 / 3))];
            [weightShadowView.layer setShadowPath:[UIBezierPath bezierPathWithRect:weightShadowView.bounds].CGPath];
            [weightShadowView.layer setShadowColor:[UIColor blackColor].CGColor];
            [weightShadowView.layer setShadowOffset:CGSizeMake(0, 0)];
            [weightShadowView.layer setShadowRadius:3];
            [weightShadowView.layer setShadowOpacity:0.15];
            [_noSleepView addSubview:weightShadowView];
            
            UIView *weightBGView = [[UIView alloc] initWithFrame:weightShadowView.frame];
            [weightBGView.layer setCornerRadius:CGWidthFromIP6P(18 / 3)];
            [weightBGView setClipsToBounds:YES];
            [weightBGView setBackgroundColor:[UIColor whiteColor]];
            [_noSleepView addSubview:weightBGView];
            
            UILabel *changeWeightLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(15), 2, CGWidthFromIP6P(130), weightBGView.frame.size.height)];
            [changeWeightLabel setBackgroundColor:[UIColor clearColor]];
            
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -2.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"dailyreport_image_icon_weight"]];
            
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  체중 변화"]
                                                                                      attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x5c5c5c) }]];
            [changeWeightLabel setAttributedText:attrString];
            [weightBGView addSubview:changeWeightLabel];
            
            
            _weightValue2Label = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(130), 2, CGWidthFromIP6P(240), weightBGView.frame.size.height)];
            [_weightValue2Label setTextAlignment:NSTextAlignmentRight];
            [_weightValue2Label setBackgroundColor:[UIColor clearColor]];
            
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"- "]
                                                                                                                            attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(27)),
                                                                                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            [attrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" kg"]
                                                                                      attributes:@{ NSFontAttributeName : LightWithSize(CGHeightFromIP6P(14)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x838383) }]];
            [_weightValue2Label setAttributedText:attrString];
            
            [weightBGView addSubview:_weightValue2Label];
            
            
            UIImageView *sensorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(364) / 3, CGHeightFromIP6P(364) / 3)];
            [sensorImageView setImage:[UIImage imageNamed:@"connectsensor_image_sensor"]];
            [sensorImageView setCenter:CGPointMake(_noSleepView.frame.size.width / 2, _noSleepView.frame.size.height / 2)];
            [_noSleepView addSubview:sensorImageView];
            
            UILabel *guideLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, sensorImageView.frame.origin.y + sensorImageView.frame.size.height + CGHeightFromIP6P(24), _noSleepView.frame.size.width, 10)];
            [guideLabel setFont:LightWithSize(15)];
            [guideLabel setTextColor:UIColorFromRGB(0x000000)];
            [guideLabel setText:@"센서를 사용하여\n수면 정보를 확인하실 수 있습니다."];
            [guideLabel setNumberOfLines:0];
            [guideLabel sizeToFit];
            [guideLabel setTextAlignment:NSTextAlignmentCenter];
            [guideLabel setFrame:CGRectMake(guideLabel.frame.origin.x, guideLabel.frame.origin.y, _noSleepView.frame.size.width, guideLabel.frame.size.height)];
            [_noSleepView addSubview:guideLabel];

            
        }
        

        
        ////////////

        
        if (IS_HIDE_SLEEP_PART) {
            _bannerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, spectrumImageView.frame.origin.y + spectrumImageView.frame.size.height + CGHeightFromIP6P(30), kSCREEN_WIDTH, (int)CGHeightFromIP6P(260 / 3))];
        } else {
            _bannerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _sleepView.frame.origin.y + _sleepView.frame.size.height + CGHeightFromIP6P(30), kSCREEN_WIDTH, (int)CGHeightFromIP6P(260 / 3))];
        }
        
        [_bannerScrollView setBackgroundColor:[UIColor clearColor]];
        [_bannerScrollView setPagingEnabled:YES];
        [_bannerScrollView setScrollEnabled:NO];
        [_mainScrollView addSubview:_bannerScrollView];
        
        
        UILabel *healthInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(16), _bannerScrollView.frame.origin.y + _bannerScrollView.frame.size.height + CGHeightFromIP6P(30), CGWidthFromIP6P(200), CGHeightFromIP6P(16))];
        [healthInfoLabel setText:@"여성 건강 정보"];
        [healthInfoLabel setFont:MediumWithSize(CGHeightFromIP6P(15))];
        [healthInfoLabel setTextColor:UIColorFromRGB(0x747474)];
        [healthInfoLabel setBackgroundColor:[UIColor clearColor]];
        [healthInfoLabel sizeToFit];
        [_mainScrollView addSubview:healthInfoLabel];
        
//        UILabel *moreInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - CGWidthFromIP6P(16) - CGWidthFromIP6P(100), healthInfoLabel.frame.origin.y + 1, CGWidthFromIP6P(100), healthInfoLabel.frame.size.height)];
//        [moreInfoLabel setText:@"더보기 >"];
//        [moreInfoLabel setTextAlignment:NSTextAlignmentRight];
//        [moreInfoLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
//        [moreInfoLabel setTextColor:UIColorFromRGB(0x747474)];
//        [moreInfoLabel setBackgroundColor:[UIColor clearColor]];
//        [_mainScrollView addSubview:moreInfoLabel];

        
        _healthCarousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, healthInfoLabel.frame.origin.y + healthInfoLabel.frame.size.height + CGHeightFromIP6P(16), kSCREEN_WIDTH, CGHeightFromIP6P(190))];
        [_healthCarousel setType:iCarouselTypeRotary];
        [_healthCarousel setBackgroundColor:[UIColor clearColor]];
        [_healthCarousel setDelegate:self];
        [_healthCarousel setDataSource:self];
        [_healthCarousel setPagingEnabled:YES];
        [_healthCarousel setBounces:NO];
        [_mainScrollView addSubview:_healthCarousel];

        _healthPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, _healthCarousel.frame.origin.y + _healthCarousel.frame.size.height + CGHeightFromIP6P(18), kSCREEN_WIDTH, CGHeightFromIP6P(13))];
        [_healthPageControl setPageIndicatorTintColor:UIColorFromRGB(0xffd8dd)];
        [_healthPageControl setCurrentPageIndicatorTintColor:UIColorFromRGB(0xf27f8e)];
        [_healthPageControl setNumberOfPages:0];
        [_healthPageControl setUserInteractionEnabled:NO];
        [_mainScrollView addSubview:_healthPageControl];
        
        UILabel *eventLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(16), _healthPageControl.frame.origin.y + _healthPageControl.frame.size.height + CGHeightFromIP6P(24), CGWidthFromIP6P(200), CGHeightFromIP6P(16))];
        [eventLabel setText:@"좋은느낌 이벤트"];
        [eventLabel setFont:MediumWithSize(CGHeightFromIP6P(15))];
        [eventLabel setTextColor:UIColorFromRGB(0x747474)];
        [eventLabel setBackgroundColor:[UIColor clearColor]];
        [eventLabel sizeToFit];
        [_mainScrollView addSubview:eventLabel];
        
        _leftEventScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake((int)CGWidthFromIP6P(48 / 3), eventLabel.frame.origin.y + eventLabel.frame.size.height + CGHeightFromIP6P(16), (int)CGWidthFromIP6P(547 / 3), (int)CGHeightFromIP6P(145))];
        [_leftEventScrollView setBackgroundColor:[UIColor clearColor]];
        [_leftEventScrollView setPagingEnabled:YES];
        [_leftEventScrollView setScrollEnabled:NO];
        [_mainScrollView addSubview:_leftEventScrollView];
        
        _rightEventScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake((int)CGWidthFromIP6P(645 / 3), eventLabel.frame.origin.y + eventLabel.frame.size.height + CGHeightFromIP6P(16), (int)CGWidthFromIP6P(547 / 3), (int)CGHeightFromIP6P(145))];
        [_rightEventScrollView setBackgroundColor:[UIColor clearColor]];
        [_rightEventScrollView setPagingEnabled:YES];
        [_rightEventScrollView setScrollEnabled:NO];
        [_mainScrollView addSubview:_rightEventScrollView];
        
        [_mainScrollView setContentSize:CGSizeMake(_mainScrollView.frame.size.width, kTOP_HEIGHT + _leftEventScrollView.frame.origin.y + _leftEventScrollView.frame.size.height + CGHeightFromIP6P(16))];

        [self performSelector:@selector(bannerRollingThread) withObject:nil afterDelay:8.0f];

        // 요기
        
        _blackMaskView = [[UIView alloc] initWithFrame:self.view.bounds];
        [_blackMaskView setBackgroundColor:[UIColor blackColor]];
        [_blackMaskView setAlpha:0];
        
        _dateSelectView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 44 + 215)];
        [_dateSelectView setBackgroundColor:[UIColor clearColor]];
        
        UIView *pickerResignView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(414), 44)];
        [pickerResignView setBackgroundColor:[UIColor clearColor]];
        
        UIView *pickerBlackMaskView = [[UIView alloc] initWithFrame:pickerResignView.bounds];
        [pickerBlackMaskView setBackgroundColor:[UIColor blackColor]];
        [pickerBlackMaskView setAlpha:0.8];
        [pickerResignView addSubview:pickerBlackMaskView];
        
        UIButton *pickerResignButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(414) - CGWidthFromIP6P(40 + 4 + 4), 3, CGWidthFromIP6P(40), 44 - 3 - 3)];
        [pickerResignButton.titleLabel setFont:RegularWithSize(14)];
        [pickerResignButton setTitle:@"완료" forState:UIControlStateNormal];
        [pickerResignButton setBackgroundColor:[UIColor clearColor]];
        [pickerResignButton addTarget:self action:@selector(resignPicker) forControlEvents:UIControlEventTouchUpInside];
        [pickerResignView addSubview:pickerResignButton];
        [_dateSelectView addSubview:pickerResignView];
        
        
        _datePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, kSCREEN_WIDTH, 215)];
        [_datePickerView setBackgroundColor:[UIColor whiteColor]];
        [_datePickerView setShowsSelectionIndicator:YES];
        [_datePickerView setDelegate:self];
        [_datePickerView setDataSource:self];
        [_dateSelectView addSubview:_datePickerView];
        
        UILabel *yearLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [yearLabel setFont:RegularWithSize(21)];
        [yearLabel setText:@"년"];
        [yearLabel setTextColor:UIColorFromRGB(0x333333)];
        [yearLabel sizeToFit];
        [yearLabel setCenter:CGPointMake(_datePickerView.frame.size.width / 2 + CGWidthFromIP6P(5), _datePickerView.frame.size.height / 2 + 1)];
        [_datePickerView addSubview:yearLabel];
        
        UILabel *monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [monthLabel setFont:RegularWithSize(21)];
        [monthLabel setText:@"월"];
        [monthLabel setTextColor:UIColorFromRGB(0x333333)];
        [monthLabel sizeToFit];
        [monthLabel setCenter:CGPointMake(kSCREEN_WIDTH - CGWidthFromIP6P(118), _datePickerView.frame.size.height / 2 + 1)];
        [_datePickerView addSubview:monthLabel];

        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"DailyReport",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"DailyReport",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getDailyReport) name:@"REFRESH_AFTER_LOGIN" object:nil];

    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)removeGuideView:(UIButton *)guideButton
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [guideButton.superview setAlpha:0];
    } completion:^(BOOL finished) {
        [guideButton.superview removeFromSuperview];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == _guideScrollView) {
        [_mainScrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y + kSTATUSBAR_HEIGHT)];
    }
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    if (carousel == _dayCarousel) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM"];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:[dateFormatter dateFromString:_dateString]];
        NSUInteger numberOfDaysInMonth = range.length;
        
        _dayViewArray = [[NSMutableArray alloc] init];
        for (int i = 0; i < numberOfDaysInMonth; i++) {
            [_dayViewArray addObject:@"dummy"];
        }
        
        return numberOfDaysInMonth;
    } else if (carousel == _healthCarousel) {
        if (_healthBannerArray) {
            return [_healthBannerArray count];
        } else {
            return 0;
        }
    }
    
    return 0;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (carousel == _dayCarousel) {

        UIView *rtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (int)CGWidthFromIP6P(44), CGHeightFromIP6P(70))];
        [rtnView setBackgroundColor:[UIColor clearColor]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:[dateFormatter dateFromString:[NSString stringWithFormat:@"%@-%02ld",  _dateString, index + 1]]];
        [components setDay:index];
        
        NSArray *weekdayArray = @[ @"", @"일", @"월", @"화", @"수", @"목", @"금", @"토" ];
        
        UILabel *weekLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (int)CGWidthFromIP6P(44), CGHeightFromIP6P(18))];
        [weekLabel setBackgroundColor:[UIColor clearColor]];
        [weekLabel setText:weekdayArray[[components weekday]]];
        [weekLabel setTextAlignment:NSTextAlignmentCenter];
        [weekLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
        [weekLabel setTextColor:UIColorFromRGB(0x89898b)];
        [rtnView addSubview:weekLabel];
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, (int)CGHeightFromIP6P(22), (int)CGWidthFromIP6P(44), (int)CGHeightFromIP6P(44))];
        [bgView.layer setCornerRadius:bgView.frame.size.width / 2];
        [bgView setClipsToBounds:YES];
        [bgView setBackgroundColor:[UIColor clearColor]];
        [rtnView addSubview:bgView];
        
        UILabel *dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, (int)CGHeightFromIP6P(22) + 1, (int)CGWidthFromIP6P(44), (int)CGHeightFromIP6P(44))];
        [dayLabel setBackgroundColor:[UIColor clearColor]];
        [dayLabel setText:[NSString stringWithFormat:@"%ld", index + 1]];
        [dayLabel setTextAlignment:NSTextAlignmentCenter];
        [dayLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(22))];
        [dayLabel setTextColor:UIColorFromRGB(0x89898b)];
        [rtnView addSubview:dayLabel];
        
        [_dayViewArray setObject:@{ @"week" : weekLabel, @"bg" : bgView, @"day" : dayLabel } atIndexedSubscript:index];
        
        if (index == carousel.currentItemIndex) {
            [weekLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(13))];
            [weekLabel setTextColor:UIColorFromRGB(0xf16774)];
            [dayLabel setFont:BoldWithSize(CGHeightFromIP6P(22))];
            [dayLabel setTextColor:UIColorFromRGB(0xf16774)];
            [bgView setBackgroundColor:UIColorFromRGB(0xffd8dc)];
        }
        return rtnView;
    } else if (carousel == _healthCarousel) {

        UIView *rtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (int)CGWidthFromIP6P(1146 / 3), (int)CGHeightFromIP6P(549 / 3))];
        [rtnView setBackgroundColor:[UIColor clearColor]];
        
        UIButton *bannerButton = [[UIButton alloc] initWithFrame:rtnView.bounds];
        [bannerButton setBackgroundColor:[UIColor grayColor]];
        [bannerButton.layer setCornerRadius:CGWidthFromIP6P(18 / 3)];
        [bannerButton setClipsToBounds:YES];
        [bannerButton setTag:[_healthBannerArray[index][@"idx"] intValue]];
        [bannerButton addTarget:self action:@selector(touchBanner:) forControlEvents:UIControlEventTouchUpInside];
        [rtnView addSubview:bannerButton];

        UIActivityIndicatorView *loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [loadingIndicatorView setCenter:CGPointMake(bannerButton.frame.size.width / 2, bannerButton.frame.size.height / 2)];
        [bannerButton addSubview:loadingIndicatorView];
        [loadingIndicatorView startAnimating];

        [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[_healthBannerArray[index][@"banner"][@"BannerPath"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                     options:0
                                                    progress:nil
                                                   completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                       if (!error) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [loadingIndicatorView stopAnimating];
                                                               [loadingIndicatorView removeFromSuperview];
                                                               [bannerButton setImage:image forState:UIControlStateNormal];
                                                               [bannerButton setBackgroundColor:[UIColor clearColor]];
                                                           });
                                                       } else {
                                                       }
                                                   }];

        
        UIView *shadowView = [[UIView alloc] initWithFrame:rtnView.bounds];
        [shadowView.layer setShadowPath:[UIBezierPath bezierPathWithRect:shadowView.bounds].CGPath];
        [shadowView.layer setShadowColor:[UIColor blackColor].CGColor];
        [shadowView.layer setShadowOffset:CGSizeMake(0, 0)];
        [shadowView.layer setShadowRadius:5];
        [shadowView.layer setShadowOpacity:0.3];
        [shadowView setBackgroundColor:[UIColor clearColor]];
        [shadowView setClipsToBounds:NO];
        [shadowView addSubview:rtnView];

        return shadowView;
    }
    return nil;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (carousel == _dayCarousel) {
        if (option == iCarouselOptionSpacing) {
            return value * 1.45;
        }
    } else if (carousel == _healthCarousel) {
        if (option == iCarouselOptionSpacing) {
            return value * 1.1;
        } else if (option == iCarouselOptionWrap) {
            return NO;
        } else if (option == iCarouselOptionVisibleItems) {
            if (carousel.currentItemIndex == 0 || (carousel.currentItemIndex == carousel.numberOfItems - 1)) {
                return 2;
            } else {
                return 3;
            }
        }
    }
    return value;
}

- (void)carouselDidEndDecelerating:(iCarousel *)carousel
{
    if (carousel == _dayCarousel) {
    }
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    if (carousel == _dayCarousel) {
        for (int i = 0; i < [_dayViewArray count]; i++) {
            if (![_dayViewArray[i] isKindOfClass:[NSDictionary class]]) {
                continue;
            }
            
            UILabel *weekLabel = _dayViewArray[i][@"week"];
            UILabel *dayLabel = _dayViewArray[i][@"day"];
            UIView *bgView = _dayViewArray[i][@"bg"];
            
            if (i == index) {
                [weekLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(13))];
                [weekLabel setTextColor:UIColorFromRGB(0xf16774)];
                [dayLabel setFont:BoldWithSize(CGHeightFromIP6P(22))];
                [dayLabel setTextColor:UIColorFromRGB(0xf16774)];
                
                [bgView setBackgroundColor:UIColorFromRGB(0xffd8dc)];
            } else {
                [weekLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
                [weekLabel setTextColor:UIColorFromRGB(0x89898b)];
                [dayLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(22))];
                [dayLabel setTextColor:UIColorFromRGB(0x89898b)];
                
                [bgView setBackgroundColor:[UIColor clearColor]];
            }
        }
    } else if (carousel == _healthCarousel) {
        [_healthPageControl setCurrentPage:carousel.currentItemIndex];
    }
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
    if (carousel == _dayCarousel) {
        for (int i = 0; i < [_dayViewArray count]; i++) {
            if (![_dayViewArray[i] isKindOfClass:[NSDictionary class]]) {
                continue;
            }
            
            UILabel *weekLabel = _dayViewArray[i][@"week"];
            UILabel *dayLabel = _dayViewArray[i][@"day"];
            UIView *bgView = _dayViewArray[i][@"bg"];
            
            if (i == carousel.currentItemIndex) {
                [weekLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(13))];
                [weekLabel setTextColor:UIColorFromRGB(0xf16774)];
                [dayLabel setFont:BoldWithSize(CGHeightFromIP6P(22))];
                [dayLabel setTextColor:UIColorFromRGB(0xf16774)];
                
                [bgView setBackgroundColor:UIColorFromRGB(0xffd8dc)];
            } else {
                [weekLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
                [weekLabel setTextColor:UIColorFromRGB(0x89898b)];
                [dayLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(22))];
                [dayLabel setTextColor:UIColorFromRGB(0x89898b)];
                
                [bgView setBackgroundColor:[UIColor clearColor]];
            }
        }
        [self getDailyReport];
    } else if (carousel == _healthCarousel) {
        [_healthPageControl setCurrentPage:carousel.currentItemIndex];
    }
}



// UIPickerView Delegate & DataSource
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        if (component == 0) {
            return 100;
        } else if (component == 1) {
            return 80;
        }
    }
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        return 50;
    }
    
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        if (component == 0) {
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                           fromDate:[NSDate date]];
            
            return [NSString stringWithFormat:@"%ld", (long)row + ([components year] - 10)];
        } else if(component == 1) {
            return [NSString stringWithFormat:@"%ld", (long)row + 1];
        }
    }
    return nil;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        if (component == 0) {
            return 21;
        } else if(component == 1) {
            return 12;
        }
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView == _datePickerView) {
        return 2;
    }
    return 0;
}


@end
