//
//  MomQViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "MomQViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface MomQViewController ()

@end

@implementation MomQViewController

- (void)loadStore
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_STORE_GET_INFO]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            self->_storeDictionary = responseData[@"data"];
            [self->_mainTableView reloadData];
        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xefeff4)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"스토어"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x000000)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu_black"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu_black"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
        
        UIButton *cycleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_cycle"].size.width, [UIImage imageNamed:@"common_button_cycle"].size.height)];
        [cycleButton setImage:[UIImage imageNamed:@"common_button_cycle"] forState:UIControlStateNormal];
        [cycleButton addTarget:(RootViewController *)_pClass action:@selector(changeToCycleDayInfoViewController) forControlEvents:UIControlEventTouchUpInside];
        [cycleButton setAlpha:0];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cycleButton]];

        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT) style:UITableViewStylePlain];
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setTableFooterView:[[UIView alloc] init]];
        [_mainTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.view addSubview:_mainTableView];
        
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"Store",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"Store",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];

        [self loadStore];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [MSAnalytics trackEvent:@"Store clicked" withProperties:@{
                                                                   @"GoodsNo" : [NSString stringWithFormat:@"%d", [_storeDictionary[@"PlanInfo"][indexPath.row][@"GoodsNo"] intValue]],
                                                                   @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                   @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                             [NSString stringWithFormat:@"%d", [_storeDictionary[@"PlanInfo"][indexPath.row][@"GoodsNo"] intValue]],
                                                                             [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                             ]
                                                                   }];

        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"ykmall://redirect?url=%@", _storeDictionary[@"PlanInfo"][indexPath.row][@"TarGetUrl"]]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"ykmall://redirect?url=%@", _storeDictionary[@"PlanInfo"][indexPath.row][@"TarGetUrl"]]]];
        } else {
            WebViewNavigationController *webViewNavigationController = [[WebViewNavigationController alloc] initWithURL:[_storeDictionary[@"PlanInfo"][indexPath.row][@"TarGetUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] title:@""];
            [self.navigationController presentViewController:webViewNavigationController animated:YES completion:^{
                //
            }];
        }
        
    } else if (indexPath.section == 1) {
        [MSAnalytics trackEvent:@"Store clicked" withProperties:@{
                                                                  @"GoodsNo" : [NSString stringWithFormat:@"%d", [_storeDictionary[@"StoreInfo"][indexPath.row][@"GoodsNo"] intValue]],
                                                                  @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                  @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                            [NSString stringWithFormat:@"%d", [_storeDictionary[@"StoreInfo"][indexPath.row][@"GoodsNo"] intValue]],
                                                                            [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                            ]
                                                                  }];

        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"ykmall://redirect?url=%@", _storeDictionary[@"StoreInfo"][indexPath.row][@"TarGetUrl"]]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"ykmall://redirect?url=%@", _storeDictionary[@"StoreInfo"][indexPath.row][@"TarGetUrl"]]]];
        } else {
            WebViewNavigationController *webViewNavigationController = [[WebViewNavigationController alloc] initWithURL:[_storeDictionary[@"StoreInfo"][indexPath.row][@"TarGetUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] title:@""];
            [self.navigationController presentViewController:webViewNavigationController animated:YES completion:^{
                //
            }];

        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

#pragma mark -
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 5;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 5;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return CGHeightFromIP6P(474 / 3);
    } else {
        return CGHeightFromIP6P(554 / 3);
    }

    return 0;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 5)];
        return headerView;
    }
    return [[UIView alloc] init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_storeDictionary) {
        if (section == 0) {
            return [_storeDictionary[@"PlanInfo"] count];
        } else {
            return [_storeDictionary[@"StoreInfo"] count];
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NSString *cellId = [NSString stringWithFormat:@"Cell_Plan_%d", [_storeDictionary[@"PlanInfo"][indexPath.row][@"GoodsNo"] intValue]];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell.contentView setBackgroundColor:[UIColor clearColor]];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

            UIImageView *planImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, CGHeightFromIP6P(474 / 3))];
            [planImageView setBackgroundColor:[UIColor grayColor]];
            [cell.contentView addSubview:planImageView];
            
            UIActivityIndicatorView *loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [loadingIndicatorView setCenter:CGPointMake(planImageView.frame.size.width / 2, planImageView.frame.size.height / 2)];
            [planImageView addSubview:loadingIndicatorView];
            [loadingIndicatorView startAnimating];
            
            [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[_storeDictionary[@"PlanInfo"][indexPath.row][@"GoodsImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                         options:0
                                                        progress:nil
                                                       completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                           if (!error) {
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [loadingIndicatorView stopAnimating];
                                                                   [loadingIndicatorView removeFromSuperview];
                                                                   [planImageView setBackgroundColor:[UIColor clearColor]];
                                                                   [planImageView setImage:image];
                                                               });
                                                           } else {
                                                           }
                                                       }];

        }

        return cell;
    } else {
        NSString *cellId = [NSString stringWithFormat:@"Cell_Store_%d", [_storeDictionary[@"StoreInfo"][indexPath.row][@"GoodsNo"] intValue]];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell.contentView setBackgroundColor:[UIColor clearColor]];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            UIImageView *storeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(35.5 / 3), CGHeightFromIP6P(5.5 / 3), CGWidthFromIP6P(1171 / 3), CGHeightFromIP6P(543 / 3))];
            [storeImageView setBackgroundColor:[UIColor lightGrayColor]];
            [storeImageView.layer setCornerRadius:CGWidthFromIP6P(18 / 3)];
            [storeImageView setClipsToBounds:YES];
            [storeImageView setContentMode:UIViewContentModeScaleToFill];
            [cell.contentView addSubview:storeImageView];

            UIActivityIndicatorView *loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [loadingIndicatorView setCenter:CGPointMake(storeImageView.frame.size.width / 2, storeImageView.frame.size.height / 2)];
            [storeImageView addSubview:loadingIndicatorView];
            [loadingIndicatorView startAnimating];

            [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[_storeDictionary[@"StoreInfo"][indexPath.row][@"GoodsImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                         options:0
                                                        progress:nil
                                                       completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                           if (!error) {
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [loadingIndicatorView stopAnimating];
                                                                   [loadingIndicatorView removeFromSuperview];
                                                                   [storeImageView setBackgroundColor:[UIColor clearColor]];
                                                                   [storeImageView setImage:image];
                                                               });
                                                           } else {
                                                           }
                                                       }];

        }
        
        return cell;
    }
    
    return nil;
}

//- (void)viewDidLayoutSubviews
//{
//    [super viewDidLayoutSubviews];
//
//    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
//        [_tableView setSeparatorInset:UIEdgeInsetsZero];
//    }
//
//    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
//        [_tableView setLayoutMargins:UIEdgeInsetsZero];
//    }
//}
//
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Remove seperator inset
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//
//    // Prevent the cell from inheriting the Table View's margin settings
//    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
//        [cell setPreservesSuperviewLayoutMargins:NO];
//    }
//
//    // Explictly set your cell's layout margins
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

@end
