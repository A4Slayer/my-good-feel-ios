//
//  ConnectSensorListViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "ConnectSensorListViewController.h"

@interface ConnectSensorListViewController ()

@end

@implementation ConnectSensorListViewController

- (void)pushConnectSensorDoneViewController
{
    ConnectSensorDoneViewController *connectSensorDoneViewController = [[ConnectSensorDoneViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    [self.navigationController pushViewController:connectSensorDoneViewController animated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xffffff)];

        [self setTitle:@"센서 연동하기"];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setBackgroundColor:[UIColor clearColor]];
        [backButton setTitle:@"이전" forState:UIControlStateNormal];
        [backButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
        [backButton.titleLabel setFont:SemiBoldWithSize(16)];
        [backButton setAlpha:0];
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [self.navigationItem setLeftBarButtonItem:backBarButtonItem];
        
        UIButton *skipButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 55/3, 60/3)];
        [skipButton setBackgroundColor:[UIColor clearColor]];
        [skipButton setTitle:@"Skip" forState:UIControlStateNormal];
        [skipButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
        [skipButton.titleLabel setFont:SemiBoldWithSize(16)];
        [skipButton setAlpha:0];
        UIBarButtonItem *skipBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:skipButton];
        [self.navigationItem setRightBarButtonItem:skipBarButtonItem];

        _sensorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"connectsensor_image_connecting"]];
        [_sensorImageView setCenter:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2 - 120 )];
        [self.view addSubview:_sensorImageView];

        _processLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, _sensorImageView.frame.origin.y + _sensorImageView.frame.size.height + 60, kSCREEN_WIDTH - 10 - 10, 20)];
        [_processLabel setBackgroundColor:[UIColor clearColor]];
        [_processLabel setFont:[UIFont systemFontOfSize:15]];
        [_processLabel setTextColor:UIColorFromRGB(0x666666)];
        [_processLabel setText:@"연동중..."];
        [_processLabel setTextAlignment:NSTextAlignmentCenter];
        [_processLabel sizeToFit];
        [self.view addSubview:_processLabel];
        
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [_processLabel setCenter:CGPointMake(self.view.frame.size.width / 2 - _indicatorView.frame.size.width / 2 - 1.5, _processLabel.center.y)];
        [_indicatorView setFrame:CGRectMake(_processLabel.frame.origin.x + _processLabel.frame.size.width + 3, _processLabel.frame.origin.y + (_processLabel.frame.size.height / 2 - _indicatorView.frame.size.height / 2), _indicatorView.frame.size.width, _indicatorView.frame.size.height)];
        [self.view addSubview:_indicatorView];
        
        [_indicatorView startAnimating];

        [[BLEManager sharedInstance] startScanForSensors];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorConnected) name:[[BLEManager sharedInstance] notificationForType:BLENotificationTypeConnectedToSensor] object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(discoveredSensor) name:[[BLEManager sharedInstance] notificationForType:BLENotificationTypeDiscoveredSensor] object:nil];

        findingCount = 8;
        found = NO;
        [self performSelector:@selector(findingCount) withObject:nil afterDelay:1.0f];

    }
    return self;
}

- (void)findingCount
{
    if (found) {
        return;
    }

    if (findingCount > 0) {
        findingCount--;
        [self performSelector:@selector(findingCount) withObject:nil afterDelay:1.0f];
    } else {
        [[[UIAlertView alloc] initWithTitle:nil message:@"센서를 찾을 수 없습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        [[BLEManager sharedInstance] stopScanForSensors];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self.navigationController popViewControllerAnimated:YES];
        // 못찾음
    }
}

- (void)discoveredSensor
{
    NSLog(@"discovered");
    NSDictionary *availableSensorDictionary = [[BLEManager sharedInstance] getAvailableSensors];

    if ([[availableSensorDictionary allKeys] count]) {
        for (NSString *key in [availableSensorDictionary allKeys]) {
            [[BLEManager sharedInstance] connectToSensor:availableSensorDictionary[key][BLE_KEY_PERIPHERAL_UUID]];
            found = YES;
            break;
        }
    }
}

- (void)sensorConnected
{
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"sensor_connected"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    found = YES;
    
    NSLog(@"sensor connected ! : %@", [[BLEManager sharedInstance] getConnectedSensor]);
    [[BLEManager sharedInstance] stopScanForSensors];
    
    [_indicatorView stopAnimating];
    [_indicatorView removeFromSuperview];

    [_sensorImageView setImage:[UIImage imageNamed:@"connectsensor_image_connected"]];
    [_sensorImageView setFrame:CGRectMake(0, _sensorImageView.frame.origin.y, [UIImage imageNamed:@"connectsensor_image_connected"].size.width, [UIImage imageNamed:@"connectsensor_image_connected"].size.height)];
    [_sensorImageView setCenter:CGPointMake(self.view.frame.size.width / 2, _sensorImageView.center.y)];

    [_processLabel setText:@"연동 완료"];
    [_processLabel setTextColor:UIColorFromRGB(0xf27f8e)];
    [_processLabel sizeToFit];
    [_processLabel setCenter:CGPointMake(self.view.frame.size.width / 2, _processLabel.center.y)];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[[BLEManager sharedInstance] notificationForType:BLENotificationTypeConnectedToSensor] object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[[BLEManager sharedInstance] notificationForType:BLENotificationTypeDiscoveredSensor] object:nil];

    [[NetworkAPIManager sharedInstance] registerSensor:[[BLEManager sharedInstance] getConnectedSensor][BLE_KEY_MAC_ADDR] withCompletion:^(enum NetworkAPIStatusCode returnCode) {
        NSLog(@"sensor register rtn : %d", returnCode);
    }];
    
    [self performSelector:@selector(pushConnectSensorDoneViewController) withObject:nil afterDelay:1.0f];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
