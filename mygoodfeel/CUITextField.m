//
//  CUITextField.m
//  amorelecture
//
//  Created by Lim daewang on 2014. 4. 23..
//  Copyright (c) 2014년 appknot. All rights reserved.
//

#import "CUITextField.h"

@implementation CUITextField

- (id)initWithFrame:(CGRect)frame withEdgeInsets:(UIEdgeInsets)withEdgeInsets
{
    self = [super initWithFrame:frame];
    if (self) {
        self.edgeInsets = withEdgeInsets;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder withEdgeInsets:(UIEdgeInsets)withEdgeInsets
{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.edgeInsets = withEdgeInsets;
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [super editingRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
