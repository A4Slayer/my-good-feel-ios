//
//  SleepDayInfoViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 27..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ESFramework/ESFramework.h>
#import "CUITextField.h"
#import "CNSTextAttachment.h"
#import "iCarousel.h"
#import "SleepDiaryViewController.h"

#import "SleepDayInfoGuideViewController.h"

#import <JustPieChart/PieChartView.h>

@interface SleepDayInfoViewController : UIViewController <iCarouselDelegate, iCarouselDataSource, PieChartViewDelegate, PieChartViewDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) iCarousel *cardViewCarousel;

@property (strong, nonatomic) UIButton *dateButton;
@property (strong, nonatomic) UIButton *blackMaskButton;
@property (strong, nonatomic) UIDatePicker *datePicker;

@property (strong, nonatomic) NSString *dateString;

@property (strong, nonatomic) NSMutableDictionary *dataDictionary;
@property (strong, nonatomic) UIPageControl *cardPageControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass dateString:(NSString *)dateString;

@end
