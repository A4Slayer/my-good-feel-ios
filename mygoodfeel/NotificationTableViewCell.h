//
//  NotificationTableViewCell.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 6..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier dataDictionary:(NSDictionary *)dataDictionary;

@end
