//
//  AppDelegate.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 13..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "AppDelegate.h"
#import "JNKeychain.h"
#import <WindowsAzureMessaging/WindowsAzureMessaging.h>
#import <UserNotifications/UserNotifications.h>

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void) application:(UIApplication *) application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) deviceToken {
    
    NSString *uniqueIdentifier = nil;
    
    uniqueIdentifier = [JNKeychain loadValueForKey:@"APPKNOT_UDID"];
    
    if (uniqueIdentifier == nil || uniqueIdentifier.length == 0) {
        if([UIDevice instancesRespondToSelector:@selector(identifierForVendor)]) {
            uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        } else {
            uniqueIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:@"identiferForVendor"];
            if( !uniqueIdentifier ) {
                CFUUIDRef uuid = CFUUIDCreate(NULL);
                uniqueIdentifier = (__bridge_transfer NSString*)CFUUIDCreateString(NULL, uuid);
                CFRelease(uuid);
                [[NSUserDefaults standardUserDefaults] setObject:uniqueIdentifier forKey:@"identifierForVendor"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        [JNKeychain saveValue:uniqueIdentifier forKey:@"APPKNOT_UDID"];
    }

    SBNotificationHub *hub = [[SBNotificationHub alloc] initWithConnectionString:HUBLISTENACCESS
                                                             notificationHubPath:HUBNAME];
    
    [hub registerNativeWithDeviceToken:deviceToken tags:[[NSSet alloc] initWithArray:@[ [NSString stringWithFormat:@"DeviceId:%@", uniqueIdentifier] ]] completion:^(NSError* error) {
        if (error != nil) {
            NSLog(@"Error registering for notifications: %@", error);
        }
        else {
            NSLog(@"Registered");
//            [self MessageBox:@"Registration Status" message:@"Registered"];
        }
    }];
}

-(void)MessageBox:(NSString *) title message:(NSString *)messageText
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:messageText preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okAction];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification: (NSDictionary *)userInfo {
//    [self MessageBox:@"Notification" message:[[userInfo objectForKey:@"aps"] valueForKey:@"alert"]];
//    [[[UIAlertView alloc] initWithTitle:@"" message:[[userInfo objectForKey:@"aps"] valueForKey:@"alert"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    _rootViewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    [self.window setRootViewController:_rootViewController];
    [self.window makeKeyAndVisible];
    
    [MSAppCenter start:@"f417e71c-121e-4d97-941c-4800e4b810d8" withServices:@[
                                                                              [MSAnalytics class],
                                                                              [MSCrashes class]
                                                                              ]];
    
    if (launchOptions) {
        [[DataSingleton sharedSingletonClass] setNotificationInfo:[NSMutableDictionary dictionaryWithDictionary:launchOptions[@"UIApplicationLaunchOptionsRemoteNotificationKey"]]];
    }

    if (!IS_HIDE_SLEEP_PART) {
        [Earlysense init:@"el" appVersion:@"1.0" suffix:@"" appId:15 urlBroker:@"https://live-ub.earlysense-wellnessweb.com/URLBroker/getUrl.htm"];
    }

    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             if( !error ) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[UIApplication sharedApplication] registerForRemoteNotifications];
                 });
             }
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"first_launch"] intValue] == 0) {
        // 최초 실행
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"first_launch"];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"day_mark"];
    } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"first_launch"] intValue] == 1) {
        // 두번째 실행
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"first_launch"];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"sleep_alert"] == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"sleep_alert"];

        [[NSUserDefaults standardUserDefaults] setObject:@"80" forKey:@"hrmax"];
        [[NSUserDefaults standardUserDefaults] setObject:@"53" forKey:@"hrmin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"22" forKey:@"rrmax"];
        [[NSUserDefaults standardUserDefaults] setObject:@"14" forKey:@"rrmin"];
    }

    
    [[NSUserDefaults standardUserDefaults] synchronize];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"app_password"]) {
        [[DataSingleton sharedSingletonClass] attachPasswordView];
    }

}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[DataSingleton sharedSingletonClass] unlockUsingBIOContext];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
