//
//  ChangeStepAveragePeriodViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 23..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "DataSingleton.h"
#import "ChangeStepAverageCycleViewController.h"

@interface ChangeStepAveragePeriodViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UIPickerView *periodPickerView;

@property (strong, nonatomic) ChangeStepAverageCycleViewController *changeStepAverageCycleViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
