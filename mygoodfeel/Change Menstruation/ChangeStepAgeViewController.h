//
//  ChangeStepAgeViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 14..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "ChangeStepLastMenstruationViewController.h"

@interface ChangeStepAgeViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UIPickerView *agePickerView;

@property (strong, nonatomic) ChangeStepLastMenstruationViewController *changeStepLastMenstruationViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
