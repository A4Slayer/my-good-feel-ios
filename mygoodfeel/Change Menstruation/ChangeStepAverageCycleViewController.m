//
//  ChangeStepAverageCycleViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 23..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "ChangeStepAverageCycleViewController.h"

@interface ChangeStepAverageCycleViewController ()

@end

@implementation ChangeStepAverageCycleViewController

- (void)alertCalcGuide
{
    [[[UIAlertView alloc] initWithTitle:@"MY좋은느낌 생리주기 계산법" message:@"\nMY좋은느낌은 두 가지 주기 계산법을 적용합니다.\n\n 26~32일의 생리 주기를 가진 고객\n조지타운 대학교 생식건강연구소에서 개발한 \"표준일 피임법\"을 적용하며 생리 시작일부터 세어 처음 7일 동안은 안전기로, 8일부터 19일까지는 가임기, 20일부터 다음 예정 생리 시작일 전날까지 다시 안전기로 계산하는 방법입니다.\n\n25일 이하 또는 33일 이상의 주기를 가진 고객\n대한산부인과의사회에서 적용하는 계산법을 적용하며 첫 예상 생리 시작일은 생리가 끝난 날부터 28일 후로 예상, 이후 3개월의 평균치로 다음 주기를 예측합니다." delegate:nil cancelButtonTitle:@"닫기" otherButtonTitles:nil] show];
}

- (void)alertNoExistOption
{
    [[[UIAlertView alloc] initWithTitle:@"선택 항목에 내 주기가 없어요" message:@"\n주기가 불규칙하거나 정확한 정보를 모르시는 경우 이 질문을 건너뛰실 수 있습니다.\n\n질문을 건너뛰는 경우 첫 예상 생리 시작일은 평균 28일 후로 예상하며 '설정'메뉴에서 다시 입력하실 수 있습니다.\n\n주기 일수가 불규칙한 경우 병원 방문을 추천드립니다.\n(전 주기 일수와 현재 주기 일수 차이가 2주 이상인 경우)\n\n'20일 이하' 선택지를 입력하시면 20일 이후로,\n'36일 이상' 선택지를 입력하시면 36일 이후로 첫 예상 생리 시작일을 예상하며, 이후 주기는 입력하시는 주기 정보가 축적되며 더욱 정확하게 예측할 수 있습니다." delegate:nil cancelButtonTitle:@"닫기" otherButtonTitles:nil] show];
}

- (void)backToStepAveragePeriodViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)skipAverageCycle
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (void)loadAverageCycle
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_GET_USER_BASIC]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        switch ([responseData[@"code"] intValue]) {
            case 0: {
                
                if ([responseData[@"data"][@"AvgMenstrualRepeatCycle"] intValue] >= 20) {
                    [self->_cyclePickerView selectRow:[responseData[@"data"][@"AvgMenstrualRepeatCycle"] intValue] - 20 inComponent:0 animated:NO];
                } else {
                    [self->_cyclePickerView selectRow:8 inComponent:0 animated:NO];
                }
                
            }
                break;
                
            default:
                [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:responseData[@"msg"][@"ko"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                break;
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        NSDictionary *responseData;
        if (error != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:[NSString stringWithFormat:@"%@", responseData] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];
}

- (void)setAverageCycle
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_SET_USER_BASIC]];
    
    [urlString appendFormat:@"/%@", @"104"];        // avr period
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];           // ID
    [urlString appendFormat:@"/%@", [NSString stringWithFormat:@"%ld", [_cyclePickerView selectedRowInComponent:0] + 20]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        switch ([responseData[@"code"] intValue]) {
            case 0:
            {
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                    //
                }];
            }
                break;
                
            default:
                [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:responseData[@"msg"][@"ko"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                break;
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        NSDictionary *responseData;
        if (error != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:[NSString stringWithFormat:@"%@", responseData] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setBackgroundColor:[UIColor clearColor]];
        [backButton setTitle:@"이전" forState:UIControlStateNormal];
        [backButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
        [backButton.titleLabel setFont:SemiBoldWithSize(16)];
        [backButton addTarget:self action:@selector(backToStepAveragePeriodViewController) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [self.navigationItem setLeftBarButtonItem:backBarButtonItem];
        
        UIButton *skipButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 55/3, 60/3)];
        [skipButton setBackgroundColor:[UIColor clearColor]];
        [skipButton setTitle:@"잘 모르겠어요" forState:UIControlStateNormal];
        [skipButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
        [skipButton.titleLabel setFont:SemiBoldWithSize(16)];
        [skipButton addTarget:self action:@selector(skipAverageCycle) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *skipBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:skipButton];
        [self.navigationItem setRightBarButtonItem:skipBarButtonItem];
        
        
        UILabel *averageCycleTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 100)];
        [averageCycleTitleLabel setBackgroundColor:[UIColor clearColor]];
        [averageCycleTitleLabel setNumberOfLines:0];
        [averageCycleTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"주기는 며칠마다 반복되나요?"]
                                                                                       attributes:@{ NSFontAttributeName : BoldWithSize(20),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }];
        [averageCycleTitleLabel setAttributedText:attrString];
        [averageCycleTitleLabel sizeToFit];
        [averageCycleTitleLabel setCenter:CGPointMake(self.view.center.x, self.view.center.y - 240)];
        [self.view addSubview:averageCycleTitleLabel];
        
        
        _cyclePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, kSCREEN_WIDTH, 280)];
        [_cyclePickerView setBackgroundColor:[UIColor clearColor]];
        [_cyclePickerView setCenter:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2 - 60)];
        [_cyclePickerView setDelegate:self];
        [_cyclePickerView setDataSource:self];
        [self.view addSubview:_cyclePickerView];
        
        
        UIButton *calcGuideButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(689 / 3), CGHeightFromIP6P(133 / 3))];
        [calcGuideButton setBackgroundImage:[DataSingleton imageFromColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [calcGuideButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xe1e1e1)] forState:UIControlStateHighlighted];
        [calcGuideButton setCenter:CGPointMake(self.view.frame.size.width / 2, _cyclePickerView.frame.origin.y + _cyclePickerView.frame.size.height + CGHeightFromIP6P(60))];
        [calcGuideButton.layer setBorderWidth:k3PX];
        [calcGuideButton.layer setBorderColor:UIColorFromRGB(0xf6c3ca).CGColor];
        [calcGuideButton.layer setCornerRadius:CGWidthFromIP6P(14 / 3)];
        [calcGuideButton setTitle:@"생리 주기 계산 가이드" forState:UIControlStateNormal];
        [calcGuideButton setTitleColor:UIColorFromRGB(0xf27f8e) forState:UIControlStateNormal];
        [calcGuideButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(14))];
        [calcGuideButton setClipsToBounds:YES];
        [calcGuideButton addTarget:self action:@selector(alertCalcGuide) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:calcGuideButton];
        
        
        UIButton *cycleQuestionButton = [[UIButton alloc] initWithFrame:CGRectMake(calcGuideButton.frame.origin.x, calcGuideButton.frame.origin.y + calcGuideButton.frame.size.height + CGHeightFromIP6P(23), calcGuideButton.frame.size.width, CGHeightFromIP6P(23))];
        [cycleQuestionButton addTarget:self action:@selector(alertNoExistOption) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:cycleQuestionButton];
        
        CNSTextAttachment *imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -2.5)];
        [imageAttachment setImage:[UIImage imageNamed:@"common_image_qmark"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", @" 선택 항목에 내 주기가 없어요."]
                                                                           attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(13)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0xbdbdbd) }]];
        [cycleQuestionButton setAttributedTitle:attrString forState:UIControlStateNormal];
        
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", @" 선택 항목에 내 주기가 없어요."]
                                                                           attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(13)),
                                                                                         NSForegroundColorAttributeName : [UIColor grayColor] }]];
        
        [cycleQuestionButton setAttributedTitle:attrString forState:UIControlStateHighlighted];
        
        [cycleQuestionButton sizeToFit];
        [cycleQuestionButton setCenter:CGPointMake(self.view.center.x, cycleQuestionButton.center.y)];
        
        UIView *underlineView = [[UIView alloc] initWithFrame:CGRectMake(0, cycleQuestionButton.frame.size.height - CGHeightFromIP6P(3), cycleQuestionButton.frame.size.width, k2PX)];
        [underlineView setBackgroundColor:UIColorFromRGB(0xbdbdbd)];
        [cycleQuestionButton addSubview:underlineView];
        
        
        UIButton *nextButton = [[UIButton alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - 80, kSCREEN_HEIGHT - kBOTTOM_HEIGHT - 50, 80, 50)];
        [nextButton setBackgroundColor:[UIColor clearColor]];
        [nextButton setTitle:@"완료" forState:UIControlStateNormal];
        [nextButton.titleLabel setFont:SemiBoldWithSize(16)];
        [nextButton setTitleColor:UIColorFromRGB(0xf27f8e) forState:UIControlStateNormal];
        [nextButton addTarget:self action:@selector(setAverageCycle) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextButton];
        
        
        UIPageControl *stepPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(80, kSCREEN_HEIGHT - kBOTTOM_HEIGHT - 50, kSCREEN_WIDTH - 80 - 80, 50)];
        [stepPageControl setNumberOfPages:4];
        [stepPageControl setCurrentPage:4];
        [stepPageControl setUserInteractionEnabled:NO];
        [stepPageControl setPageIndicatorTintColor:UIColorFromRGB(0xffd8dd)];
        [stepPageControl setCurrentPageIndicatorTintColor:UIColorFromRGB(0xf27f8e)];
        [self.view addSubview:stepPageControl];
        
        [self loadAverageCycle];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return kSCREEN_WIDTH;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 50;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel*)view;
    
    if (!pickerLabel) {
        pickerLabel = [[UILabel alloc] init];
        [pickerLabel setFont:SemiBoldWithSize(28)];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setTextColor:[UIColor blackColor]];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
    }
    
    if (row == 0) {
        [pickerLabel setText:[NSString stringWithFormat:@"20일 이하"]];
    } else if (row == 1) {
        [pickerLabel setText:[NSString stringWithFormat:@"21일"]];
    } else if (row == 2) {
        [pickerLabel setText:[NSString stringWithFormat:@"22일"]];
    } else if (row == 3) {
        [pickerLabel setText:[NSString stringWithFormat:@"23일"]];
    } else if (row == 4) {
        [pickerLabel setText:[NSString stringWithFormat:@"24일"]];
    } else if (row == 5) {
        [pickerLabel setText:[NSString stringWithFormat:@"25일"]];
    } else if (row == 6) {
        [pickerLabel setText:[NSString stringWithFormat:@"26일"]];
    } else if (row == 7) {
        [pickerLabel setText:[NSString stringWithFormat:@"27일"]];
    } else if (row == 8) {
        [pickerLabel setText:[NSString stringWithFormat:@"28일"]];
    } else if (row == 9) {
        [pickerLabel setText:[NSString stringWithFormat:@"29일"]];
    } else if (row == 10) {
        [pickerLabel setText:[NSString stringWithFormat:@"30일"]];
    } else if (row == 11) {
        [pickerLabel setText:[NSString stringWithFormat:@"31일"]];
    } else if (row == 12) {
        [pickerLabel setText:[NSString stringWithFormat:@"32일"]];
    } else if (row == 13) {
        [pickerLabel setText:[NSString stringWithFormat:@"33일"]];
    } else if (row == 14) {
        [pickerLabel setText:[NSString stringWithFormat:@"34일"]];
    } else if (row == 15) {
        [pickerLabel setText:[NSString stringWithFormat:@"35일"]];
    } else if (row == 16) {
        [pickerLabel setText:[NSString stringWithFormat:@"36일 이상"]];
    }
    
    
    return pickerLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 17;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
