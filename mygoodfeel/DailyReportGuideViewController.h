//
//  DailyReportGuideViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 11..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNSTextAttachment.h"

@interface DailyReportGuideViewController : UIViewController

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UIScrollView *mainScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end

