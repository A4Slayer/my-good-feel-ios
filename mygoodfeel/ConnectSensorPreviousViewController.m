//
//  ConnectSensorPreviousViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "ConnectSensorPreviousViewController.h"
#import "RootViewController.h"
#import "PreferenceSensorViewController.h"

@interface ConnectSensorPreviousViewController ()

@end

@implementation ConnectSensorPreviousViewController

- (void)pushSensorListViewController
{
    NSLog(@"called push");
    ConnectSensorListViewController *connectSensorListViewController = [[ConnectSensorListViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    [self.navigationController pushViewController:connectSensorListViewController animated:YES];
}

- (void)doneAgreementView
{
    [self pushSensorListViewController];
}

- (void)presentSensorAgreementViewController
{
    _connectSensorAgreementViewController = [[ConnectSensorAgreementViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    
    _connectSensorAgreementNavigationController = [[UINavigationController alloc] initWithRootViewController:_connectSensorAgreementViewController];
    [_connectSensorAgreementNavigationController.navigationBar setTranslucent:NO];
    
    [self presentViewController:_connectSensorAgreementNavigationController animated:YES completion:^{
        //
    }];
}

- (void)skipConnectSensor
{
    [(RootViewController *)self.navigationController.presentingViewController attachMainViewController];
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xffffff)];
        [self setTitle:@"센서 연동하기"];
        float minusMargin = 0;
        
        if ([pClass isKindOfClass:[PreferenceSensorViewController class]]) {
            UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
            [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
            [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
            [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
            minusMargin = kTOP_HEIGHT;
        } else {
            UIButton *skipButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 55/3, 60/3)];
            [skipButton setBackgroundColor:[UIColor clearColor]];
            [skipButton setTitle:@"Skip" forState:UIControlStateNormal];
            [skipButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
            [skipButton.titleLabel setFont:SemiBoldWithSize(16)];
            [skipButton addTarget:self action:@selector(skipConnectSensor) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *skipBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:skipButton];
            [self.navigationItem setRightBarButtonItem:skipBarButtonItem];
        }
        
        UIImageView *bluetoothImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"connectsensor_image_bluetooth"]];
        [bluetoothImageView setCenter:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2 - 120 - minusMargin)];
        [self.view addSubview:bluetoothImageView];
        
        
        UILabel *bluetoothTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 100)];
        [bluetoothTitleLabel setBackgroundColor:[UIColor clearColor]];
        [bluetoothTitleLabel setNumberOfLines:0];
        [bluetoothTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"블루투스 활성화"]
                                                                                       attributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:23],
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }];
        [bluetoothTitleLabel setAttributedText:attrString];
        [bluetoothTitleLabel sizeToFit];
        [bluetoothTitleLabel setCenter:CGPointMake(self.view.center.x, self.view.center.y + 120 - minusMargin)];
        [self.view addSubview:bluetoothTitleLabel];
        
        
        
        UILabel *bluetoothDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(6), 0, kSCREEN_WIDTH - CGWidthFromIP6P(12), 100)];
        [bluetoothDescLabel setBackgroundColor:[UIColor clearColor]];
        [bluetoothDescLabel setNumberOfLines:0];
        [bluetoothDescLabel setTextAlignment:NSTextAlignmentCenter];
        
        attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", @"제어센터에서 블루투스 아이콘을 Tap하거나\n휴대전화 설정에서 블루투스를\n활성화시킬 수 있습니다."]
                                                            attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:15],
                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x666666) }];
        [bluetoothDescLabel setAttributedText:attrString];
        [bluetoothDescLabel sizeToFit];
        [bluetoothDescLabel setFrame:CGRectMake(0, bluetoothTitleLabel.frame.origin.y + bluetoothTitleLabel.frame.size.height + 6, kSCREEN_WIDTH, bluetoothDescLabel.frame.size.height)];
        [self.view addSubview:bluetoothDescLabel];

        
        UIButton *nextButton = [[UIButton alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - 80, kSCREEN_HEIGHT - kBOTTOM_HEIGHT - 50 - minusMargin, 80, 50)];
        [nextButton setBackgroundColor:[UIColor clearColor]];
        [nextButton setTitle:@"다음" forState:UIControlStateNormal];
        [nextButton.titleLabel setFont:SemiBoldWithSize(16)];
        [nextButton setTitleColor:UIColorFromRGB(0xf27f8e) forState:UIControlStateNormal];
//        [nextButton addTarget:self action:@selector(pushSensorListViewController) forControlEvents:UIControlEventTouchUpInside];
        [nextButton addTarget:self action:@selector(presentSensorAgreementViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextButton];
        
        [[BLEManager sharedInstance] disconnectFromSensor];
        [[BLEManager sharedInstance] setLastConnectedSensor:nil];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
