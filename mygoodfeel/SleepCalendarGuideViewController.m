//
//  SleepCalendarGuideViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 12. 5..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SleepCalendarGuideViewController.h"

@interface SleepCalendarGuideViewController ()

@end

@implementation SleepCalendarGuideViewController

- (void)dismissSelfView
{
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_close_pink"].size.width, [UIImage imageNamed:@"common_button_close_pink"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"common_button_close_pink"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];
        
        [self setTitle:@"수면 캘린더 읽는 법"];
        
        
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT)];
        [_mainScrollView setBackgroundColor:UIColorFromRGB(0xffffff)];
        [self.view addSubview:_mainScrollView];
        
        
        UILabel *sleepSpectrumGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(CGHeightFromIP6P(79 / 3)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [sleepSpectrumGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [sleepSpectrumGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [sleepSpectrumGuideLabel setClipsToBounds:YES];
        [sleepSpectrumGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [sleepSpectrumGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [sleepSpectrumGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [sleepSpectrumGuideLabel setText:@"수면 스펙트럼"];
        [_mainScrollView addSubview:sleepSpectrumGuideLabel];
        
        UILabel *sleepSpectrumDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepSpectrumGuideLabel.frame.origin.y + sleepSpectrumGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepSpectrumDescLabel setNumberOfLines:0];
        [sleepSpectrumDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepSpectrumDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepSpectrumDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepSpectrumDescLabel setText:@"사용자가 수면 센서를 통해 축적한 한 달 간의 수면 점수를 그래프로 비교할 수 있으며 생리 기간을 배경색으로 나타내어 생리 기간 동안 수면 점수에 얼만큼의 영향을 끼치는지 비교하실 수 있습니다."];
        [sleepSpectrumDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepSpectrumDescLabel];
        
        
        UILabel *sleepScoreGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepSpectrumDescLabel.frame.origin.y + sleepSpectrumDescLabel.frame.size.height + CGHeightFromIP6P(28)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [sleepScoreGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [sleepScoreGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [sleepScoreGuideLabel setClipsToBounds:YES];
        [sleepScoreGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [sleepScoreGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [sleepScoreGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [sleepScoreGuideLabel setText:@"수면 점수 비교표"];
        [_mainScrollView addSubview:sleepScoreGuideLabel];
        
        UILabel *sleepScoreDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepScoreGuideLabel.frame.origin.y + sleepScoreGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepScoreDescLabel setNumberOfLines:0];
        [sleepScoreDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepScoreDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepScoreDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepScoreDescLabel setText:@"한 달 간의 수면 점수와 평균값을 확인하실 수 있으며, 저번 달과 이번 달의 나의 수면의 질을 비교하실 수 있습니다."];
        [sleepScoreDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepScoreDescLabel];
        
        
        UILabel *sleepTimeGraphGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepScoreDescLabel.frame.origin.y + sleepScoreDescLabel.frame.size.height + CGHeightFromIP6P(28)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [sleepTimeGraphGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [sleepTimeGraphGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [sleepTimeGraphGuideLabel setClipsToBounds:YES];
        [sleepTimeGraphGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [sleepTimeGraphGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [sleepTimeGraphGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [sleepTimeGraphGuideLabel setText:@"수면 시간 그래프"];
        [_mainScrollView addSubview:sleepTimeGraphGuideLabel];
        
        UILabel *sleepTimeGraphDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepTimeGraphGuideLabel.frame.origin.y + sleepTimeGraphGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepTimeGraphDescLabel setNumberOfLines:0];
        [sleepTimeGraphDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepTimeGraphDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepTimeGraphDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepTimeGraphDescLabel setText:@"사용자가 수면 센서를 통해 축적한 한 달 간의 수면 점수/수면 시간을 그래프로 비교할 수 있으며 생리 기간을 배경색으로 나타내어 생리 기간 동안의 수면 시간을 비교할 수 있습니다."];
        [sleepTimeGraphDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepTimeGraphDescLabel];
        
        
        UILabel *sleepTimeCompareGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepTimeGraphDescLabel.frame.origin.y + sleepTimeGraphDescLabel.frame.size.height + CGHeightFromIP6P(28)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [sleepTimeCompareGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [sleepTimeCompareGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [sleepTimeCompareGuideLabel setClipsToBounds:YES];
        [sleepTimeCompareGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [sleepTimeCompareGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [sleepTimeCompareGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [sleepTimeCompareGuideLabel setText:@"수면 시간 비교표"];
        [_mainScrollView addSubview:sleepTimeCompareGuideLabel];
        
        UILabel *sleepTimeCompareDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepTimeCompareGuideLabel.frame.origin.y + sleepTimeCompareGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepTimeCompareDescLabel setNumberOfLines:0];
        [sleepTimeCompareDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepTimeCompareDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepTimeCompareDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepTimeCompareDescLabel setText:@"한 달 간의 수면 시간의 평균값을 확인하실 수 있으며, 저번 달과 이번 달의 나의 수면 시간을 비교하실 수 있습니다."];
        [sleepTimeCompareDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepTimeCompareDescLabel];

        [_mainScrollView setContentSize:CGSizeMake(_mainScrollView.frame.size.width, sleepTimeCompareDescLabel.frame.origin.y + sleepTimeCompareDescLabel.frame.size.height + CGHeightFromIP6P(16))];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
