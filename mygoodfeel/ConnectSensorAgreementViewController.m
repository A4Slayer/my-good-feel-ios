//
//  ConnectSensorAgreementViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 4. 16..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "ConnectSensorAgreementViewController.h"
#import "ConnectSensorPreviousViewController.h"

@interface ConnectSensorAgreementViewController ()

@end

@implementation ConnectSensorAgreementViewController

- (void)detachAgreementViewController
{
    if (isCheckedAgreement) {
        [self dismissViewControllerAnimated:YES completion:^{
            [(ConnectSensorPreviousViewController *)self->_pClass doneAgreementView];
        }];
    } else {
        [[[UIAlertView alloc] initWithTitle:nil message:@"약관에 동의하셔야 서비스를 이용할 수 있습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }
}

- (void)toggleAgreementButton:(UIButton *)button
{
    isCheckedAgreement = !isCheckedAgreement;
    if (isCheckedAgreement) {
        [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
    } else {
        [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
    }
}

- (void)dismissSelfView
{
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        _pClass = pClass;
        [self.navigationItem setHidesBackButton:YES animated:NO];
        [self setTitle:@"약관 동의"];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_close"].size.width, [UIImage imageNamed:@"common_button_close"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"common_button_close"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];

        isCheckedAgreement = NO;
        
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, (int)(kSCREEN_HEIGHT - kTOP_HEIGHT - (CGHeightFromIP6P(184 / 3) + kBOTTOM_HEIGHT)))];
        [_mainScrollView setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView setContentSize:CGSizeMake(kSCREEN_WIDTH, (int)CGHeightFromIP6P(1988) / 3 + CGHeightFromIP6P(13) / 3 + CGHeightFromIP6P(13) / 3)];
        [_mainScrollView setBounces:YES];
        [self.view addSubview:_mainScrollView];
        
        UIView *frameView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(41) / 3, CGHeightFromIP6P(41) / 3, CGWidthFromIP6P(1160) / 3, CGHeightFromIP6P(1988) / 3)];
        [frameView setBackgroundColor:[UIColor whiteColor]];
        [frameView.layer setBorderColor:UIColorFromRGB(0xd7d7d7).CGColor];
        [frameView.layer setBorderWidth:k1PX];
        [_mainScrollView addSubview:frameView];
        
        UILabel *agreementLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(59 / 3), CGHeightFromIP6P(13), CGWidthFromIP6P(814) / 3, CGHeightFromIP6P(57) / 3)];
        [frameView addSubview:agreementLabel];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"센서 연동 개인정보 수집 동의"]
                                                                                       attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(17)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x353535) }];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@""]
                                                                           attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }]];
        [agreementLabel setAttributedText:attrString];
        [agreementLabel sizeToFit];
        
        _agreementButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1030 / 3) - 12, agreementLabel.frame.origin.y - 12, 44, 44)];
        [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_agreementButton addTarget:self action:@selector(toggleAgreementButton:) forControlEvents:UIControlEventTouchUpInside];
        [frameView addSubview:_agreementButton];
        
        UIWebView *agreementWebView = [[UIWebView alloc] initWithFrame:CGRectMake((int)CGWidthFromIP6P(59 / 3), (int)CGHeightFromIP6P(136 / 3), (int)CGWidthFromIP6P(1037 / 3), frameView.frame.size.height - (int)CGHeightFromIP6P(136 / 3) - CGHeightFromIP6P(13))];
        [agreementWebView.layer setBorderColor:UIColorFromRGB(0xd7d7d7).CGColor];
        [agreementWebView.layer setBorderWidth:k1PX];
        [agreementWebView setScalesPageToFit:YES];
        [agreementWebView loadRequest:[NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:@"1" withExtension:@"html"]]];
        [frameView addSubview:agreementWebView];
        
        _agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - kTOP_HEIGHT - kBOTTOM_HEIGHT - CGHeightFromIP6P(184 / 3), kSCREEN_WIDTH, CGHeightFromIP6P(184 / 3) + kBOTTOM_HEIGHT)];
        [_agreeButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf27f8e)] forState:UIControlStateNormal];
        [_agreeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_agreeButton.titleLabel setFont:MediumWithSize(CGHeightFromIP6P(20))];
        [_agreeButton setTitle:@"다음" forState:UIControlStateNormal];
        [_agreeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, kBOTTOM_HEIGHT, 0)];
        [_agreeButton addTarget:self action:@selector(detachAgreementViewController) forControlEvents:UIControlEventTouchUpInside];
        //        [_agreeButton setEnabled:NO];
        [self.view addSubview:_agreeButton];
    }
    return self;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
