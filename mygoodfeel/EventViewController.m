//
//  EventViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 14..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "EventViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface EventViewController ()

@end

@implementation EventViewController

- (void)loadEvent
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_EVENT_GET_INFO]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            self->_eventDictionary = responseData[@"data"];
            [self->_mainTableView reloadData];
        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xefeff4)];

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"이벤트"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x000000)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu_black"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu_black"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
        
        UIButton *cycleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_cycle"].size.width, [UIImage imageNamed:@"common_button_cycle"].size.height)];
        [cycleButton setImage:[UIImage imageNamed:@"common_button_cycle"] forState:UIControlStateNormal];
        [cycleButton addTarget:(RootViewController *)_pClass action:@selector(changeToCycleDayInfoViewController) forControlEvents:UIControlEventTouchUpInside];
        [cycleButton setAlpha:0];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cycleButton]];
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT) style:UITableViewStylePlain];
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setTableFooterView:[[UIView alloc] init]];
        [_mainTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.view addSubview:_mainTableView];
        
        [self loadEvent];

        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"Event",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"Event",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_eventDictionary[@"EventInfo"][indexPath.row][@"EventNo"]];
    [self.navigationController pushViewController:eventDetailViewController animated:YES];
    
    [MSAnalytics trackEvent:@"Event clicked" withProperties:@{
                                                              @"EventNo" : [NSString stringWithFormat:@"%d", [_eventDictionary[@"EventInfo"][indexPath.row][@"EventNo"] intValue]],
                                                              @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                              @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                        [NSString stringWithFormat:@"%d", [_eventDictionary[@"EventInfo"][indexPath.row][@"EventNo"] intValue]],
                                                                        [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                        ]
                                                              }];
}

#pragma mark -
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CGHeightFromIP6P(158) + 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_eventDictionary) {
        return [_eventDictionary[@"EventInfo"] count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [NSString stringWithFormat:@"Cell_Store_%d", [_eventDictionary[@"EventInfo"][indexPath.row][@"EventNo"] intValue]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.contentView setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        UIImageView *eventImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, CGHeightFromIP6P(158))];
        [eventImageView setBackgroundColor:[UIColor lightGrayColor]];
        [cell.contentView addSubview:eventImageView];
        
        UIActivityIndicatorView *loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [loadingIndicatorView setCenter:CGPointMake(eventImageView.frame.size.width / 2, eventImageView.frame.size.height / 2)];
        [eventImageView addSubview:loadingIndicatorView];
        [loadingIndicatorView startAnimating];
        
        [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[_eventDictionary[@"EventInfo"][indexPath.row][@"EventImagePath"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                     options:0
                                                    progress:nil
                                                   completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                       if (!error) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [loadingIndicatorView stopAnimating];
                                                               [loadingIndicatorView removeFromSuperview];
                                                               [eventImageView setBackgroundColor:[UIColor clearColor]];
                                                               [eventImageView setImage:image];
                                                               
//                                                               float ratio = kSCREEN_WIDTH / image.size.width;
//
//                                                               [eventImageView setFrame:CGRectMake(0, 0, kSCREEN_WIDTH, (int)(ratio * image.size.height))];
//                                                               self->cellHeight[indexPath.row] = (int)(ratio * image.size.height);
//                                                               [tableView reloadRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section] ] withRowAnimation:UITableViewRowAnimationAutomatic];
                                                           });
                                                       } else {
                                                       }
                                                   }];

    }
    
    return cell;
}

@end
