//
//  DailyReportViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import <AKPickerView/AKPickerView.h>
#import "AKSDWebImageManager.h"
#import "DailyReportGuideViewController.h"

@interface DailyReportViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, iCarouselDelegate, iCarouselDataSource, UIScrollViewDelegate>

@property (strong, nonatomic) id pClass;

@property (strong, nonatomic) NSDictionary *dataDictionary;

@property (strong, nonatomic) UIScrollView *mainScrollView;
@property (strong, nonatomic) UIButton *dateButton;

@property (strong, nonatomic) NSString *dateString;
@property (strong, nonatomic) iCarousel *dayCarousel;

@property (strong, nonatomic) UILabel *weekdayLabel;
@property (strong, nonatomic) UILabel *statusLabel;
@property (strong, nonatomic) UILabel *descLabel;

@property (strong, nonatomic) UILabel *weightValueLabel;
@property (strong, nonatomic) UILabel *weightValue2Label;
@property (strong, nonatomic) UILabel *sleepValueLabel;

@property (strong, nonatomic) NSMutableArray *dayViewArray;

@property (strong, nonatomic) UIScrollView *bannerScrollView;

@property (strong, nonatomic) iCarousel *healthCarousel;
@property (strong, nonatomic) UIPageControl *healthPageControl;

@property (strong, nonatomic) UIScrollView *leftEventScrollView;
@property (strong, nonatomic) UIScrollView *rightEventScrollView;

@property (strong, nonatomic) NSMutableArray *healthBannerArray;

@property (strong, nonatomic) UIView *blackMaskView;
@property (strong, nonatomic) UIView *dateSelectView;
@property (strong, nonatomic) UIView *pickerResignView;
@property (strong, nonatomic) UIPickerView *datePickerView;

@property (strong, nonatomic) NSString *lastDateString;

@property (strong, nonatomic) UIScrollView *guideScrollView;

@property (strong, nonatomic) UIView *sleepView;
@property (strong, nonatomic) UIView *noSleepView;

@property (strong, nonatomic) UILabel *sleepTimeValueLabel;
@property (strong, nonatomic) UILabel *deepSleepTimeValueLabel;

@property (strong, nonatomic) UILabel *sleepStressValueLabel;
@property (strong, nonatomic) UILabel *rrValueLabel;
@property (strong, nonatomic) UILabel *hrValueLabel;

@property (strong, nonatomic) UILabel *startTimeLabel;
@property (strong, nonatomic) UILabel *endTimeLabel;
@property (strong, nonatomic) UIView *spectrumView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
