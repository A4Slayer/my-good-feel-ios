//
//  PasswordViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 17..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import "PasswordSettingViewController.h"
#import "PasswordChangeViewController.h"


@interface PasswordViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UITableView *mainTableView;

@property (strong, nonatomic) UISwitch *passwordSwitch;
@property (strong, nonatomic) UISwitch *bioAuthSwitch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
