//
//  GuestAgreementViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 14..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "GuestAgreementViewController.h"
#import "SignViewController.h"

@interface GuestAgreementViewController ()

@end

@implementation GuestAgreementViewController

- (void)detachAgreementViewController
{
    if (isCheckedAgreement && isCheckedPrivacy) {
        [[DataSingleton sharedSingletonClass] setTermAgreeStr:@"Y"];
        [[DataSingleton sharedSingletonClass] setPersonalDataAgreeStr:@"Y"];
        [[DataSingleton sharedSingletonClass] setMarketingAgreeStr:@"N"];
        if (isCheckedMarketing) {
            [[DataSingleton sharedSingletonClass] setMarketingAgreeStr:@"Y"];
        }
        
        [[DataSingleton sharedSingletonClass] setNeedUpdateTerm:YES];
        
        [self dismissViewControllerAnimated:YES completion:^{
            [(SignViewController *)self->_pClass doneAgreementView];
        }];
    } else {
        [[[UIAlertView alloc] initWithTitle:nil message:@"필수항목에는 꼭 동의하셔야 서비스를 이용할 수 있습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }
}

- (void)toggleAgreementButton:(UIButton *)button
{
    if (button == _allAgreementButton) {
        if (isCheckedAgreement && isCheckedPrivacy && isCheckedMarketing) {
            isCheckedAgreement = isCheckedPrivacy = isCheckedMarketing = NO;
            
            [_allAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
            [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
            [_privacyAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
            [_marketingAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        } else {
            isCheckedAgreement = isCheckedPrivacy = isCheckedMarketing = YES;
            
            [_allAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
            [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
            [_privacyAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
            [_marketingAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        }
    } else if (button == _agreementButton) {
        isCheckedAgreement = !isCheckedAgreement;
        if (isCheckedAgreement) {
            [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        } else {
            [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        }
    } else if (button == _privacyAgreementButton) {
        isCheckedPrivacy = !isCheckedPrivacy;
        if (isCheckedPrivacy) {
            [_privacyAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        } else {
            [_privacyAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        }
    } else if (button == _marketingAgreementButton) {
        isCheckedMarketing = !isCheckedMarketing;
        if (isCheckedMarketing) {
            [_marketingAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
        } else {
            [_marketingAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        }
    }
    
    if (isCheckedAgreement && isCheckedPrivacy && isCheckedMarketing) {
        [_allAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateNormal];
    } else {
        [_allAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
    }

//    if (isCheckedAgreement && isCheckedPrivacy) {
//        [_agreeButton setEnabled:YES];
//    } else {
//        [_agreeButton setEnabled:NO];
//    }

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        _pClass = pClass;
        [self.navigationItem setHidesBackButton:YES animated:NO];
        [self setTitle:@"약관 동의"];
        
        isCheckedAgreement = NO;
        isCheckedPrivacy = NO;
        isCheckedMarketing = NO;
        
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, (int)(kSCREEN_HEIGHT - kTOP_HEIGHT - (CGHeightFromIP6P(184 / 3) + kBOTTOM_HEIGHT)))];
        [_mainScrollView setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView setContentSize:CGSizeMake(kSCREEN_WIDTH, (int)CGHeightFromIP6P(1988) / 3 + CGHeightFromIP6P(13) / 3 + CGHeightFromIP6P(13) / 3)];
        [_mainScrollView setBounces:YES];
        [self.view addSubview:_mainScrollView];
        
        UIView *frameView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(41) / 3, CGHeightFromIP6P(41) / 3, CGWidthFromIP6P(1160) / 3, CGHeightFromIP6P(1988) / 3)];
        [frameView setBackgroundColor:[UIColor whiteColor]];
        [frameView.layer setBorderColor:UIColorFromRGB(0xd7d7d7).CGColor];
        [frameView.layer setBorderWidth:k1PX];
        [_mainScrollView addSubview:frameView];
        
        UILabel *allAgreementLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(59 / 3), 0, CGWidthFromIP6P(814) / 3, CGHeightFromIP6P(318) / 3)];
        [allAgreementLabel setTextColor:UIColorFromRGB(0x353535)];
        [allAgreementLabel setNumberOfLines:0];
        [allAgreementLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [allAgreementLabel setText:@"이용약관, 개인정보 수집 이용 동의,\n개인정보 마케팅 목적 이용 동의(선택)에\n모두 동의합니다."];
        [frameView addSubview:allAgreementLabel];

        _allAgreementButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1030 / 3) - 12, 0, 44, 44)];
        [_allAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_allAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_allAgreementButton addTarget:self action:@selector(toggleAgreementButton:) forControlEvents:UIControlEventTouchUpInside];
        [_allAgreementButton setCenter:CGPointMake(_allAgreementButton.center.x, CGHeightFromIP6P(318) / 3 / 2)];
        [frameView addSubview:_allAgreementButton];
        // 1030

        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(320 / 3), frameView.frame.size.width, k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xd7d7d7)];
        [frameView addSubview:separatorView];
        
        
        UILabel *agreementLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(59 / 3), CGHeightFromIP6P(398) / 3, CGWidthFromIP6P(814) / 3, CGHeightFromIP6P(57) / 3)];
        [frameView addSubview:agreementLabel];

        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"MY좋은느낌 이용약관 동의 "]
                                                                                       attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(17)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x353535) }];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"(필수)"]
                                                                           attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }]];
        [agreementLabel setAttributedText:attrString];
        [agreementLabel sizeToFit];

        _agreementButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1030 / 3) - 12, agreementLabel.frame.origin.y - 12, 44, 44)];
        [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_agreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_agreementButton addTarget:self action:@selector(toggleAgreementButton:) forControlEvents:UIControlEventTouchUpInside];
        [frameView addSubview:_agreementButton];

        UIWebView *agreementWebView = [[UIWebView alloc] initWithFrame:CGRectMake((int)CGWidthFromIP6P(59 / 3), (int)CGHeightFromIP6P(495 / 3), (int)CGWidthFromIP6P(1037 / 3), (int)CGHeightFromIP6P(356 / 3))];
        [agreementWebView.layer setBorderColor:UIColorFromRGB(0xd7d7d7).CGColor];
        [agreementWebView.layer setBorderWidth:k1PX];
        [agreementWebView setScalesPageToFit:YES];
        [agreementWebView loadRequest:[NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:@"1" withExtension:@"html"]]];
        [frameView addSubview:agreementWebView];

        
        UILabel *privacyAgreementLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(59 / 3), CGHeightFromIP6P(919) / 3, CGWidthFromIP6P(814) / 3, CGHeightFromIP6P(57) / 3)];
        [frameView addSubview:privacyAgreementLabel];
        
        attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"개인정보 수집 이용 동의 "]
                                                            attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(17)),
                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x353535) }];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"(필수)"]
                                                                           attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }]];
        [privacyAgreementLabel setAttributedText:attrString];
        [privacyAgreementLabel sizeToFit];
        
        _privacyAgreementButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1030 / 3) - 12, privacyAgreementLabel.frame.origin.y - 12, 44, 44)];
        [_privacyAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_privacyAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_privacyAgreementButton addTarget:self action:@selector(toggleAgreementButton:) forControlEvents:UIControlEventTouchUpInside];
        [frameView addSubview:_privacyAgreementButton];
        
        UIWebView *privacyAgreementWebView = [[UIWebView alloc] initWithFrame:CGRectMake((int)CGWidthFromIP6P(59 / 3), (int)CGHeightFromIP6P(1026 / 3), (int)CGWidthFromIP6P(1037 / 3), (int)CGHeightFromIP6P(356 / 3))];
        [privacyAgreementWebView.layer setBorderColor:UIColorFromRGB(0xd7d7d7).CGColor];
        [privacyAgreementWebView.layer setBorderWidth:k1PX];
        [privacyAgreementWebView setScalesPageToFit:YES];
        [privacyAgreementWebView loadRequest:[NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:@"2" withExtension:@"html"]]];
        [frameView addSubview:privacyAgreementWebView];

        
        UILabel *marketingAgreementLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(59 / 3), CGHeightFromIP6P(1450) / 3, CGWidthFromIP6P(814) / 3, CGHeightFromIP6P(57) / 3)];
        [frameView addSubview:marketingAgreementLabel];
        
        attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"마케팅 목적 이용 동의 "]
                                                            attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(17)),
                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x353535) }];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"(선택)"]
                                                                           attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x8f8f8f) }]];
        [marketingAgreementLabel setAttributedText:attrString];
        [marketingAgreementLabel sizeToFit];
        
        _marketingAgreementButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1030 / 3) - 12, marketingAgreementLabel.frame.origin.y - 12, 44, 44)];
        [_marketingAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
        [_marketingAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
        [_marketingAgreementButton addTarget:self action:@selector(toggleAgreementButton:) forControlEvents:UIControlEventTouchUpInside];
        [frameView addSubview:_marketingAgreementButton];
        
        UIWebView *marketingAgreementWebView = [[UIWebView alloc] initWithFrame:CGRectMake((int)CGWidthFromIP6P(59 / 3), (int)CGHeightFromIP6P(1557 / 3), (int)CGWidthFromIP6P(1037 / 3), (int)CGHeightFromIP6P(356 / 3))];
        [marketingAgreementWebView.layer setBorderColor:UIColorFromRGB(0xd7d7d7).CGColor];
        [marketingAgreementWebView.layer setBorderWidth:k1PX];
        [marketingAgreementWebView setScalesPageToFit:YES];
        [marketingAgreementWebView loadRequest:[NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:@"3" withExtension:@"html"]]];
        [frameView addSubview:marketingAgreementWebView];


        
//        UILabel *privacyAgreementLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(43 / 3), CGHeightFromIP6P(61 / 3), CGWidthFromIP6P(300), CGHeightFromIP6P(51 / 3))];
//        [privacyAgreementLabel setTextColor:[UIColor blackColor]];
//        [privacyAgreementLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
//        [privacyAgreementLabel setText:@"개인정보 수집 동의"];
//        [privacyAgreementLabel sizeToFit];
//        [self.view addSubview:privacyAgreementLabel];
//
//        UIWebView *privacyAgreementWebView = [[UIWebView alloc] initWithFrame:CGRectMake((int)CGWidthFromIP6P(46 / 3), (int)CGHeightFromIP6P(172 / 3), (int)CGWidthFromIP6P(1159 / 3), (int)CGHeightFromIP6P(523 / 3))];
//        [privacyAgreementWebView setScalesPageToFit:YES];
//        [self.view addSubview:privacyAgreementWebView];
//
//        _privacyAgreementButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(919 / 3), CGHeightFromIP6P(731 / 3), CGWidthFromIP6P(300 / 3), CGHeightFromIP6P(94 / 3))];
//        [_privacyAgreementButton setTitleColor:UIColorFromRGB(0xa7a7a7) forState:UIControlStateNormal];
//        [_privacyAgreementButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
//        [_privacyAgreementButton setTitle:@"동의합니다.   " forState:UIControlStateNormal];
//        [_privacyAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
//        [_privacyAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
//        [_privacyAgreementButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
//        [_privacyAgreementButton addTarget:self action:@selector(toggleAgreementButton:) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:_privacyAgreementButton];
//
//        // 46 172 1159 523
//        UILabel *marketingAgreementLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(43 / 3), CGHeightFromIP6P(940 / 3), CGWidthFromIP6P(300), CGHeightFromIP6P(51 / 3))];
//        [marketingAgreementLabel setTextColor:[UIColor blackColor]];
//        [marketingAgreementLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
//        [marketingAgreementLabel setText:@"제 3자 정보제공 동의"];
//        [marketingAgreementLabel sizeToFit];
//        [self.view addSubview:marketingAgreementLabel];
//
//        UIWebView *marketingAgreementWebView = [[UIWebView alloc] initWithFrame:CGRectMake((int)CGWidthFromIP6P(46 / 3), (int)CGHeightFromIP6P(1051 / 3), (int)CGWidthFromIP6P(1159 / 3), (int)CGHeightFromIP6P(523 / 3))];
//        [marketingAgreementWebView setScalesPageToFit:YES];
//        [self.view addSubview:marketingAgreementWebView];
//
//        _marketingAgreementButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(919 / 3), CGHeightFromIP6P(1631 / 3), CGWidthFromIP6P(300 / 3), CGHeightFromIP6P(94 / 3))];
//        [_marketingAgreementButton setTitleColor:UIColorFromRGB(0xa7a7a7) forState:UIControlStateNormal];
//        [_marketingAgreementButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
//        [_marketingAgreementButton setTitle:@"동의합니다.   " forState:UIControlStateNormal];
//        [_marketingAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox"] forState:UIControlStateNormal];
//        [_marketingAgreementButton setImage:[UIImage imageNamed:@"agreement_button_checkbox_h"] forState:UIControlStateHighlighted];
//        [_marketingAgreementButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
//        [_marketingAgreementButton addTarget:self action:@selector(toggleAgreementButton:) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:_marketingAgreementButton];

        
        
        _agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - kTOP_HEIGHT - kBOTTOM_HEIGHT - CGHeightFromIP6P(184 / 3), kSCREEN_WIDTH, CGHeightFromIP6P(184 / 3) + kBOTTOM_HEIGHT)];
        [_agreeButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf27f8e)] forState:UIControlStateNormal];
        [_agreeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_agreeButton.titleLabel setFont:MediumWithSize(CGHeightFromIP6P(20))];
        [_agreeButton setTitle:@"다음" forState:UIControlStateNormal];
        [_agreeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, kBOTTOM_HEIGHT, 0)];
        [_agreeButton addTarget:self action:@selector(detachAgreementViewController) forControlEvents:UIControlEventTouchUpInside];
//        [_agreeButton setEnabled:NO];
        [self.view addSubview:_agreeButton];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
