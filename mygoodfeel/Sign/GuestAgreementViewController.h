//
//  GuestAgreementViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 14..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"

@interface GuestAgreementViewController : UIViewController
{
    BOOL isCheckedAgreement;
    BOOL isCheckedPrivacy;
    BOOL isCheckedMarketing;
}

@property (strong, nonatomic) id pClass;

@property (strong, nonatomic) UIScrollView *mainScrollView;

@property (strong, nonatomic) UIButton *allAgreementButton;
@property (strong, nonatomic) UIButton *agreementButton;
@property (strong, nonatomic) UIButton *privacyAgreementButton;
@property (strong, nonatomic) UIButton *marketingAgreementButton;

@property (strong, nonatomic) UIButton *agreeButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
