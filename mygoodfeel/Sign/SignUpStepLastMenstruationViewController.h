//
//  SignUpStepLastMenstruationViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "DataSingleton.h"
#import "SignUpStepAveragePeriodViewController.h"
#import "RangePickerCell.h"

#import <JTCalendar/JTCalendar.h>

@interface SignUpStepLastMenstruationViewController : UIViewController <JTCalendarDelegate>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) SignUpStepAveragePeriodViewController *signUpStepAveragePeriodViewController;

@property (strong, nonatomic) UILabel *eventLabel;
@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) NSDate *displayingMonthDate;
@property (strong, nonatomic) NSDate *selectedDate;


@property (strong, nonatomic) JTCalendarMenuView *calendarMenuView;
@property (strong, nonatomic) JTHorizontalCalendarView *calendarContentView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
