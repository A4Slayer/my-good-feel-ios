//
//  SignUpStepAgeViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SignUpStepAgeViewController.h"

@interface SignUpStepAgeViewController ()

@end

@implementation SignUpStepAgeViewController

- (void)backToWelcomeViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushStepLastMenstruationViewController
{
    _signUpStepLastMenstruationViewController = [[SignUpStepLastMenstruationViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    [self.navigationController pushViewController:_signUpStepLastMenstruationViewController animated:YES];
}

- (void)skipAge
{
    [self pushStepLastMenstruationViewController];
}

- (void)setAge
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_SET_USER_BASIC]];
    
    [urlString appendFormat:@"/%@", @"101"];        // age
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];           // ID
    [urlString appendFormat:@"/%@", [NSString stringWithFormat:@"%ld", [_agePickerView selectedRowInComponent:0] + 14]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        switch ([responseData[@"code"] intValue]) {
            case 0:
                [self pushStepLastMenstruationViewController];
                break;
                
            default:
                [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:responseData[@"msg"][@"ko"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                break;
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        NSDictionary *responseData;
        if (error != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:[NSString stringWithFormat:@"%@", responseData] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setBackgroundColor:[UIColor clearColor]];
        [backButton setTitle:@"이전" forState:UIControlStateNormal];
        [backButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
        [backButton.titleLabel setFont:SemiBoldWithSize(16)];
        [backButton addTarget:self action:@selector(backToWelcomeViewController) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [self.navigationItem setLeftBarButtonItem:backBarButtonItem];

        UIButton *skipButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 55/3, 60/3)];
        [skipButton setBackgroundColor:[UIColor clearColor]];
        [skipButton setTitle:@"다음에 입력" forState:UIControlStateNormal];
        [skipButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
        [skipButton.titleLabel setFont:SemiBoldWithSize(16)];
        [skipButton addTarget:self action:@selector(skipAge) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *skipBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:skipButton];
        [self.navigationItem setRightBarButtonItem:skipBarButtonItem];


        UILabel *ageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 100)];
        [ageTitleLabel setBackgroundColor:[UIColor clearColor]];
        [ageTitleLabel setNumberOfLines:0];
        [ageTitleLabel setTextAlignment:NSTextAlignmentCenter];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"나이를 선택해주세요."]
                                                                                       attributes:@{ NSFontAttributeName : BoldWithSize(20),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }];
        [ageTitleLabel setAttributedText:attrString];
        [ageTitleLabel sizeToFit];
        [ageTitleLabel setCenter:CGPointMake(self.view.center.x, self.view.center.y - 240)];
        [self.view addSubview:ageTitleLabel];

        
        _agePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, kSCREEN_WIDTH, 280)];
        [_agePickerView setBackgroundColor:[UIColor clearColor]];
        [_agePickerView setCenter:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2 + 60)];
        [_agePickerView setDelegate:self];
        [_agePickerView setDataSource:self];
        [self.view addSubview:_agePickerView];
        
        UILabel *agePrefixLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
        [agePrefixLabel setBackgroundColor:[UIColor clearColor]];
        [agePrefixLabel setTextColor:UIColorFromRGB(0x4d4d4d)];
        [agePrefixLabel setText:@"만"];
        [agePrefixLabel setFont:RegularWithSize(22)];
        [agePrefixLabel sizeToFit];
        [agePrefixLabel setCenter:CGPointMake(_agePickerView.frame.size.width / 2 - 60, _agePickerView.frame.size.height / 2)];
        [_agePickerView addSubview:agePrefixLabel];
        
        UILabel *ageSuffixLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
        [ageSuffixLabel setBackgroundColor:[UIColor clearColor]];
        [ageSuffixLabel setTextColor:UIColorFromRGB(0x4d4d4d)];
        [ageSuffixLabel setText:@"세"];
        [ageSuffixLabel setFont:RegularWithSize(22)];
        [ageSuffixLabel sizeToFit];
        [ageSuffixLabel setCenter:CGPointMake(_agePickerView.frame.size.width / 2 + 40, _agePickerView.frame.size.height / 2)];
        [_agePickerView addSubview:ageSuffixLabel];

        
        UIButton *nextButton = [[UIButton alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - 80, kSCREEN_HEIGHT - kBOTTOM_HEIGHT - 50, 80, 50)];
        [nextButton setBackgroundColor:[UIColor clearColor]];
        [nextButton setTitle:@"다음" forState:UIControlStateNormal];
        [nextButton.titleLabel setFont:SemiBoldWithSize(16)];
        [nextButton setTitleColor:UIColorFromRGB(0xf27f8e) forState:UIControlStateNormal];
        [nextButton addTarget:self action:@selector(setAge) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextButton];
        
        
        UIPageControl *stepPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(80, kSCREEN_HEIGHT - kBOTTOM_HEIGHT - 50, kSCREEN_WIDTH - 80 - 80, 50)];
        [stepPageControl setNumberOfPages:4];
        [stepPageControl setCurrentPage:0];
        [stepPageControl setUserInteractionEnabled:NO];
        [stepPageControl setPageIndicatorTintColor:UIColorFromRGB(0xffd8dd)];
        [stepPageControl setCurrentPageIndicatorTintColor:UIColorFromRGB(0xf27f8e)];
        [self.view addSubview:stepPageControl];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// returns width of column and height of row for each component.
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 100;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 44;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel*)view;
    
    if (!pickerLabel) {
        pickerLabel = [[UILabel alloc] init];
        [pickerLabel setFont:BoldWithSize(28)];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setTextColor:UIColorFromRGB(0x4d4d4d)];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
    }
    [pickerLabel setText:[NSString stringWithFormat:@"%d", (int)row + 14]];
    
    return pickerLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 86;
}


@end
