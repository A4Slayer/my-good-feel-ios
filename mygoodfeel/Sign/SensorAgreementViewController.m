//
//  SensorAgreementViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SensorAgreementViewController.h"
#import "SignUpStepCheckSensorViewController.h"

@interface SensorAgreementViewController ()

@end

@implementation SensorAgreementViewController

- (void)detachAgreementViewController
{
    [(SignUpStepCheckSensorViewController *)_pClass procConnectSensor];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xfafafa)];
        
        [self setTitle:@"센서 연동 개인정보 수집 동의"];

        UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - kTOP_HEIGHT - kBOTTOM_HEIGHT - CGHeightFromIP6P(184 / 3), kSCREEN_WIDTH, CGHeightFromIP6P(184 / 3) + kBOTTOM_HEIGHT)];
        [agreeButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf27f8e)] forState:UIControlStateNormal];
        [agreeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [agreeButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(17))];
        [agreeButton setTitle:@"동의" forState:UIControlStateNormal];
        [agreeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, kBOTTOM_HEIGHT, 0)];
        [agreeButton addTarget:self action:@selector(detachAgreementViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:agreeButton];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
