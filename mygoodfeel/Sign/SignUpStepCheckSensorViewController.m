//
//  SignUpStepCheckSensorViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SignUpStepCheckSensorViewController.h"
#import "SignViewController.h"

@interface SignUpStepCheckSensorViewController ()

@end

@implementation SignUpStepCheckSensorViewController

- (void)procConnectSensor
{
    [_sensorAgreementNavigationController dismissViewControllerAnimated:YES completion:^{
        SignViewController *signViewController = (SignViewController *)self.navigationController.presentingViewController;
        [signViewController connectSensorViewController];
    }];
}

- (void)skipConnectSensor
{
    SignViewController *signViewController = (SignViewController *)self.navigationController.presentingViewController;
    [signViewController dismissWelcomeViewController];
}

- (void)presentSensorAgreementViewController
{
    _sensorAgreementViewController = [[SensorAgreementViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    _sensorAgreementNavigationController = [[UINavigationController alloc] initWithRootViewController:_sensorAgreementViewController];
    [_sensorAgreementNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffffff)]
                                                           forBarPosition:UIBarPositionAny
                                                               barMetrics:UIBarMetricsDefault];

    [self.navigationController presentViewController:_sensorAgreementNavigationController animated:YES completion:^{
        //
    }];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIView alloc] init]];
        [self.navigationItem setLeftBarButtonItem:backBarButtonItem];

        UILabel *sensorTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 100)];
        [sensorTitleLabel setBackgroundColor:[UIColor clearColor]];
        [sensorTitleLabel setNumberOfLines:0];
        [sensorTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"수면정보 측정 센서를\n갖고계세요?"]
                                                                                       attributes:@{ NSFontAttributeName : BoldWithSize(20),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }];
        [sensorTitleLabel setAttributedText:attrString];
        [sensorTitleLabel sizeToFit];
        [sensorTitleLabel setCenter:CGPointMake(self.view.center.x, self.view.center.y - 240)];
        [self.view addSubview:sensorTitleLabel];

        
        UIImageView *sensorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signup_image_sensor"]];
        [sensorImageView setCenter:CGPointMake(self.view.frame.size.width / 2, sensorTitleLabel.center.y + 140)];
        [self.view addSubview:sensorImageView];
        
        
        UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 80 - CGHeightFromIP6P(320 / 3), CGWidthFromIP6P(320 / 3), CGHeightFromIP6P(320 / 3))];
        [okButton setImage:[UIImage imageNamed:@"signup_button_yes"] forState:UIControlStateNormal];
        [okButton setImage:[UIImage imageNamed:@"signup_button_yes_h"] forState:UIControlStateHighlighted];
        [okButton setCenter:CGPointMake(self.view.frame.size.width / 2 - CGWidthFromIP6P(34 / 3) - CGWidthFromIP6P(320 / 3 / 2), okButton.center.y)];
        [okButton addTarget:self action:@selector(presentSensorAgreementViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:okButton];
        
        UIButton *noButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 80 - CGHeightFromIP6P(320 / 3), CGWidthFromIP6P(320 / 3), CGHeightFromIP6P(320 / 3))];
        [noButton setImage:[UIImage imageNamed:@"signup_button_no"] forState:UIControlStateNormal];
        [noButton setImage:[UIImage imageNamed:@"signup_button_no_h"] forState:UIControlStateHighlighted];
        [noButton setCenter:CGPointMake(self.view.frame.size.width / 2 + CGWidthFromIP6P(34 / 3) + CGWidthFromIP6P(320 / 3 / 2), noButton.center.y)];
        [noButton addTarget:self action:@selector(skipConnectSensor) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:noButton];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
