//
//  SignUpWelcomeViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUpStepAgeViewController.h"

@interface SignUpWelcomeViewController : UIViewController

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) SignUpStepAgeViewController *signUpStepAgeViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
