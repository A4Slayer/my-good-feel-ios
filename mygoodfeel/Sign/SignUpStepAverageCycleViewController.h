//
//  SignUpStepAverageCycleViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNSTextAttachment.h"
#import "SignUpStepCheckSensorViewController.h"

@interface SignUpStepAverageCycleViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UIPickerView *cyclePickerView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
