//
//  SignViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SignViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface SignViewController ()

@end

@implementation SignViewController

- (void)attachMainViewController
{
    [(RootViewController *)self->_pClass attachMainViewController];
}

- (void)connectSensorViewController
{
    [_signUpNavigationController dismissViewControllerAnimated:YES completion:^{
        [(RootViewController *)self->_pClass attachConnectSensorViewController];
    }];
}

- (void)dismissWelcomeViewController
{
    [_signUpNavigationController dismissViewControllerAnimated:YES completion:^{
        [self attachMainViewController];
    }];
}

- (void)forceWelcomeView
{
    self->_signUpWelcomeViewController = [[SignUpWelcomeViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    
    self->_signUpNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_signUpWelcomeViewController];
    [self->_signUpNavigationController.navigationBar setBackgroundImage:[UIImage new]
                                                          forBarMetrics:UIBarMetricsDefault];
    self->_signUpNavigationController.navigationBar.shadowImage = [UIImage new];
    self->_signUpNavigationController.navigationBar.translucent = YES;
    
    [self presentViewController:self->_signUpNavigationController animated:NO completion:^{
    }];
}

- (void)presentSignUpWelcomeViewController
{
    [_guestAgreementNavigationController dismissViewControllerAnimated:YES completion:^{
        self->_signUpWelcomeViewController = [[SignUpWelcomeViewController alloc] initWithNibName:nil bundle:nil pClass:self];
        
        self->_signUpNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_signUpWelcomeViewController];
        [self->_signUpNavigationController.navigationBar setBackgroundImage:[UIImage new]
                                                              forBarMetrics:UIBarMetricsDefault];
        self->_signUpNavigationController.navigationBar.shadowImage = [UIImage new];
        self->_signUpNavigationController.navigationBar.translucent = YES;
        
        [self presentViewController:self->_signUpNavigationController animated:NO completion:^{
        }];
    }];
}

- (void)doneAgreementView
{
    if (isSignUp) {
        self->_signUpWelcomeViewController = [[SignUpWelcomeViewController alloc] initWithNibName:nil bundle:nil pClass:self];
        
        self->_signUpNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_signUpWelcomeViewController];
        [self->_signUpNavigationController.navigationBar setBackgroundImage:[UIImage new]
                                                              forBarMetrics:UIBarMetricsDefault];
        self->_signUpNavigationController.navigationBar.shadowImage = [UIImage new];
        self->_signUpNavigationController.navigationBar.translucent = YES;
        
        [self presentViewController:self->_signUpNavigationController animated:NO completion:^{
        }];
    } else {
        [self attachMainViewController];
    }
}

- (void)presentGuestAgreementViewController
{
    _guestAgreementViewController = [[GuestAgreementViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    
    _guestAgreementNavigationController = [[UINavigationController alloc] initWithRootViewController:_guestAgreementViewController];
    [_guestAgreementNavigationController.navigationBar setTranslucent:NO];
    
    [self presentViewController:_guestAgreementNavigationController animated:YES completion:^{
        //
    }];
}

- (void)guestSignUp
{
    NSString *uniqueIdentifier = nil;
    
    uniqueIdentifier = [JNKeychain loadValueForKey:@"APPKNOT_UDID"];
    
    if (uniqueIdentifier == nil || uniqueIdentifier.length == 0) {
        if([UIDevice instancesRespondToSelector:@selector(identifierForVendor)]) {
            uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        } else {
            uniqueIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:@"identiferForVendor"];
            if( !uniqueIdentifier ) {
                CFUUIDRef uuid = CFUUIDCreate(NULL);
                uniqueIdentifier = (__bridge_transfer NSString*)CFUUIDCreateString(NULL, uuid);
                CFRelease(uuid);
                [[NSUserDefaults standardUserDefaults] setObject:uniqueIdentifier forKey:@"identifierForVendor"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        [JNKeychain saveValue:uniqueIdentifier forKey:@"APPKNOT_UDID"];
    }
    
    
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_FIRST_LOGIN]];
    
    [urlString appendFormat:@"/%@", uniqueIdentifier];
    [urlString appendFormat:@"/%@", uniqueIdentifier];
    [urlString appendFormat:@"/2"]; // os
    [urlString appendFormat:@"/102"];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        switch ([responseData[@"code"] intValue]) {
            case 0: {
                [[DataSingleton sharedSingletonClass] setUserInfo:responseData[@"data"]];
                if (IS_HIDE_SLEEP_PART) {
                    [[NSUserDefaults standardUserDefaults] setObject:uniqueIdentifier forKey:@"userid"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [[DataSingleton sharedSingletonClass] setBadgeString:responseData[@"data"][@"ReadCnt"]];
                    [self presentGuestAgreementViewController];
                } else {
                    [[UserManager sharedInstance] registerWithEmail:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserName"]
                                                        andPassword:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserPw"]
                                                     withCompletion:^(enum NetworkAPIStatusCode returnCode) {
                                                         if (returnCode == NetworkAPIStatusCodeUserAlreadyExists || returnCode == NetworkAPIStatusCodeSuccess) {
                                                             [[UserManager sharedInstance] loginWithEmail:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserName"]
                                                                                              andPassword:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserPw"]
                                                                                               customKeys:nil
                                                                                                    force:YES
                                                                                           withCompletion:^(enum NetworkAPIStatusCode returnCode, User *userData) {
                                                                                               
                                                                                               [[HistoryManager sharedInstance] loadAllSummariesWithProgress:^(double downloadProgress) {
                                                                                                   
                                                                                               } andCompletion:^(enum NetworkAPIStatusCode code) {
                                                                                                   
                                                                                                   [[BLEManager sharedInstance] stopScanForSensors];
                                                                                                   //                                                                                                   NSLog(@"last connect : %@", [[BLEManager sharedInstance] getLastConnectedSensor]);
                                                                                                   [[NSUserDefaults standardUserDefaults] setObject:uniqueIdentifier forKey:@"userid"];
                                                                                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                                                                                   
                                                                                                   [[DataSingleton sharedSingletonClass] setBadgeString:responseData[@"data"][@"ReadCnt"]];
                                                                                                   [self presentGuestAgreementViewController];
                                                                                                   //
                                                                                               }];
                                                                                           }];
                                                         } else {
                                                             NSLog(@"login fail !! %d", returnCode);
                                                         }
                                                     }];
                }
                
                [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                         @"PageId" : @"Login",
                                                                         @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                         @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                                   @"Login",
                                                                                   [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                                   ]
                                                                         }];
                
                break;
            }

            default:
                [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:responseData[@"msg"][@"ko"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                break;
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *responseData;
        if (error != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:[NSString stringWithFormat:@"%@", responseData] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];
    
}

- (void)removeWebView:(UIButton *)button
{
    [button removeFromSuperview];
    
    if (_loginView) {
        [_loginView removeFromSuperview];
        _loginView = nil;
    }
    [_webView removeFromSuperview];
    _webView = nil;
}

- (void)presentSignUpWebView
{
    isSignUp = YES;
    
    _loginView = [[UIView alloc] initWithFrame:self.view.bounds];
    [_loginView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_loginView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kSTATUSBAR_HEIGHT, kSCREEN_WIDTH, kNAVIGATIONBAR_HEIGHT)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:[UIColor blackColor]];
    [titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(18))];
    [titleLabel setText:@"유한킴벌리 통합회원 로그인"];
    [_loginView addSubview:titleLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, k1PX)];
    [separatorView setBackgroundColor:[UIColor darkGrayColor]];
    [_loginView addSubview:separatorView];

    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT + k1PX, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT - k1PX)];
    [_webView setScalesPageToFit:YES];
    [_webView setDelegate:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[[DataSingleton sharedSingletonClass] signUpURL]]];
    [request setTimeoutInterval:10];
    [_webView loadRequest:request];
    [_loginView addSubview:_webView];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(5, kSTATUSBAR_HEIGHT, 44, 44)];
    [closeButton setImage:[UIImage imageNamed:@"common_button_close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(removeWebView:) forControlEvents:UIControlEventTouchUpInside];
    [_loginView addSubview:closeButton];
}

- (void)presentLoginWebView
{
    _loginView = [[UIView alloc] initWithFrame:self.view.bounds];
    [_loginView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_loginView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kSTATUSBAR_HEIGHT, kSCREEN_WIDTH, kNAVIGATIONBAR_HEIGHT)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:[UIColor blackColor]];
    [titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(18))];
    [titleLabel setText:@"유한킴벌리 통합회원 로그인"];
    [_loginView addSubview:titleLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, k1PX)];
    [separatorView setBackgroundColor:[UIColor darkGrayColor]];
    [_loginView addSubview:separatorView];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT + k1PX, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT - k1PX)];
    [_webView setScalesPageToFit:YES];
    [_webView setDelegate:self];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[DataSingleton sharedSingletonClass] loginURL]]]];
    [_loginView addSubview:_webView];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(5, kSTATUSBAR_HEIGHT, 44, 44)];
    [closeButton setImage:[UIImage imageNamed:@"common_button_close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(removeWebView:) forControlEvents:UIControlEventTouchUpInside];
    [_loginView addSubview:closeButton];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        isSignUp = YES;
        [self guestSignUp];
    }
}

- (void)presentGuestAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"비회원으로 서비스를 이용할 경우\n입력한 주기정보가 기기 변경 시\n삭제될 수 있습니다."
                                                       delegate:self
                                              cancelButtonTitle:@"취소"
                                              otherButtonTitles:@"확인", nil];

    [alertView show];
}

- (void)startAnimate
{
    [UIView animateWithDuration:0.6f animations:^{
        [self->_beforeBGImageView setAlpha:0];
    } completion:^(BOOL finished) {
    }];
    
    [UIView animateWithDuration:1.6f animations:^{
        [self->_afterBGImageView setAlpha:1];
    } completion:^(BOOL finished) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]) {
            [self getUserInfo];
        } else {
            [UIView animateWithDuration:1.0f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                [self->_beforeTitleImageView setFrame:CGRectMake(self->_beforeTitleImageView.frame.origin.x, self->_beforeTitleImageView.frame.origin.y - 140, self->_beforeTitleImageView.frame.size.width, self->_beforeTitleImageView.frame.size.height)];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.6f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    [self->_loginButton setAlpha:1];
                    [self->_guestButton setAlpha:1];
                    [self->_signUpButton setAlpha:1];
                } completion:^(BOOL finished) {
                }];
            }];
        }
    }];
    
    [UIView animateWithDuration:0.4f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self->_afterTitleImageView setAlpha:1];
        [self->_beforeCopyrightImageView setAlpha:0];
        [self->_afterCopyrightImageView setAlpha:1];
    } completion:^(BOOL finished) {
    }];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        isSignUp = NO;

        float bgRatio = self.view.frame.size.height / [UIImage imageNamed:@"splash_image_bg_before"].size.height;
        
        if (bgRatio < 1) {
            bgRatio = self.view.frame.size.width / [UIImage imageNamed:@"splash_image_bg_before"].size.width;
        }
        
        _beforeBGImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"splash_image_bg_before"].size.width * bgRatio, [UIImage imageNamed:@"splash_image_bg_before"].size.height * bgRatio)];
        [_beforeBGImageView setImage:[UIImage imageNamed:@"splash_image_bg_before"]];
        [_beforeBGImageView setCenter:self.view.center];
        [self.view addSubview:_beforeBGImageView];
        
        _afterBGImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"splash_image_bg_before"].size.width * bgRatio, [UIImage imageNamed:@"splash_image_bg_before"].size.height * bgRatio)];
        [_afterBGImageView setImage:[UIImage imageNamed:@"splash_image_bg_after"]];
        [_afterBGImageView setCenter:self.view.center];
        [_afterBGImageView setAlpha:0];
        [self.view addSubview:_afterBGImageView];
        
        _beforeTitleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash_image_title_before"]];
        [_beforeTitleImageView setCenter:self.view.center];
        [self.view addSubview:_beforeTitleImageView];
        
        _afterTitleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash_image_title_after"]];
        [_afterTitleImageView setAlpha:0];
        [_beforeTitleImageView addSubview:_afterTitleImageView];
        


        // 웹뷰 연결 후 콜백으로 회원가입이나 로그인 진행
//        _loginButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"splash_button_login"].size.width, [UIImage imageNamed:@"splash_button_login"].size.height)];
//        [_loginButton setImage:[UIImage imageNamed:@"splash_button_login"] forState:UIControlStateNormal];
//        [_loginButton setCenter:self.view.center];
//        [_loginButton addTarget:self action:@selector(presentLoginWebView) forControlEvents:UIControlEventTouchUpInside];
//        [_loginButton setAlpha:0];
//        [self.view addSubview:_loginButton];
        
        _loginButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"splash_button_login_rect"].size.width, [UIImage imageNamed:@"splash_button_login_rect"].size.height)];
        [_loginButton setImage:[UIImage imageNamed:@"splash_button_login_rect"] forState:UIControlStateNormal];
        [_loginButton setCenter:CGPointMake(self.view.center.x, self.view.center.y - CGHeightFromIP6P(40))];
        [_loginButton setAlpha:0];
        [_loginButton addTarget:self action:@selector(presentLoginWebView) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_loginButton];
        
        _guestButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"splash_button_guest_rect"].size.width, [UIImage imageNamed:@"splash_button_guest_rect"].size.height)];
        [_guestButton setImage:[UIImage imageNamed:@"splash_button_guest_rect"] forState:UIControlStateNormal];
        [_guestButton setCenter:CGPointMake(self.view.center.x, self.view.center.y + CGHeightFromIP6P(40))];
        [_guestButton setAlpha:0];
        [_guestButton addTarget:self action:@selector(presentGuestAlert) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_guestButton];

        
        
        _signUpButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(200), CGHeightFromIP6P(30))];
        [_signUpButton setBackgroundColor:[UIColor clearColor]];
        [_signUpButton setTitle:@"아직 회원이 아니세요? ＞" forState:UIControlStateNormal];
        [_signUpButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [_signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_signUpButton setCenter:CGPointMake(_loginButton.center.x, _loginButton.center.y + _loginButton.frame.size.height / 2 + CGHeightFromIP6P(140))];
        [_signUpButton setAlpha:0];
        [_signUpButton addTarget:self action:@selector(presentSignUpWebView) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_signUpButton];
        
        _beforeCopyrightImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash_image_copyright_before"]];
        [_beforeCopyrightImageView setCenter:CGPointMake(self.view.center.x, self.view.frame.size.height - 50)];
        [self.view addSubview:_beforeCopyrightImageView];
        
        _afterCopyrightImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash_image_copyright_after"]];
        [_afterCopyrightImageView setFrame:_beforeCopyrightImageView.frame];
        [_afterCopyrightImageView setAlpha:0];
        [self.view addSubview:_afterCopyrightImageView];

        [self performSelector:@selector(startAnimate) withObject:nil afterDelay:1.1f];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getUserInfo
{
    NSString *uniqueIdentifier = nil;
    
    uniqueIdentifier = [JNKeychain loadValueForKey:@"APPKNOT_UDID"];
    
    if (uniqueIdentifier == nil || uniqueIdentifier.length == 0) {
        if([UIDevice instancesRespondToSelector:@selector(identifierForVendor)]) {
            uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        } else {
            uniqueIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:@"identiferForVendor"];
            if( !uniqueIdentifier ) {
                CFUUIDRef uuid = CFUUIDCreate(NULL);
                uniqueIdentifier = (__bridge_transfer NSString*)CFUUIDCreateString(NULL, uuid);
                CFRelease(uuid);
                [[NSUserDefaults standardUserDefaults] setObject:uniqueIdentifier forKey:@"identifierForVendor"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        [JNKeychain saveValue:uniqueIdentifier forKey:@"APPKNOT_UDID"];
    }
    
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_GET_USER_INFO]];
    
    [urlString appendFormat:@"/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    [urlString appendFormat:@"/%@", uniqueIdentifier];
    [urlString appendFormat:@"/2"]; // os

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }

        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        switch ([responseData[@"code"] intValue]) {
            case 0:
                if (![responseData[@"data"][@"GFTermsAgreeYN"] isEqualToString:@"Y"] ||
                    ![responseData[@"data"][@"MQTermsAgreeYN"] isEqualToString:@"Y"] ||
                    ![responseData[@"data"][@"TBTermsAgreeYN"] isEqualToString:@"Y"] ) {
                    
                    [UIView animateWithDuration:1.0f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                        [self->_beforeTitleImageView setFrame:CGRectMake(self->_beforeTitleImageView.frame.origin.x, self->_beforeTitleImageView.frame.origin.y - 140, self->_beforeTitleImageView.frame.size.width, self->_beforeTitleImageView.frame.size.height)];
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:0.6f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                            [self->_loginButton setAlpha:1];
                            [self->_guestButton setAlpha:1];
                            [self->_signUpButton setAlpha:1];
                        } completion:^(BOOL finished) {
                        }];
                    }];
                } else {
                    [[DataSingleton sharedSingletonClass] setUserInfo:responseData[@"data"]];
                    if (IS_HIDE_SLEEP_PART) {
                        [[DataSingleton sharedSingletonClass] setBadgeString:responseData[@"data"][@"ReadCnt"]];
                        [(RootViewController *)self->_pClass attachMainViewController];
                    } else {
                        [[UserManager sharedInstance] registerWithEmail:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserName"]
                                                            andPassword:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserPw"]
                                                         withCompletion:^(enum NetworkAPIStatusCode returnCode) {
                                                             if (returnCode == NetworkAPIStatusCodeUserAlreadyExists) {
                                                                 [[UserManager sharedInstance] loginWithEmail:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserName"]
                                                                                                  andPassword:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserPw"]
                                                                                                   customKeys:nil
                                                                                                        force:YES
                                                                                               withCompletion:^(enum NetworkAPIStatusCode returnCode, User *userData) {
                                                                                                   
                                                                                                   [[HistoryManager sharedInstance] loadAllSummariesWithProgress:^(double downloadProgress) {
                                                                                                       
                                                                                                   } andCompletion:^(enum NetworkAPIStatusCode code) {
                                                                                                       
                                                                                                       [[BLEManager sharedInstance] stopScanForSensors];
                                                                                                       
                                                                                                       [[DataSingleton sharedSingletonClass] setBadgeString:responseData[@"data"][@"ReadCnt"]];
                                                                                                       [(RootViewController *)self->_pClass attachMainViewController];
                                                                                                       //
                                                                                                   }];
                                                                                               }];
                                                             } else {
                                                                 NSLog(@"login fail !! %d", returnCode);
                                                                 // 로그인 실패
                                                             }
                                                         }];
                    }

                    [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                             @"PageId" : @"Splash",
                                                                             @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                             @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                                       @"Splash",
                                                                                       [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                                       ]
                                                                             }];
                    
                }
                break;
                
            default:
                [[[UIAlertView alloc] initWithTitle:@"로그인 실패" message:@"로그인에 실패하였습니다.\n다시 시도해주시기 바랍니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                [UIView animateWithDuration:1.0f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    [self->_beforeTitleImageView setFrame:CGRectMake(self->_beforeTitleImageView.frame.origin.x, self->_beforeTitleImageView.frame.origin.y - 140, self->_beforeTitleImageView.frame.size.width, self->_beforeTitleImageView.frame.size.height)];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.6f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        [self->_loginButton setAlpha:1];
                        [self->_guestButton setAlpha:1];
                        [self->_signUpButton setAlpha:1];
                    } completion:^(BOOL finished) {
                    }];
                }];
                break;
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];

        NSLog(@"33");

        NSDictionary *responseData;
        if (error != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[[UIAlertView alloc] initWithTitle:@"로그인 실패" message:@"로그인에 실패하였습니다.\n다시 시도해주시기 바랍니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        [UIView animateWithDuration:1.0f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self->_beforeTitleImageView setFrame:CGRectMake(self->_beforeTitleImageView.frame.origin.x, self->_beforeTitleImageView.frame.origin.y - 140, self->_beforeTitleImageView.frame.size.width, self->_beforeTitleImageView.frame.size.height)];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.6f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                [self->_loginButton setAlpha:1];
                [self->_guestButton setAlpha:1];
                [self->_signUpButton setAlpha:1];
            } completion:^(BOOL finished) {
            }];
        }];
    }];
}

- (void)firstLogin:(NSString *)userID
{
    NSString *uniqueIdentifier = nil;
    
    uniqueIdentifier = [JNKeychain loadValueForKey:@"APPKNOT_UDID"];
    
    if (uniqueIdentifier == nil || uniqueIdentifier.length == 0) {
        if([UIDevice instancesRespondToSelector:@selector(identifierForVendor)]) {
            uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        } else {
            uniqueIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:@"identiferForVendor"];
            if( !uniqueIdentifier ) {
                CFUUIDRef uuid = CFUUIDCreate(NULL);
                uniqueIdentifier = (__bridge_transfer NSString*)CFUUIDCreateString(NULL, uuid);
                CFRelease(uuid);
                [[NSUserDefaults standardUserDefaults] setObject:uniqueIdentifier forKey:@"identifierForVendor"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        [JNKeychain saveValue:uniqueIdentifier forKey:@"APPKNOT_UDID"];
    }

    
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_FIRST_LOGIN]];
    
    [urlString appendFormat:@"/%@", userID];           // ID
    [urlString appendFormat:@"/%@", uniqueIdentifier];
    [urlString appendFormat:@"/2"];
    [urlString appendFormat:@"/101"];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];

    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        switch ([responseData[@"code"] intValue]) {
            case 0: {
                [[DataSingleton sharedSingletonClass] setUserInfo:responseData[@"data"]];
                if (IS_HIDE_SLEEP_PART) {
                    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"userid"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [[DataSingleton sharedSingletonClass] setBadgeString:responseData[@"data"][@"ReadCnt"]];
                    
                    if ([responseData[@"data"][@"AppTermAgreeYN"] isEqualToString:@"N"] || [responseData[@"data"][@"PersonalDataAgreeYN"] isEqualToString:@"N"]) {
                        [self presentGuestAgreementViewController];
                    } else {
                        [(RootViewController *)self->_pClass attachMainViewController];
                    }
                } else {
                    [[UserManager sharedInstance] registerWithEmail:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserName"]
                                                        andPassword:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserPw"]
                                                     withCompletion:^(enum NetworkAPIStatusCode returnCode) {
                                                         if (returnCode == NetworkAPIStatusCodeUserAlreadyExists) {
                                                             [[UserManager sharedInstance] loginWithEmail:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserName"]
                                                                                              andPassword:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserPw"]
                                                                                               customKeys:nil
                                                                                                    force:YES
                                                                                           withCompletion:^(enum NetworkAPIStatusCode returnCode, User *userData) {
                                                                                               [[HistoryManager sharedInstance] loadAllSummariesWithProgress:^(double downloadProgress) {
                                                                                               } andCompletion:^(enum NetworkAPIStatusCode code) {
                                                                                                   
                                                                                                   [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"userid"];
                                                                                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                                                                                   
                                                                                                   [[DataSingleton sharedSingletonClass] setBadgeString:responseData[@"data"][@"ReadCnt"]];
                                                                                                   
                                                                                                   if ([responseData[@"data"][@"AppTermAgreeYN"] isEqualToString:@"N"] || [responseData[@"data"][@"PersonalDataAgreeYN"] isEqualToString:@"N"]) {
                                                                                                       [self presentGuestAgreementViewController];
                                                                                                   } else {
                                                                                                       [(RootViewController *)self->_pClass attachMainViewController];
                                                                                                   }
                                                                                                   
                                                                                               }];
                                                                                           }];
                                                         }
                                                     }];
                }

                [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                         @"PageId" : @"Login",
                                                                         @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                         @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                                   @"Login",
                                                                                   [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                                   ]
                                                                         }];
            }
                break;
                
            default:
                [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:responseData[@"msg"][@"ko"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                break;
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *responseData;
        if (error != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:[NSString stringWithFormat:@"%@", responseData] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];

}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[request.URL scheme] isEqualToString:@"elle"]) {
        if (self->isSignUp) {
            if ([[[request.URL path] substringWithRange:NSMakeRange(1, [[request.URL path] length] - 1)] intValue] == 101) {
                // 가입 성공
                if (_loginView) {
                    [_loginView removeFromSuperview];
                    _loginView = nil;
                }
                if (_webView) {
                    [_webView removeFromSuperview];
                    _webView = nil;
                }
                
                _loginView = [[UIView alloc] initWithFrame:self.view.bounds];
                [_loginView setBackgroundColor:[UIColor whiteColor]];
                [self.view addSubview:_loginView];
                
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kSTATUSBAR_HEIGHT, kSCREEN_WIDTH, kNAVIGATIONBAR_HEIGHT)];
                [titleLabel setTextAlignment:NSTextAlignmentCenter];
                [titleLabel setBackgroundColor:[UIColor clearColor]];
                [titleLabel setTextColor:[UIColor blackColor]];
                [titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(18))];
                [titleLabel setText:@"유한킴벌리 통합회원 로그인"];
                [_loginView addSubview:titleLabel];
                
                UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, k1PX)];
                [separatorView setBackgroundColor:[UIColor darkGrayColor]];
                [_loginView addSubview:separatorView];
                
                _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT + k1PX, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT - k1PX)];
                [_webView setScalesPageToFit:YES];
                [_webView setDelegate:self];
                [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[DataSingleton sharedSingletonClass] loginURL]]]];
                [_loginView addSubview:_webView];
                
                UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(5, kSTATUSBAR_HEIGHT, 44, 44)];
                [closeButton setImage:[UIImage imageNamed:@"common_button_close"] forState:UIControlStateNormal];
                [closeButton addTarget:self action:@selector(removeWebView:) forControlEvents:UIControlEventTouchUpInside];
                [_loginView addSubview:closeButton];
            } else {
                // 가입 이후 최초 로그인일듯

                NSArray *resultArray = [request.URL.path componentsSeparatedByString:@"/"];
                [[NSUserDefaults standardUserDefaults] setObject:resultArray[2] forKey:@"cookie"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self firstLogin:resultArray[1]];
            }
            
        } else {
            NSArray *resultArray = [request.URL.path componentsSeparatedByString:@"/"];
            [[NSUserDefaults standardUserDefaults] setObject:resultArray[2] forKey:@"cookie"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self firstLogin:resultArray[1]];
        }
    }
    
    return YES;
}

@end
