//
//  CNSTextAttachment.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 30..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "CNSTextAttachment.h"

@implementation CNSTextAttachment

- (id)initWithMargin:(CGPoint)point
{
    self = [super init];
    if (self) {
        margin = point;
    }
    return self;
}

- (CGRect)attachmentBoundsForTextContainer:(NSTextContainer *)textContainer proposedLineFragment:(CGRect)lineFrag glyphPosition:(CGPoint)position characterIndex:(NSUInteger)charIndex
{
    CGRect bounds;
    bounds.origin = CGPointMake(margin.x, margin.y);
    bounds.size = self.image.size;
    return bounds;
}

@end
