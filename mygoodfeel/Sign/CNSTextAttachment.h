//
//  CNSTextAttachment.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 30..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNSTextAttachment : NSTextAttachment
{
    CGPoint margin;
}

- (id)initWithMargin:(CGPoint)point;

@end
