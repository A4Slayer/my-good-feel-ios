//
//  SignViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GuestAgreementViewController.h"
#import "SignUpWelcomeViewController.h"
#import "JNKeychain.h"

@interface SignViewController : UIViewController <UIWebViewDelegate, UIAlertViewDelegate>
{
    BOOL isSignUp;
}

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UINavigationController *agreementNavigationController;

@property (strong, nonatomic) GuestAgreementViewController *guestAgreementViewController;
@property (strong, nonatomic) UINavigationController *guestAgreementNavigationController;

@property (strong, nonatomic) SignUpWelcomeViewController *signUpWelcomeViewController;
@property (strong, nonatomic) UINavigationController *signUpNavigationController;

@property (strong, nonatomic) UIView *loginView;
@property (strong, nonatomic) UIWebView *webView;


@property (strong, nonatomic) UIImageView *beforeBGImageView;
@property (strong, nonatomic) UIImageView *afterBGImageView;
@property (strong, nonatomic) UIImageView *beforeTitleImageView;
@property (strong, nonatomic) UIImageView *afterTitleImageView;
@property (strong, nonatomic) UIImageView *beforeCopyrightImageView;
@property (strong, nonatomic) UIImageView *afterCopyrightImageView;

@property (strong, nonatomic) UIButton *loginButton;
@property (strong, nonatomic) UIButton *guestButton;

@property (strong, nonatomic) UIButton *signUpButton;

- (void)doneAgreementView;
- (void)presentSignUpWelcomeViewController;
- (void)connectSensorViewController;

- (void)dismissWelcomeViewController;
- (void)presentWelcomeViewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
