//
//  SignUpWelcomeViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SignUpWelcomeViewController.h"
#import "SignViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface SignUpWelcomeViewController ()

@end

@implementation SignUpWelcomeViewController

- (void)pushStepAgeViewController
{
//    [[BLEManager sharedInstance] stopScanForSensors];
    [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                             @"PageId" : @"UserInfoRegister",
                                                             @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                             @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                       @"UserInfoRegister",
                                                                       [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                       ]
                                                             }];

    _signUpStepAgeViewController = [[SignUpStepAgeViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    [self.navigationController pushViewController:_signUpStepAgeViewController animated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:UIColorFromRGB(0xfef5f6)];
        _pClass = pClass;
        
        
        UILabel *welcomeTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 100)];
        [welcomeTitleLabel setBackgroundColor:[UIColor clearColor]];
        [welcomeTitleLabel setNumberOfLines:0];
        [welcomeTitleLabel setTextAlignment:NSTextAlignmentCenter];

        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"환영합니다."]
                                                                                       attributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:23],
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }];
        [welcomeTitleLabel setAttributedText:attrString];
        [welcomeTitleLabel sizeToFit];
        [welcomeTitleLabel setCenter:CGPointMake(self.view.center.x, self.view.center.y - 200)];
        [self.view addSubview:welcomeTitleLabel];

        
        
        UILabel *welcomeDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(6), 0, kSCREEN_WIDTH - CGWidthFromIP6P(12), 100)];
        [welcomeDescLabel setBackgroundColor:[UIColor clearColor]];
        [welcomeDescLabel setNumberOfLines:0];
        [welcomeDescLabel setTextAlignment:NSTextAlignmentCenter];

        attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", @"이제 몇 개의 항목에 답해주세요.\n답해주시는 정보는 보다 정확한\n주기 정보 제공에 도움이 됩니다."]
                                                            attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:15],
                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x666666) }];
        [welcomeDescLabel setAttributedText:attrString];
        [welcomeDescLabel sizeToFit];
        [welcomeDescLabel setFrame:CGRectMake(0, welcomeTitleLabel.frame.origin.y + welcomeTitleLabel.frame.size.height + 6, kSCREEN_WIDTH, welcomeDescLabel.frame.size.height)];
        [self.view addSubview:welcomeDescLabel];

        
        UIButton *signInButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"signup_button_next"].size.width, [UIImage imageNamed:@"signup_button_next"].size.height)];
        [signInButton setImage:[UIImage imageNamed:@"signup_button_next"] forState:UIControlStateNormal];
        [signInButton setCenter:CGPointMake(self.view.center.x, self.view.frame.size.height - 100 - kBOTTOM_HEIGHT)];
        [signInButton addTarget:self action:@selector(pushStepAgeViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:signInButton];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
