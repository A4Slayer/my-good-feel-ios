//
//  SignUpStepLastMenstruationViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SignUpStepLastMenstruationViewController.h"

@interface SignUpStepLastMenstruationViewController ()

@end

@implementation SignUpStepLastMenstruationViewController

- (void)backToStepAgeViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushStepAveragePeriodViewController
{
    _signUpStepAveragePeriodViewController = [[SignUpStepAveragePeriodViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    [self.navigationController pushViewController:_signUpStepAveragePeriodViewController animated:YES];
}

- (void)skipLastMenstruation
{
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"skip_menstruation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self pushStepAveragePeriodViewController];
}

- (void)setLastMenstruation
{
    if (!_selectedDate) {
        [[[UIAlertView alloc] initWithTitle:nil message:@"마지막 주기가 시작된 날짜를 선택해주세요." delegate:nil cancelButtonTitle:@"닫기" otherButtonTitles:nil] show];
    } else {
        
        NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_SET_USER_BASIC]];

        NSDateFormatter *dateFormatter;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];

        [urlString appendFormat:@"/%@", @"102"];        // last menstrual day
        [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];           // ID
        [urlString appendFormat:@"/%@", [dateFormatter stringFromDate:_selectedDate]];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
        [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
        
        [[DataSingleton sharedSingletonClass] attachLoadingView];
        
        [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *responseData;
            NSError *error = nil;
            if (responseObject != nil) {
                responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                               options:NSJSONReadingMutableContainers
                                                                 error:&error];
            }
            
            [[DataSingleton sharedSingletonClass] detachLoadingView];
            
            switch ([responseData[@"code"] intValue]) {
                case 0:
                    [self pushStepAveragePeriodViewController];
                    break;
                    
                default:
                    [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:responseData[@"msg"][@"ko"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                    break;
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [[DataSingleton sharedSingletonClass] detachLoadingView];
            
            NSDictionary *responseData;
            if (error != nil) {
                responseData = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                               options:NSJSONReadingMutableContainers
                                                                 error:&error];
            }
            
            [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:[NSString stringWithFormat:@"%@", responseData] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }];

        
    }
}

- (void)prevMonth
{
    if (_selectedDate) {
        _selectedDate = nil;
    }

    NSCalendar *mCalendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear
                                                                   fromDate:_displayingMonthDate];
    [components setTimeZone:[NSTimeZone localTimeZone]];
    [components setMonth:[components month] - 1];
    _displayingMonthDate = [mCalendar dateFromComponents:components];
    [_calendarManager setDate:_displayingMonthDate];
    [_calendarManager reload];
}

- (void)nextMonth
{
    NSDateComponents *displayedComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear
                                                                            fromDate:_displayingMonthDate];
    [displayedComponents setTimeZone:[NSTimeZone localTimeZone]];
    NSDateComponents *currComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                                       fromDate:[NSDate date]];
    [currComponents setTimeZone:[NSTimeZone localTimeZone]];

    if ([displayedComponents year] >= [currComponents year] &&
        [displayedComponents month] >= [currComponents month]) {
        return;
    }

    if (_selectedDate) {
        _selectedDate = nil;
    }

    NSCalendar *mCalendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear
                                                                   fromDate:_displayingMonthDate];
    [components setTimeZone:[NSTimeZone localTimeZone]];
    [components setMonth:[components month] + 1];
    _displayingMonthDate = [mCalendar dateFromComponents:components];
    [_calendarManager setDate:_displayingMonthDate];
    [_calendarManager reload];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];

        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setBackgroundColor:[UIColor clearColor]];
        [backButton setTitle:@"이전" forState:UIControlStateNormal];
        [backButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
        [backButton.titleLabel setFont:SemiBoldWithSize(16)];
        [backButton addTarget:self action:@selector(backToStepAgeViewController) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [self.navigationItem setLeftBarButtonItem:backBarButtonItem];
        
        UIButton *skipButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 55/3, 60/3)];
        [skipButton setBackgroundColor:[UIColor clearColor]];
        [skipButton setTitle:@"잘 모르겠어요" forState:UIControlStateNormal];
        [skipButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
        [skipButton.titleLabel setFont:SemiBoldWithSize(16)];
        [skipButton addTarget:self action:@selector(skipLastMenstruation) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *skipBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:skipButton];
        [self.navigationItem setRightBarButtonItem:skipBarButtonItem];
        
        
        UILabel *lastMenstruationTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 100)];
        [lastMenstruationTitleLabel setBackgroundColor:[UIColor clearColor]];
        [lastMenstruationTitleLabel setNumberOfLines:0];
        [lastMenstruationTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"마지막 주기는 언제 시작되었나요?"]
                                                                                       attributes:@{ NSFontAttributeName : BoldWithSize(20),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }];
        [lastMenstruationTitleLabel setAttributedText:attrString];
        [lastMenstruationTitleLabel sizeToFit];
        [lastMenstruationTitleLabel setCenter:CGPointMake(self.view.center.x, self.view.center.y - 240)];
        [self.view addSubview:lastMenstruationTitleLabel];

        _displayingMonthDate = [NSDate date];


        _calendarManager = [[JTCalendarManager alloc] initWithLocale:[NSLocale localeWithLocaleIdentifier:@"ko-KP"] andTimeZone:[NSTimeZone localTimeZone]];
        _calendarManager.delegate = self;
        
        _calendarMenuView = [[JTCalendarMenuView alloc] initWithFrame:CGRectMake(30, lastMenstruationTitleLabel.frame.origin.y + lastMenstruationTitleLabel.frame.size.height + 40, kSCREEN_WIDTH - 60, 54)];
        [self.view addSubview:_calendarMenuView];
        
        _calendarContentView = [[JTHorizontalCalendarView alloc] initWithFrame:CGRectMake(_calendarMenuView.frame.origin.x, _calendarMenuView.frame.origin.y + _calendarMenuView.frame.size.height, _calendarMenuView.frame.size.width, 360)];
        [self.view addSubview:_calendarContentView];
        
        // Generate random events sort by date using a dateformatter for the demonstration
        _calendarMenuView.contentRatio = .75;
        _calendarManager.settings.weekDayFormat = JTCalendarWeekDayFormatSingle;
        [_calendarManager.settings setPageViewNumberOfWeeks:6];
        [_calendarManager.settings setZeroPaddedDayFormat:NO];
        
        [_calendarManager setMenuView:_calendarMenuView];
        [_calendarManager setContentView:_calendarContentView];
        [_calendarManager setDate:[NSDate date]];

        UIButton *prevMonthButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 2, 30, 50)];
        [prevMonthButton setBackgroundColor:[UIColor clearColor]];
        [prevMonthButton setImage:[UIImage imageNamed:@"common_button_left"] forState:UIControlStateNormal];
        [prevMonthButton addTarget:self action:@selector(prevMonth) forControlEvents:UIControlEventTouchUpInside];
        [_calendarMenuView addSubview:prevMonthButton];

        UIButton *nextMonthButton = [[UIButton alloc] initWithFrame:CGRectMake(_calendarMenuView.frame.size.width - 0 - 30, 2, 30, 50)];
        [nextMonthButton setBackgroundColor:[UIColor clearColor]];
        [nextMonthButton setImage:[UIImage imageNamed:@"common_button_right"] forState:UIControlStateNormal];
        [nextMonthButton addTarget:self action:@selector(nextMonth) forControlEvents:UIControlEventTouchUpInside];
        [_calendarMenuView addSubview:nextMonthButton];

        
        
        UIButton *nextButton = [[UIButton alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - 80, kSCREEN_HEIGHT - kBOTTOM_HEIGHT - 50, 80, 50)];
        [nextButton setBackgroundColor:[UIColor clearColor]];
        [nextButton setTitle:@"다음" forState:UIControlStateNormal];
        [nextButton.titleLabel setFont:SemiBoldWithSize(16)];
        [nextButton setTitleColor:UIColorFromRGB(0xf27f8e) forState:UIControlStateNormal];
        [nextButton addTarget:self action:@selector(setLastMenstruation) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextButton];
        
        
        UIPageControl *stepPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(80, kSCREEN_HEIGHT - kBOTTOM_HEIGHT - 50, kSCREEN_WIDTH - 80 - 80, 50)];
        [stepPageControl setNumberOfPages:4];
        [stepPageControl setCurrentPage:1];
        [stepPageControl setUserInteractionEnabled:NO];
        [stepPageControl setPageIndicatorTintColor:UIColorFromRGB(0xffd8dd)];
        [stepPageControl setCurrentPageIndicatorTintColor:UIColorFromRGB(0xf27f8e)];
        [self.view addSubview:stepPageControl];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CalendarManager delegate

- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return NO;
}

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.hidden = NO;
    [dayView setBackgroundColor:[UIColor whiteColor]];

    // Other month
    if([dayView isFromAnotherMonth]){
        dayView.hidden = NO;
        [dayView.textLabel setTextColor:UIColorFromRGB(0xd3d3d3)];
    }
    else if (![_calendarManager.dateHelper date:[NSDate date] isEqualOrAfter:dayView.date]) {
        // 미래
        dayView.hidden = NO;
        [dayView.textLabel setTextColor:UIColorFromRGB(0xd3d3d3)];
    }
////     Today
////    else if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
////        dayView.circleView.hidden = YES;
////        dayView.circleView.backgroundColor = [UIColor blueColor];
////        dayView.dotView.backgroundColor = [UIColor whiteColor];
////        dayView.textLabel.textColor = [UIColor whiteColor];
////    }
    // Selected date
    else if(_selectedDate && [_calendarManager.dateHelper date:_selectedDate isTheSameDayThan:dayView.date]){
        [dayView setBackgroundColor:UIColorFromRGB(0xf27f8e)];
        [dayView.textLabel setTextColor:[UIColor whiteColor]];
//        dayView.circleView.hidden = NO;
//        dayView.circleView.backgroundColor = [UIColor redColor];
//        dayView.dotView.backgroundColor = [UIColor whiteColor];
    }
    // Another day of the current month
    else{
        [dayView.textLabel setTextColor:UIColorFromRGB(0x636363)];
    }
    
//    if([self haveEventForDay:dayView.date]){
//        dayView.dotView.hidden = NO;
//    }
//    else{
        dayView.dotView.hidden = YES;
//    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]) {
        if ([_calendarManager.dateHelper date:[NSDate date] isEqualOrAfter:dayView.date]) {
            _displayingMonthDate = dayView.date;
            _selectedDate = dayView.date;
            [_calendarManager setDate:_selectedDate];
            [_calendarManager reload];
            return;
        } else {
            return;
        }
    } else if (![_calendarManager.dateHelper date:[NSDate date] isEqualOrAfter:dayView.date]) {
        // 같은 달이어도 현재보다 미래라면 리턴
        return;
    }
    
    _selectedDate = dayView.date;
    [self->_calendarManager reload];

}

#pragma mark - Views customization

- (UIView *)calendarBuildMenuItemView:(JTCalendarManager *)calendar
{
    UILabel *label = [UILabel new];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Avenir-Medium" size:16];
    
    return label;
}

- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy년 M월";
        
        dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    
    [menuItemView setFont:MediumWithSize(18)];
    [menuItemView setTextColor:UIColorFromRGB(0x636363)];
    menuItemView.text = [dateFormatter stringFromDate:date];
}

- (UIView<JTCalendarWeekDay> *)calendarBuildWeekDayView:(JTCalendarManager *)calendar
{
    JTCalendarWeekDayView *view = [JTCalendarWeekDayView new];
    
    for(UILabel *label in view.dayViews){
        [label setTextColor:UIColorFromRGB(0xb1b1b1)];
        [label setFont:SemiBoldWithSize(14)];
    }
    
    return view;
}

- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
{
    JTCalendarDayView *view = [JTCalendarDayView new];
    
    [view.layer setBorderColor:UIColorFromRGB(0xebebeb).CGColor];
    [view.layer setBorderWidth:k1PX];

    [view.textLabel setFont:SemiBoldWithSize(14)];
    [view.textLabel setTextColor:UIColorFromRGB(0x636363)];
    
    view.circleRatio = .8;
    view.dotRatio = 1. / .9;
    
    return view;
}
#pragma mark - Fake data

// Used only to have a key for _eventsByDate
//- (NSDateFormatter *)dateFormatter
//{
//    static NSDateFormatter *dateFormatter;
//    if(!dateFormatter){
//        dateFormatter = [NSDateFormatter new];
//        dateFormatter.dateFormat = @"dd-MM-yyyy";
//    }
//
//    return dateFormatter;
//}
//
//- (BOOL)haveEventForDay:(NSDate *)date
//{
//    NSString *key = [[self dateFormatter] stringFromDate:date];
//
//    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
//        return YES;
//    }
//
//    return NO;
//
//}
//
//- (void)createRandomEvents
//{
//    _eventsByDate = [NSMutableDictionary new];
//
//    for(int i = 0; i < 30; ++i){
//        // Generate 30 random dates between now and 60 days later
//        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
//
//        // Use the date as key for eventsByDate
//        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
//
//        if(!_eventsByDate[key]){
//            _eventsByDate[key] = [NSMutableArray new];
//        }
//
//        [_eventsByDate[key] addObject:randomDate];
//    }
//}


@end

