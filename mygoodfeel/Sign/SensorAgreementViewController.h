//
//  SensorAgreementViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"

@interface SensorAgreementViewController : UIViewController

@property (strong, nonatomic) id pClass;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
