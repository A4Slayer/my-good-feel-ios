//
//  SignUpStepAveragePeriodViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "DataSingleton.h"
#import "SignUpStepAverageCycleViewController.h"

@interface SignUpStepAveragePeriodViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UIPickerView *periodPickerView;

@property (strong, nonatomic) SignUpStepAverageCycleViewController *signUpStepAverageCycleViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
