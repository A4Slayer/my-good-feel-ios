//
//  WebViewNavigationController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 8..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"
#import "DataSingleton.h"

@interface WebViewNavigationController : UINavigationController

@property (strong, nonatomic) WebViewController *webViewController;

- (id)initWithURL:(NSString *)urlString title:(NSString *)title;

@end
