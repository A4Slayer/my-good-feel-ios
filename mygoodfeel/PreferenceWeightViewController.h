//
//  PreferenceWeightViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 8..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"
#import "AFNetworking.h"

@interface PreferenceWeightViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) UITableView *mainTableView;
@property (strong, nonatomic) UISwitch *weightNotificationSwitch;

@property (strong, nonatomic) UIPickerView *timePickerView;
@property (strong, nonatomic) UIView *pickerResignView;
                      
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@end
