//
//  WebViewNavigationController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 8..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "WebViewNavigationController.h"

@interface WebViewNavigationController ()

@end

@implementation WebViewNavigationController

- (id)initWithURL:(NSString *)urlString title:(NSString *)title
{
    WebViewController *webViewController = [[WebViewController alloc] initWithNibName:nil bundle:nil webURL:urlString title:title];
    self = [super initWithRootViewController:webViewController];
    if (self) {
        [self.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                forBarPosition:UIBarPositionAny
                                    barMetrics:UIBarMetricsDefault];
        [self.navigationBar setShadowImage:[[UIImage alloc] init]];
//        [self.navigationBar setTranslucent:NO];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
