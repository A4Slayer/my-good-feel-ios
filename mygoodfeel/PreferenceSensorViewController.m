//
//  PreferenceSensorViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 4. 16..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "PreferenceSensorViewController.h"

@interface PreferenceSensorViewController ()

@end

@implementation PreferenceSensorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_mainTableView reloadData];
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"센서 설정"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x3c3c3e)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
        [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT) style:UITableViewStyleGrouped];
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setTableFooterView:[[UIView alloc] init]];
        [self.view addSubview:_mainTableView];
        
    }
    return self;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark -
#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        ConnectSensorPreviousViewController *connectSensorPreviousViewController = [[ConnectSensorPreviousViewController alloc] initWithNibName:nil bundle:nil pClass:self];
        [self.navigationController pushViewController:connectSensorPreviousViewController animated:YES];
    } else if (indexPath.row == 1) {
        PreferenceSensorNotificaionViewController *preferenceSensorNotificaionViewController = [[PreferenceSensorNotificaionViewController alloc] initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:preferenceSensorNotificaionViewController animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 60;
    } else if (indexPath.row == 1) {
        return 76;
    }
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [NSString stringWithFormat:@"Cell_%ld_%ld", (long)indexPath.section, (long)indexPath.row];
    if (indexPath.row == 0) {
        cellId = [NSString stringWithFormat:@"Cell_%ld_%ld_%d", (long)indexPath.section, (long)indexPath.row, [[BLEManager sharedInstance] isConnectedToSensor]];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            [cell setBackgroundColor:[UIColor whiteColor]];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 11, CGWidthFromIP6P(300), 19)];
            [titleLabel setText:@"센서 연동"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:titleLabel];
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, CGWidthFromIP6P(300), 23)];
            if ([[BLEManager sharedInstance] isConnectedToSensor]) {
                [descLabel setText:[[BLEManager sharedInstance] getConnectedSensor][@"keyBLEPeripheralName"]];
            } else {
                [descLabel setText:@"연동되어있지 않습니다."];
            }
            [descLabel setTextColor:[UIColor lightGrayColor]];
            [descLabel setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:descLabel];

        }
        return cell;
    } else if (indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            [cell setBackgroundColor:[UIColor whiteColor]];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 11, CGWidthFromIP6P(300), 19)];
            [titleLabel setText:@"알림 설정"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:titleLabel];
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, CGWidthFromIP6P(300), 23)];
            [descLabel setText:[NSString stringWithFormat:@"심박수, 호흡주기가 기준치를 넘거나 모자랄 때\n푸시알림을 받습니다."]];
            [descLabel setTextColor:[UIColor lightGrayColor]];
            [descLabel setFont:[UIFont systemFontOfSize:14]];
            [descLabel setNumberOfLines:0];
            [descLabel sizeToFit];
            [cell.contentView addSubview:descLabel];
            
        }
        return cell;
    } else {
        NSString *cellId = [NSString stringWithFormat:@"DEFAULT"];
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        return cell;
    }
    
    return nil;
}

@end
