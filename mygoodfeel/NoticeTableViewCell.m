//
//  NoticeTableViewCell.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 5..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "NoticeTableViewCell.h"

@implementation NoticeTableViewCell

- (void)hideNewImage
{
    [_nImageView setAlpha:0];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier dataDictionary:(NSDictionary *)dataDictionary
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50) / 3, CGHeightFromIP6P(59) / 3, CGWidthFromIP6P(1035) / 3, CGHeightFromIP6P(48) / 3)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(16))];
        [titleLabel setTextColor:UIColorFromRGB(0x3c3c3e)];
        if ([dataDictionary[@"MainYN"] isEqualToString:@"Y"]) {
            [titleLabel setTextColor:UIColorFromRGB(0xffa4bc)];
        }
        [titleLabel setText:dataDictionary[@"BoardTitle"]];
        [titleLabel sizeToFit];
        [titleLabel setFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y, CGWidthFromIP6P(1035) / 3, titleLabel.frame.size.height)];
        [self.contentView addSubview:titleLabel];
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y + titleLabel.frame.size.height + CGHeightFromIP6P(6), titleLabel.frame.size.width, CGHeightFromIP6P(11))];
        [dateLabel setBackgroundColor:[UIColor clearColor]];
        [dateLabel setFont:MediumWithSize(CGHeightFromIP6P(11))];
        [dateLabel setTextColor:UIColorFromRGB(0x757575)];
        [dateLabel setText:dataDictionary[@"CreateDt"]];
        [dateLabel sizeToFit];
        [dateLabel setFrame:CGRectMake(dateLabel.frame.origin.x, dateLabel.frame.origin.y, titleLabel.frame.size.width, dateLabel.frame.size.height)];
        [self.contentView addSubview:dateLabel];
        
        _nImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1107) / 3, CGHeightFromIP6P(62) / 3, CGWidthFromIP6P(85) / 3, CGHeightFromIP6P(42) / 3)];
        [_nImageView setImage:[UIImage imageNamed:@"notice_image_new"]];
        [self.contentView addSubview:_nImageView];
        
        if ([dataDictionary[@"ReadYN"] isEqualToString:@"Y"]) {
            [_nImageView setAlpha:0];
        }
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
