//
//  DataSingleton.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <ESFramework/ESFramework.h>
#import <UserNotifications/UserNotifications.h>

@interface DataSingleton : NSObject
{
    long lastSleepSessionTimestamp;
    int passwordIndex;
    BOOL isLastAuthFail;
    BOOL needUpdateTerm;
}

@property (strong, nonatomic) NSString *hostString;             // 서버 상태에 따라 Host및 Key가 변경된다. 메인 서버가 죽어있을경우 서브호스트로 연결하기 위해 Singleton에서 호스트 주소를 메모리에 담고있음
@property (strong, nonatomic) NSString *keyString;

@property (strong, nonatomic) NSString *bioProcessing;          // 생체인증에 돌입하면 applicationWillResignActive 이 호출된다. 이게 바탕화면으로 나간것인지, 생체인증으로 인해 Deactive 된것인지 판단하기 위한 플래그

@property (strong, nonatomic) NSString *loginURL;               // SSO 연동 주소가 가변적임
@property (strong, nonatomic) NSString *signUpURL;

@property (strong, nonatomic) NSMutableDictionary *userInfo;           // 로그인 후 회원 정보
@property (strong, nonatomic) NSDictionary *notificationInfo;   // Notification을 터치해서 실행시, 실행 이후 처리하기 위해 임시로 담아둠

@property (strong, nonatomic) UIView *snackBarView;

@property (strong, nonatomic) UIView *loadingView;              // 공용으로 사용될 로딩 인디케이터. YK/신텍에서 로딩 인디케이터가 싫다고해서 현재 사용하지 않음 ㅡㅡ;
@property (strong, nonatomic) UIView *passwordView;             // 앱 잠금 걸어놓은 경우, 앱 Resign 이후 다시 활성화 될 때 보여질 암호 입력 화면
@property (strong, nonatomic) NSMutableArray *passwordArray;    // 앱 잠금 걸어놓은 경우, 암호 입력시 입력 순서대로 한글자씩 배열에 담아서 관리함

@property (strong, nonatomic) UIView *passwordCharacter1View;   // 암호 첫글자
@property (strong, nonatomic) UIView *passwordCharacter2View;   // 암호 두번째 글자
@property (strong, nonatomic) UIView *passwordCharacter3View;   // 암호 세번째 글자
@property (strong, nonatomic) UIView *passwordCharacter4View;   // 암호 네번째 글자

@property (strong, nonatomic) NSString *termAgreeStr;           // 가입시 서버로 보낼 약관 동의 여부 임시 저장소, 사실상 marketingAgreeStr을 제외하곤 항상 Y여야 함
@property (strong, nonatomic) NSString *personalDataAgreeStr;
@property (strong, nonatomic) NSString *marketingAgreeStr;

@property (strong, nonatomic) NSString *badgeString;            // 앱 뱃지 갯수 관리

@property (strong, nonatomic) void (^okAction)(void);

+ (DataSingleton *)sharedSingletonClass;
+ (UIImage *)imageFromColor:(UIColor *)color;

- (BOOL)needUpdateTerm;                                         // 가입 시 약관 동의 여부. 클래스가 달라서 Singleton 에서 저장
- (void)setNeedUpdateTerm:(BOOL)value;

- (void)removeWeightLocalNotification;                          // 체중 알람 Local Notification 등록 / 제거 메소드
- (void)registWeightLocalNotification;

- (void)unlockUsingBIOContext;                                  // 앱 최초 실행, 비활성화 후 재활성화시 바이오 인증 호출 (얼굴인식/지문인식)
- (void)attachPasswordView;

- (void)attachLoadingView;                                      // YK/신텍 요청으로 사용 안함
- (void)detachLoadingView;

- (void)attachSnackBarView;
- (void)detachSnackBarView;

- (void)makeWakeNotification;

@end
