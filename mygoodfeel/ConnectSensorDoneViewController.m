//
//  ConnectSensorDoneViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 22..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "ConnectSensorDoneViewController.h"
#import "RootViewController.h"
#import "PreferenceViewController.h"

@interface ConnectSensorDoneViewController ()

@end

@implementation ConnectSensorDoneViewController

- (void)attachMainViewController
{
    UIViewController *pViewController = _pClass;
    if ([pViewController.navigationController.viewControllers[0] isKindOfClass:[PreferenceViewController class]]) {
        [pViewController.navigationController popToViewController:pViewController.navigationController.viewControllers[1] animated:YES];
    } else {
        [(RootViewController *)self.navigationController.presentingViewController attachMainViewController];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xffffff)];
        float minusMargin = 0;
        
        UIViewController *pViewController = pClass;
        if ([pViewController.navigationController.viewControllers[0] isKindOfClass:[PreferenceViewController class]]) {
            minusMargin = kTOP_HEIGHT;
        }

        [self setTitle:@"센서 연동하기"];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setBackgroundColor:[UIColor clearColor]];
        [backButton setTitle:@"이전" forState:UIControlStateNormal];
        [backButton setTitleColor:UIColorFromRGB(0xaaaaaa) forState:UIControlStateNormal];
        [backButton.titleLabel setFont:SemiBoldWithSize(16)];
        [backButton setAlpha:0];
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [self.navigationItem setLeftBarButtonItem:backBarButtonItem];

        UIImageView *doneImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"connectsensor_image_done"]];
        [doneImageView setCenter:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2 - 120 - minusMargin)];
        [self.view addSubview:doneImageView];

        UILabel *doneTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 100)];
        [doneTitleLabel setBackgroundColor:[UIColor clearColor]];
        [doneTitleLabel setNumberOfLines:0];
        [doneTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"센서를 매트리스 아래에 놓아주세요."]
                                                                                       attributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:23],
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0xf27f8e) }];
        [doneTitleLabel setAttributedText:attrString];
        [doneTitleLabel sizeToFit];
        [doneTitleLabel setCenter:CGPointMake(self.view.center.x, self.view.center.y + 120 - minusMargin)];
        [self.view addSubview:doneTitleLabel];
        
        
        
        UILabel *doneDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(6), 0, kSCREEN_WIDTH - CGWidthFromIP6P(12), 100)];
        [doneDescLabel setBackgroundColor:[UIColor clearColor]];
        [doneDescLabel setNumberOfLines:0];
        [doneDescLabel setTextAlignment:NSTextAlignmentCenter];
        
        attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", @"센서의 위치는 정확한 예측을 위해\n반드시 지켜주세요."]
                                                            attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:15],
                                                                          NSForegroundColorAttributeName : UIColorFromRGB(0x666666) }];
        [doneDescLabel setAttributedText:attrString];
        [doneDescLabel sizeToFit];
        [doneDescLabel setFrame:CGRectMake(0, doneTitleLabel.frame.origin.y + doneTitleLabel.frame.size.height + 6 - minusMargin, kSCREEN_WIDTH, doneDescLabel.frame.size.height)];
        [self.view addSubview:doneDescLabel];

        
        
        UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - 120, kSCREEN_HEIGHT - kBOTTOM_HEIGHT - 50 - minusMargin, 120, 50)];
        [doneButton setBackgroundColor:[UIColor clearColor]];
        [doneButton setTitle:@"준비 완료" forState:UIControlStateNormal];
        [doneButton.titleLabel setFont:SemiBoldWithSize(16)];
        [doneButton setTitleColor:UIColorFromRGB(0xf27f8e) forState:UIControlStateNormal];
        [doneButton addTarget:self action:@selector(attachMainViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:doneButton];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
