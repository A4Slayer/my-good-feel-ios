//
//  DailyReportGuideViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 11..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "DailyReportGuideViewController.h"

@interface DailyReportGuideViewController ()

@end

@implementation DailyReportGuideViewController

- (void)dismissSelfView
{
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        [self setTitle:@"일간 리포트 읽는 법"];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_close_pink"].size.width, [UIImage imageNamed:@"common_button_close_pink"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"common_button_close_pink"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];
        
        
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT)];
        [_mainScrollView setBackgroundColor:UIColorFromRGB(0xffffff)];
        [self.view addSubview:_mainScrollView];
        
        
        UILabel *emptyIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(124 / 3), CGWidthFromIP6P(540 / 3), CGHeightFromIP6P(93 / 3))];
        [emptyIconLabel setBackgroundColor:[UIColor clearColor]];
        [emptyIconLabel setAdjustsFontSizeToFitWidth:YES];
        [_mainScrollView addSubview:emptyIconLabel];
        CNSTextAttachment *imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_none"]];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  주기 아님 / 생리 양 없음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [emptyIconLabel setAttributedText:attrString];
        
        
        UILabel *bloodNormalIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(124 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodNormalIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodNormalIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_much"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 양 많음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodNormalIconLabel setAttributedText:attrString];
        

        UILabel *bloodLessIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(286 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodLessIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodLessIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_less"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 양 적음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodLessIconLabel setAttributedText:attrString];

        UILabel *pyrexiaIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(286 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [pyrexiaIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:pyrexiaIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_pre"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  예상 주기"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [pyrexiaIconLabel setAttributedText:attrString];
        
        

        UILabel *bloodMuchIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(448 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodMuchIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodMuchIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_normal"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 양 보통"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodMuchIconLabel setAttributedText:attrString];
        


        UILabel *bloodNoneIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(448 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodNoneIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodNoneIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_availablemenstruation"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  예상 가임기"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodNoneIconLabel setAttributedText:attrString];

        UILabel *sleepSpectrumGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(CGHeightFromIP6P(660 / 3)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [sleepSpectrumGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [sleepSpectrumGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [sleepSpectrumGuideLabel setClipsToBounds:YES];
        [sleepSpectrumGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [sleepSpectrumGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [sleepSpectrumGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [sleepSpectrumGuideLabel setText:@"수면 스펙트럼"];
        [_mainScrollView addSubview:sleepSpectrumGuideLabel];
        
        UILabel *sleepSpectrumDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepSpectrumGuideLabel.frame.origin.y + sleepSpectrumGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepSpectrumDescLabel setNumberOfLines:0];
        [sleepSpectrumDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepSpectrumDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepSpectrumDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepSpectrumDescLabel setText:@"사용자의 수면 시간 중의 수면 깊이를 색으로 나타냅니다. 렘수면 구간이 넓거나 수면 스트레스가 많을수록 수면 점수가 낮아지며, 규칙적인 시간에 잠자리에 들면 수면 점수가 높아질 수 있습니다."];
        [sleepSpectrumDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepSpectrumDescLabel];
        
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(49 / 3), sleepSpectrumDescLabel.frame.origin.y + sleepSpectrumDescLabel.frame.size.height + CGHeightFromIP6P(19), kSCREEN_WIDTH - CGWidthFromIP6P(49 / 3) - CGWidthFromIP6P(49 / 3), k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        [_mainScrollView addSubview:separatorView];
        
        
        UIView *wakeIconView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(48 / 3), separatorView.frame.origin.y + separatorView.frame.size.height + CGHeightFromIP6P(20), CGWidthFromIP6P(46 / 3), CGHeightFromIP6P(46 / 3))];
        [wakeIconView.layer setCornerRadius:wakeIconView.frame.size.width / 2];
        [wakeIconView setClipsToBounds:YES];
        [wakeIconView setBackgroundColor:UIColorFromRGB(0xffbdbd)];
        [_mainScrollView addSubview:wakeIconView];
        
        UILabel *wakeLabel = [[UILabel alloc] initWithFrame:CGRectMake(wakeIconView.frame.origin.x + wakeIconView.frame.size.width + CGWidthFromIP6P(6), wakeIconView.frame.origin.y, CGWidthFromIP6P(100), wakeIconView.frame.size.height)];
        [wakeLabel setBackgroundColor:[UIColor clearColor]];
        [wakeLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
        [wakeLabel setTextColor:UIColorFromRGB(0x646464)];
        [wakeLabel setText:@"깨어남"];
        [_mainScrollView addSubview:wakeLabel];
        
        UIView *nonSleepIconView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(289 / 3), separatorView.frame.origin.y + separatorView.frame.size.height + CGHeightFromIP6P(20), CGWidthFromIP6P(46 / 3), CGHeightFromIP6P(46 / 3))];
        [nonSleepIconView.layer setCornerRadius:wakeIconView.frame.size.width / 2];
        [nonSleepIconView setClipsToBounds:YES];
        [nonSleepIconView setBackgroundColor:UIColorFromRGB(0xfcf066)];
        [_mainScrollView addSubview:nonSleepIconView];
        
        UILabel *nonSleepLabel = [[UILabel alloc] initWithFrame:CGRectMake(nonSleepIconView.frame.origin.x + nonSleepIconView.frame.size.width + CGWidthFromIP6P(6), nonSleepIconView.frame.origin.y, CGWidthFromIP6P(100), nonSleepIconView.frame.size.height)];
        [nonSleepLabel setBackgroundColor:[UIColor clearColor]];
        [nonSleepLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
        [nonSleepLabel setTextColor:UIColorFromRGB(0x646464)];
        [nonSleepLabel setText:@"뒤척임"];
        [_mainScrollView addSubview:nonSleepLabel];

        UIView *remSleepIconView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(527 / 3), separatorView.frame.origin.y + separatorView.frame.size.height + CGHeightFromIP6P(20), CGWidthFromIP6P(46 / 3), CGHeightFromIP6P(46 / 3))];
        [remSleepIconView.layer setCornerRadius:wakeIconView.frame.size.width / 2];
        [remSleepIconView setClipsToBounds:YES];
        [remSleepIconView setBackgroundColor:UIColorFromRGB(0x84ccc9)];
        [_mainScrollView addSubview:remSleepIconView];
        
        UILabel *remSleepLabel = [[UILabel alloc] initWithFrame:CGRectMake(remSleepIconView.frame.origin.x + remSleepIconView.frame.size.width + CGWidthFromIP6P(6), remSleepIconView.frame.origin.y, CGWidthFromIP6P(100), remSleepIconView.frame.size.height)];
        [remSleepLabel setBackgroundColor:[UIColor clearColor]];
        [remSleepLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
        [remSleepLabel setTextColor:UIColorFromRGB(0x646464)];
        [remSleepLabel setText:@"램수면"];
        [_mainScrollView addSubview:remSleepLabel];

        
        UIView *lightSleepIconView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(763 / 3), separatorView.frame.origin.y + separatorView.frame.size.height + CGHeightFromIP6P(20), CGWidthFromIP6P(46 / 3), CGHeightFromIP6P(46 / 3))];
        [lightSleepIconView.layer setCornerRadius:wakeIconView.frame.size.width / 2];
        [lightSleepIconView setClipsToBounds:YES];
        [lightSleepIconView setBackgroundColor:UIColorFromRGB(0xaae67f)];
        [_mainScrollView addSubview:lightSleepIconView];
        
        UILabel *lightSleepLabel = [[UILabel alloc] initWithFrame:CGRectMake(lightSleepIconView.frame.origin.x + lightSleepIconView.frame.size.width + CGWidthFromIP6P(6), lightSleepIconView.frame.origin.y, CGWidthFromIP6P(100), lightSleepIconView.frame.size.height)];
        [lightSleepLabel setBackgroundColor:[UIColor clearColor]];
        [lightSleepLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
        [lightSleepLabel setTextColor:UIColorFromRGB(0x646464)];
        [lightSleepLabel setText:@"얕은 잠"];
        [_mainScrollView addSubview:lightSleepLabel];

        
        UIView *deepSleepIconView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1011 / 3), separatorView.frame.origin.y + separatorView.frame.size.height + CGHeightFromIP6P(20), CGWidthFromIP6P(46 / 3), CGHeightFromIP6P(46 / 3))];
        [deepSleepIconView.layer setCornerRadius:wakeIconView.frame.size.width / 2];
        [deepSleepIconView setClipsToBounds:YES];
        [deepSleepIconView setBackgroundColor:UIColorFromRGB(0xdaade9)];
        [_mainScrollView addSubview:deepSleepIconView];
        
        UILabel *deepSleepLabel = [[UILabel alloc] initWithFrame:CGRectMake(deepSleepIconView.frame.origin.x + deepSleepIconView.frame.size.width + CGWidthFromIP6P(6), deepSleepIconView.frame.origin.y, CGWidthFromIP6P(100), deepSleepIconView.frame.size.height)];
        [deepSleepLabel setBackgroundColor:[UIColor clearColor]];
        [deepSleepLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
        [deepSleepLabel setTextColor:UIColorFromRGB(0x646464)];
        [deepSleepLabel setText:@"깊은 잠"];
        [_mainScrollView addSubview:deepSleepLabel];


        
        UILabel *sleepBreathGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(deepSleepLabel.frame.origin.y + deepSleepLabel.frame.size.height + CGHeightFromIP6P(111 / 3)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [sleepBreathGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [sleepBreathGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [sleepBreathGuideLabel setClipsToBounds:YES];
        [sleepBreathGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [sleepBreathGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [sleepBreathGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [sleepBreathGuideLabel setText:@"취침 중 심박수 / 호흡주기"];
        [_mainScrollView addSubview:sleepBreathGuideLabel];
        
        UILabel *sleepBreathDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepBreathGuideLabel.frame.origin.y + sleepBreathGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepBreathDescLabel setNumberOfLines:0];
        [sleepBreathDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepBreathDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepBreathDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepBreathDescLabel setText:@"취침 중에 매트리스 밑 수면 센서가 사용자의 호흡주기와 심박수를 추적합니다. 일간 리포트에서는 수면 중 측정된 심박수와 호흡 주기의 평균치를 확인하실 수 있습니다. 매일 아침 센서가 추적한 심박수와 호흡주기가 기준치를 미달/초과하였을 경우 PUSH 알림을 띄우며, \"설정\"에서 심박수와 호흡주기의 기준을 조정할 수 있습니다."];
        [sleepBreathDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepBreathDescLabel];
        
        
        UILabel *sleepStressGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepBreathDescLabel.frame.origin.y + sleepBreathDescLabel.frame.size.height + CGHeightFromIP6P(111 / 3)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [sleepStressGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [sleepStressGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [sleepStressGuideLabel setClipsToBounds:YES];
        [sleepStressGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [sleepStressGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [sleepStressGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [sleepStressGuideLabel setText:@"수면 스트레스"];
        [_mainScrollView addSubview:sleepStressGuideLabel];
        
        UILabel *sleepStressDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepStressGuideLabel.frame.origin.y + sleepStressGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepStressDescLabel setNumberOfLines:0];
        [sleepStressDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepStressDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepStressDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepStressDescLabel setText:@"취침 중에 수면 센서가 사용자의 심박수를 추적합니다. 심박수의 변화가 심할수록 수면 스트레스를 높게 책정합니다. 심박수의 변화는 꿈을 꾸거나 불편한 자세로 수면, 통증 등이 있을 경우 급격하게 증가할 수 있습니다. \"설정\"에서 심박수와 호흡주기의 기준을 조정할 수 있습니다."];
        [sleepStressDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepStressDescLabel];
        
        
        [_mainScrollView setContentSize:CGSizeMake(_mainScrollView.frame.size.width, sleepStressDescLabel.frame.origin.y + sleepStressDescLabel.frame.size.height + kBOTTOM_HEIGHT + CGHeightFromIP6P(16))];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
