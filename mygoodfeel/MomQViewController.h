//
//  MomQViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKSDWebImageManager.h"

@interface MomQViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UITableView *mainTableView;

@property (strong, nonatomic) NSDictionary *storeDictionary;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
