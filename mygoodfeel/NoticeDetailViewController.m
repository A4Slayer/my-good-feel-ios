//
//  NoticeDetailViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 5..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "NoticeDetailViewController.h"
#import "NoticeViewController.h"

@interface NoticeDetailViewController ()

@end

@implementation NoticeDetailViewController

- (void)updateBoard:(UIButton *)button
{
    _boardNo = [NSString stringWithFormat:@"%d", (int)button.tag];
    [self requestNoticeDetail];
}

- (void)noPrev
{
    [[[UIAlertView alloc] initWithTitle:nil message:@"이전 글이 없습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
}

- (void)noNext
{
    [[[UIAlertView alloc] initWithTitle:nil message:@"다음 글이 없습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
}

- (void)goList
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTICE_LIST" object:nil];
}

- (void)requestNoticeDetail
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_NOTICE_GET_DETAIL]];
    
    [urlString appendFormat:@"/%@", _boardNo];
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    if ([_pClass isKindOfClass:[NoticeViewController class]]) {
        [(NoticeViewController *)_pClass addReadBoardNo:_boardNo];
    }
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            
            [[DataSingleton sharedSingletonClass] setBadgeString:responseData[@"data"][@"ReadCnt"]];
            
            if (self->_contentView) {
                [self->_contentView removeFromSuperview];
                self->_contentView = nil;
            }
            
            // 그리기 시작
            
            self->_contentView = [[UIView alloc] initWithFrame:self.view.bounds];
            [self->_contentView setBackgroundColor:[UIColor whiteColor]];
            [self.view addSubview:self->_contentView];
            
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50) / 3, CGHeightFromIP6P(59) / 3, CGWidthFromIP6P(1035) / 3, CGHeightFromIP6P(48) / 3)];
            [titleLabel setBackgroundColor:[UIColor clearColor]];
            [titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(16))];
            [titleLabel setTextColor:UIColorFromRGB(0x3c3c3c)];
            [titleLabel setText:responseData[@"data"][@"BoardTitle"]];
            if ([responseData[@"data"][@"MainYN"] isEqualToString:@"Y"]) {
                [titleLabel setTextColor:UIColorFromRGB(0xffa4bc)];
            }
            [titleLabel sizeToFit];
            [titleLabel setFrame:CGRectMake(CGWidthFromIP6P(50) / 3, CGHeightFromIP6P(59) / 3, CGWidthFromIP6P(1035) / 3, CGHeightFromIP6P(48) / 3)];
            [self->_contentView addSubview:titleLabel];
            
            UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y + titleLabel.frame.size.height + CGHeightFromIP6P(6), titleLabel.frame.size.width, CGHeightFromIP6P(11))];
            [dateLabel setBackgroundColor:[UIColor clearColor]];
            [dateLabel setFont:MediumWithSize(CGHeightFromIP6P(11))];
            [dateLabel setTextColor:UIColorFromRGB(0x757575)];
            [dateLabel setText:responseData[@"data"][@"CreateDt"]];
            [dateLabel sizeToFit];
            [dateLabel setFrame:CGRectMake(dateLabel.frame.origin.x, dateLabel.frame.origin.y, titleLabel.frame.size.width, dateLabel.frame.size.height)];
            [self->_contentView addSubview:dateLabel];
            
            if ([responseData[@"data"][@"ReadYN"] isEqualToString:@"N"]) {
                UIImageView *nImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1107) / 3, CGHeightFromIP6P(62) / 3, CGWidthFromIP6P(85) / 3, CGHeightFromIP6P(42) / 3)];
                [nImageView setImage:[UIImage imageNamed:@"notice_image_new"]];
                [self->_contentView addSubview:nImageView];
            }
            
            UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(216) / 3, kSCREEN_WIDTH, k2PX)];
            [separatorView setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
            [self->_contentView addSubview:separatorView];
            
            UIWebView *contentWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, separatorView.frame.origin.y + separatorView.frame.size.height, kSCREEN_WIDTH, self->_contentView.frame.size.height - separatorView.frame.origin.y - separatorView.frame.size.height - CGHeightFromIP6P(50) - kBOTTOM_HEIGHT)];
            [contentWebView loadHTMLString:responseData[@"data"][@"BoardContent"] baseURL:nil];
            [self->_contentView addSubview:contentWebView];
            
            
            UIButton *prevButton = [[UIButton alloc] initWithFrame:CGRectMake(0, contentWebView.frame.origin.y + contentWebView.frame.size.height, (int)(kSCREEN_WIDTH / 3), CGHeightFromIP6P(50) + kBOTTOM_HEIGHT)];
            [prevButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffa4bc)] forState:UIControlStateNormal];
            [prevButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, kBOTTOM_HEIGHT, 0)];
            [prevButton addTarget:self action:@selector(updateBoard:) forControlEvents:UIControlEventTouchUpInside];
            [prevButton setTag:[responseData[@"data"][@"PreviousNo"] intValue]];
            [self->_contentView addSubview:prevButton];

            CNSTextAttachment *imageAttachment;
            NSMutableAttributedString *attrString;
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -1.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"notice_button_arrow_left"]];
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  이전글"]
                                                                               attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(17)),
                                                                                             NSForegroundColorAttributeName : [UIColor whiteColor] }]];
            [prevButton setAttributedTitle:attrString forState:UIControlStateNormal];

            
            UIButton *listButton = [[UIButton alloc] initWithFrame:CGRectMake((int)(prevButton.frame.origin.x + prevButton.frame.size.width), contentWebView.frame.origin.y + contentWebView.frame.size.height, (int)(kSCREEN_WIDTH / 3), CGHeightFromIP6P(50) + kBOTTOM_HEIGHT)];
            [listButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffa4bc)] forState:UIControlStateNormal];
            [listButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, kBOTTOM_HEIGHT, 0)];
            [listButton.titleLabel setFont:MediumWithSize(CGHeightFromIP6P(17))];
            [listButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [listButton setTitle:@"목록" forState:UIControlStateNormal];
            [listButton addTarget:self action:@selector(goList) forControlEvents:UIControlEventTouchUpInside];
            [self->_contentView addSubview:listButton];
            
            UIButton *nextButton = [[UIButton alloc] initWithFrame:CGRectMake((int)(listButton.frame.origin.x + listButton.frame.size.width), contentWebView.frame.origin.y + contentWebView.frame.size.height, kSCREEN_WIDTH - (listButton.frame.origin.x + listButton.frame.size.width), CGHeightFromIP6P(50) + kBOTTOM_HEIGHT)];
            [nextButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffa4bc)] forState:UIControlStateNormal];
            [nextButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, kBOTTOM_HEIGHT, 0)];
            [nextButton addTarget:self action:@selector(updateBoard:) forControlEvents:UIControlEventTouchUpInside];
            [nextButton setTag:[responseData[@"data"][@"NextNo"] intValue]];
            [self->_contentView addSubview:nextButton];

            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"다음글  "]
                                                                               attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(17)),
                                                                                             NSForegroundColorAttributeName : [UIColor whiteColor] }]];
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -1.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"notice_button_arrow_right"]];
            [attrString appendAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            [nextButton setAttributedTitle:attrString forState:UIControlStateNormal];
            
            
            if ([responseData[@"data"][@"PreviousNo"] intValue] == 0) {
                [prevButton removeTarget:self action:@selector(updateBoard:) forControlEvents:UIControlEventTouchUpInside];
                [prevButton addTarget:self action:@selector(noPrev) forControlEvents:UIControlEventTouchUpInside];
            }
            if ([responseData[@"data"][@"NextNo"] intValue] == 0) {
                [nextButton removeTarget:self action:@selector(updateBoard:) forControlEvents:UIControlEventTouchUpInside];
                [nextButton addTarget:self action:@selector(noNext) forControlEvents:UIControlEventTouchUpInside];
            }
            

            separatorView = [[UIView alloc] initWithFrame:CGRectMake(prevButton.frame.size.width, prevButton.frame.origin.y, k1PX, prevButton.frame.size.height)];
            [separatorView setBackgroundColor:UIColorFromRGB(0xffc9d7)];
            [self->_contentView addSubview:separatorView];
            
            separatorView = [[UIView alloc] initWithFrame:CGRectMake(listButton.frame.origin.x + listButton.frame.size.width, listButton.frame.origin.y, k1PX, listButton.frame.size.height)];
            [separatorView setBackgroundColor:UIColorFromRGB(0xffc9d7)];
            [self->_contentView addSubview:separatorView];

        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass boardNo:(NSString *)boardNo
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        _boardNo = boardNo;
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"공지사항"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x3c3c3e)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
        [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
        
        [self requestNoticeDetail];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
