//
//  DataSingleton.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "DataSingleton.h"

@implementation DataSingleton

static DataSingleton *singletonClass;

+ (DataSingleton *) sharedSingletonClass
{
    if(singletonClass == nil) {
        @synchronized(self) {
            if(singletonClass == nil) {
                singletonClass = [[self alloc] init];
            }
        }
    }
    return singletonClass;
}

+ (UIImage *)imageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)callbackSensorChangeState:(NSNotification *)nc
{
    if ([[nc name] isEqualToString:[[BLEManager sharedInstance] notificationForType:BLENotificationTypeConnectedToSensor]]) {
        // 연결됨
    } else if ([[nc name] isEqualToString:[[BLEManager sharedInstance] notificationForType:BLENotificationTypeConnectedToSensor]]) {
        // 연결 해제됨
    }
}

- (void)registerSensorNotification
{
    if (!IS_HIDE_SLEEP_PART) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callbackSensorChangeState:) name:[[BLEManager sharedInstance] notificationForType:BLENotificationTypeConnectedToSensor] object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callbackSensorChangeState:) name:[[BLEManager sharedInstance] notificationForType:BLENotificationTypeDisconnectedFromSensor] object:nil];
    }
}

- (void)wakeupNotification
{
    NSDate *tomorrow = [NSDate dateWithTimeInterval:(24 * 60 * 60) sinceDate:[NSDate date]];
    SleepSummaryComplete *lastSummary = [[HistoryManager sharedInstance] getPrevSummaryFrom:tomorrow forUser:[[UserManager sharedInstance] getUser].email];
    
    if ( lastSleepSessionTimestamp && lastSleepSessionTimestamp < (long)[lastSummary getSleepParams].timestamp ) {

        float hrMax = 0;
        float hrMin = 999;
        
        for (int j = 0; j < [lastSummary.hrGraph count]; j++) {
            if ([lastSummary.hrGraph[j] floatValue] > 0) {
                if (hrMax < [lastSummary.hrGraph[j] floatValue]) {
                    hrMax = [lastSummary.hrGraph[j] floatValue];
                }
                
                if (hrMin > [lastSummary.hrGraph[j] floatValue]) {
                    hrMin = [lastSummary.hrGraph[j] floatValue];
                }
            }
        }
        
        if (hrMin == 999) {
            hrMin = 0;
        }
        
        float rrMax = 0;
        float rrMin = 999;
        
        for (int j = 0; j < [lastSummary.rrGraph count]; j++) {
            if ([lastSummary.rrGraph[j] floatValue] > 0) {
                if (rrMax < [lastSummary.rrGraph[j] floatValue]) {
                    rrMax = [lastSummary.rrGraph[j] floatValue];
                }
                
                if (rrMin > [lastSummary.rrGraph[j] floatValue]) {
                    rrMin = [lastSummary.rrGraph[j] floatValue];
                }
            }
        }
        
        if (rrMin == 999) {
            rrMin = 0;
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"sleep_alert"] isEqualToString:@"1"]) {
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"hrmin"] intValue] < hrMin || [[[NSUserDefaults standardUserDefaults] objectForKey:@"hrmax"] intValue] > hrMax) {
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                
                [localNotification setFireDate:[NSDate date]];
                [localNotification setTimeZone:[NSTimeZone localTimeZone]];
                [localNotification setAlertBody:[NSString stringWithFormat:@"이상 범주의 심박수가 감지되었습니다."]];
                [localNotification setAlertAction:@"확인"];
                [localNotification setSoundName:UILocalNotificationDefaultSoundName];
                [localNotification setUserInfo:@{
                                                 @"type" : @"hrerror"
                                                 }];
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"rrmin"] intValue] < rrMin || [[[NSUserDefaults standardUserDefaults] objectForKey:@"rrmax"] intValue] > rrMax) {
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                
                [localNotification setFireDate:[NSDate date]];
                [localNotification setTimeZone:[NSTimeZone localTimeZone]];
                [localNotification setAlertBody:[NSString stringWithFormat:@"이상 범주의 호흡주기가 감지되었습니다."]];
                [localNotification setAlertAction:@"확인"];
                [localNotification setSoundName:UILocalNotificationDefaultSoundName];
                [localNotification setUserInfo:@{
                                                 @"type" : @"rrerror"
                                                 }];
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        
        lastSleepSessionTimestamp = (long)[lastSummary getSleepParams].timestamp;
    }
    
    [self performSelector:@selector(wakeupNotification) withObject:nil afterDelay:30.0f];
}

- (void)makeWakeNotification
{
    if (!IS_HIDE_SLEEP_PART) {
        NSDate *tomorrow = [NSDate dateWithTimeInterval:(24 * 60 * 60) sinceDate:[NSDate date]];
        SleepSummaryComplete *lastSummary = [[HistoryManager sharedInstance] getPrevSummaryFrom:tomorrow forUser:[[UserManager sharedInstance] getUser].email];
        
        lastSleepSessionTimestamp = (long)[lastSummary getSleepParams].timestamp;
        [self performSelector:@selector(wakeupNotification) withObject:nil afterDelay:30.0f];
    }
}

- (void)removeWeightLocalNotification
{
    NSArray *localNotificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0; i < [localNotificationArray count]; i++) {
        UILocalNotification *localNotification = [localNotificationArray objectAtIndex:i];
        if ([localNotification.userInfo[@"type"] isEqualToString:@"weight"]) {
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
        }
    }
}

- (void)registWeightLocalNotification
{
    [self removeWeightLocalNotification];

    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *componentsForFireDate = [calendar components:(NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[NSDate date]];
    
    [componentsForFireDate setHour:[[[NSUserDefaults standardUserDefaults] objectForKey:@"weight_hour"] intValue]];
    [componentsForFireDate setMinute:[[[NSUserDefaults standardUserDefaults] objectForKey:@"weight_min"] intValue]];
    [componentsForFireDate setSecond:0];
    
    NSDate *fireDateOfNotification = [calendar dateFromComponents:componentsForFireDate];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    [localNotification setFireDate:fireDateOfNotification];
    [localNotification setTimeZone:[NSTimeZone localTimeZone]];
    [localNotification setAlertBody:[NSString stringWithFormat:@"몸무게를 잴 시간입니다."]];
    [localNotification setAlertAction:@"재러가기"];
    [localNotification setRepeatInterval:NSCalendarUnitDay];
    [localNotification setSoundName:UILocalNotificationDefaultSoundName];
    [localNotification setUserInfo:@{
                                     @"type" : @"weight"
                                     }];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];

}

- (void)setBadgeString:(NSString *)badgeString
{
    _badgeString = badgeString;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_LEFT_MENU" object:nil];
}

- (BOOL)needUpdateTerm
{
    return needUpdateTerm;
}

- (void)setNeedUpdateTerm:(BOOL)value
{
    needUpdateTerm = value;
}

- (id)init
{
    self = [super init];
    if (self) {
        _bioProcessing = @"0";
        isLastAuthFail = NO;
        needUpdateTerm = NO;
        [self makeWakeNotification];
        [self registerSensorNotification];
    }
    return self;
}

- (void)drawPasswordIndicator
{
    [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter3View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter4View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    
    if (passwordIndex == 1) {
        [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xff9699)];
    } else if (passwordIndex == 2) {
        [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xff9699)];
    } else if (passwordIndex == 3) {
        [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter3View setBackgroundColor:UIColorFromRGB(0xff9699)];
    } else if (passwordIndex == 4) {
        [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter3View setBackgroundColor:UIColorFromRGB(0xff9699)];
        [_passwordCharacter4View setBackgroundColor:UIColorFromRGB(0xff9699)];
        
        NSString *passwordString = [NSString stringWithFormat:@"%@%@%@%@", _passwordArray[0], _passwordArray[1], _passwordArray[2], _passwordArray[3]];
        
        if ([passwordString isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"app_password"]]) {
            // 확인
            [_passwordView removeFromSuperview];
            _passwordView = nil;
            
        } else {
            // 비밀번호 다름 오류
            [[[UIAlertView alloc] initWithTitle:@"" message:@"비밀번호가 일치하지 않습니다" delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
            passwordIndex = 0;
            [self drawPasswordIndicator];
        }
    }
}

- (void)touchNumber:(UIButton *)button
{
    if (button.tag == -1) {
        passwordIndex--;
        if (passwordIndex < 0) {
            passwordIndex = 0;
        }
        [self drawPasswordIndicator];
    } else {
        [_passwordArray setObject:[NSString stringWithFormat:@"%ld", (long)button.tag] atIndexedSubscript:passwordIndex];
        passwordIndex++;
        
        [self drawPasswordIndicator];
    }
}

- (void)unlockUsingBIOContext
{
    if (!_passwordView) {
        return;
    }
    if (isLastAuthFail) {
        return;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"app_password_bio"] intValue] == 1) {
        LAContext *laContext = [[LAContext alloc] init];
        NSError *error;
        
        if ([laContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            
            if (error != NULL) {
                NSLog(@"can evaluate error: %@", error);
                // handle error
            } else {
                
                NSString *reasonString = @"Touch ID로 잠금 해제";
                BOOL isSupport = NO;
                
                if (@available(iOS 11.0.1, *)) {
                    if (laContext.biometryType == LABiometryTypeFaceID) {
                        reasonString = @"Face ID로 잠금 해제";
                        isSupport = YES;
                    } else if (laContext.biometryType == LABiometryTypeTouchID) {
                        isSupport = YES;
                    } else {
                        // can not support bio
                    }
                } else {
                    // Fallback on earlier versions
                    isSupport = NO;
                    NSLog(@"not available under ios 11");
                }
                
                if (isSupport) {
                    [laContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:reasonString reply:^(BOOL success, NSError * _Nullable error) {
                        
                        NSLog(@"error: %@", error);
                        
                        if (error != NULL) {
                            if (error.code == -2) {
                                self->isLastAuthFail = YES;
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"생체 인증을 취소했습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                                });
                            } else {
                                self->isLastAuthFail = YES;
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"생체 정보 인증에 실패했습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                                });
                            }
                        } else if (success) {
                            self->isLastAuthFail = NO;
                            [self performSelectorOnMainThread:@selector(detachPasswordView) withObject:nil waitUntilDone:NO];
                            // handle success response
                        } else {
                            self->isLastAuthFail = YES;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"생체 정보 인증에 실패했습니다."] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                            });
                            // handle false respon1se
                        }
                    }];
                }
            }
        }
    }
}

- (void)detachPasswordView
{
    [self->_passwordView removeFromSuperview];
    self->_passwordView = nil;
}

- (void)attachPasswordView
{
    if (_passwordView) {
        return;
    }
    if ([_bioProcessing intValue] == 1) {
        return;
    }
    
    isLastAuthFail = NO;
    
    passwordIndex = 0;
    _passwordArray = [[NSMutableArray alloc] init];
    _passwordView = [[UIView alloc] initWithFrame:[[UIApplication sharedApplication] keyWindow].bounds];
    
    UIView *bgView = [[UIView alloc] initWithFrame:_passwordView.bounds];
    [bgView setBackgroundColor:UIColorFromRGB(0xffffff)];
    [_passwordView addSubview:bgView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(64), kSCREEN_WIDTH, CGHeightFromIP6P(22))];
    [titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(21))];
    [titleLabel setTextColor:UIColorFromRGB(0x474747)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setText:@"앱 잠금 암호 입력"];
    [titleLabel sizeToFit];
    [titleLabel setFrame:CGRectMake(0, titleLabel.frame.origin.y, kSCREEN_WIDTH, titleLabel.frame.size.height)];
    [_passwordView addSubview:titleLabel];
    
    UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.frame.origin.y + titleLabel.frame.size.height + CGHeightFromIP6P(4), kSCREEN_WIDTH, CGHeightFromIP6P(17))];
    [descLabel setFont:MediumWithSize(CGHeightFromIP6P(16))];
    [descLabel setTextColor:UIColorFromRGB(0x474747)];
    [descLabel setTextAlignment:NSTextAlignmentCenter];
    [descLabel setText:@"PIN 번호 4자리를 입력해주세요"];
    [descLabel sizeToFit];
    [descLabel setFrame:CGRectMake(0, descLabel.frame.origin.y, kSCREEN_WIDTH, descLabel.frame.size.height)];
    [_passwordView addSubview:descLabel];
    
    
    if (_passwordCharacter1View) {
        [_passwordCharacter1View removeFromSuperview];
        _passwordCharacter1View = nil;
    }
    if (_passwordCharacter2View) {
        [_passwordCharacter2View removeFromSuperview];
        _passwordCharacter2View = nil;
    }
    if (_passwordCharacter3View) {
        [_passwordCharacter3View removeFromSuperview];
        _passwordCharacter3View = nil;
    }
    if (_passwordCharacter4View) {
        [_passwordCharacter4View removeFromSuperview];
        _passwordCharacter4View = nil;
    }

    _passwordCharacter1View = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(409 / 3), descLabel.frame.origin.y + descLabel.frame.size.height + CGHeightFromIP6P(59), CGWidthFromIP6P(42 / 3), CGHeightFromIP6P(42 / 3))];
    [_passwordCharacter1View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter1View.layer setCornerRadius:_passwordCharacter1View.frame.size.width / 2];
    [_passwordCharacter1View setClipsToBounds:YES];
    [_passwordView addSubview:_passwordCharacter1View];
    
    _passwordCharacter2View = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(537 / 3), descLabel.frame.origin.y + descLabel.frame.size.height + CGHeightFromIP6P(59), CGWidthFromIP6P(42 / 3), CGHeightFromIP6P(42 / 3))];
    [_passwordCharacter2View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter2View.layer setCornerRadius:_passwordCharacter1View.frame.size.width / 2];
    [_passwordCharacter2View setClipsToBounds:YES];
    [_passwordView addSubview:_passwordCharacter2View];
    
    _passwordCharacter3View = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(664 / 3), descLabel.frame.origin.y + descLabel.frame.size.height + CGHeightFromIP6P(59), CGWidthFromIP6P(42 / 3), CGHeightFromIP6P(42 / 3))];
    [_passwordCharacter3View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter3View.layer setCornerRadius:_passwordCharacter1View.frame.size.width / 2];
    [_passwordCharacter3View setClipsToBounds:YES];
    [_passwordView addSubview:_passwordCharacter3View];
    
    _passwordCharacter4View = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(792 / 3), descLabel.frame.origin.y + descLabel.frame.size.height + CGHeightFromIP6P(59), CGWidthFromIP6P(42 / 3), CGHeightFromIP6P(42 / 3))];
    [_passwordCharacter4View setBackgroundColor:UIColorFromRGB(0xffd5d6)];
    [_passwordCharacter4View.layer setCornerRadius:_passwordCharacter1View.frame.size.width / 2];
    [_passwordCharacter4View setClipsToBounds:YES];
    [_passwordView addSubview:_passwordCharacter4View];
    
    
    float additionalSpacing = kSCREEN_HEIGHT * 0.023;
    
    
    UIButton *number1Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(154 / 3), CGHeightFromIP6P(254), CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number1Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number1Button setTitle:@"1" forState:UIControlStateNormal];
    [number1Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number1Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number1Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number1Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number1Button setTag:1];
    [number1Button.layer setCornerRadius:number1Button.frame.size.width / 2];
    [number1Button setClipsToBounds:YES];
    [number1Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number1Button];
    
    UIButton *number2Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(495 / 3), CGHeightFromIP6P(254), CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number2Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number2Button setTitle:@"2" forState:UIControlStateNormal];
    [number2Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number2Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number2Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number2Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number2Button setTag:2];
    [number2Button.layer setCornerRadius:number2Button.frame.size.width / 2];
    [number2Button setClipsToBounds:YES];
    [number2Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number2Button];
    
    UIButton *number3Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(839 / 3), CGHeightFromIP6P(254), CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number3Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number3Button setTitle:@"3" forState:UIControlStateNormal];
    [number3Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number3Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number3Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number3Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number3Button setTag:3];
    [number3Button.layer setCornerRadius:number3Button.frame.size.width / 2];
    [number3Button setClipsToBounds:YES];
    [number3Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number3Button];
    
    
    UIButton *number4Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(154 / 3), number1Button.frame.origin.y + number1Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number4Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number4Button setTitle:@"4" forState:UIControlStateNormal];
    [number4Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number4Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number4Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number4Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number4Button setTag:4];
    [number4Button.layer setCornerRadius:number4Button.frame.size.width / 2];
    [number4Button setClipsToBounds:YES];
    [number4Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number4Button];
    
    UIButton *number5Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(495 / 3), number1Button.frame.origin.y + number1Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number5Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number5Button setTitle:@"5" forState:UIControlStateNormal];
    [number5Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number5Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number5Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number5Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number5Button setTag:5];
    [number5Button.layer setCornerRadius:number5Button.frame.size.width / 2];
    [number5Button setClipsToBounds:YES];
    [number5Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number5Button];
    
    UIButton *number6Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(839 / 3), number1Button.frame.origin.y + number1Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number6Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number6Button setTitle:@"6" forState:UIControlStateNormal];
    [number6Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number6Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number6Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number6Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number6Button setTag:6];
    [number6Button.layer setCornerRadius:number6Button.frame.size.width / 2];
    [number6Button setClipsToBounds:YES];
    [number6Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number6Button];
    
    
    UIButton *number7Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(154 / 3), number4Button.frame.origin.y + number4Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number7Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number7Button setTitle:@"7" forState:UIControlStateNormal];
    [number7Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number7Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number7Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number7Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number7Button setTag:7];
    [number7Button.layer setCornerRadius:number7Button.frame.size.width / 2];
    [number7Button setClipsToBounds:YES];
    [number7Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number7Button];
    
    UIButton *number8Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(495 / 3), number4Button.frame.origin.y + number4Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number8Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number8Button setTitle:@"8" forState:UIControlStateNormal];
    [number8Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number8Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number8Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number8Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number8Button setTag:8];
    [number8Button.layer setCornerRadius:number8Button.frame.size.width / 2];
    [number8Button setClipsToBounds:YES];
    [number8Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number8Button];
    
    UIButton *number9Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(839 / 3), number4Button.frame.origin.y + number4Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number9Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number9Button setTitle:@"9" forState:UIControlStateNormal];
    [number9Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number9Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number9Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number9Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number9Button setTag:9];
    [number9Button.layer setCornerRadius:number9Button.frame.size.width / 2];
    [number9Button setClipsToBounds:YES];
    [number9Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number9Button];
    
    
//    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(154 / 3), number7Button.frame.origin.y + number7Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
//    [cancelButton.titleLabel setFont:MediumWithSize(CGHeightFromIP6P(18))];
//    [cancelButton setTitle:@"취소" forState:UIControlStateNormal];
//    [cancelButton setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
//    [cancelButton addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
//    [cancelButton setTag:100];
//    [cancelButton setClipsToBounds:YES];
//    [cancelButton setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
//    [_passwordView addSubview:cancelButton];
    
    UIButton *number0Button = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(495 / 3), number7Button.frame.origin.y + number7Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [number0Button.titleLabel setFont:BoldWithSize(CGHeightFromIP6P(33))];
    [number0Button setTitle:@"0" forState:UIControlStateNormal];
    [number0Button setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [number0Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f1)] forState:UIControlStateNormal];
    [number0Button setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffdada)] forState:UIControlStateHighlighted];
    [number0Button addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [number0Button setTag:0];
    [number0Button.layer setCornerRadius:number0Button.frame.size.width / 2];
    [number0Button setClipsToBounds:YES];
    [number0Button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:number0Button];
    
    UIButton *removeButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(839 / 3), number7Button.frame.origin.y + number7Button.frame.size.height + additionalSpacing, CGWidthFromIP6P(249 / 3), CGHeightFromIP6P(249 / 3))];
    [removeButton.titleLabel setFont:MediumWithSize(CGHeightFromIP6P(18))];
    [removeButton setTitle:@"지움" forState:UIControlStateNormal];
    [removeButton setTitleColor:UIColorFromRGB(0x474747) forState:UIControlStateNormal];
    [removeButton addTarget:self action:@selector(touchNumber:) forControlEvents:UIControlEventTouchUpInside];
    [removeButton setTag:-1];
    [removeButton setClipsToBounds:YES];
    [removeButton setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_passwordView addSubview:removeButton];

    [[[UIApplication sharedApplication] keyWindow] addSubview:_passwordView];
    
}

- (void)attachSnackBarView:(NSString *)message okTitle:(NSString *)okTitle okAction:(void (^)(void))okAction
{
    _okAction = okAction;
}

- (void)attachLoadingView
{
    if (_loadingView) {
        return;
    }
    _loadingView = [[UIView alloc] initWithFrame:[[UIApplication sharedApplication] keyWindow].bounds];
    
    UIView *blackMaskView = [[UIView alloc] initWithFrame:_loadingView.bounds];
    [blackMaskView setAlpha:0];
    [blackMaskView setBackgroundColor:[UIColor blackColor]];
    [_loadingView addSubview:blackMaskView];
    
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicatorView setCenter:CGPointMake(_loadingView.frame.size.width / 2, _loadingView.frame.size.height / 2)];
    [indicatorView startAnimating];
    [_loadingView addSubview:indicatorView];
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:_loadingView];

    [UIView animateWithDuration:.5f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self->_loadingView setAlpha:0.3];
    } completion:^(BOOL finished) {
    }];
}

- (void)detachLoadingView
{
    [UIView animateWithDuration:.25f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self->_loadingView setAlpha:0];
    } completion:^(BOOL finished) {
        if (self->_loadingView) {
            for (UIView *sView in [self->_loadingView subviews]) {
                [sView removeFromSuperview];
            }
            [self->_loadingView removeFromSuperview];
            self->_loadingView = nil;
        }
    }];
}

@end
