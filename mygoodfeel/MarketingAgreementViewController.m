//
//  MarketingAgreementViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "MarketingAgreementViewController.h"
#import "SignViewController.h"

@interface MarketingAgreementViewController ()

@end

@implementation MarketingAgreementViewController

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        _pClass = pClass;
        [self.navigationItem setHidesBackButton:YES animated:NO];
        [self setTitle:@"MY좋은느낌 개인정보 처리 방침"];

        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
        [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
        
        UIWebView *privacyWebView = [[UIWebView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(41) / 3, CGHeightFromIP6P(41) / 3, CGWidthFromIP6P(1160) / 3, CGHeightFromIP6P(1988) / 3 + kIPHONE_X_BOTTOM_MARGIN)];
        [privacyWebView setBackgroundColor:[UIColor whiteColor]];
        [privacyWebView.layer setBorderColor:UIColorFromRGB(0xd7d7d7).CGColor];
        [privacyWebView.layer setBorderWidth:k1PX];
        [privacyWebView setScalesPageToFit:YES];
        [privacyWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://m.ykbrand.co.kr/Terms/TermsPop?type=yk8"]]];
        [self.view addSubview:privacyWebView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
