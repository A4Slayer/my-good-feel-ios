//
//  EventSignViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 15..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "EventSignViewController.h"
#import "EventDetailViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface EventSignViewController ()

@end

@implementation EventSignViewController

- (void)updateEventView
{
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self->_pClass isKindOfClass:[EventDetailViewController class]]) {
            [(EventDetailViewController *)self->_pClass updateView];
        }
    }];
}

- (void)login:(NSString *)userID
{
    NSString *uniqueIdentifier = nil;
    
    uniqueIdentifier = [JNKeychain loadValueForKey:@"APPKNOT_UDID"];
    
    if (uniqueIdentifier == nil || uniqueIdentifier.length == 0) {
        if([UIDevice instancesRespondToSelector:@selector(identifierForVendor)]) {
            uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        } else {
            uniqueIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:@"identiferForVendor"];
            if( !uniqueIdentifier ) {
                CFUUIDRef uuid = CFUUIDCreate(NULL);
                uniqueIdentifier = (__bridge_transfer NSString*)CFUUIDCreateString(NULL, uuid);
                CFRelease(uuid);
                [[NSUserDefaults standardUserDefaults] setObject:uniqueIdentifier forKey:@"identifierForVendor"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        [JNKeychain saveValue:uniqueIdentifier forKey:@"APPKNOT_UDID"];
    }
    
    
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_FIRST_LOGIN]];
    
    [urlString appendFormat:@"/%@", userID];           // ID
    [urlString appendFormat:@"/%@", uniqueIdentifier];
    [urlString appendFormat:@"/2"];
    [urlString appendFormat:@"/101"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        switch ([responseData[@"code"] intValue]) {
            case 0:
                [[DataSingleton sharedSingletonClass] setUserInfo:responseData[@"data"]];
                if (IS_HIDE_SLEEP_PART) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_LEFT_MENU" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_EVENT_DETAIL" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"REFRESH_AFTER_LOGIN" object:nil];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"userid"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [self updateEventView];
                } else {
                    [[UserManager sharedInstance] registerWithEmail:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserName"]
                                                        andPassword:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserPw"]
                                                     withCompletion:^(enum NetworkAPIStatusCode returnCode) {
                                                         if (returnCode == NetworkAPIStatusCodeUserAlreadyExists || returnCode == NetworkAPIStatusCodeSuccess) {
                                                             [[UserManager sharedInstance] loginWithEmail:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserName"]
                                                                                              andPassword:[[DataSingleton sharedSingletonClass] userInfo][@"ESUserPw"]
                                                                                               customKeys:nil
                                                                                                    force:YES
                                                                                           withCompletion:^(enum NetworkAPIStatusCode returnCode, User *userData) {
                                                                                               
                                                                                               [[HistoryManager sharedInstance] loadAllSummariesWithProgress:^(double downloadProgress) {
                                                                                                   
                                                                                               } andCompletion:^(enum NetworkAPIStatusCode code) {
                                                                                                   
                                                                                                   [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_LEFT_MENU" object:nil];
                                                                                                   [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_EVENT_DETAIL" object:nil];
                                                                                                   [[NSNotificationCenter defaultCenter] postNotificationName:@"REFRESH_AFTER_LOGIN" object:nil];
                                                                                                   
                                                                                                   [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"userid"];
                                                                                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                                                                                   
                                                                                                   [self updateEventView];
                                                                                               }];
                                                                                           }];
                                                         } else {
                                                             NSLog(@"login fail !! %d", returnCode);
                                                         }
                                                     }];
                }

                [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                         @"PageId" : @"Login",
                                                                         @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                         @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                                   @"Login",
                                                                                   [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                                   ]
                                                                         }];

                break;
                
            default:
                [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:responseData[@"msg"][@"ko"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                break;
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *responseData;
        if (error != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:[NSString stringWithFormat:@"%@", responseData] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];
    
}


- (void)dismissSelfView
{
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass type:(int)type
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        isSignUp = type;
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xefeff4)];
        [self setTitle:@"유한킴벌리 통합회원 로그인"];

        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT)];
        if (type == 0) {
            [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[DataSingleton sharedSingletonClass] loginURL]]]];
        } else {
            [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[DataSingleton sharedSingletonClass] signUpURL]]]];
        }
        [webView setDelegate:self];
        [webView setScalesPageToFit:YES];
        [self.view addSubview:webView];

        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_close"].size.width, [UIImage imageNamed:@"common_button_close"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"common_button_close"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[request.URL scheme] isEqualToString:@"elle"]) {
        
        if (isSignUp) {
            if (_webView) {
                [_webView removeFromSuperview];
                _webView = nil;
            }
            _webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
            [_webView setDelegate:self];
            [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[DataSingleton sharedSingletonClass] loginURL]]]];
            [_webView setScalesPageToFit:YES];
            [self.view addSubview:_webView];
            
            isSignUp = NO;
//            [self setTitle:@"로그인"];
        } else {
            NSArray *resultArray = [request.URL.path componentsSeparatedByString:@"/"];
            [[NSUserDefaults standardUserDefaults] setObject:resultArray[2] forKey:@"cookie"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self login:resultArray[1]];
        }
        
    }
    
    return YES;
}


@end
