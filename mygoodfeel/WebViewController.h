//
//  WebViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 8..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UIWebView *webView;

@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UIButton *forwardButton;
@property (strong, nonatomic) UIButton *shareButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil webURL:(NSString *)webURL title:(NSString *)title;

@end
