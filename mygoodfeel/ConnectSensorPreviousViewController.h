//
//  ConnectSensorPreviousViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectSensorListViewController.h"
#import "ConnectSensorAgreementViewController.h"
#import <ESFramework/ESFramework.h>

@interface ConnectSensorPreviousViewController : UIViewController

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) ConnectSensorAgreementViewController *connectSensorAgreementViewController;
@property (strong, nonatomic) UINavigationController *connectSensorAgreementNavigationController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;
- (void)doneAgreementView;

@end
