//
//  SleepDayInfoViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 27..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SleepDayInfoViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface SleepDayInfoViewController ()

@end

@implementation SleepDayInfoViewController

- (void)getSleepDailyInfo
{
    if (IS_HIDE_SLEEP_PART) {
        return;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    
    NSArray *sleepArray = [[HistoryManager sharedInstance] getSleepSegmentsForDate:[formatter dateFromString:_dateString] forUser:[[UserManager sharedInstance] getUser].email];
    if ([sleepArray count]) {
        for (int i = 0; i < [sleepArray count]; i++) {
            SleepSummaryComplete *sleepSummary = sleepArray[i];
            
            float hrMax = 0;
            float hrMin = 999;
            
            for (int j = 0; j < [sleepSummary.hrGraph count]; j++) {
                if ([sleepSummary.hrGraph[j] floatValue] > 0) {
                    if (hrMax < [sleepSummary.hrGraph[j] floatValue]) {
                        hrMax = [sleepSummary.hrGraph[j] floatValue];
                    }
                    
                    if (hrMin > [sleepSummary.hrGraph[j] floatValue]) {
                        hrMin = [sleepSummary.hrGraph[j] floatValue];
                    }
                }
            }
            
            if (hrMin == 999) {
                hrMin = 0;
            }

            float rrMax = 0;
            float rrMin = 999;

            for (int j = 0; j < [sleepSummary.rrGraph count]; j++) {
                if ([sleepSummary.rrGraph[j] floatValue] > 0) {
                    if (rrMax < [sleepSummary.rrGraph[j] floatValue]) {
                        rrMax = [sleepSummary.rrGraph[j] floatValue];
                    }
                    
                    if (rrMin > [sleepSummary.rrGraph[j] floatValue]) {
                        rrMin = [sleepSummary.rrGraph[j] floatValue];
                    }
                }
            }
            
            if (rrMin == 999) {
                rrMin = 0;
            }
            
            int sleepType1Count = 0;
            int sleepType2Count = 0;
            int sleepType3Count = 0;
            int sleepType4Count = 0;
            int sleepType5Count = 0;

            for (int j = 0; j < [sleepSummary.hypnoTypeGraph count]; j++) {
                if ([sleepSummary.hypnoTypeGraph[j] intValue] == 0) {
                    sleepType1Count++;
                } else if ([sleepSummary.hypnoTypeGraph[j] intValue] == 1) {
                    sleepType2Count++;
                } else if ([sleepSummary.hypnoTypeGraph[j] intValue] == 2) {
                    sleepType3Count++;
                } else if ([sleepSummary.hypnoTypeGraph[j] intValue] == 3) {
                    sleepType4Count++;
                } else if ([sleepSummary.hypnoTypeGraph[j] intValue] == 4) {
                    sleepType5Count++;
                }
            }
            
            _dataDictionary = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                @"hrMax" : [NSString stringWithFormat:@"%d", (int)hrMax],
                                                                                @"hrMin" : [NSString stringWithFormat:@"%d", (int)hrMin],
                                                                                @"rrMax" : [NSString stringWithFormat:@"%d", (int)rrMax],
                                                                                @"rrMin" : [NSString stringWithFormat:@"%d", (int)rrMin],
                                                                                @"sleepType1Count" : [NSString stringWithFormat:@"%d", sleepType1Count],
                                                                                @"sleepType2Count" : [NSString stringWithFormat:@"%d", sleepType2Count],
                                                                                @"sleepType3Count" : [NSString stringWithFormat:@"%d", sleepType3Count],
                                                                                @"sleepType4Count" : [NSString stringWithFormat:@"%d", sleepType4Count],
                                                                                @"sleepType5Count" : [NSString stringWithFormat:@"%d", sleepType5Count],
                                                                                @"sleepScore" : [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].scoreTotalSleepTime],
                                                                                @"sleep_awake" : [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].minutesAwake],
                                                                                @"sleep_awakeaftersleep" : [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].minutesAwakeAfterSleep],
                                                                                @"sleep_deepsleep" : [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].minutesDeepSleep],
                                                                                @"sleep_lightsleep" : [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].minutesLightSleep],
                                                                                @"sleep_outofbed" : [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].minutesOutOfBed],
                                                                                @"sleep_rem" : [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].minutesREM],
                                                                                @"graph" : sleepSummary.hypnoTypeGraph,
                                                                                @"stress" : [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].avgStressCategory],
                                                                                @"sleep_starttime" : [NSString stringWithFormat:@"%d", (int)[sleepSummary getSleepParams].timestamp]
                                                                                }];
            
            if ([_dataDictionary[@"sleepScore"] intValue] > 80) {
                [_dataDictionary setObject:@"0" forKey:@"sleepScore"];
            }

            [_cardPageControl setNumberOfPages:2];
        }
    } else {
        _dataDictionary = nil;
        [_cardPageControl setNumberOfPages:1];
    }
    [_cardViewCarousel reloadData];

}

- (void)presentSleepDayInfoGuide
{
    SleepDayInfoGuideViewController *sleepDayInfoGuideViewController = [[SleepDayInfoGuideViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *sleepDayInfoGuideNavigationController = [[UINavigationController alloc] initWithRootViewController:sleepDayInfoGuideViewController];
    [sleepDayInfoGuideNavigationController.navigationBar setTranslucent:NO];
    [sleepDayInfoGuideNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                              forBarPosition:UIBarPositionAny
                                                                  barMetrics:UIBarMetricsDefault];
    [sleepDayInfoGuideNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [sleepDayInfoGuideNavigationController.navigationBar setTitleTextAttributes:@{
                                                                                   NSFontAttributeName: SemiBoldWithSize(18),
                                                                                   NSForegroundColorAttributeName: UIColorFromRGB(0xf05971)
                                                                                   }];
    [sleepDayInfoGuideNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffeff1)] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController presentViewController:sleepDayInfoGuideNavigationController animated:YES completion:^{
        //
    }];
}

- (void)presentSleepDiaryViewController:(UIButton *)button
{
    NSString *dateString = [NSString stringWithFormat:@"%ld", (long)button.tag];
    NSLog(@"dateString: %@", dateString);
    dateString = [NSString stringWithFormat:@"%@-%@-%@", [dateString substringWithRange:NSMakeRange(0, 4)], [dateString substringWithRange:NSMakeRange(4, 2)], [dateString substringWithRange:NSMakeRange(6, 2)]];
    
    SleepDiaryViewController *sleepDiaryViewController = [[SleepDiaryViewController alloc] initWithNibName:nil bundle:nil pClass:self dateString:dateString];
    UINavigationController *sleepDiaryNavigationController = [[UINavigationController alloc] initWithRootViewController:sleepDiaryViewController];
    [sleepDiaryNavigationController.navigationBar setTranslucent:NO];
    [sleepDiaryNavigationController.navigationBar setTitleTextAttributes:@{
                                                                           NSFontAttributeName: SemiBoldWithSize(18),
                                                                           NSForegroundColorAttributeName: UIColorFromRGB(0x000000)
                                                                           }];
    [sleepDiaryNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffffff)] forBarMetrics:UIBarMetricsDefault];

    [self.navigationController presentViewController:sleepDiaryNavigationController animated:YES completion:^{
        //
    }];
}

- (void)changeToSleepCalendarViewController
{
    [(RootViewController *)_pClass changeToSleepCalendarViewController:_dateString];
}

- (void)becomeDatePicker
{
    if (![_blackMaskButton superview]) {
        [self.navigationController.view addSubview:_blackMaskButton];
        [self.navigationController.view addSubview:_datePicker];
    }
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self->_blackMaskButton setAlpha:0.7];
        [self->_datePicker setFrame:CGRectMake(0, kSCREEN_HEIGHT - 215, kSCREEN_WIDTH, 215)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)resignDatePicker
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self->_blackMaskButton setAlpha:0];
        [self->_datePicker setFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 215)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)changedDatePickerValue:(UIDatePicker *)datePicker
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = @"yyyy-MM-dd";
    _dateString = [formatter stringFromDate:datePicker.date];
    
    formatter.dateFormat = @"yyyy년 M월 d일";
    [_dateButton setTitle:[formatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
    
    [self getSleepDailyInfo];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass dateString:(NSString *)dateString
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        _dateString = dateString;
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        NSMutableAttributedString *attrString;
        CNSTextAttachment *imageAttachment;
        
        UIButton *titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleButton setBackgroundColor:[UIColor clearColor]];
        [titleButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleButton addTarget:self action:@selector(presentSleepDayInfoGuide) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setTitleView:titleButton];
        
        if (@available(iOS 9.0, *)) {
            [titleButton setTitle:@"수면 정보 " forState:UIControlStateNormal];
            [titleButton.titleLabel setFont:SemiBoldWithSize(19)];
            [titleButton setTitleColor:UIColorFromRGB(0x3c3c3e) forState:UIControlStateNormal];
            [titleButton setImage:[UIImage imageNamed:@"cycledayinfo_button_qmark"] forState:UIControlStateNormal];
            [titleButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
            [titleButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 3.5 / 2, 0)];
        } else {
            attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"주기 정보 "]
                                                                attributes:@{ NSFontAttributeName : SemiBoldWithSize(19),
                                                                              NSForegroundColorAttributeName : [UIColor whiteColor] }];
            
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -3.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_button_qmark"]];
            [attrString appendAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            
            [titleButton setAttributedTitle:attrString forState:UIControlStateNormal];
        }
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu_black"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu_black"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
        
        UIButton *cycleCalendarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cycledayinfo_button_calendar"].size.width, [UIImage imageNamed:@"cycledayinfo_button_calendar"].size.height)];
        [cycleCalendarButton setImage:[UIImage imageNamed:@"cycledayinfo_button_calendar"] forState:UIControlStateNormal];
        [cycleCalendarButton addTarget:self action:@selector(changeToSleepCalendarViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cycleCalendarButton]];
        
        
        _dateButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(284 / 3), kTOP_HEIGHT + CGHeightFromIP6P(15), CGWidthFromIP6P(674 / 3), CGHeightFromIP6P(148 / 3))];
        [_dateButton.layer setCornerRadius:_dateButton.frame.size.height / 2];
        [_dateButton setClipsToBounds:YES];
        [_dateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_dateButton.titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(19))];
        [_dateButton setTitle:[NSString stringWithFormat:@"%@년 %d월 %d일", [dateString substringWithRange:NSMakeRange(0, 4)], [[dateString substringWithRange:NSMakeRange(5, 2)] intValue], [[dateString substringWithRange:NSMakeRange(8, 2)] intValue]] forState:UIControlStateNormal];
        [_dateButton setTitleEdgeInsets:UIEdgeInsetsMake(CGHeightFromIP6P(2), 0, 0, 0)];
        [_dateButton addTarget:self action:@selector(becomeDatePicker) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_dateButton];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = _dateButton.bounds;
        gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [_dateButton.layer insertSublayer:gradient atIndex:0];
        
        UIImageView *optionImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_option"]];
        [optionImageView setCenter:CGPointMake(_dateButton.frame.size.width - CGWidthFromIP6P(23), _dateButton.frame.size.height / 2)];
        [_dateButton addSubview:optionImageView];
        
        
        _cardViewCarousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, _dateButton.frame.origin.y + _dateButton.frame.size.height + CGHeightFromIP6P(10), kSCREEN_WIDTH, CGHeightFromIP6P(1654 / 3))];
        [_cardViewCarousel setType:iCarouselTypeRotary];
        [_cardViewCarousel setBackgroundColor:[UIColor clearColor]];
        [_cardViewCarousel setDelegate:self];
        [_cardViewCarousel setDataSource:self];
        [_cardViewCarousel setPagingEnabled:YES];
        [_cardViewCarousel setBounces:NO];
        [self.view addSubview:_cardViewCarousel];
        
        
        _cardPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, _cardViewCarousel.frame.origin.y + _cardViewCarousel.frame.size.height + CGHeightFromIP6P(18), kSCREEN_WIDTH, CGHeightFromIP6P(13))];
        [_cardPageControl setPageIndicatorTintColor:UIColorFromRGB(0xffd8dd)];
        [_cardPageControl setCurrentPageIndicatorTintColor:UIColorFromRGB(0xf27f8e)];
        [_cardPageControl setNumberOfPages:2];
        [_cardPageControl setUserInteractionEnabled:NO];
        [self.view addSubview:_cardPageControl];
        
        if (IS_HIDE_SLEEP_PART) {
            [_cardPageControl setAlpha:0];
        }
        
        
        _blackMaskButton = [[UIButton alloc] initWithFrame:self.view.bounds];
        [_blackMaskButton setBackgroundColor:[UIColor blackColor]];
        [_blackMaskButton setAlpha:0];
        [_blackMaskButton addTarget:self action:@selector(resignDatePicker) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.view addSubview:_blackMaskButton];
        
        _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 215)];
        [_datePicker setBackgroundColor:[UIColor whiteColor]];
        [_datePicker setDatePickerMode:UIDatePickerModeDate];
        [_datePicker addTarget:self action:@selector(changedDatePickerValue:) forControlEvents:UIControlEventValueChanged];
        [_datePicker setLocale:[NSLocale localeWithLocaleIdentifier:@"ko"]];
        [self.navigationController.view addSubview:_datePicker];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                       fromDate:[NSDate date]];
        [components setTimeZone:[NSTimeZone localTimeZone]];
        [components setMonth:1];
        [components setDay:1];
        [components setYear:[components year] - 10];
        [_datePicker setMinimumDate:[[NSCalendar currentCalendar] dateFromComponents:components]];
        
        components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                     fromDate:[NSDate date]];
        [components setTimeZone:[NSTimeZone localTimeZone]];
        [components setMonth:12];
        [components setDay:31];
        [components setYear:[components year] + 10];
        [_datePicker setMaximumDate:[[NSCalendar currentCalendar] dateFromComponents:components]];
        
        NSDateFormatter *selectedDateFormatter = [[NSDateFormatter alloc] init];
        [selectedDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [selectedDateFormatter setDateFormat:@"yyyy-MM-dd"];
        [_datePicker setDate:[selectedDateFormatter dateFromString:dateString]];
        
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"CycleInfoDaily",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"CycleInfoDaily",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];
        
        [self getSleepDailyInfo];
        
    }
    return self;
}

- (void)removeGuideView:(UIButton *)guideButton
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [guideButton setAlpha:0];
    } completion:^(BOOL finished) {
        [guideButton removeFromSuperview];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma marㄷ -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    if (_dataDictionary) {
        return 2;
    } else {
        return 1;
    }
}

- (void)balloonAnimation:(UIImageView *)balloonImageView
{
    CGPoint balloonCenter = balloonImageView.center;
    [balloonImageView setFrame:CGRectMake(0, 0, 0, 0)];
    [balloonImageView setCenter:balloonCenter];
    [balloonImageView setAlpha:1];
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [balloonImageView setFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.width, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.height)];
        [balloonImageView setCenter:balloonCenter];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            [balloonImageView setFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.width * 0.7, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.height * 0.7)];
            [balloonImageView setCenter:balloonCenter];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                [balloonImageView setFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.width, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.height)];
                [balloonImageView setCenter:balloonCenter];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5 delay:3 options:UIViewAnimationOptionCurveLinear animations:^{
                    [balloonImageView setAlpha:0];
                } completion:^(BOOL finished) {
                }];
            }];
        }];
    }];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (index == 0) {       // info view
        UIView *infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(1586 / 3))];
        [infoView setBackgroundColor:[UIColor whiteColor]];
        [infoView setClipsToBounds:YES];
        [infoView.layer setCornerRadius:CGWidthFromIP6P(38 / 3)];
        
        if (IS_HIDE_SLEEP_PART) {
            UIImageView *dummyCoverImageView = [[UIImageView alloc] initWithFrame:infoView.bounds];
            [dummyCoverImageView setImage:[UIImage imageNamed:@"sleepdayinfo_image_dummy_cover1"]];
            [infoView addSubview:dummyCoverImageView];
            
            view = infoView;
            
            UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(1586 / 3))];
            [shadowView.layer setShadowPath:[UIBezierPath bezierPathWithRect:view.bounds].CGPath];
            [shadowView.layer setShadowColor:[UIColor blackColor].CGColor];
            [shadowView.layer setShadowOffset:CGSizeMake(2, 2)];
            [shadowView.layer setShadowRadius:14];
            [shadowView.layer setShadowOpacity:0.6];
            [shadowView setBackgroundColor:[UIColor clearColor]];
            [shadowView setClipsToBounds:NO];
            [shadowView addSubview:view];
            
            view = shadowView;
            
            return view;
        }
        
        if (_dataDictionary) {
            NSString *dateString = _dateString;
            
            NSDateFormatter *weekdayDateFormatter = [[NSDateFormatter alloc] init];
            [weekdayDateFormatter setDateFormat:@"yyyy-MM-dd"];
            [weekdayDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            
            UILabel *scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), CGHeightFromIP6P(109 / 3), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [scoreLabel setFont:MediumWithSize(16)];
            [scoreLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
            [scoreLabel setText:@"수면 점수"];
            [scoreLabel sizeToFit];
            [infoView addSubview:scoreLabel];
            
            UILabel *scoreValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(scoreLabel.frame.origin.x, scoreLabel.frame.origin.y + scoreLabel.frame.size.height + CGHeightFromIP6P(10), CGWidthFromIP6P(200), CGHeightFromIP6P(52))];
            [scoreValueLabel setBackgroundColor:[UIColor clearColor]];
            [scoreValueLabel setText:_dataDictionary[@"sleepScore"]];
            [scoreValueLabel setFont:BoldWithSize(CGHeightFromIP6P(64))];
            [scoreValueLabel setTextColor:UIColorFromRGB(0xf36e7d)];
            [scoreValueLabel sizeToFit];
            [infoView addSubview:scoreValueLabel];
            
            UILabel *scorePointLabel = [[UILabel alloc] initWithFrame:CGRectMake(scoreValueLabel.frame.origin.x + scoreValueLabel.frame.size.width + CGWidthFromIP6P(4), 0, 10, 10)];
            [scorePointLabel setBackgroundColor:[UIColor clearColor]];
            [scorePointLabel setText:@"점"];
            [scorePointLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(23))];
            [scorePointLabel setTextColor:UIColorFromRGB(0xf36e7d)];
            [scorePointLabel sizeToFit];
            [scorePointLabel setFrame:CGRectMake(scoreValueLabel.frame.origin.x + scoreValueLabel.frame.size.width, scoreValueLabel.frame.origin.y + scoreValueLabel.frame.size.height - scorePointLabel.frame.size.height - CGHeightFromIP6P(13), scorePointLabel.frame.size.width, scorePointLabel.frame.size.height)];
            [infoView addSubview:scorePointLabel];
            
            
            UIButton *diaryButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(993 / 3), CGHeightFromIP6P(113 / 3), [UIImage imageNamed:@"cycledayinfo_button_diary"].size.width, [UIImage imageNamed:@"cycledayinfo_button_diary"].size.height)];
            [diaryButton setImage:[UIImage imageNamed:@"cycledayinfo_button_diary"] forState:UIControlStateNormal];
            [diaryButton setTag:[[dateString stringByReplacingOccurrencesOfString:@"-" withString:@""] intValue]];
            [diaryButton addTarget:self action:@selector(presentSleepDiaryViewController:) forControlEvents:UIControlEventTouchUpInside];
            [infoView addSubview:diaryButton];
            
            if ([[weekdayDateFormatter dateFromString:dateString] compare:[NSDate date]] == NSOrderedDescending) {
                [diaryButton setAlpha:0];
            } else {
                UIImageView *balloonImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_balloon"]];
                [balloonImageView setFrame:CGRectMake(diaryButton.frame.origin.x + diaryButton.frame.size.width - balloonImageView.frame.size.width + CGWidthFromIP6P(14), diaryButton.frame.origin.y + diaryButton.frame.size.height + CGHeightFromIP6P(6), balloonImageView.frame.size.width, balloonImageView.frame.size.height)];
                [balloonImageView setAlpha:0];
                [infoView addSubview:balloonImageView];
                
                [self performSelector:@selector(balloonAnimation:) withObject:balloonImageView afterDelay:0.0f];
            }
            
            int validSleepCount = 0;
            
            for (int i = 0; i < [_dataDictionary[@"graph"] count]; i++) {
                if ([_dataDictionary[@"graph"][i] intValue] != -1) {
                    validSleepCount++;
                }
            }
            
            validSleepCount = validSleepCount / 2;
            
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), CGHeightFromIP6P(459 / 3), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [timeLabel setFont:MediumWithSize(16)];
            [timeLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
            [timeLabel setText:@"수면 시간"];
            [timeLabel sizeToFit];
            [infoView addSubview:timeLabel];
            
            UILabel *timeValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(timeLabel.frame.origin.x, timeLabel.frame.origin.y + timeLabel.frame.size.height + CGHeightFromIP6P(7), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [infoView addSubview:timeValueLabel];
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d시간 %d분", validSleepCount / 60, validSleepCount % 60]
                                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }];
            [timeValueLabel setAttributedText:attrString];
            [timeValueLabel sizeToFit];
            
            
            UILabel *stressLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(576 / 3), CGHeightFromIP6P(459 / 3), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [stressLabel setFont:MediumWithSize(16)];
            [stressLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
            [stressLabel setText:@"수면 중 스트레스"];
            [stressLabel sizeToFit];
            [infoView addSubview:stressLabel];
            
            UILabel *stressValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(stressLabel.frame.origin.x, stressLabel.frame.origin.y + stressLabel.frame.size.height + CGHeightFromIP6P(7), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [infoView addSubview:stressValueLabel];
            
            
            CNSTextAttachment *imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -5)];
            
            if ([_dataDictionary[@"stress"] intValue] == 1) {
                [imageAttachment setImage:[UIImage imageNamed:@"sleepdayinfo_image_stress_less"]];
                attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 낮음"]
                                                                                   attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                                 NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            } else if ([_dataDictionary[@"stress"] intValue] == 2) {
                [imageAttachment setImage:[UIImage imageNamed:@"sleepdayinfo_image_stress_normal"]];
                attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 보통"]
                                                                                   attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                                 NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            } else if ([_dataDictionary[@"stress"] intValue] == 3) {
                [imageAttachment setImage:[UIImage imageNamed:@"sleepdayinfo_image_stress_high"]];
                attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 높음"]
                                                                                   attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                                 NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            } else {
                attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"알 수 없음"]
                                                                                   attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                                 NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
            }
            
            [stressValueLabel setAttributedText:attrString];
            [stressValueLabel sizeToFit];
            
            [timeValueLabel setFrame:CGRectMake(timeValueLabel.frame.origin.x, stressValueLabel.frame.origin.y, timeValueLabel.frame.size.width, stressValueLabel.frame.size.height)];
            
            
            
            UILabel *breathCycleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), timeValueLabel.frame.origin.y + timeValueLabel.frame.size.height + CGHeightFromIP6P(36), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [breathCycleLabel setFont:MediumWithSize(16)];
            [breathCycleLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
            [breathCycleLabel setText:@"취침 중 호흡주기"];
            [breathCycleLabel sizeToFit];
            [infoView addSubview:breathCycleLabel];
            
            
            UIView *circleBGView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), breathCycleLabel.frame.origin.y + breathCycleLabel.frame.size.height + CGHeightFromIP6P(7), CGWidthFromIP6P(64 / 3), CGHeightFromIP6P(64 / 3))];
            [circleBGView setBackgroundColor:UIColorFromRGB(0xf696a1)];
            [circleBGView.layer setCornerRadius:circleBGView.frame.size.width / 2];
            [circleBGView setClipsToBounds:YES];
            [infoView addSubview:circleBGView];
            UILabel *rowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 1, circleBGView.frame.size.width, circleBGView.frame.size.height)];
            [rowLabel setBackgroundColor:[UIColor clearColor]];
            [rowLabel setTextColor:[UIColor whiteColor]];
            [rowLabel setTextAlignment:NSTextAlignmentCenter];
            [rowLabel setText:@"저"];
            [rowLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
            [circleBGView addSubview:rowLabel];
            
            UILabel *rowValue = [[UILabel alloc] initWithFrame:CGRectMake(circleBGView.frame.origin.x + circleBGView.frame.size.width + CGWidthFromIP6P(3), circleBGView.frame.origin.y + 1, 100, circleBGView.frame.size.height)];
            [rowValue setText:_dataDictionary[@"rrMin"]];
            [rowValue setTextColor:UIColorFromRGB(0x525252)];
            [rowValue setFont:MediumWithSize(CGHeightFromIP6P(20))];
            [rowValue sizeToFit];
            [rowValue setFrame:CGRectMake(rowValue.frame.origin.x, rowValue.frame.origin.y, rowValue.frame.size.width, circleBGView.frame.size.height)];
            [infoView addSubview:rowValue];
            
            circleBGView = [[UIView alloc] initWithFrame:CGRectMake(rowValue.frame.origin.x + rowValue.frame.size.width + CGWidthFromIP6P(8), breathCycleLabel.frame.origin.y + breathCycleLabel.frame.size.height + CGHeightFromIP6P(7), CGWidthFromIP6P(64 / 3), CGHeightFromIP6P(64 / 3))];
            [circleBGView setBackgroundColor:UIColorFromRGB(0xf696a1)];
            [circleBGView.layer setCornerRadius:circleBGView.frame.size.width / 2];
            [circleBGView setClipsToBounds:YES];
            [infoView addSubview:circleBGView];
            UILabel *highLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 1, circleBGView.frame.size.width, circleBGView.frame.size.height)];
            [highLabel setBackgroundColor:[UIColor clearColor]];
            [highLabel setTextColor:[UIColor whiteColor]];
            [highLabel setTextAlignment:NSTextAlignmentCenter];
            [highLabel setText:@"고"];
            [highLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
            [circleBGView addSubview:highLabel];
            
            UILabel *highValue = [[UILabel alloc] initWithFrame:CGRectMake(circleBGView.frame.origin.x + circleBGView.frame.size.width + CGWidthFromIP6P(3), circleBGView.frame.origin.y + 1, 100, circleBGView.frame.size.height)];
            [highValue setText:_dataDictionary[@"rrMax"]];
            [highValue setTextColor:UIColorFromRGB(0x525252)];
            [highValue setFont:MediumWithSize(CGHeightFromIP6P(20))];
            [highValue sizeToFit];
            [highValue setFrame:CGRectMake(highValue.frame.origin.x, highValue.frame.origin.y, highValue.frame.size.width, circleBGView.frame.size.height)];
            [infoView addSubview:highValue];
            
            
            UILabel *heartrateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(576 / 3), breathCycleLabel.frame.origin.y, CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [heartrateLabel setFont:MediumWithSize(16)];
            [heartrateLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
            [heartrateLabel setText:@"취침 중 심박수"];
            [heartrateLabel sizeToFit];
            [infoView addSubview:heartrateLabel];
            
            circleBGView = [[UIView alloc] initWithFrame:CGRectMake(heartrateLabel.frame.origin.x, heartrateLabel.frame.origin.y + heartrateLabel.frame.size.height + CGHeightFromIP6P(7), CGWidthFromIP6P(64 / 3), CGHeightFromIP6P(64 / 3))];
            [circleBGView setBackgroundColor:UIColorFromRGB(0xf696a1)];
            [circleBGView.layer setCornerRadius:circleBGView.frame.size.width / 2];
            [circleBGView setClipsToBounds:YES];
            [infoView addSubview:circleBGView];
            rowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 1, circleBGView.frame.size.width, circleBGView.frame.size.height)];
            [rowLabel setBackgroundColor:[UIColor clearColor]];
            [rowLabel setTextColor:[UIColor whiteColor]];
            [rowLabel setTextAlignment:NSTextAlignmentCenter];
            [rowLabel setText:@"저"];
            [rowLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
            [circleBGView addSubview:rowLabel];
            
            rowValue = [[UILabel alloc] initWithFrame:CGRectMake(circleBGView.frame.origin.x + circleBGView.frame.size.width + CGWidthFromIP6P(3), circleBGView.frame.origin.y + 1, 100, circleBGView.frame.size.height)];
            [rowValue setText:_dataDictionary[@"hrMin"]];
            [rowValue setTextColor:UIColorFromRGB(0x525252)];
            [rowValue setFont:MediumWithSize(CGHeightFromIP6P(20))];
            [rowValue sizeToFit];
            [rowValue setFrame:CGRectMake(rowValue.frame.origin.x, rowValue.frame.origin.y, rowValue.frame.size.width, circleBGView.frame.size.height)];
            [infoView addSubview:rowValue];
            
            circleBGView = [[UIView alloc] initWithFrame:CGRectMake(rowValue.frame.origin.x + rowValue.frame.size.width + CGWidthFromIP6P(8), breathCycleLabel.frame.origin.y + breathCycleLabel.frame.size.height + CGHeightFromIP6P(7), CGWidthFromIP6P(64 / 3), CGHeightFromIP6P(64 / 3))];
            [circleBGView setBackgroundColor:UIColorFromRGB(0xf696a1)];
            [circleBGView.layer setCornerRadius:circleBGView.frame.size.width / 2];
            [circleBGView setClipsToBounds:YES];
            [infoView addSubview:circleBGView];
            highLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 1, circleBGView.frame.size.width, circleBGView.frame.size.height)];
            [highLabel setBackgroundColor:[UIColor clearColor]];
            [highLabel setTextColor:[UIColor whiteColor]];
            [highLabel setTextAlignment:NSTextAlignmentCenter];
            [highLabel setText:@"고"];
            [highLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
            [circleBGView addSubview:highLabel];
            
            highValue = [[UILabel alloc] initWithFrame:CGRectMake(circleBGView.frame.origin.x + circleBGView.frame.size.width + CGWidthFromIP6P(3), circleBGView.frame.origin.y + 1, 100, circleBGView.frame.size.height)];
            [highValue setText:_dataDictionary[@"hrMax"]];
            [highValue setTextColor:UIColorFromRGB(0x525252)];
            [highValue setFont:MediumWithSize(CGHeightFromIP6P(20))];
            [highValue sizeToFit];
            [highValue setFrame:CGRectMake(highValue.frame.origin.x, highValue.frame.origin.y, highValue.frame.size.width, circleBGView.frame.size.height)];
            [infoView addSubview:highValue];
            
            
            UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), highValue.frame.origin.y + highValue.frame.size.height + CGHeightFromIP6P(30), CGWidthFromIP6P(330), k2PX)];
            [separatorView setBackgroundColor:UIColorFromRGB(0xe9e9e9)];
            [infoView addSubview:separatorView];
            
            
            UILabel *spectrumLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), separatorView.frame.origin.y + separatorView.frame.size.height + CGHeightFromIP6P(30), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [spectrumLabel setFont:MediumWithSize(16)];
            [spectrumLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
            [spectrumLabel setText:@"수면 깊이 스펙트럼"];
            [spectrumLabel sizeToFit];
            [infoView addSubview:spectrumLabel];
            
            
            int startTimestamp = [_dataDictionary[@"sleep_starttime"] intValue] - (60 * 60 * 9);
            int endTimestamp = startTimestamp + validSleepCount * 60;
            
            NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:startTimestamp];
            NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:endTimestamp];
            
            NSDateFormatter *currentDateFormatter = [[NSDateFormatter alloc] init];
            [currentDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            [currentDateFormatter setDateFormat:@"HH:mm a"];
            
            UILabel *startTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), spectrumLabel.frame.origin.y + spectrumLabel.frame.size.height + CGHeightFromIP6P(26), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [startTimeLabel setFont:MediumWithSize(13)];
            [startTimeLabel setTextColor:UIColorFromRGB(0x3b3b3b)];
            [startTimeLabel setText:[currentDateFormatter stringFromDate:startDate]];
            [startTimeLabel sizeToFit];
            [infoView addSubview:startTimeLabel];
            
            
            UILabel *endTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(startTimeLabel.frame.origin.x, startTimeLabel.frame.origin.y, CGWidthFromIP6P(330), startTimeLabel.frame.size.height)];
            [endTimeLabel setTextAlignment:NSTextAlignmentRight];
            [endTimeLabel setFont:MediumWithSize(13)];
            [endTimeLabel setTextColor:UIColorFromRGB(0x3b3b3b)];
            [endTimeLabel setText:[currentDateFormatter stringFromDate:endDate]];
            [infoView addSubview:endTimeLabel];
            
            UIView *spectrumView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), startTimeLabel.frame.origin.y + startTimeLabel.frame.size.height + CGHeightFromIP6P(9), CGWidthFromIP6P(330), CGHeightFromIP6P(32))];
            [spectrumView setBackgroundColor:UIColorFromRGB(0xa4e581)];
            [infoView addSubview:spectrumView];
            
            
            float lastX = 0;
            
            for (int i = 0; i < [_dataDictionary[@"graph"] count]; i++) {
                if ([_dataDictionary[@"graph"][i] intValue] == -1) {
                    continue;
                }
                UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(lastX, 0, spectrumView.frame.size.width / (float)(validSleepCount * 2), spectrumView.frame.size.height)];
                [spectrumView addSubview:colorView];
                
                if ([_dataDictionary[@"graph"][i] intValue] == 0) {
                    [colorView setBackgroundColor:UIColorFromRGB(0xffbfba)];
                } else if ([_dataDictionary[@"graph"][i] intValue] == 1) {
                    [colorView setBackgroundColor:UIColorFromRGB(0xfdef67)];
                } else if ([_dataDictionary[@"graph"][i] intValue] == 2) {
                    [colorView setBackgroundColor:UIColorFromRGB(0xa4e581)];
                } else if ([_dataDictionary[@"graph"][i] intValue] == 3) {
                    [colorView setBackgroundColor:UIColorFromRGB(0xa4e581)];
                } else if ([_dataDictionary[@"graph"][i] intValue] == 4) {
                    [colorView setBackgroundColor:UIColorFromRGB(0xdaade9)];
                }
                
                lastX = lastX + colorView.frame.size.width;
                //
            }
            
            
            UIView *dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), spectrumView.frame.origin.y + spectrumView.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
            [dotView setBackgroundColor:UIColorFromRGB(0xffbfba)];
            [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
            [dotView setClipsToBounds:YES];
            [infoView addSubview:dotView];
            
            UILabel *sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
            [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
            [sleepStatusLabel setTextColor:UIColorFromRGB(0x010101)];
            [sleepStatusLabel setText:@"깨어남"];
            [sleepStatusLabel sizeToFit];
            [infoView addSubview:sleepStatusLabel];
            
            
            dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(344 / 3), spectrumView.frame.origin.y + spectrumView.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
            [dotView setBackgroundColor:UIColorFromRGB(0xfdef67)];
            [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
            [dotView setClipsToBounds:YES];
            [infoView addSubview:dotView];
            
            sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
            [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
            [sleepStatusLabel setTextColor:UIColorFromRGB(0x010101)];
            [sleepStatusLabel setText:@"뒤척임"];
            [sleepStatusLabel sizeToFit];
            [infoView addSubview:sleepStatusLabel];
            
            
            dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(608 / 3), spectrumView.frame.origin.y + spectrumView.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
            [dotView setBackgroundColor:UIColorFromRGB(0xa4e581)];
            [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
            [dotView setClipsToBounds:YES];
            [infoView addSubview:dotView];
            
            sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
            [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
            [sleepStatusLabel setTextColor:UIColorFromRGB(0x010101)];
            [sleepStatusLabel setText:@"얕은잠"];
            [sleepStatusLabel sizeToFit];
            [infoView addSubview:sleepStatusLabel];
            
            
            dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(872 / 3), spectrumView.frame.origin.y + spectrumView.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
            [dotView setBackgroundColor:UIColorFromRGB(0xdaade9)];
            [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
            [dotView setClipsToBounds:YES];
            [infoView addSubview:dotView];
            
            sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
            [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
            [sleepStatusLabel setTextColor:UIColorFromRGB(0x010101)];
            [sleepStatusLabel setText:@"깊은잠"];
            [sleepStatusLabel sizeToFit];
            [infoView addSubview:sleepStatusLabel];
        } else {
            // 수면 정보 없음
            NSString *dateString = _dateString;

            UILabel *emptyLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), CGHeightFromIP6P(109 / 3), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
            [emptyLabel setFont:BoldWithSize(20)];
            [emptyLabel setTextColor:UIColorFromRGB(0x000000)];
            [emptyLabel setText:@"수면 데이터 없음"];
            [emptyLabel sizeToFit];
            [infoView addSubview:emptyLabel];

            UIButton *diaryButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(993 / 3), CGHeightFromIP6P(113 / 3), [UIImage imageNamed:@"cycledayinfo_button_diary"].size.width, [UIImage imageNamed:@"cycledayinfo_button_diary"].size.height)];
            [diaryButton setImage:[UIImage imageNamed:@"cycledayinfo_button_diary"] forState:UIControlStateNormal];
            [diaryButton setTag:[[dateString stringByReplacingOccurrencesOfString:@"-" withString:@""] intValue]];
            [diaryButton addTarget:self action:@selector(presentSleepDiaryViewController:) forControlEvents:UIControlEventTouchUpInside];
            [infoView addSubview:diaryButton];
            
            UIImageView *sensorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(364) / 3, CGHeightFromIP6P(364) / 3)];
            [sensorImageView setImage:[UIImage imageNamed:@"connectsensor_image_sensor"]];
            [sensorImageView setCenter:CGPointMake(infoView.frame.size.width / 2, infoView.frame.size.height / 2)];
            [infoView addSubview:sensorImageView];
            
            UILabel *guideLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, sensorImageView.frame.origin.y + sensorImageView.frame.size.height + CGHeightFromIP6P(24), infoView.frame.size.width, 10)];
            [guideLabel setFont:LightWithSize(15)];
            [guideLabel setTextColor:UIColorFromRGB(0x000000)];
            [guideLabel setText:@"센서를 사용하여\n수면 정보를 확인하실 수 있습니다."];
            [guideLabel setNumberOfLines:0];
            [guideLabel sizeToFit];
            [guideLabel setTextAlignment:NSTextAlignmentCenter];
            [guideLabel setFrame:CGRectMake(guideLabel.frame.origin.x, guideLabel.frame.origin.y, infoView.frame.size.width, guideLabel.frame.size.height)];
            [infoView addSubview:guideLabel];
            
        }


        view = infoView;
    } else {
        UIView *infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(1586 / 3))];
        [infoView setBackgroundColor:[UIColor whiteColor]];
        [infoView setClipsToBounds:YES];
        [infoView.layer setCornerRadius:CGWidthFromIP6P(38 / 3)];
        
        NSString *dateString = _dateString;

        NSDateFormatter *weekdayDateFormatter = [[NSDateFormatter alloc] init];
        [weekdayDateFormatter setDateFormat:@"yyyy-MM-dd"];
        [weekdayDateFormatter setTimeZone:[NSTimeZone localTimeZone]];

        UILabel *sleepTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), CGHeightFromIP6P(109 / 3), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
        [sleepTimeLabel setFont:MediumWithSize(16)];
        [sleepTimeLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [sleepTimeLabel setText:@"총 수면시간"];
        [sleepTimeLabel sizeToFit];
        [infoView addSubview:sleepTimeLabel];

        int validSleepCount = 0;
        
        int sleepType1 = 0;
        int sleepType2 = 0;
        int sleepType3 = 0;
        int sleepType4 = 0;

        for (int i = 0; i < [_dataDictionary[@"graph"] count]; i++) {
            if ([_dataDictionary[@"graph"][i] intValue] != -1) {
                validSleepCount++;
            }
            
            if ([_dataDictionary[@"graph"][i] intValue] == 0) {
                sleepType1++;
            } else if ([_dataDictionary[@"graph"][i] intValue] == 1) {
                sleepType2++;
            } else if ([_dataDictionary[@"graph"][i] intValue] == 2) {
                sleepType3++;
            } else if ([_dataDictionary[@"graph"][i] intValue] == 3) {
                sleepType3++;
            } else if ([_dataDictionary[@"graph"][i] intValue] == 4) {
                sleepType4++;
            }
        }

        sleepType1 = sleepType1 / 2;
        sleepType2 = sleepType2 / 2;
        sleepType3 = sleepType3 / 2;
        sleepType4 = sleepType4 / 2;

        validSleepCount = validSleepCount / 2;

        UILabel *sleepTimeValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(sleepTimeLabel.frame.origin.x, sleepTimeLabel.frame.origin.y + sleepTimeLabel.frame.size.height + CGHeightFromIP6P(10), CGWidthFromIP6P(200), CGHeightFromIP6P(52))];
        [sleepTimeValueLabel setBackgroundColor:[UIColor clearColor]];
        [sleepTimeValueLabel setText:[NSString stringWithFormat:@"%d시간 %d분", validSleepCount / 60, validSleepCount % 60]];
        [sleepTimeValueLabel setFont:BoldWithSize(CGHeightFromIP6P(32))];
        [sleepTimeValueLabel setTextColor:UIColorFromRGB(0x525252)];
        [sleepTimeValueLabel sizeToFit];
        [infoView addSubview:sleepTimeValueLabel];

        UIButton *diaryButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(993 / 3), CGHeightFromIP6P(113 / 3), [UIImage imageNamed:@"cycledayinfo_button_diary"].size.width, [UIImage imageNamed:@"cycledayinfo_button_diary"].size.height)];
        [diaryButton setImage:[UIImage imageNamed:@"cycledayinfo_button_diary"] forState:UIControlStateNormal];
        [diaryButton setTag:[[dateString stringByReplacingOccurrencesOfString:@"-" withString:@""] intValue]];
        [diaryButton addTarget:self action:@selector(presentSleepDiaryViewController:) forControlEvents:UIControlEventTouchUpInside];
        [infoView addSubview:diaryButton];

        if ([[weekdayDateFormatter dateFromString:dateString] compare:[NSDate date]] == NSOrderedDescending) {
            [diaryButton setAlpha:0];
        } else {
            UIImageView *balloonImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_balloon"]];
            [balloonImageView setFrame:CGRectMake(diaryButton.frame.origin.x + diaryButton.frame.size.width - balloonImageView.frame.size.width + CGWidthFromIP6P(14), diaryButton.frame.origin.y + diaryButton.frame.size.height + CGHeightFromIP6P(6), balloonImageView.frame.size.width, balloonImageView.frame.size.height)];
            [balloonImageView setAlpha:0];
            [infoView addSubview:balloonImageView];

            [self performSelector:@selector(balloonAnimation:) withObject:balloonImageView afterDelay:0.0f];
        }

        UILabel *stageTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), CGHeightFromIP6P(459 / 3), CGWidthFromIP6P(180 / 3), CGHeightFromIP6P(50 / 3))];
        [stageTimeLabel setFont:MediumWithSize(16)];
        [stageTimeLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [stageTimeLabel setText:@"단계별 수면 시간"];
        [stageTimeLabel sizeToFit];
        [infoView addSubview:stageTimeLabel];


        PieChartView *circleChartView = [[PieChartView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(357 / 3), stageTimeLabel.frame.origin.y + stageTimeLabel.frame.size.height + CGHeightFromIP6P(6), CGWidthFromIP6P(435 / 3), CGHeightFromIP6P(435 / 3))];
        [circleChartView setDelegate:self];
        [circleChartView setDatasource:self];
//        [circleChartView setBackgroundColor:UIColorFromRGB(0xffbfba)];
//        [circleChartView.layer setCornerRadius:circleChartView.frame.size.width / 2];
//        [circleChartView setClipsToBounds:YES];
        [circleChartView.layer setShadowColor:[UIColor clearColor].CGColor];
        [infoView addSubview:circleChartView];



        UIView *dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), circleChartView.frame.origin.y + circleChartView.frame.size.height + CGHeightFromIP6P(35), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
        [dotView setBackgroundColor:UIColorFromRGB(0xffbfba)];
        [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
        [dotView setClipsToBounds:YES];
        [infoView addSubview:dotView];

        UILabel *sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y + 1, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
        [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
        [sleepStatusLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [sleepStatusLabel setText:@"깨어남"];
        [sleepStatusLabel sizeToFit];
        [sleepStatusLabel setFrame:CGRectMake(sleepStatusLabel.frame.origin.x, sleepStatusLabel.frame.origin.y, sleepStatusLabel.frame.size.width, dotView.frame.size.height)];
        [infoView addSubview:sleepStatusLabel];
        
        NSMutableString *sleepTimeString = [[NSMutableString alloc] init];
        if (sleepType1 >= 60) {
            [sleepTimeString appendFormat:@"%d시간", sleepType1 / 60];
        }
        if (sleepType1 % 60 || sleepType1 == 0) {
            [sleepTimeString appendFormat:@" %d분", sleepType1 % 60];
        }

        UILabel *timeValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x, dotView.frame.origin.y + dotView.frame.size.height + CGHeightFromIP6P(9), CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
        [timeValueLabel setFont:MediumWithSize(CGHeightFromIP6P(20))];
        [timeValueLabel setTextColor:UIColorFromRGB(0x525252)];
        [timeValueLabel setText:[sleepTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        [timeValueLabel sizeToFit];
        [infoView addSubview:timeValueLabel];

        UILabel *percentageValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(timeValueLabel.frame.origin.x + timeValueLabel.frame.size.width + CGWidthFromIP6P(4), timeValueLabel.frame.origin.y , CGWidthFromIP6P(100), timeValueLabel.frame.size.height)];
        [percentageValueLabel setFont:MediumWithSize(CGHeightFromIP6P(19))];
        [percentageValueLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [percentageValueLabel setText:[NSString stringWithFormat:@"%.1f%%", ((float)sleepType1 / validSleepCount) * 100.0]];
        [percentageValueLabel sizeToFit];
        [percentageValueLabel setFrame:CGRectMake(percentageValueLabel.frame.origin.x, percentageValueLabel.frame.origin.y, percentageValueLabel.frame.size.width, timeValueLabel.frame.size.height + CGHeightFromIP6P(1))];
        [infoView addSubview:percentageValueLabel];


        dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(576 / 3), dotView.frame.origin.y, CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
        [dotView setBackgroundColor:UIColorFromRGB(0xa4e581)];
        [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
        [dotView setClipsToBounds:YES];
        [infoView addSubview:dotView];

        sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y + 1, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
        [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
        [sleepStatusLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [sleepStatusLabel setText:@"얕은잠"];
        [sleepStatusLabel sizeToFit];
        [sleepStatusLabel setFrame:CGRectMake(sleepStatusLabel.frame.origin.x, sleepStatusLabel.frame.origin.y, sleepStatusLabel.frame.size.width, dotView.frame.size.height)];
        [infoView addSubview:sleepStatusLabel];

        sleepTimeString = [[NSMutableString alloc] init];
        if (sleepType3 >= 60) {
            [sleepTimeString appendFormat:@"%d시간", sleepType3 / 60];
        }
        if (sleepType3 % 60 || sleepType3 == 0) {
            [sleepTimeString appendFormat:@" %d분", sleepType3 % 60];
        }
        
        timeValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x, dotView.frame.origin.y + dotView.frame.size.height + CGHeightFromIP6P(9), CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
        [timeValueLabel setFont:MediumWithSize(CGHeightFromIP6P(20))];
        [timeValueLabel setTextColor:UIColorFromRGB(0x525252)];
        [timeValueLabel setText:[sleepTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        [timeValueLabel sizeToFit];
        [infoView addSubview:timeValueLabel];

        percentageValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(timeValueLabel.frame.origin.x + timeValueLabel.frame.size.width + CGWidthFromIP6P(4), timeValueLabel.frame.origin.y, CGWidthFromIP6P(100), timeValueLabel.frame.size.height)];
        [percentageValueLabel setFont:MediumWithSize(CGHeightFromIP6P(19))];
        [percentageValueLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [percentageValueLabel setText:[NSString stringWithFormat:@"%.1f%%", ((float)sleepType3 / validSleepCount) * 100.0]];
        [percentageValueLabel sizeToFit];
        [percentageValueLabel setFrame:CGRectMake(percentageValueLabel.frame.origin.x, percentageValueLabel.frame.origin.y, percentageValueLabel.frame.size.width, timeValueLabel.frame.size.height + CGHeightFromIP6P(1))];
        [infoView addSubview:percentageValueLabel];





        dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), timeValueLabel.frame.origin.y + timeValueLabel.frame.size.height + CGHeightFromIP6P(33), CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
        [dotView setBackgroundColor:UIColorFromRGB(0xfdef67)];
        [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
        [dotView setClipsToBounds:YES];
        [infoView addSubview:dotView];

        sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y + 1, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
        [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
        [sleepStatusLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [sleepStatusLabel setText:@"뒤척임"];
        [sleepStatusLabel sizeToFit];
        [sleepStatusLabel setFrame:CGRectMake(sleepStatusLabel.frame.origin.x, sleepStatusLabel.frame.origin.y, sleepStatusLabel.frame.size.width, dotView.frame.size.height)];
        [infoView addSubview:sleepStatusLabel];

        sleepTimeString = [[NSMutableString alloc] init];
        if (sleepType2 >= 60) {
            [sleepTimeString appendFormat:@"%d시간", sleepType2 / 60];
        }
        if (sleepType2 % 60 || sleepType2 == 0) {
            [sleepTimeString appendFormat:@" %d분", sleepType2 % 60];
        }

        timeValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x, dotView.frame.origin.y + dotView.frame.size.height + CGHeightFromIP6P(9), CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
        [timeValueLabel setFont:MediumWithSize(CGHeightFromIP6P(20))];
        [timeValueLabel setTextColor:UIColorFromRGB(0x525252)];
        [timeValueLabel setText:[sleepTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        [timeValueLabel sizeToFit];
        [infoView addSubview:timeValueLabel];

        percentageValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(timeValueLabel.frame.origin.x + timeValueLabel.frame.size.width + CGWidthFromIP6P(4), timeValueLabel.frame.origin.y, CGWidthFromIP6P(100), timeValueLabel.frame.size.height)];
        [percentageValueLabel setFont:MediumWithSize(CGHeightFromIP6P(19))];
        [percentageValueLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [percentageValueLabel setText:[NSString stringWithFormat:@"%.1f%%", ((float)sleepType2 / validSleepCount) * 100.0]];
        [percentageValueLabel sizeToFit];
        [percentageValueLabel setFrame:CGRectMake(percentageValueLabel.frame.origin.x, percentageValueLabel.frame.origin.y, percentageValueLabel.frame.size.width, timeValueLabel.frame.size.height + CGHeightFromIP6P(1))];
        [infoView addSubview:percentageValueLabel];


        dotView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(576 / 3), dotView.frame.origin.y, CGWidthFromIP6P(54 / 3), CGHeightFromIP6P(54 / 3))];
        [dotView setBackgroundColor:UIColorFromRGB(0xdaade9)];
        [dotView.layer setCornerRadius:dotView.frame.size.width / 2];
        [dotView setClipsToBounds:YES];
        [infoView addSubview:dotView];

        sleepStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x + dotView.frame.size.width + CGWidthFromIP6P(6), dotView.frame.origin.y + 1, CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
        [sleepStatusLabel setFont:LightWithSize(CGHeightFromIP6P(16))];
        [sleepStatusLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [sleepStatusLabel setText:@"깊은잠"];
        [sleepStatusLabel sizeToFit];
        [sleepStatusLabel setFrame:CGRectMake(sleepStatusLabel.frame.origin.x, sleepStatusLabel.frame.origin.y, sleepStatusLabel.frame.size.width, dotView.frame.size.height)];
        [infoView addSubview:sleepStatusLabel];

        sleepTimeString = [[NSMutableString alloc] init];
        if (sleepType4 >= 60) {
            [sleepTimeString appendFormat:@"%d시간", sleepType4 / 60];
        }
        if (sleepType4 % 60 || sleepType4 == 0) {
            [sleepTimeString appendFormat:@" %d분", sleepType4 % 60];
        }
        
        timeValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(dotView.frame.origin.x, dotView.frame.origin.y + dotView.frame.size.height + CGHeightFromIP6P(9), CGWidthFromIP6P(100), CGHeightFromIP6P(100))];
        [timeValueLabel setFont:MediumWithSize(CGHeightFromIP6P(20))];
        [timeValueLabel setTextColor:UIColorFromRGB(0x525252)];
        [timeValueLabel setText:[sleepTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        [timeValueLabel sizeToFit];
        [infoView addSubview:timeValueLabel];

        percentageValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(timeValueLabel.frame.origin.x + timeValueLabel.frame.size.width + CGWidthFromIP6P(4), timeValueLabel.frame.origin.y, CGWidthFromIP6P(100), timeValueLabel.frame.size.height)];
        [percentageValueLabel setFont:MediumWithSize(CGHeightFromIP6P(19))];
        [percentageValueLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [percentageValueLabel setText:[NSString stringWithFormat:@"%.1f%%", ((float)sleepType4 / validSleepCount) * 100.0]];
        [percentageValueLabel sizeToFit];
        [percentageValueLabel setFrame:CGRectMake(percentageValueLabel.frame.origin.x, percentageValueLabel.frame.origin.y, percentageValueLabel.frame.size.width, timeValueLabel.frame.size.height + CGHeightFromIP6P(1))];
        [infoView addSubview:percentageValueLabel];


        view = infoView;
    }
    
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(1586 / 3))];
    [shadowView.layer setShadowPath:[UIBezierPath bezierPathWithRect:view.bounds].CGPath];
    [shadowView.layer setShadowColor:[UIColor blackColor].CGColor];
    [shadowView.layer setShadowOffset:CGSizeMake(2, 2)];
    [shadowView.layer setShadowRadius:14];
    [shadowView.layer setShadowOpacity:0.6];
    [shadowView setBackgroundColor:[UIColor clearColor]];
    [shadowView setClipsToBounds:NO];
    [shadowView addSubview:view];
    
    view = shadowView;
    
    return view;
}

- (void)touchCardBanner:(UITapGestureRecognizer *)gesture
{
    [MSAnalytics trackEvent:@"Banner clicked" withProperties:@{
                                                               @"PageId" : @"CycleInfoDaily",
                                                               @"BannerNo" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][gesture.view.tag][@"BannerNo"] intValue]],
                                                               @"BannerPublishGroupCd" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][gesture.view.tag][@"BannerPublishGroupCd"] intValue]],
                                                               @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                               @"All" : [NSString stringWithFormat:@"%@|%@|%@|%@",
                                                                         @"CycleInfoDaily",
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][gesture.view.tag][@"BannerNo"] intValue]],
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][gesture.view.tag][@"BannerPublishGroupCd"] intValue]],
                                                                         [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                         ]
                                                               }];
    
    if ([_dataDictionary[@"Banner"][gesture.view.tag][@"MoveType"] intValue] == 101) {
        [(RootViewController *)_pClass changeToEventViewController];
        EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_dataDictionary[@"Banner"][gesture.view.tag][@"IndexNo"]];
        UINavigationController *eventNavigationController = (UINavigationController *)[[(RootViewController *)_pClass drawerViewController] centerViewController];
        [eventNavigationController pushViewController:eventDetailViewController animated:YES];

//        EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_dataDictionary[@"Banner"][gesture.view.tag][@"IndexNo"]];
//        [self.navigationController pushViewController:eventDetailViewController animated:YES];
    } else if ([_dataDictionary[@"Banner"][gesture.view.tag][@"MoveType"] intValue] == 102) {
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][gesture.view.tag][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] options:@{} completionHandler:^(BOOL success) {
                exit(0);
            }];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][gesture.view.tag][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
        }
    }
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing) {
        return value * 1.28;
    } else if (option == iCarouselOptionWrap) {
        return NO;
    } else if (option == iCarouselOptionVisibleItems) {
        if (carousel.currentItemIndex == 0 || (carousel.currentItemIndex == carousel.numberOfItems - 1)) {
            return 2;
        } else {
            return 3;
        }
    }
    return value;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    [_cardPageControl setCurrentPage:carousel.currentItemIndex];
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
    [_cardPageControl setCurrentPage:carousel.currentItemIndex];
}

    
- (int)numberOfSlicesInPieChartView:(PieChartView *)pieChartView {
    return 4;
}
    
- (UIColor *)pieChartView:(PieChartView *)pieChartView colorForSliceAtIndex:(NSUInteger)index {
    if (index == 0) {
        return UIColorFromRGB(0xffbfba);
    } else if (index == 1) {
        return UIColorFromRGB(0xfdef67);
    } else if (index == 2) {
        return UIColorFromRGB(0xa4e581);
    } else if (index == 3) {
        return UIColorFromRGB(0xdaade9);
    }
    return UIColorFromRGB(0x000000);
}
    
- (double)pieChartView:(PieChartView *)pieChartView valueForSliceAtIndex:(NSUInteger)index {
    
    int validSleepCount = 0;
    
    int sleepType1 = 0;
    int sleepType2 = 0;
    int sleepType3 = 0;
    int sleepType4 = 0;
    
    for (int i = 0; i < [_dataDictionary[@"graph"] count]; i++) {
        if ([_dataDictionary[@"graph"][i] intValue] != -1) {
            validSleepCount++;
        }
        
        if ([_dataDictionary[@"graph"][i] intValue] == 0) {
            sleepType1++;
        } else if ([_dataDictionary[@"graph"][i] intValue] == 1) {
            sleepType2++;
        } else if ([_dataDictionary[@"graph"][i] intValue] == 2) {
            sleepType3++;
        } else if ([_dataDictionary[@"graph"][i] intValue] == 3) {
            sleepType3++;
        } else if ([_dataDictionary[@"graph"][i] intValue] == 4) {
            sleepType4++;
        }
    }
    
    sleepType1 = sleepType1 / 2;
    sleepType2 = sleepType2 / 2;
    sleepType3 = sleepType3 / 2;
    sleepType4 = sleepType4 / 2;
    
    validSleepCount = validSleepCount / 2;
    
    if (index == 0) {
        return ((float)sleepType1 / validSleepCount);
    } else if (index == 1) {
        return ((float)sleepType2 / validSleepCount);
    } else if (index == 2) {
        return ((float)sleepType3 / validSleepCount);
    } else if (index == 3) {
        return ((float)sleepType4 / validSleepCount);
    }
    return 0;
}
    
    
@end
