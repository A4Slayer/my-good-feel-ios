//
//  CycleDayInfoGuideViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "CycleDayInfoGuideViewController.h"

@interface CycleDayInfoGuideViewController ()

@end

@implementation CycleDayInfoGuideViewController

- (void)dismissSelfView
{
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        [self setTitle:@"일간 주기 정보 읽는 법"];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_close_pink"].size.width, [UIImage imageNamed:@"common_button_close_pink"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"common_button_close_pink"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];
        
        
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT)];
        [_mainScrollView setBackgroundColor:UIColorFromRGB(0xffffff)];
        [self.view addSubview:_mainScrollView];
        

        UILabel *emptyIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(124 / 3), CGWidthFromIP6P(540 / 3), CGHeightFromIP6P(93 / 3))];
        [emptyIconLabel setBackgroundColor:[UIColor clearColor]];
        [emptyIconLabel setAdjustsFontSizeToFitWidth:YES];
        [_mainScrollView addSubview:emptyIconLabel];
        CNSTextAttachment *imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_none"]];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  주기 아님/생리 양 없음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [emptyIconLabel setAttributedText:attrString];
        
        
        UILabel *preIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(124 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [preIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:preIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_pre"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  예상 주기"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [preIconLabel setAttributedText:attrString];


        UILabel *bloodLessIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(286 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodLessIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodLessIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_less"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 양 적음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodLessIconLabel setAttributedText:attrString];

        
        UILabel *bloodNormalIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(286 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodNormalIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodNormalIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_availablemenstruation"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  예상 가임기"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodNormalIconLabel setAttributedText:attrString];

        
        UILabel *bloodMuchIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(448 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodMuchIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodMuchIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_normal"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리 양 보통"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodMuchIconLabel setAttributedText:attrString];
        
        
        UILabel *bloodNoneIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(448 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [bloodNoneIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:bloodNoneIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_pyrexia"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  발열 있음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [bloodNoneIconLabel setAttributedText:attrString];
        
        
        UILabel *pyrexiaIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(140 / 3), CGHeightFromIP6P(610 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [pyrexiaIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:pyrexiaIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_much"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  생리양 많음"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [pyrexiaIconLabel setAttributedText:attrString];
        
        
        UILabel *loveIconLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(703 / 3), CGHeightFromIP6P(610 / 3), CGWidthFromIP6P(414 / 3), CGHeightFromIP6P(93 / 3))];
        [loveIconLabel setBackgroundColor:[UIColor clearColor]];
        [_mainScrollView addSubview:loveIconLabel];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -10)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_sex"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  사랑"]
                                                                           attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(16)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x434343) }]];
        [loveIconLabel setAttributedText:attrString];

        
        UILabel *pyrexiaGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(CGHeightFromIP6P(822 / 3)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [pyrexiaGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [pyrexiaGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [pyrexiaGuideLabel setClipsToBounds:YES];
        [pyrexiaGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [pyrexiaGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [pyrexiaGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [pyrexiaGuideLabel setText:@"발열과 생리 주기의 관계"];
        [_mainScrollView addSubview:pyrexiaGuideLabel];
        
        UILabel *pyrexiaDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(pyrexiaGuideLabel.frame.origin.y + pyrexiaGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [pyrexiaDescLabel setNumberOfLines:0];
        [pyrexiaDescLabel setBackgroundColor:[UIColor clearColor]];
        [pyrexiaDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [pyrexiaDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [pyrexiaDescLabel setText:@"배란 이후 체내에 프로게스테론(황체호르몬)의 분비가 증가함에 따라 생리 시작일 2주 전부터 미열을 느낄 수 있습니다. 이후 생리 시작일부터 체온이 정상으로 돌아옵니다."];
        [pyrexiaDescLabel sizeToFit];
        [_mainScrollView addSubview:pyrexiaDescLabel];

        
        UILabel *weightGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(pyrexiaDescLabel.frame.origin.y + pyrexiaDescLabel.frame.size.height + CGHeightFromIP6P(111 / 3)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [weightGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [weightGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [weightGuideLabel setClipsToBounds:YES];
        [weightGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [weightGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [weightGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [weightGuideLabel setText:@"몸무게와 생리 주기의 관계"];
        [_mainScrollView addSubview:weightGuideLabel];
        
        UILabel *weightDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(weightGuideLabel.frame.origin.y + weightGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [weightDescLabel setNumberOfLines:0];
        [weightDescLabel setBackgroundColor:[UIColor clearColor]];
        [weightDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [weightDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [weightDescLabel setText:@"배란 이후 체내에 프로게스테론(황체호르몬)의 분비가 증가함에 따라 임신을 대비하여 체내에 수분과 영양분을 축적합니다. 역시 생리 시작일 2주 전부터 몸무게가 증가하며 이후 생리 시작일부터 다시 몸무게가 줄어듭니다."];
        [weightDescLabel sizeToFit];
        [_mainScrollView addSubview:weightDescLabel];

        [_mainScrollView setContentSize:CGSizeMake(_mainScrollView.frame.size.width, weightDescLabel.frame.origin.y + weightDescLabel.frame.size.height + kBOTTOM_HEIGHT + CGHeightFromIP6P(16))];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
