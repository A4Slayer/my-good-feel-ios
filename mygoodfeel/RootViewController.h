//
//  RootViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 13..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"
#import "CMMDrawerViewController.h"

#import "SignViewController.h"

#import "LeftMenuViewController.h"
#import "MainViewController.h"
#import "CycleDayInfoViewController.h"

#import "SleepDayInfoViewController.h"
#import "SleepCalendarViewController.h"

#import "DailyReportViewController.h"
#import "MenstruationingViewController.h"

#import "EventViewController.h"
#import "MomQViewController.h"

#import "NoticeViewController.h"

#import "NotificationViewController.h"

#import "PreferenceViewController.h"

#import "ConnectSensorPreviousViewController.h"

@interface RootViewController : UIViewController <UIAlertViewDelegate>

@property (strong, nonatomic) SignViewController *signViewController;

@property (strong, nonatomic) CMMDrawerViewController *drawerViewController;
@property (strong, nonatomic) LeftMenuViewController *leftMenuViewController;
@property (strong, nonatomic) MainViewController *mainViewController;
@property (strong, nonatomic) CycleDayInfoViewController *cycleDayInfoViewController;

@property (strong, nonatomic) DailyReportViewController *dailyReportViewController;
@property (strong, nonatomic) MenstruationingViewController *menstruationingViewController;
@property (strong, nonatomic) EventViewController *eventViewController;
@property (strong, nonatomic) MomQViewController *momQViewController;
@property (strong, nonatomic) NoticeViewController *noticeViewController;
@property (strong, nonatomic) NotificationViewController *notificationViewController;
@property (strong, nonatomic) PreferenceViewController *preferenceViewController;

@property (strong, nonatomic) SleepDayInfoViewController *sleepDayInfoViewController;
@property (strong, nonatomic) SleepCalendarViewController *sleepCalendarViewController;

@property (strong, nonatomic) UINavigationController *connectSensorNavigationController;

@property (strong, nonatomic) UIView *statusBarBGView;
@property (strong, nonatomic) UIButton *centerCoverButton;
@property (strong, nonatomic) UIView *statusBarCoverView;

@property (strong, nonatomic) UIImageView *beforeBGImageView;
@property (strong, nonatomic) UIImageView *beforeTitleImageView;
@property (strong, nonatomic) UIImageView *beforeCopyrightImageView;

@property (strong, nonatomic) NSString *updateURL;

- (void)toggleLeftMenu;
- (void)attachMainViewController;
- (void)attachSignViewController;
- (void)changeToNoticeViewController;
- (void)changeToNotificationViewController;
- (void)changeToCycleDayInfoViewController:(NSString *)dateString;
- (void)changeToCycleDayInfoViewController;
- (void)changeToMainViewController;
- (void)changeToMainViewController:(NSString *)dateString;
- (void)changeToSleepDayInfoViewController;
- (void)changeToSleepCalendarViewController:(NSString *)dateString;
- (void)changeToDailyReportViewController;
- (void)changeToMenstruationingViewController;
- (void)changeToEventViewController;
- (void)changeToMomQViewController;
- (void)changeToPreferenceViewController;

- (void)attachConnectSensorViewController;
- (void)attachSignViewControllerFromLogout;

@end
