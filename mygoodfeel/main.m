//
//  main.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 13..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
