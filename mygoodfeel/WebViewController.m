//
//  WebViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 8..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)refreshWebView
{
    [_webView reload];
}

- (void)backWebView
{
    [_webView goBack];
}

- (void)forwardWebView
{
    [_webView goForward];
}

- (void)dismissSelfView
{
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil webURL:(NSString *)webURL title:(NSString *)title
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        [self.view setBackgroundColor:UIColorFromRGB(0x00ff00)];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.view.bounds;
        gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [self.view.layer insertSublayer:gradient atIndex:0];

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:title];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cyclecalendar_image_close"].size.width, [UIImage imageNamed:@"cyclecalendar_image_close"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"cyclecalendar_image_close"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];

        UIButton *refreshButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_refresh"].size.width, [UIImage imageNamed:@"common_button_refresh"].size.height)];
        [refreshButton setImage:[UIImage imageNamed:@"common_button_refresh"] forState:UIControlStateNormal];
        [refreshButton addTarget:self action:@selector(refreshWebView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:refreshButton]];
        
        
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT - CGHeightFromIP6P(162 / 3) - kBOTTOM_HEIGHT)];
        [_webView setBackgroundColor:[UIColor clearColor]];
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webURL]]];
        [_webView setDelegate:self];
        [_webView setScalesPageToFit:YES];
        [self.view addSubview:_webView];
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - CGHeightFromIP6P(162 / 3) - kBOTTOM_HEIGHT, kSCREEN_WIDTH, CGHeightFromIP6P(162 / 3) + kBOTTOM_HEIGHT)];
        [bottomView setBackgroundColor:UIColorFromRGB(0xf9f9f9)];
        [self.view addSubview:bottomView];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, bottomView.frame.origin.y - k1PX, kSCREEN_WIDTH, k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xb2b2b2)];
        [self.view addSubview:separatorView];
        
        
        _backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(44), CGHeightFromIP6P(44))];
        [_backButton setImage:[UIImage imageNamed:@"common_button_back_disable"] forState:UIControlStateNormal];
        [_backButton setCenter:CGPointMake(bottomView.frame.size.width / 2 / 3 - CGWidthFromIP6P(18), (bottomView.frame.size.height - kBOTTOM_HEIGHT) / 2)];
        [_backButton addTarget:self action:@selector(backWebView) forControlEvents:UIControlEventTouchUpInside];
        [_backButton setEnabled:NO];
        [bottomView addSubview:_backButton];
        
        _forwardButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(44), CGHeightFromIP6P(44))];
        [_forwardButton setImage:[UIImage imageNamed:@"common_button_forward_disable"] forState:UIControlStateNormal];
        [_forwardButton setCenter:CGPointMake(bottomView.frame.size.width / 2 / 3 * 2 - CGWidthFromIP6P(4), (bottomView.frame.size.height - kBOTTOM_HEIGHT) / 2)];
        [_forwardButton addTarget:self action:@selector(forwardWebView) forControlEvents:UIControlEventTouchUpInside];
        [_forwardButton setEnabled:NO];
        [bottomView addSubview:_forwardButton];
        
        _shareButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(44), CGHeightFromIP6P(44))];
        [_shareButton setImage:[UIImage imageNamed:@"common_button_share"] forState:UIControlStateNormal];
        [_shareButton setCenter:CGPointMake(bottomView.frame.size.width / 2, (bottomView.frame.size.height - kBOTTOM_HEIGHT) / 2)];
        [_shareButton addTarget:self action:@selector(shareAction) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:_shareButton];
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    UIView *navigationBarContentView = self.navigationController.navigationBar.subviews[2];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = navigationBarContentView.bounds;
    gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
    [gradient setStartPoint:CGPointMake(0.0, 0.5)];
    [gradient setEndPoint:CGPointMake(1.0, 0.5)];
    [navigationBarContentView.layer insertSublayer:gradient atIndex:0];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)shareAction
{
    NSString *title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    NSURL *url = _webView.request.URL;
    NSArray *postItems = @[title, url];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:postItems applicationActivities:nil];
    
    activityVC.excludedActivityTypes = @[];
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([webView canGoBack]) {
        [_backButton setEnabled:YES];
        [_backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
    } else {
        [_backButton setEnabled:NO];
        [_backButton setImage:[UIImage imageNamed:@"common_button_back_disable"] forState:UIControlStateNormal];
    }
    
    if ([webView canGoForward]) {
        [_forwardButton setEnabled:YES];
        [_forwardButton setImage:[UIImage imageNamed:@"common_button_forward"] forState:UIControlStateNormal];
    } else {
        [_forwardButton setEnabled:NO];
        [_forwardButton setImage:[UIImage imageNamed:@"common_button_forward_disable"] forState:UIControlStateNormal];
    }
}

@end
