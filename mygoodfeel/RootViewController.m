//
//  RootViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 13..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)dealloc
{
    [self->_drawerViewController.centerViewController.view.superview removeObserver:self forKeyPath:@"frame"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSString *contextStr = [NSString stringWithFormat:@"%@", context];
    if ([contextStr isEqualToString:@"MovedCenterView"]) {

        float ratio = self->_drawerViewController.centerViewController.view.superview.frame.origin.x / CGWidthFromIP6P(855 / 3);
        [_centerCoverButton setAlpha:0.5 * ratio];
        [_statusBarCoverView setAlpha:0.5 * ratio];

    }
}


- (void)toggleLeftMenu
{
    [_drawerViewController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        //
    }];
}

- (void)changeToPreferenceViewController
{
    self->_preferenceViewController = [[PreferenceViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *preferenceViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_preferenceViewController];
    
    [preferenceViewNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf9f9f9)]
                                                           forBarPosition:UIBarPositionAny
                                                               barMetrics:UIBarMetricsDefault];
    
    [_drawerViewController setCenterViewController:preferenceViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToNotificationViewController
{
    self->_notificationViewController = [[NotificationViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *notificationViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_notificationViewController];
    
    [notificationViewNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f5)]
                                                      forBarPosition:UIBarPositionAny
                                                          barMetrics:UIBarMetricsDefault];
    [notificationViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    
    [_drawerViewController setCenterViewController:notificationViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToNoticeViewController
{
    self->_noticeViewController = [[NoticeViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *noticeViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_noticeViewController];
    
    [noticeViewNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f5)]
                                                      forBarPosition:UIBarPositionAny
                                                          barMetrics:UIBarMetricsDefault];
    [noticeViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    
    [_drawerViewController setCenterViewController:noticeViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToDailyReportViewController
{
    self->_dailyReportViewController = [[DailyReportViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *dailyReportViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_dailyReportViewController];
    
    [dailyReportViewNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf1f1f5)]
                                                           forBarPosition:UIBarPositionAny
                                                               barMetrics:UIBarMetricsDefault];
    [dailyReportViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    
    [_drawerViewController setCenterViewController:dailyReportViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToMenstruationingViewController
{
    self->_menstruationingViewController = [[MenstruationingViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *menstruationingViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_menstruationingViewController];
    
    [menstruationingViewNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                           forBarPosition:UIBarPositionAny
                                                               barMetrics:UIBarMetricsDefault];
    [menstruationingViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];

    [_drawerViewController setCenterViewController:menstruationingViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToEventViewController
{
    self->_eventViewController = [[EventViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *eventViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_eventViewController];
    
    [eventViewNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf9f9f9)]
                                                     forBarPosition:UIBarPositionAny
                                                         barMetrics:UIBarMetricsDefault];
    [eventViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    [_drawerViewController setCenterViewController:eventViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToMomQViewController
{
    self->_momQViewController = [[MomQViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *momQViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_momQViewController];
    
    [momQViewNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf9f9f9)]
                                                          forBarPosition:UIBarPositionAny
                                                              barMetrics:UIBarMetricsDefault];
    [momQViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    
    [_drawerViewController setCenterViewController:momQViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToCycleDayInfoViewController:(NSString *)dateString
{
    _cycleDayInfoViewController = [[CycleDayInfoViewController alloc] initWithNibName:nil bundle:nil pClass:self dateString:dateString];
    UINavigationController *cycleDayInfoViewNavigationController = [[UINavigationController alloc] initWithRootViewController:_cycleDayInfoViewController];
    [cycleDayInfoViewNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                            forBarPosition:UIBarPositionAny
                                                                barMetrics:UIBarMetricsDefault];
    [cycleDayInfoViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [cycleDayInfoViewNavigationController.navigationBar setTintColor:[UIColor clearColor]];
    
    
    [_drawerViewController setCenterViewController:cycleDayInfoViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToCycleDayInfoViewController
{
    NSDateFormatter *currentDateFormatter = [[NSDateFormatter alloc] init];
    [currentDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [currentDateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    _cycleDayInfoViewController = [[CycleDayInfoViewController alloc] initWithNibName:nil bundle:nil pClass:self dateString:[currentDateFormatter stringFromDate:[NSDate date]]];
    UINavigationController *cycleDayInfoViewNavigationController = [[UINavigationController alloc] initWithRootViewController:_cycleDayInfoViewController];
    [cycleDayInfoViewNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                    forBarPosition:UIBarPositionAny
                                                        barMetrics:UIBarMetricsDefault];
    [cycleDayInfoViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [cycleDayInfoViewNavigationController.navigationBar setTintColor:[UIColor clearColor]];

    
    [_drawerViewController setCenterViewController:cycleDayInfoViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToMainViewController:(NSString *)dateString
{
    NSDateFormatter *currentDateFormatter = [[NSDateFormatter alloc] init];
    [currentDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [currentDateFormatter setDateFormat:@"yyyy-MM-dd"];

    self->_mainViewController = [[MainViewController alloc] initWithNibName:nil bundle:nil pClass:self date:[currentDateFormatter dateFromString:dateString]];
    UINavigationController *mainViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_mainViewController];
    [mainViewNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                    forBarPosition:UIBarPositionAny
                                                        barMetrics:UIBarMetricsDefault];
    [mainViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    
    [_drawerViewController setCenterViewController:mainViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToSleepCalendarViewController:(NSString *)dateString
{
    NSDateFormatter *currentDateFormatter = [[NSDateFormatter alloc] init];
    [currentDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [currentDateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    _sleepCalendarViewController = [[SleepCalendarViewController alloc] initWithNibName:nil bundle:nil pClass:self date:[currentDateFormatter dateFromString:dateString]];
    UINavigationController *sleepCalendarNavigationController = [[UINavigationController alloc] initWithRootViewController:_sleepCalendarViewController];
    [sleepCalendarNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                         forBarPosition:UIBarPositionAny
                                                             barMetrics:UIBarMetricsDefault];
    [sleepCalendarNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [sleepCalendarNavigationController.navigationBar setTintColor:[UIColor clearColor]];
    
    
    [_drawerViewController setCenterViewController:sleepCalendarNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)changeToSleepDayInfoViewController
{
    NSDateFormatter *currentDateFormatter = [[NSDateFormatter alloc] init];
    [currentDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [currentDateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    _sleepDayInfoViewController = [[SleepDayInfoViewController alloc] initWithNibName:nil bundle:nil pClass:self dateString:[currentDateFormatter stringFromDate:[NSDate date]]];
    UINavigationController *sleepDayInfoNavigationController = [[UINavigationController alloc] initWithRootViewController:_sleepDayInfoViewController];
    [sleepDayInfoNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                        forBarPosition:UIBarPositionAny
                                                            barMetrics:UIBarMetricsDefault];
    [sleepDayInfoNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [sleepDayInfoNavigationController.navigationBar setTintColor:[UIColor clearColor]];
    
    
    [_drawerViewController setCenterViewController:sleepDayInfoNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];

}

- (void)changeToMainViewController
{
    self->_mainViewController = [[MainViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *mainViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_mainViewController];
    
    [mainViewNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                    forBarPosition:UIBarPositionAny
                                                        barMetrics:UIBarMetricsDefault];
    [mainViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];


    [_drawerViewController setCenterViewController:mainViewNavigationController];
    [_centerCoverButton.superview bringSubviewToFront:_centerCoverButton];
}

- (void)attachConnectSensorViewController
{
    [_signViewController dismissViewControllerAnimated:NO completion:^{
        ConnectSensorPreviousViewController *connectSensorPreviousViewController = [[ConnectSensorPreviousViewController alloc] initWithNibName:nil bundle:nil pClass:self];
        self->_connectSensorNavigationController = [[UINavigationController alloc] initWithRootViewController:connectSensorPreviousViewController];
        
        [self->_connectSensorNavigationController.navigationBar setBackgroundImage:[UIImage new]
                                                              forBarMetrics:UIBarMetricsDefault];
        [self->_connectSensorNavigationController.navigationBar setShadowImage:[UIImage new]];
        [self->_connectSensorNavigationController.navigationBar setTranslucent:YES];
        
        [self presentViewController:self->_connectSensorNavigationController animated:NO completion:^{
        }];
    }];
}

- (void)attachMainViewController
{
    UIViewController *currController = _signViewController;
    
    if (_connectSensorNavigationController) {
        currController = _connectSensorNavigationController;
    }
    
    [currController dismissViewControllerAnimated:NO completion:^{

        self->_mainViewController = [[MainViewController alloc] initWithNibName:nil bundle:nil pClass:self];
        UINavigationController *mainViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self->_mainViewController];
        
        [mainViewNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                        forBarPosition:UIBarPositionAny
                                                            barMetrics:UIBarMetricsDefault];
        [mainViewNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
        
        self->_leftMenuViewController = [[LeftMenuViewController alloc] initWithNibName:nil bundle:nil pClass:self];

        if (self->_drawerViewController) {
            [self->_drawerViewController.centerViewController.view.superview removeObserver:self forKeyPath:@"frame"];
            self->_drawerViewController = nil;
        }

        self->_drawerViewController = [[CMMDrawerViewController alloc] initWithCenterViewController:mainViewNavigationController leftDrawerViewController:self->_leftMenuViewController];
        [self->_drawerViewController setMaximumLeftDrawerWidth:CGWidthFromIP6P(855 / 3)];
        [self->_drawerViewController setShowsShadow:NO];
        [self->_drawerViewController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningNavigationBar];
        [self->_drawerViewController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeTapCenterView | MMCloseDrawerGestureModePanningCenterView | MMCloseDrawerGestureModePanningDrawerView | MMCloseDrawerGestureModePanningNavigationBar];
        [self->_drawerViewController.centerViewController.view.superview addObserver:self forKeyPath:@"center" options:NSKeyValueObservingOptionNew context:@"MovedCenterView"];
        [self->_drawerViewController.centerViewController.view.superview addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:@"MovedCenterView"];
        [self->_drawerViewController setCenterHiddenInteractionMode:MMDrawerOpenCenterInteractionModeFull];
        
        [self presentViewController:self->_drawerViewController animated:NO completion:^{
            self->_centerCoverButton = [[UIButton alloc] initWithFrame:self->_drawerViewController.view.bounds];
            [self->_centerCoverButton setBackgroundColor:[UIColor blackColor]];
            [self->_centerCoverButton setUserInteractionEnabled:YES];
            [self->_centerCoverButton setAlpha:0];
            [self->_centerCoverButton addTarget:self action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];

            [self->_drawerViewController.centerViewController.view.superview addSubview:self->_centerCoverButton];

            UIView *navigationBarContentView = mainViewNavigationController.navigationBar.subviews[2];
            
            CAGradientLayer *gradient = [CAGradientLayer layer];
            gradient.frame = navigationBarContentView.bounds;
            gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
            [gradient setStartPoint:CGPointMake(0.0, 0.5)];
            [gradient setEndPoint:CGPointMake(1.0, 0.5)];
            [navigationBarContentView.layer insertSublayer:gradient atIndex:0];

            [self->_centerCoverButton.superview bringSubviewToFront:self->_centerCoverButton];

            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"app_password"]) {
                [[DataSingleton sharedSingletonClass] attachPasswordView];
                [[DataSingleton sharedSingletonClass] unlockUsingBIOContext];
            }
            
        }];
    }];
}

- (void)attachSignViewControllerFromLogout
{
    [self->_drawerViewController dismissViewControllerAnimated:NO
                                                    completion:^{
                                                        self->_signViewController = [[SignViewController alloc] initWithNibName:nil bundle:nil pClass:self];
                                                        [self presentViewController:self->_signViewController animated:NO completion:^{
                                                        }];
                                                    }];
}


- (void)attachSignViewController
{
    self->_signViewController = [[SignViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    [self presentViewController:self->_signViewController animated:NO completion:^{
        [self->_beforeBGImageView setImage:[UIImage imageNamed:@"splash_image_bg_after"]];
        [self->_beforeTitleImageView setImage:[UIImage imageNamed:@"splash_image_title_after"]];
        [self->_beforeCopyrightImageView setImage:[UIImage imageNamed:@"splash_image_copyright_after"]];
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_updateURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] options:@{} completionHandler:^(BOOL success) {
        exit(0);
    }];
}

- (void)checkVersion
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", kSERVER_HOST, kAPI_PATH, kAPI_APP_VERSION_CHECK]];
    [urlString appendFormat:@"/102"];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:kAPI_SUBSCRIPTION_KEY forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }

        switch ([responseData[@"code"] intValue]) {
            case 0: {
                
                if ([responseData[@"data"][@"UpdateYN"] isEqualToString:@"Y"]) {
                    
                    int majorVersion = [[responseData[@"data"][@"VersionInfo"] componentsSeparatedByString:@"."][0] intValue];
                    int minorVersion = [[responseData[@"data"][@"VersionInfo"] componentsSeparatedByString:@"."][1] intValue];
                    int buildVersion = 0;
                    if ([[responseData[@"data"][@"VersionInfo"] componentsSeparatedByString:@"."] count] > 2) {
                        buildVersion = [[responseData[@"data"][@"VersionInfo"] componentsSeparatedByString:@"."][2] intValue];
                    }
                    
                    
                    if (majorVersion > [[[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] componentsSeparatedByString:@"."][0] intValue]) {
                        self->_updateURL = responseData[@"data"][@"AppStoreUrl"];
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"업데이트"
                                                                            message:@"새로운 버전이 출시되었습니다."
                                                                           delegate:self
                                                                  cancelButtonTitle:@"업데이트"
                                                                  otherButtonTitles:nil];
                        [alertView show];
                    } else if (minorVersion > [[[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] componentsSeparatedByString:@"."][1] intValue]) {
                        self->_updateURL = responseData[@"data"][@"AppStoreUrl"];
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"업데이트"
                                                                            message:@"새로운 버전이 출시되었습니다."
                                                                           delegate:self
                                                                  cancelButtonTitle:@"업데이트"
                                                                  otherButtonTitles:nil];
                        [alertView show];
                    } else if (buildVersion && [[[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] componentsSeparatedByString:@"."] count] > 2 && buildVersion > [[[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] componentsSeparatedByString:@"."][2] intValue]) {
                        self->_updateURL = responseData[@"data"][@"AppStoreUrl"];
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"업데이트"
                                                                            message:@"새로운 버전이 출시되었습니다."
                                                                           delegate:self
                                                                  cancelButtonTitle:@"업데이트"
                                                                  otherButtonTitles:nil];
                        [alertView show];
                    } else {
                        [self attachSignViewController];
                    }
                } else {
                    [self attachSignViewController];
                }
                
            }
                break;
                
            default: {
                [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
            }
                break;
        }

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];
}

- (void)checkSSOURL
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", kSERVER_HOST, kAPI_PATH, kAPI_SSO_URL_CHECK]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:kAPI_SUBSCRIPTION_KEY forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] setLoginURL:responseData[@"data"][@"LoginUrl"]];
        [[DataSingleton sharedSingletonClass] setSignUpURL:responseData[@"data"][@"JoinUrl"]];
        [self checkVersion];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];
    
}

- (void)checkServer
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", kSERVER_HOST, kAPI_PATH, kAPI_CONNECTION_CHECK]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:kAPI_SUBSCRIPTION_KEY forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    [manager.requestSerializer setTimeoutInterval:5];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }

        [[DataSingleton sharedSingletonClass] setHostString:kSERVER_HOST];
        [[DataSingleton sharedSingletonClass] setKeyString:kAPI_SUBSCRIPTION_KEY];
        [self checkSSOURL];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", kSERVER_HOST_SUB, kAPI_PATH, kAPI_CONNECTION_CHECK]];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
        [manager.requestSerializer setValue:kAPI_SUBSCRIPTION_KEY_SUB forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
        
        [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *responseData;
            NSError *error = nil;
            if (responseObject != nil) {
                responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                               options:NSJSONReadingMutableContainers
                                                                 error:&error];
            }
            
            // 서브서버
            [[DataSingleton sharedSingletonClass] setHostString:kSERVER_HOST_SUB];
            [[DataSingleton sharedSingletonClass] setKeyString:kAPI_SUBSCRIPTION_KEY_SUB];
            [self checkSSOURL];

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];

        }];

    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];

        float bgRatio = self.view.frame.size.height / [UIImage imageNamed:@"splash_image_bg_before"].size.height;
        
        if (bgRatio < 1) {
            bgRatio = self.view.frame.size.width / [UIImage imageNamed:@"splash_image_bg_before"].size.width;
        }

        _beforeBGImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"splash_image_bg_before"].size.width * bgRatio, [UIImage imageNamed:@"splash_image_bg_before"].size.height * bgRatio)];
        [_beforeBGImageView setImage:[UIImage imageNamed:@"splash_image_bg_before"]];
        [_beforeBGImageView setCenter:self.view.center];
        [self.view addSubview:_beforeBGImageView];

        _beforeTitleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash_image_title_before"]];
        [_beforeTitleImageView setCenter:self.view.center];
        [self.view addSubview:_beforeTitleImageView];

        _beforeCopyrightImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash_image_copyright_before"]];
        [_beforeCopyrightImageView setCenter:CGPointMake(self.view.center.x, self.view.frame.size.height - 50)];
        [self.view addSubview:_beforeCopyrightImageView];

        [self performSelector:@selector(checkServer) withObject:nil afterDelay:0.01f];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeToNoticeViewController) name:@"NOTICE_LIST" object:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
