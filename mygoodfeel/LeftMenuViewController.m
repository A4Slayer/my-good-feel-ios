//
//  LeftMenuViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 24..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "RootViewController.h"

@interface LeftMenuViewController ()

@end

@implementation LeftMenuViewController

- (void)loadAveragePeriod
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_GET_USER_BASIC]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        switch ([responseData[@"code"] intValue]) {
            case 0: {
                if ([responseData[@"data"][@"AvgMenstrualPeriod"] intValue] >= 3) {
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"당신의 평균주기는\n"]
                                                                                                   attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(16)),
                                                                                                                 NSForegroundColorAttributeName : UIColorFromRGB(0x5c5c5c) }];
                    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d일", [responseData[@"data"][@"AvgMenstrualPeriod"] intValue]]
                                                                                       attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(16)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0xfd81a2) }]];
                    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 입니다."]
                                                                                       attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(16)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x5c5c5c) }]];
                    [self->_menstruationLabel setAttributedText:attrString];
                    [self->_menstruationLabel sizeToFit];
                } else {
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"당신의 평균주기는\n"]
                                                                                                   attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(16)),
                                                                                                                 NSForegroundColorAttributeName : UIColorFromRGB(0x5c5c5c) }];
                    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"5일"]
                                                                                       attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(16)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0xfd81a2) }]];
                    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 입니다."]
                                                                                       attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(16)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x5c5c5c) }]];
                    [self->_menstruationLabel setAttributedText:attrString];
                    [self->_menstruationLabel sizeToFit];
                }
            }
                break;
                
            default:
                break;
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
}

- (void)login:(UIButton *)button
{
    EventSignViewController *eventSignViewController = [[EventSignViewController alloc] initWithNibName:nil bundle:nil pClass:self type:(int)button.tag];
    
    UINavigationController *eventSignNavigationController = [[UINavigationController alloc] initWithRootViewController:eventSignViewController];
    [eventSignNavigationController.navigationBar setTranslucent:NO];
    [self presentViewController:eventSignNavigationController animated:YES completion:^{
        //
    }];
}

- (void)changeMenu:(UIButton *)button
{
    [_cycleInfoButton setImage:[UIImage imageNamed:@"leftmenu_button_cycle"] forState:UIControlStateNormal];
    [_cycleUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];

    [_sleepInfoButton setImage:[UIImage imageNamed:@"leftmenu_button_sleep"] forState:UIControlStateNormal];
    [_sleepUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];

    [_dailyReportButton setImage:[UIImage imageNamed:@"leftmenu_button_daily"] forState:UIControlStateNormal];
    [_dailyUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];
    
    [_menstruationingButton setImage:[UIImage imageNamed:@"leftmenu_button_menstruationing"] forState:UIControlStateNormal];
    [_menstruationingUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];

    [_momQButton setImage:[UIImage imageNamed:@"leftmenu_button_momq"] forState:UIControlStateNormal];
    [_momQUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];

    [_eventButton setImage:[UIImage imageNamed:@"leftmenu_button_event"] forState:UIControlStateNormal];
    [_eventUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];

    [_noticeButton setImage:[UIImage imageNamed:@"leftmenu_button_notice"] forState:UIControlStateNormal];
    [_noticeUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];

    [_preferenceButton setImage:[UIImage imageNamed:@"leftmenu_button_preference"] forState:UIControlStateNormal];
    [_preferenceUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];

    if (button == _cycleInfoButton) {
        [_cycleInfoButton setImage:[UIImage imageNamed:@"leftmenu_button_cycle_h"] forState:UIControlStateNormal];
        [_cycleUnderlineView setBackgroundColor:UIColorFromRGB(0xffa4bc)];
        [(RootViewController *)_pClass changeToMainViewController];
    } else if (button == _sleepInfoButton) {
        [_sleepInfoButton setImage:[UIImage imageNamed:@"leftmenu_button_sleep_h"] forState:UIControlStateNormal];
        [_sleepUnderlineView setBackgroundColor:UIColorFromRGB(0xffa4bc)];
        [(RootViewController *)_pClass changeToSleepDayInfoViewController];
    } else if (button == _dailyReportButton) {
        [_dailyReportButton setImage:[UIImage imageNamed:@"leftmenu_button_daily_h"] forState:UIControlStateNormal];
        [_dailyUnderlineView setBackgroundColor:UIColorFromRGB(0xffa4bc)];
        [(RootViewController *)_pClass changeToDailyReportViewController];
    } else if (button == _menstruationingButton) {
        [_menstruationingButton setImage:[UIImage imageNamed:@"leftmenu_button_menstruationing_h"] forState:UIControlStateNormal];
        [_menstruationingUnderlineView setBackgroundColor:UIColorFromRGB(0xffa4bc)];
        [(RootViewController *)_pClass changeToMenstruationingViewController];
    } else if (button == _momQButton) {
        [_momQButton setImage:[UIImage imageNamed:@"leftmenu_button_momq_h"] forState:UIControlStateNormal];
        [_momQUnderlineView setBackgroundColor:UIColorFromRGB(0xffa4bc)];
        [(RootViewController *)_pClass changeToMomQViewController];
    } else if (button == _eventButton) {
        [_eventButton setImage:[UIImage imageNamed:@"leftmenu_button_event_h"] forState:UIControlStateNormal];
        [_eventUnderlineView setBackgroundColor:UIColorFromRGB(0xffa4bc)];
        [(RootViewController *)_pClass changeToEventViewController];
    } else if (button == _noticeButton) {
        [_noticeButton setImage:[UIImage imageNamed:@"leftmenu_button_notice_h"] forState:UIControlStateNormal];
        [_noticeUnderlineView setBackgroundColor:UIColorFromRGB(0xffa4bc)];
        [(RootViewController *)_pClass changeToNoticeViewController];
    } else if (button == _preferenceButton) {
        [_preferenceButton setImage:[UIImage imageNamed:@"leftmenu_button_preference_h"] forState:UIControlStateNormal];
        [_preferenceUnderlineView setBackgroundColor:UIColorFromRGB(0xa3a3a3)];
        [(RootViewController *)_pClass changeToPreferenceViewController];
    } else if (button == _notificationButton) {
        [(RootViewController *)_pClass changeToNotificationViewController];
    }
    
    [(RootViewController *)_pClass toggleLeftMenu];
}

- (void)updateTopView
{
    for (UIView *sView in [_topView subviews]) {
        [sView removeFromSuperview];
    }
    
    if ([[[DataSingleton sharedSingletonClass] userInfo][@"UserType"] intValue] == 102) {
        
        // ffa4bc
        NSMutableAttributedString *attrString;

        UIButton *loginButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(105 / 3), CGHeightFromIP6P(36) + kIPHONE_X_TOP_MARGIN, CGWidthFromIP6P(210 / 3), CGHeightFromIP6P(62 / 3))];
        [loginButton setImage:[UIImage imageNamed:@"leftmenu_button_login_new"] forState:UIControlStateNormal];
        [loginButton setTag:0];
        [loginButton addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:loginButton];
        
        
        UIButton *signUpButton = [[UIButton alloc] initWithFrame:CGRectMake(loginButton.frame.origin.x, loginButton.frame.origin.y + loginButton.frame.size.height + CGHeightFromIP6P(14), CGWidthFromIP6P(400 / 3), CGHeightFromIP6P(42 / 3))];
        [signUpButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [signUpButton setTag:1];
        [signUpButton addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:signUpButton];
        
        attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"아직 회원이 아니세요?"]
                                                                                       attributes:@{
                                                                                                    NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(13)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0xafafaf),
                                                                                                    NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)
                                                                                                     }
                                                 ];
        [signUpButton setAttributedTitle:attrString forState:UIControlStateNormal];

        
    } else {
        UIImageView *menstruationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(106 / 3), CGHeightFromIP6P(123 / 3) + kIPHONE_X_TOP_MARGIN, CGWidthFromIP6P(74 / 3), CGHeightFromIP6P(97 / 3))];
        [menstruationImageView setImage:[UIImage imageNamed:@"leftmenu_image_menstruation"]];
        [_topView addSubview:menstruationImageView];
        
        _menstruationLabel = [[UILabel alloc] initWithFrame:CGRectMake(menstruationImageView.frame.origin.x + menstruationImageView.frame.size.width + CGWidthFromIP6P(12), menstruationImageView.frame.origin.y - 2, CGWidthFromIP6P(500 / 3), menstruationImageView.frame.size.height)];
        [_menstruationLabel setNumberOfLines:0];
        [_topView addSubview:_menstruationLabel];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"평균 주기를 불러오고 있습니다."]
                                                                                       attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(16)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x5c5c5c) }];
        [_menstruationLabel setAttributedText:attrString];
        [_menstruationLabel sizeToFit];

        [self loadAveragePeriod];
    }
    
    if (_notificationButton) {
        [_notificationButton removeFromSuperview];
        _notificationButton = nil;
    }
    
    _notificationButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P([UIImage imageNamed:@"leftmenu_button_notification"].size.width), CGHeightFromIP6P([UIImage imageNamed:@"leftmenu_button_notification"].size.height))];
    [_notificationButton setFrame:CGRectMake(CGWidthFromIP6P(224), CGHeightFromIP6P(66), _notificationButton.frame.size.width, _notificationButton.frame.size.height)];
    [_notificationButton setImage:[UIImage imageNamed:@"leftmenu_button_notification_badge"] forState:UIControlStateNormal];
    [_notificationButton addTarget:self action:@selector(changeMenu:) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_notificationButton];
    
    UILabel *notificationBadgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(19), CGHeightFromIP6P(5), CGWidthFromIP6P(16), CGHeightFromIP6P(16))];
    [notificationBadgeLabel setBackgroundColor:[UIColor clearColor]];
    [notificationBadgeLabel.layer setCornerRadius:notificationBadgeLabel.frame.size.width / 2];
    [notificationBadgeLabel setClipsToBounds:YES];
    [notificationBadgeLabel setFont:RegularWithSize(CGHeightFromIP6P(13))];
    [notificationBadgeLabel setTextColor:[UIColor whiteColor]];
    [notificationBadgeLabel setTextAlignment:NSTextAlignmentCenter];
    [notificationBadgeLabel setText:[[DataSingleton sharedSingletonClass] badgeString]];
    [_notificationButton addSubview:notificationBadgeLabel];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(855 / 3), CGHeightFromIP6P(343 / 3) + kIPHONE_X_TOP_MARGIN)];
        [self.view addSubview:_topView];
        
        [self updateTopView];
        
        //343 13
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(343 / 3) + kIPHONE_X_TOP_MARGIN, CGWidthFromIP6P(855 / 3), CGHeightFromIP6P(13 / 3))];
        [separatorView setBackgroundColor:UIColorFromRGB(0xf9f9f9)];
        [self.view addSubview:separatorView];
        
        
        _cycleInfoButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(119 / 3), separatorView.frame.origin.y + separatorView.frame.size.height, CGWidthFromIP6P(736 / 3), CGHeightFromIP6P(206 / 3))];
        [_cycleInfoButton setImage:[UIImage imageNamed:@"leftmenu_button_cycle_h"] forState:UIControlStateNormal];
        [_cycleInfoButton setImage:[UIImage imageNamed:@"leftmenu_button_cycle_h"] forState:UIControlStateHighlighted];
        [_cycleInfoButton setTitle:@"    주기 정보" forState:UIControlStateNormal];
        [_cycleInfoButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(23))];
        [_cycleInfoButton setTitleColor:UIColorFromRGB(0xffaec1) forState:UIControlStateNormal];
        [_cycleInfoButton setTitleColor:UIColorFromRGB(0xff6b92) forState:UIControlStateHighlighted];
        [_cycleInfoButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_cycleInfoButton addTarget:self action:@selector(changeMenu:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_cycleInfoButton];
        
        _cycleUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, _cycleInfoButton.frame.size.height - CGHeightFromIP6P(1), _cycleInfoButton.frame.size.width, CGHeightFromIP6P(1))];
        [_cycleUnderlineView setBackgroundColor:UIColorFromRGB(0xffa4bc)];
        [_cycleInfoButton addSubview:_cycleUnderlineView];

        
        _sleepInfoButton = [[UIButton alloc] initWithFrame:CGRectMake(_cycleInfoButton.frame.origin.x, _cycleInfoButton.frame.origin.y + _cycleInfoButton.frame.size.height, CGWidthFromIP6P(736 / 3), CGHeightFromIP6P(206 / 3))];
        [_sleepInfoButton setImage:[UIImage imageNamed:@"leftmenu_button_sleep"] forState:UIControlStateNormal];
        [_sleepInfoButton setImage:[UIImage imageNamed:@"leftmenu_button_sleep_h"] forState:UIControlStateHighlighted];
        [_sleepInfoButton setTitle:@"    수면 정보" forState:UIControlStateNormal];
        [_sleepInfoButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(23))];
        [_sleepInfoButton setTitleColor:UIColorFromRGB(0xffaec1) forState:UIControlStateNormal];
        [_sleepInfoButton setTitleColor:UIColorFromRGB(0xff6b92) forState:UIControlStateHighlighted];
        [_sleepInfoButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_sleepInfoButton addTarget:self action:@selector(changeMenu:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_sleepInfoButton];
        
        _sleepUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, _sleepInfoButton.frame.size.height - CGHeightFromIP6P(1), _sleepInfoButton.frame.size.width, CGHeightFromIP6P(1))];
        [_sleepUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];
        [_sleepInfoButton addSubview:_sleepUnderlineView];


        _dailyReportButton = [[UIButton alloc] initWithFrame:CGRectMake(_sleepInfoButton.frame.origin.x, _sleepInfoButton.frame.origin.y + _sleepInfoButton.frame.size.height, CGWidthFromIP6P(736 / 3), CGHeightFromIP6P(206 / 3))];
        [_dailyReportButton setImage:[UIImage imageNamed:@"leftmenu_button_daily"] forState:UIControlStateNormal];
        [_dailyReportButton setImage:[UIImage imageNamed:@"leftmenu_button_daily_h"] forState:UIControlStateHighlighted];
        [_dailyReportButton setTitle:@"    일간 리포트" forState:UIControlStateNormal];
        [_dailyReportButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(23))];
        [_dailyReportButton setTitleColor:UIColorFromRGB(0xffaec1) forState:UIControlStateNormal];
        [_dailyReportButton setTitleColor:UIColorFromRGB(0xff6b92) forState:UIControlStateHighlighted];
        [_dailyReportButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_dailyReportButton addTarget:self action:@selector(changeMenu:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_dailyReportButton];
        
        _dailyUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, _dailyReportButton.frame.size.height - CGHeightFromIP6P(1), _dailyReportButton.frame.size.width, CGHeightFromIP6P(1))];
        [_dailyUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];
        [_dailyReportButton addSubview:_dailyUnderlineView];

        
        _menstruationingButton = [[UIButton alloc] initWithFrame:CGRectMake(_cycleInfoButton.frame.origin.x, _dailyReportButton.frame.origin.y + _dailyReportButton.frame.size.height, CGWidthFromIP6P(736 / 3), CGHeightFromIP6P(206 / 3))];
        [_menstruationingButton setImage:[UIImage imageNamed:@"leftmenu_button_menstruationing"] forState:UIControlStateNormal];
        [_menstruationingButton setImage:[UIImage imageNamed:@"leftmenu_button_menstruationing_h"] forState:UIControlStateHighlighted];
        [_menstruationingButton setTitle:@"    생리 건강정보" forState:UIControlStateNormal];
        [_menstruationingButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(23))];
        [_menstruationingButton setTitleColor:UIColorFromRGB(0xffaec1) forState:UIControlStateNormal];
        [_menstruationingButton setTitleColor:UIColorFromRGB(0xff6b92) forState:UIControlStateHighlighted];
        [_menstruationingButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_menstruationingButton addTarget:self action:@selector(changeMenu:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_menstruationingButton];
        
        _menstruationingUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, _menstruationingButton.frame.size.height - CGHeightFromIP6P(1), _menstruationingButton.frame.size.width, CGHeightFromIP6P(1))];
        [_menstruationingUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];
        [_menstruationingButton addSubview:_menstruationingUnderlineView];
        
        
        _eventButton = [[UIButton alloc] initWithFrame:CGRectMake(_cycleInfoButton.frame.origin.x, _menstruationingButton.frame.origin.y + _menstruationingButton.frame.size.height, CGWidthFromIP6P(736 / 3), CGHeightFromIP6P(206 / 3))];
        [_eventButton setImage:[UIImage imageNamed:@"leftmenu_button_event"] forState:UIControlStateNormal];
        [_eventButton setImage:[UIImage imageNamed:@"leftmenu_button_event_h"] forState:UIControlStateHighlighted];
        [_eventButton setTitle:@"    이벤트" forState:UIControlStateNormal];
        [_eventButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(23))];
        [_eventButton setTitleColor:UIColorFromRGB(0xffaec1) forState:UIControlStateNormal];
        [_eventButton setTitleColor:UIColorFromRGB(0xff6b92) forState:UIControlStateHighlighted];
        [_eventButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_eventButton addTarget:self action:@selector(changeMenu:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_eventButton];
        
        _eventUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, _eventButton.frame.size.height - CGHeightFromIP6P(1), _eventButton.frame.size.width, CGHeightFromIP6P(1))];
        [_eventUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];
        [_eventButton addSubview:_eventUnderlineView];

        
        _momQButton = [[UIButton alloc] initWithFrame:CGRectMake(_cycleInfoButton.frame.origin.x, _eventButton.frame.origin.y + _eventButton.frame.size.height, CGWidthFromIP6P(736 / 3), CGHeightFromIP6P(206 / 3))];
        [_momQButton setImage:[UIImage imageNamed:@"leftmenu_button_momq"] forState:UIControlStateNormal];
        [_momQButton setImage:[UIImage imageNamed:@"leftmenu_button_momq_h"] forState:UIControlStateHighlighted];
        [_momQButton setTitle:@"    스토어" forState:UIControlStateNormal];
        [_momQButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(23))];
        [_momQButton setTitleColor:UIColorFromRGB(0xffaec1) forState:UIControlStateNormal];
        [_momQButton setTitleColor:UIColorFromRGB(0xff6b92) forState:UIControlStateHighlighted];
        [_momQButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_momQButton addTarget:self action:@selector(changeMenu:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_momQButton];
        
        _momQUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, _momQButton.frame.size.height - CGHeightFromIP6P(1), _momQButton.frame.size.width, CGHeightFromIP6P(1))];
        [_momQUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];
        [_momQButton addSubview:_momQUnderlineView];

        
        _noticeButton = [[UIButton alloc] initWithFrame:CGRectMake(_cycleInfoButton.frame.origin.x, _momQButton.frame.origin.y + _momQButton.frame.size.height, CGWidthFromIP6P(736 / 3), CGHeightFromIP6P(206 / 3))];
        [_noticeButton setImage:[UIImage imageNamed:@"leftmenu_button_notice"] forState:UIControlStateNormal];
        [_noticeButton setImage:[UIImage imageNamed:@"leftmenu_button_notice_h"] forState:UIControlStateHighlighted];
        [_noticeButton setTitle:@"    공지사항" forState:UIControlStateNormal];
        [_noticeButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(23))];
        [_noticeButton setTitleColor:UIColorFromRGB(0xffaec1) forState:UIControlStateNormal];
        [_noticeButton setTitleColor:UIColorFromRGB(0xff6b92) forState:UIControlStateHighlighted];
        [_noticeButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_noticeButton addTarget:self action:@selector(changeMenu:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_noticeButton];
        
        _noticeUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, _noticeButton.frame.size.height - CGHeightFromIP6P(1), _noticeButton.frame.size.width, CGHeightFromIP6P(1))];
        [_noticeUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];
        [_noticeButton addSubview:_noticeUnderlineView];

        
        _preferenceButton = [[UIButton alloc] initWithFrame:CGRectMake(_cycleInfoButton.frame.origin.x, _noticeButton.frame.origin.y + _noticeButton.frame.size.height + CGHeightFromIP6P(160 / 3), CGWidthFromIP6P(736 / 3), CGHeightFromIP6P(206 / 3))];
        [_preferenceButton setImage:[UIImage imageNamed:@"leftmenu_button_preference"] forState:UIControlStateNormal];
        [_preferenceButton setImage:[UIImage imageNamed:@"leftmenu_button_preference_h"] forState:UIControlStateHighlighted];
        [_preferenceButton setTitle:@"    설정" forState:UIControlStateNormal];
        [_preferenceButton.titleLabel setFont:RegularWithSize(CGHeightFromIP6P(23))];
        [_preferenceButton setTitleColor:UIColorFromRGB(0xa3a3a3) forState:UIControlStateNormal];
        [_preferenceButton setTitleColor:UIColorFromRGB(0x969696) forState:UIControlStateHighlighted];
        [_preferenceButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_preferenceButton addTarget:self action:@selector(changeMenu:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_preferenceButton];
        
        _preferenceUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, _preferenceButton.frame.size.height - CGHeightFromIP6P(1), _preferenceButton.frame.size.width, CGHeightFromIP6P(1))];
        [_preferenceUnderlineView setBackgroundColor:UIColorFromRGB(0xebebeb)];
        [_preferenceButton addSubview:_preferenceUnderlineView];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTopView) name:@"UPDATE_LEFT_MENU" object:nil];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
