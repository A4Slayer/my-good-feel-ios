//
//  SleepDiaryViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 12. 7..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "DataSingleton.h"

@interface SleepDiaryViewController : UIViewController
{
    BOOL isCaffeine;
    BOOL isAlcohol;
    BOOL isExercise;
    BOOL isOvereat;
    BOOL isStressed;

    BOOL isOvernight;
}

@property (strong, nonatomic) id pClass;

@property (strong, nonatomic) UIButton *caffeineCheckButton;
@property (strong, nonatomic) UIButton *alcoholCheckButton;
@property (strong, nonatomic) UIButton *exerciseCheckButton;
@property (strong, nonatomic) UIButton *overeatCheckButton;
@property (strong, nonatomic) UIButton *stressedCheckButton;
@property (strong, nonatomic) UIButton *overnightCheckButton;

@property (strong, nonatomic) NSString *dateString;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass dateString:(NSString *)dateString;

@end
