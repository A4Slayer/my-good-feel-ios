//
//  MainViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 24..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CycleCalendarGuideViewController.h"
#import "AFNetworking.h"
#import "AKSDWebImageManager.h"
#import "ChangeStepAgeViewController.h"
#import "WebViewNavigationController.h"

#import "EventDetailViewController.h"
#import "NoticeDetailViewController.h"

@interface MainViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    BOOL isShowAgreementAlert;
    
    BOOL isSelectedMenstruation;
    BOOL isSelectedWeight;
    BOOL isSelectedMarketing;
    
    BOOL isOpenAgreement;
}

@property (strong, nonatomic) id pClass;

@property (strong, nonatomic) UIScrollView *mainScrollView;

@property (strong, nonatomic) UIView *calendarView;

@property (strong, nonatomic) UILabel *yearLabel;

@property (strong, nonatomic) UILabel *beforeCycleValueLabel;
@property (strong, nonatomic) UILabel *currentCycleValueLabel;
@property (strong, nonatomic) UILabel *afterCycleValueLabel;

@property (strong, nonatomic) UILabel *beforePeriodValueLabel;
@property (strong, nonatomic) UILabel *currentPeriodValueLabel;
@property (strong, nonatomic) UILabel *afterPeriodValueLabel;

@property (strong, nonatomic) NSDictionary *dataDictionary;
@property (strong, nonatomic) UIScrollView *bannerScrollView;

@property (strong, nonatomic) UIView *blackMaskView;
@property (strong, nonatomic) UIView *dateSelectView;
@property (strong, nonatomic) UIView *pickerResignView;
@property (strong, nonatomic) UIPickerView *datePickerView;

@property (strong, nonatomic) UIScrollView *monthScrollView;
@property (strong, nonatomic) NSMutableArray *monthItemArray;

@property (strong, nonatomic) UITableView *agreementTableView;

@property (strong, nonatomic) UIImageView *agreementAllCheckBoxImageView;
@property (strong, nonatomic) UIImageView *agreementMenstruationCheckBoxImageView;
@property (strong, nonatomic) UIImageView *agreementWeightCheckBoxImageView;
@property (strong, nonatomic) UIImageView *agreementMarketingCheckBoxImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass date:(NSDate *)date;

@end
