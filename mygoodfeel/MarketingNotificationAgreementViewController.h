//
//  MarketingNotificationAgreementViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 23..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketingNotificationAgreementViewController : UIViewController

@property (strong, nonatomic) id pClass;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
