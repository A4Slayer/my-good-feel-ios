//
//  PreferenceSensorViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 4. 16..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectSensorPreviousViewController.h"
#import "PreferenceSensorNotificaionViewController.h"

@interface PreferenceSensorViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *mainTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@end
