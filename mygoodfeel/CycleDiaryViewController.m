//
//  CycleDiaryViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "CycleDiaryViewController.h"
#import "CycleDayInfoViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface CycleDiaryViewController ()

@end

@implementation CycleDiaryViewController

- (void)setDailyStatus
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CYCLEDAILY_SET_STATUS]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];

    NSString *weightString = @"0";
    
    if (![_weightTextField.text isEqualToString:@""]) {
        weightString = [_weightTextField.text stringByReplacingOccurrencesOfString:@"kg" withString:@""];
    }

    [urlString appendFormat:@"/%@", _menstruationCdString];     // menstrual cd
    [urlString appendFormat:@"/%@", _pyrexiaString];            // fever yn
    [urlString appendFormat:@"/%@", _sexString];                // sex yn
    [urlString appendFormat:@"/%@", weightString];              // weight
    [urlString appendFormat:@"/%@", _dateString];               // date
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [self dismissSelfView];
        [[[UIAlertView alloc] initWithTitle:nil message:@"저장되었습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
    
}

- (void)getDailyStatus
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CYCLEDAILY_GET_STATUS]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@", _dateString];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        self->_dataDictionary = responseData[@"data"];

        if ([self->_dataDictionary[@"FeverYN"] isEqualToString:@"Y"]) {
            self->_pyrexiaString = @"Y";
            [self->_pyrexiaButton setImage:[UIImage imageNamed:@"cyclediary_button_pyrexia_h"] forState:UIControlStateNormal];
        } else {
            self->_pyrexiaString = @"N";
            [self->_pyrexiaButton setImage:[UIImage imageNamed:@"cyclediary_button_pyrexia"] forState:UIControlStateNormal];
        }
        
        if ([self->_dataDictionary[@"SexYN"] isEqualToString:@"Y"]) {
            self->_sexString = @"Y";
            [self->_sexButton setImage:[UIImage imageNamed:@"cyclediary_button_sex_h"] forState:UIControlStateNormal];
        } else {
            self->_sexString = @"N";
            [self->_sexButton setImage:[UIImage imageNamed:@"cyclediary_button_sex"] forState:UIControlStateNormal];
        }

        [self->_menstruationNoneButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_none"] forState:UIControlStateNormal];
        [self->_menstruationLowButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_low"] forState:UIControlStateNormal];
        [self->_menstruationNormalButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_medium"] forState:UIControlStateNormal];
        [self->_menstruationMuchButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_many"] forState:UIControlStateNormal];

        if ([self->_dataDictionary[@"MenstrualCd"] intValue] == 101) {
            self->_menstruationCdString = @"101";
            [self->_menstruationNoneButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_none_h"] forState:UIControlStateNormal];

            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"오늘은 생리를 하지 않았어요."]
                                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }];
            [self->_menstruationLabel setAttributedText:attrString];

        } else if ([self->_dataDictionary[@"MenstrualCd"] intValue] == 104) {
            self->_menstruationCdString = @"104";
            [self->_menstruationLowButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_low_h"] forState:UIControlStateNormal];

            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"오늘의 생리 양은 "]
                                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\"적음\""]
                                                                               attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0xf16774) }]];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 입니다."]
                                                                               attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }]];
            [self->_menstruationLabel setAttributedText:attrString];

        } else if ([self->_dataDictionary[@"MenstrualCd"] intValue] == 102) {
            self->_menstruationCdString = @"102";
            [self->_menstruationNormalButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_medium_h"] forState:UIControlStateNormal];

            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"오늘의 생리 양은 "]
                                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\"보통\""]
                                                                               attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0xf16774) }]];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 입니다."]
                                                                               attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }]];
            [self->_menstruationLabel setAttributedText:attrString];

        } else if ([self->_dataDictionary[@"MenstrualCd"] intValue] == 103) {
            self->_menstruationCdString = @"103";
            [self->_menstruationMuchButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_much_h"] forState:UIControlStateNormal];

            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"오늘의 생리 양은 "]
                                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\"많음\""]
                                                                               attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0xf16774) }]];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 입니다."]
                                                                               attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }]];
            [self->_menstruationLabel setAttributedText:attrString];

        }
        
        if ([self->_dataDictionary[@"Weight"] intValue] == 0) {
            [self->_weightTextField setText:@""];
        } else {
            [self->_weightTextField setText:[NSString stringWithFormat:@"%.1fkg", [self->_dataDictionary[@"Weight"] floatValue]]];
        }

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (void)toggleMenstruation:(UIButton *)button
{

    [self->_menstruationNoneButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_none"] forState:UIControlStateNormal];
    [self->_menstruationLowButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_low"] forState:UIControlStateNormal];
    [self->_menstruationNormalButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_medium"] forState:UIControlStateNormal];
    [self->_menstruationMuchButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_many"] forState:UIControlStateNormal];
    
    if (button == _menstruationNoneButton) {
        self->_menstruationCdString = @"101";
        [self->_menstruationNoneButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_none_h"] forState:UIControlStateNormal];

        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"오늘은 생리를 하지 않았어요."]
                                                                                       attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }];
        [_menstruationLabel setAttributedText:attrString];

    } else if (button == _menstruationLowButton) {
        self->_menstruationCdString = @"104";
        [self->_menstruationLowButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_low_h"] forState:UIControlStateNormal];

        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"오늘의 생리 양은 "]
                                                                                       attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\"적음\""]
                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0xf16774) }]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 입니다."]
                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }]];
        [_menstruationLabel setAttributedText:attrString];
    } else if (button == _menstruationNormalButton) {
        self->_menstruationCdString = @"102";
        [self->_menstruationNormalButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_medium_h"] forState:UIControlStateNormal];

        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"오늘의 생리 양은 "]
                                                                                       attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\"보통\""]
                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0xf16774) }]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 입니다."]
                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }]];
        [_menstruationLabel setAttributedText:attrString];
    } else if (button == _menstruationMuchButton) {
        self->_menstruationCdString = @"103";
        [self->_menstruationMuchButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_many_h"] forState:UIControlStateNormal];

        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"오늘의 생리 양은 "]
                                                                                       attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                                     NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\"많음\""]
                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0xf16774) }]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 입니다."]
                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(CGHeightFromIP6P(17)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x4e4e4e) }]];
        [_menstruationLabel setAttributedText:attrString];
    }
    
}

- (void)togglePyrexia:(UIButton *)button
{
    if ([_pyrexiaString isEqualToString:@"Y"]) {
        _pyrexiaString = @"N";
        [_pyrexiaButton setImage:[UIImage imageNamed:@"cyclediary_button_pyrexia"] forState:UIControlStateNormal];
    } else {
        _pyrexiaString = @"Y";
        [_pyrexiaButton setImage:[UIImage imageNamed:@"cyclediary_button_pyrexia_h"] forState:UIControlStateNormal];
    }
}

- (void)toggleSex:(UIButton *)button
{
    if ([_sexString isEqualToString:@"Y"]) {
        _sexString = @"N";
        [_sexButton setImage:[UIImage imageNamed:@"cyclediary_button_sex"] forState:UIControlStateNormal];
    } else {
        _sexString = @"Y";
        [_sexButton setImage:[UIImage imageNamed:@"cyclediary_button_sex_h"] forState:UIControlStateNormal];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_weightTextField setText:[NSString stringWithFormat:@"%ld.%ldkg", (long)[_weightPickerView selectedRowInComponent:0] + 30, (long)[_weightPickerView selectedRowInComponent:1]]];
}

- (void)dismissSelfView
{
    [(CycleDayInfoViewController *)_pClass getCycleDailyInfo];
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (void)emptyWeight
{
    [_weightTextField setText:@""];
    [self resignKeyboard];
}

- (void)resignKeyboard
{
    [_weightTextField resignFirstResponder];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass dateString:(NSString *)dateString
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _dateString = dateString;
        _pClass = pClass;
        _pyrexiaString = @"N";
        _sexString = @"N";
        _menstruationCdString = @"101";
        
        [self.view setBackgroundColor:UIColorFromRGB(0xefeff4)];
        
        [self setTitle:@"주기 다이어리"];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_close_pink"].size.width, [UIImage imageNamed:@"common_button_close_pink"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"common_button_close_pink"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];

        UIButton *saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
        [saveButton setTitleColor:UIColorFromRGB(0xf16774) forState:UIControlStateNormal];
        [saveButton setTitleColor:UIColorFromRGB(0x883444) forState:UIControlStateHighlighted];
        [saveButton.titleLabel setFont:MediumWithSize(17)];
        [saveButton setTitle:@"저장" forState:UIControlStateNormal];
        [saveButton sizeToFit];
        [saveButton addTarget:self action:@selector(setDailyStatus) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:saveButton]];
        
        UILabel *checkGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, CGHeightFromIP6P(114 / 3))];
        [checkGuideLabel setBackgroundColor:[UIColor clearColor]];
        [checkGuideLabel setTextColor:UIColorFromRGB(0x87878f)];
        [checkGuideLabel setFont:MediumWithSize(CGHeightFromIP6P(13))];
        [checkGuideLabel setText:@"      해당하는 것에 체크해주세요. 주기 관리에 도움이 됩니다."];
        [self.view addSubview:checkGuideLabel];

        
        UIView *itemListView = [[UIView alloc] initWithFrame:CGRectMake(0, checkGuideLabel.frame.size.height, kSCREEN_HEIGHT, 1332 / 3 + k3PX)];
        [itemListView setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:itemListView];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(50 / 3, 726 / 3, kSCREEN_WIDTH - 50 / 3, k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xe4e4e4)];
        [itemListView addSubview:separatorView];
        
        separatorView = [[UIView alloc] initWithFrame:CGRectMake(50 / 3, 926 / 3 + k1PX, kSCREEN_WIDTH - 50 / 3, k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xe4e4e4)];
        [itemListView addSubview:separatorView];
        
        separatorView = [[UIView alloc] initWithFrame:CGRectMake(50 / 3, 1126 / 3 + k2PX, kSCREEN_WIDTH - 50 / 3, k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xe4e4e4)];
        [itemListView addSubview:separatorView];
        

        UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 726 / 3)];
        [cellView setBackgroundColor:[UIColor clearColor]];
        [itemListView addSubview:cellView];
        
        UIImageView *cellIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cyclediary_image_icon_menstruation"]];
        [cellIconImageView setCenter:CGPointMake(57 / 3 + cellIconImageView.frame.size.width / 2, 200 / 3 / 2)];
        [cellView addSubview:cellIconImageView];
        
        UILabel *cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(160 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"생리를 했어요"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        
        _menstruationNoneButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(165 / 3), CGWidthFromIP6P(227 / 3), CGWidthFromIP6P([UIImage imageNamed:@"cyclediary_button_menstruation_none"].size.width / 3), CGHeightFromIP6P([UIImage imageNamed:@"cyclediary_button_menstruation_none"].size.height / 3))];
        [_menstruationNoneButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_none"] forState:UIControlStateNormal];
        [_menstruationNoneButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_none_h"] forState:UIControlStateHighlighted];
        [_menstruationNoneButton addTarget:self action:@selector(toggleMenstruation:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_menstruationNoneButton];
        
        _menstruationLowButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(416 / 3), CGWidthFromIP6P(227 / 3), CGWidthFromIP6P([UIImage imageNamed:@"cyclediary_button_menstruation_low"].size.width / 3), CGHeightFromIP6P([UIImage imageNamed:@"cyclediary_button_menstruation_low"].size.height / 3))];
        [_menstruationLowButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_low"] forState:UIControlStateNormal];
        [_menstruationLowButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_low_h"] forState:UIControlStateHighlighted];
        [_menstruationLowButton addTarget:self action:@selector(toggleMenstruation:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_menstruationLowButton];

        _menstruationNormalButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(669 / 3), CGWidthFromIP6P(227 / 3), CGWidthFromIP6P([UIImage imageNamed:@"cyclediary_button_menstruation_medium"].size.width / 3), CGHeightFromIP6P([UIImage imageNamed:@"cyclediary_button_menstruation_medium"].size.height / 3))];
        [_menstruationNormalButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_medium"] forState:UIControlStateNormal];
        [_menstruationNormalButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_medium_h"] forState:UIControlStateHighlighted];
        [_menstruationNormalButton addTarget:self action:@selector(toggleMenstruation:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_menstruationNormalButton];
        
        _menstruationMuchButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(921 / 3), CGWidthFromIP6P(227 / 3), CGWidthFromIP6P([UIImage imageNamed:@"cyclediary_button_menstruation_many"].size.width / 3), CGHeightFromIP6P([UIImage imageNamed:@"cyclediary_button_menstruation_many"].size.height / 3))];
        [_menstruationMuchButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_many"] forState:UIControlStateNormal];
        [_menstruationMuchButton setImage:[UIImage imageNamed:@"cyclediary_button_menstruation_many_h"] forState:UIControlStateHighlighted];
        [_menstruationMuchButton addTarget:self action:@selector(toggleMenstruation:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_menstruationMuchButton];

        _menstruationLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(16), CGHeightFromIP6P(192), kSCREEN_WIDTH - CGWidthFromIP6P(32), CGHeightFromIP6P(18))];
        [_menstruationLabel setTextAlignment:NSTextAlignmentCenter];

        [cellView addSubview:_menstruationLabel];
        
        
        
        
        
        cellView = [[UIView alloc] initWithFrame:CGRectMake(0, cellView.frame.origin.y + cellView.frame.size.height + k1PX, kSCREEN_WIDTH, 200 / 3)];
        [cellView setBackgroundColor:[UIColor clearColor]];
        [itemListView addSubview:cellView];
        
        cellIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cyclediary_image_icon_pyrexia"]];
        [cellIconImageView setCenter:CGPointMake(57 / 3 + cellIconImageView.frame.size.width / 2, 200 / 3 / 2)];
        [cellView addSubview:cellIconImageView];
        
        cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(160 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"발열"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        
        _pyrexiaButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1108 / 3), 200 / 3 / 2 - CGHeightFromIP6P(70 / 3) / 2, CGWidthFromIP6P(84 / 3), CGHeightFromIP6P(70 / 3))];
        [_pyrexiaButton setImage:[UIImage imageNamed:@"cyclediary_button_pyrexia"] forState:UIControlStateNormal];
        [_pyrexiaButton setImage:[UIImage imageNamed:@"cyclediary_button_pyrexia_h"] forState:UIControlStateHighlighted];
        [_pyrexiaButton addTarget:self action:@selector(togglePyrexia:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_pyrexiaButton];
        
        
        
        cellView = [[UIView alloc] initWithFrame:CGRectMake(0, cellView.frame.origin.y + cellView.frame.size.height + k1PX, kSCREEN_WIDTH, 200 / 3)];
        [cellView setBackgroundColor:[UIColor clearColor]];
        [itemListView addSubview:cellView];
        
        cellIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cyclediary_image_icon_sex"]];
        [cellIconImageView setCenter:CGPointMake(57 / 3 + cellIconImageView.frame.size.width / 2, 200 / 3 / 2)];
        [cellView addSubview:cellIconImageView];
        
        cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(160 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"사랑"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        
        _sexButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1108 / 3), 200 / 3 / 2 - CGHeightFromIP6P(76 / 3) / 2, CGWidthFromIP6P(88 / 3), CGHeightFromIP6P(76 / 3))];
        [_sexButton setImage:[UIImage imageNamed:@"cyclediary_button_sex"] forState:UIControlStateNormal];
        [_sexButton setImage:[UIImage imageNamed:@"cyclediary_button_sex_h"] forState:UIControlStateHighlighted];
        [_sexButton addTarget:self action:@selector(toggleSex:) forControlEvents:UIControlEventTouchUpInside];
        [cellView addSubview:_sexButton];
        

        cellView = [[UIView alloc] initWithFrame:CGRectMake(0, cellView.frame.origin.y + cellView.frame.size.height + k1PX, kSCREEN_WIDTH, 200 / 3)];
        [cellView setBackgroundColor:[UIColor clearColor]];
        [itemListView addSubview:cellView];
        
        cellItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(57 / 3, 0, CGWidthFromIP6P(690 / 3), 200 / 3)];
        [cellItemLabel setFont:RegularWithSize(18)];
        [cellItemLabel setText:@"몸무게"];
        [cellItemLabel setTextColor:UIColorFromRGB(0x000000)];
        [cellView addSubview:cellItemLabel];
        
        _weightTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(860 / 3), 0, CGWidthFromIP6P(332 / 3), 200 / 3)];
        [_weightTextField setPlaceholder:@"미입력"];
        [_weightTextField setBackgroundColor:[UIColor clearColor]];
        [_weightTextField setTextAlignment:NSTextAlignmentRight];
        [_weightTextField setFont:RegularWithSize(18)];
        [_weightTextField setTextColor:[UIColor blackColor]];
        [_weightTextField setDelegate:self];
        [_weightTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        [cellView addSubview:_weightTextField];
        
        _weightPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 215)];
        [_weightPickerView setBackgroundColor:[UIColor whiteColor]];
        [_weightPickerView setDataSource:self];
        [_weightPickerView setDelegate:self];

        UIView *pickerResignView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(414), 44)];
        [pickerResignView setBackgroundColor:[UIColor clearColor]];
        
        UIView *pickerBlackMaskView = [[UIView alloc] initWithFrame:pickerResignView.bounds];
        [pickerBlackMaskView setBackgroundColor:[UIColor blackColor]];
        [pickerBlackMaskView setAlpha:0.8];
        [pickerResignView addSubview:pickerBlackMaskView];
        
        UIButton *weightPickerResignButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(414) - CGWidthFromIP6P(40 + 4 + 4), 3, CGWidthFromIP6P(40), 44 - 3 - 3)];
        [weightPickerResignButton.titleLabel setFont:RegularWithSize(14)];
        [weightPickerResignButton setTitle:@"완료" forState:UIControlStateNormal];
        [weightPickerResignButton setBackgroundColor:[UIColor clearColor]];
        [weightPickerResignButton addTarget:self action:@selector(resignKeyboard) forControlEvents:UIControlEventTouchUpInside];
        [pickerResignView addSubview:weightPickerResignButton];

        [_weightTextField setInputView:_weightPickerView];
        [_weightTextField setInputAccessoryView:pickerResignView];
        
        
        UILabel *dotLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [dotLabel setFont:MediumWithSize(30)];
        [dotLabel setText:@"."];
        [dotLabel setTextColor:UIColorFromRGB(0x333333)];
        [dotLabel sizeToFit];
        [dotLabel setCenter:CGPointMake(_weightPickerView.frame.size.width / 2 + CGWidthFromIP6P(26), _weightPickerView.frame.size.height / 2)];
        [_weightPickerView addSubview:dotLabel];
        
        
        UILabel *kgLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [kgLabel setFont:MediumWithSize(16)];
        [kgLabel setText:@"kg"];
        [kgLabel setTextColor:UIColorFromRGB(0x333333)];
        [kgLabel sizeToFit];
        [kgLabel setCenter:CGPointMake(kSCREEN_WIDTH - CGWidthFromIP6P(60), _weightPickerView.frame.size.height / 2 + 4)];
        [_weightPickerView addSubview:kgLabel];
        
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"CycleInfoDiary",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"CycleInfoDiary",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];

        [self getDailyStatus];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getDailyStatus) name:@"REFRESH_AFTER_LOGIN" object:nil];

    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [_weightTextField setText:[NSString stringWithFormat:@"%ld.%ldkg", (long)[_weightPickerView selectedRowInComponent:0] + 30, (long)[_weightPickerView selectedRowInComponent:1]]];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == 0) {
        return 100;
    } else if (component == 1) {
        return 40;
    }
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 50;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        return [NSString stringWithFormat:@"%ld", (long)row + 30];
    } else if(component == 1) {
        return [NSString stringWithFormat:@"%ld", (long)row];
    }
    return nil;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return 200 - 30;
    } else if(component == 1) {
        return 10;
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}


@end
