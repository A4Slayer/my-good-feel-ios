//
//  PreferenceViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 12..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasswordViewController.h"
#import "ChangeStepAgeViewController.h"
//#import "ChangeStepLastMenstruationViewController.h"
#import "MarketingNotificationAgreementViewController.h"

#import "AgreementViewController.h"
#import "MarketingAgreementViewController.h"

#import "PreferenceWeightViewController.h"

#import "ConnectSensorPreviousViewController.h"
#import "PreferenceSensorViewController.h"

#import "PreferenceUserInfoViewController.h"

@interface PreferenceViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UITableView *mainTableView;

@property (strong, nonatomic) UISwitch *dayMarkSwitch;

@property (strong, nonatomic) UISwitch *dayNotificationSwitch;
@property (strong, nonatomic) UISwitch *weightNotificationSwitch;
@property (strong, nonatomic) UISwitch *eventNotificationSwitch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
