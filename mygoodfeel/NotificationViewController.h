//
//  NotificationViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 6..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationTableViewCell.h"

#import "EventDetailViewController.h"
#import "NoticeDetailViewController.h"

@interface NotificationViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    BOOL isLoading;
    BOOL isMore;
}

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UITableView *mainTableView;
@property (strong, nonatomic) NSMutableArray *notificationArray;

@property (strong, nonatomic) NSString *lastPageStr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
