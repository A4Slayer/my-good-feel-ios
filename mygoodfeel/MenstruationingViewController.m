//
//  MenstruationingViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 28..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "MenstruationingViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface MenstruationingViewController ()

@end

@implementation MenstruationingViewController

- (void)presentWebView:(UIButton *)button
{
    NSString *urlStr;
    NSString *titleStr;
    
    if (button.tag == 0) {
        urlStr = @"https://m.blog.naver.com/PostList.nhn?blogId=yk_onperiod&categoryNo=8&listStyle=style1";
        titleStr = @"닥터스 칼럼";
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"BlogOnPeriod1",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"BlogOnPeriod1",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];
    } else if (button.tag == 1) {
        urlStr = @"https://m.blog.naver.com/PostList.nhn?blogId=yk_onperiod&categoryNo=9&listStyle=style1";
        titleStr = @"닥터스 Q&A";
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"BlogOnPeriod2",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"BlogOnPeriod2",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];
    } else if (button.tag == 2) {
        urlStr = @"https://m.blog.naver.com/PostList.nhn?blogId=yk_onperiod&categoryNo=12&listStyle=style1";
        titleStr = @"PERIODPEDIA";
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"BlogOnPeriod3",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"BlogOnPeriod3",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];
    } else if (button.tag == 3) {
        urlStr = @"https://m.blog.naver.com/PostList.nhn?blogId=yk_onperiod&categoryNo=13&listStyle=style1";
        titleStr = @"생리트렌드리포트";
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"BlogOnPeriod4",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"BlogOnPeriod4",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];
    } else if (button.tag == 4) {
        urlStr = @"https://blog.naver.com/ourperiodwithyk";
        titleStr = @"초경 캠페인";
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"BlogOnPeriod5",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"BlogOnPeriod5",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];
    }
    
    WebViewNavigationController *webViewNavigationController = [[WebViewNavigationController alloc] initWithURL:[urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] title:titleStr];
    [self.navigationController presentViewController:webViewNavigationController animated:YES completion:^{
        //
    }];
}

- (void)refreshWebView
{
    [_webView reload];
}

- (void)backWebView
{
    [_webView goBack];
}

- (void)forwardWebView
{
    [_webView goForward];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.view.bounds;
        gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [self.view.layer insertSublayer:gradient atIndex:0];

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"생리 건강정보"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT)];
        [bgView setBackgroundColor:UIColorFromRGB(0xeeeef2)];
        [self.view addSubview:bgView];
        
        UIButton *columnButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(48 / 3), CGHeightFromIP6P(50 / 3), CGWidthFromIP6P([UIImage imageNamed:@"menstruationing_button_column"].size.width), CGHeightFromIP6P([UIImage imageNamed:@"menstruationing_button_column"].size.height))];
        [columnButton setImage:[UIImage imageNamed:@"menstruationing_button_column"] forState:UIControlStateNormal];
        [columnButton.layer setShadowPath:[UIBezierPath bezierPathWithRoundedRect:columnButton.bounds cornerRadius:6].CGPath];
        [columnButton.layer setShadowColor:[UIColor darkGrayColor].CGColor];
        [columnButton.layer setShadowOffset:CGSizeMake(0, 0)];
        [columnButton.layer setShadowRadius:5];
        [columnButton.layer setShadowOpacity:0.3];
        [columnButton setTag:0];
        [columnButton addTarget:self action:@selector(presentWebView:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:columnButton];
        
        UIButton *qnaButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(48 / 3), columnButton.frame.origin.y + columnButton.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P([UIImage imageNamed:@"menstruationing_button_qna"].size.width), CGHeightFromIP6P([UIImage imageNamed:@"menstruationing_button_qna"].size.height))];
        [qnaButton setImage:[UIImage imageNamed:@"menstruationing_button_qna"] forState:UIControlStateNormal];
        [qnaButton.layer setShadowPath:[UIBezierPath bezierPathWithRoundedRect:qnaButton.bounds cornerRadius:6].CGPath];
        [qnaButton.layer setShadowColor:[UIColor darkGrayColor].CGColor];
        [qnaButton.layer setShadowOffset:CGSizeMake(0, 0)];
        [qnaButton.layer setShadowRadius:5];
        [qnaButton.layer setShadowOpacity:0.3];
        [qnaButton setTag:1];
        [qnaButton addTarget:self action:@selector(presentWebView:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:qnaButton];
        
        UIButton *pediaButton = [[UIButton alloc] initWithFrame:CGRectMake(columnButton.frame.origin.x + columnButton.frame.size.width - CGWidthFromIP6P([UIImage imageNamed:@"menstruationing_button_pedia"].size.width), qnaButton.frame.origin.y, CGWidthFromIP6P([UIImage imageNamed:@"menstruationing_button_pedia"].size.width), CGHeightFromIP6P([UIImage imageNamed:@"menstruationing_button_pedia"].size.height))];
        [pediaButton setImage:[UIImage imageNamed:@"menstruationing_button_pedia"] forState:UIControlStateNormal];
        [pediaButton.layer setShadowPath:[UIBezierPath bezierPathWithRoundedRect:pediaButton.bounds cornerRadius:6].CGPath];
        [pediaButton.layer setShadowColor:[UIColor darkGrayColor].CGColor];
        [pediaButton.layer setShadowOffset:CGSizeMake(0, 0)];
        [pediaButton.layer setShadowRadius:5];
        [pediaButton.layer setShadowOpacity:0.3];
        [pediaButton setTag:2];
        [pediaButton addTarget:self action:@selector(presentWebView:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:pediaButton];
        
        UIButton *reportButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(48 / 3), qnaButton.frame.origin.y + qnaButton.frame.size.height + CGHeightFromIP6P(19), CGWidthFromIP6P([UIImage imageNamed:@"menstruationing_button_report"].size.width), CGHeightFromIP6P([UIImage imageNamed:@"menstruationing_button_report"].size.height))];
        [reportButton setImage:[UIImage imageNamed:@"menstruationing_button_report"] forState:UIControlStateNormal];
        [reportButton.layer setShadowPath:[UIBezierPath bezierPathWithRoundedRect:reportButton.bounds cornerRadius:6].CGPath];
        [reportButton.layer setShadowColor:[UIColor darkGrayColor].CGColor];
        [reportButton.layer setShadowOffset:CGSizeMake(0, 0)];
        [reportButton.layer setShadowRadius:5];
        [reportButton.layer setShadowOpacity:0.3];
        [reportButton setTag:3];
        [reportButton addTarget:self action:@selector(presentWebView:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:reportButton];
        
        UIButton *campaignButton = [[UIButton alloc] initWithFrame:CGRectMake(columnButton.frame.origin.x + columnButton.frame.size.width - CGWidthFromIP6P([UIImage imageNamed:@"menstruationing_button_campaign"].size.width), reportButton.frame.origin.y, CGWidthFromIP6P([UIImage imageNamed:@"menstruationing_button_campaign"].size.width), CGHeightFromIP6P([UIImage imageNamed:@"menstruationing_button_campaign"].size.height))];
        [campaignButton setImage:[UIImage imageNamed:@"menstruationing_button_campaign"] forState:UIControlStateNormal];
        [campaignButton.layer setShadowPath:[UIBezierPath bezierPathWithRoundedRect:campaignButton.bounds cornerRadius:6].CGPath];
        [campaignButton.layer setShadowColor:[UIColor darkGrayColor].CGColor];
        [campaignButton.layer setShadowOffset:CGSizeMake(0, 0)];
        [campaignButton.layer setShadowRadius:5];
        [campaignButton.layer setShadowOpacity:0.3];
        [campaignButton setTag:4];
        [campaignButton addTarget:self action:@selector(presentWebView:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:campaignButton];

//        UIButton *refreshButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_refresh"].size.width, [UIImage imageNamed:@"common_button_refresh"].size.height)];
//        [refreshButton setImage:[UIImage imageNamed:@"common_button_refresh"] forState:UIControlStateNormal];
//        [refreshButton addTarget:self action:@selector(refreshWebView) forControlEvents:UIControlEventTouchUpInside];
//        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:refreshButton]];


//        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT - CGHeightFromIP6P(162 / 3) - kBOTTOM_HEIGHT)];
//        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://blog.naver.com/yk_onperiod"]]];
//        [_webView setDelegate:self];
//        [_webView setScalesPageToFit:YES];
//        [self.view addSubview:_webView];
//
//        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - CGHeightFromIP6P(162 / 3) - kBOTTOM_HEIGHT, kSCREEN_WIDTH, CGHeightFromIP6P(162 / 3) + kBOTTOM_HEIGHT)];
//        [bottomView setBackgroundColor:UIColorFromRGB(0xf9f9f9)];
//        [self.view addSubview:bottomView];
//
//        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, bottomView.frame.origin.y - k1PX, kSCREEN_WIDTH, k1PX)];
//        [separatorView setBackgroundColor:UIColorFromRGB(0xb2b2b2)];
//        [self.view addSubview:separatorView];
//
//
//        _backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(44), CGHeightFromIP6P(44))];
//        [_backButton setImage:[UIImage imageNamed:@"common_button_back_disable"] forState:UIControlStateNormal];
//        [_backButton setCenter:CGPointMake(bottomView.frame.size.width / 2 / 3 - CGWidthFromIP6P(18), (bottomView.frame.size.height - kBOTTOM_HEIGHT) / 2)];
//        [_backButton addTarget:self action:@selector(backWebView) forControlEvents:UIControlEventTouchUpInside];
//        [_backButton setEnabled:NO];
//        [bottomView addSubview:_backButton];
//
//        _forwardButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(44), CGHeightFromIP6P(44))];
//        [_forwardButton setImage:[UIImage imageNamed:@"common_button_forward_disable"] forState:UIControlStateNormal];
//        [_forwardButton setCenter:CGPointMake(bottomView.frame.size.width / 2 / 3 * 2 - CGWidthFromIP6P(4), (bottomView.frame.size.height - kBOTTOM_HEIGHT) / 2)];
//        [_forwardButton addTarget:self action:@selector(forwardWebView) forControlEvents:UIControlEventTouchUpInside];
//        [_forwardButton setEnabled:NO];
//        [bottomView addSubview:_forwardButton];
//
//        _shareButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(44), CGHeightFromIP6P(44))];
//        [_shareButton setImage:[UIImage imageNamed:@"common_button_share"] forState:UIControlStateNormal];
//        [_shareButton setCenter:CGPointMake(bottomView.frame.size.width / 2, (bottomView.frame.size.height - kBOTTOM_HEIGHT) / 2)];
//        [_shareButton addTarget:self action:@selector(shareAction) forControlEvents:UIControlEventTouchUpInside];
//        [bottomView addSubview:_shareButton];

        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"BlogOnPeriod",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"BlogOnPeriod",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
