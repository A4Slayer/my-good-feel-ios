//
//  PreferenceWeightViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 8..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "PreferenceWeightViewController.h"

@interface PreferenceWeightViewController ()

@end

@implementation PreferenceWeightViewController

- (void)changeConfig:(UISwitch *)configSwitch
{
    NSString *infoTypeString = @"104";
    
    NSString *infoValueString = @"N";
    
    if ([configSwitch isOn]) {
        infoValueString = @"Y";
    }
    
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CONFIG_SET_INFO]];
    
    [urlString appendFormat:@"/%@", infoTypeString];
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@", infoValueString];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            if ([self->_weightNotificationSwitch isOn]) {
                [[DataSingleton sharedSingletonClass] registWeightLocalNotification];
            } else {
                [[DataSingleton sharedSingletonClass] removeWeightLocalNotification];
                [self resignPickerView];
            }
            [self->_mainTableView reloadData];
        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (void)loadAppConfig
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CONFIG_GET_INFO]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            
            [self->_weightNotificationSwitch setOn:NO];
            if ([responseData[@"data"][@"WeightPushYN"] isEqualToString:@"Y"]) {
                [self->_weightNotificationSwitch setOn:YES];
                [[DataSingleton sharedSingletonClass] registWeightLocalNotification];
            } else {
                [[DataSingleton sharedSingletonClass] removeWeightLocalNotification];
            }
            [self->_mainTableView reloadData];
            
        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resignPickerView
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self->_pickerResignView setFrame:CGRectMake(0, self.view.frame.size.height, kSCREEN_WIDTH, 44)];
        [self->_timePickerView setFrame:CGRectMake(0, self.view.frame.size.height + 44, kSCREEN_WIDTH, 215 + 44)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)becomePickerView
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self->_pickerResignView setFrame:CGRectMake(0, self.view.frame.size.height - 215 - 44, kSCREEN_WIDTH, 44)];
        [self->_timePickerView setFrame:CGRectMake(0, self.view.frame.size.height - 215, kSCREEN_WIDTH, 215)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"몸무게 재기 알림 설정"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x3c3c3e)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
        [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT) style:UITableViewStyleGrouped];
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setTableFooterView:[[UIView alloc] init]];
        [self.view addSubview:_mainTableView];
        
        
        _timePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - kTOP_HEIGHT + 44, kSCREEN_WIDTH, 215)];
        [_timePickerView setBackgroundColor:[UIColor whiteColor]];
        [_timePickerView setDataSource:self];
        [_timePickerView setDelegate:self];
        [self.view addSubview:_timePickerView];

        [_timePickerView selectRow:[[[NSUserDefaults standardUserDefaults] objectForKey:@"weight_hour"] intValue] inComponent:0 animated:NO];
        [_timePickerView selectRow:[[[NSUserDefaults standardUserDefaults] objectForKey:@"weight_min"] intValue] inComponent:1 animated:NO];

        _pickerResignView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - kTOP_HEIGHT, kSCREEN_WIDTH, 44)];
        [_pickerResignView setBackgroundColor:[UIColor clearColor]];
        
        UIView *pickerBlackMaskView = [[UIView alloc] initWithFrame:_pickerResignView.bounds];
        [pickerBlackMaskView setBackgroundColor:[UIColor blackColor]];
        [pickerBlackMaskView setAlpha:0.8];
        [_pickerResignView addSubview:pickerBlackMaskView];
        
        UIButton *weightPickerResignButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(414) - CGWidthFromIP6P(40 + 4 + 4), 3, CGWidthFromIP6P(40), 44 - 3 - 3)];
        [weightPickerResignButton.titleLabel setFont:RegularWithSize(14)];
        [weightPickerResignButton setTitle:@"완료" forState:UIControlStateNormal];
        [weightPickerResignButton setBackgroundColor:[UIColor clearColor]];
        [weightPickerResignButton addTarget:self action:@selector(resignPickerView) forControlEvents:UIControlEventTouchUpInside];
        [_pickerResignView addSubview:weightPickerResignButton];
        
        [self.view addSubview:_pickerResignView];

        UILabel *dotLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [dotLabel setFont:MediumWithSize(18)];
        [dotLabel setText:@"시"];
        [dotLabel setTextColor:UIColorFromRGB(0x333333)];
        [dotLabel sizeToFit];
        [dotLabel setCenter:CGPointMake(_timePickerView.frame.size.width / 2 - CGWidthFromIP6P(16), _timePickerView.frame.size.height / 2 + 3)];
        [_timePickerView addSubview:dotLabel];
        
        UILabel *kgLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [kgLabel setFont:MediumWithSize(18)];
        [kgLabel setText:@"분"];
        [kgLabel setTextColor:UIColorFromRGB(0x333333)];
        [kgLabel sizeToFit];
        [kgLabel setCenter:CGPointMake(kSCREEN_WIDTH - CGWidthFromIP6P(120), _timePickerView.frame.size.height / 2 + 3)];
        [_timePickerView addSubview:kgLabel];

        [self loadAppConfig];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark -
#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        [self becomePickerView];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 44;
    } else if (indexPath.row == 1) {
        return 60;
    }
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + _weightNotificationSwitch.isOn;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        NSString *cellId = [NSString stringWithFormat:@"Cell_%ld_%ld", (long)indexPath.section, (long)indexPath.row];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            [cell setBackgroundColor:[UIColor whiteColor]];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
            [titleLabel setText:@"몸무게 재기 알림"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:titleLabel];
            
            if (_weightNotificationSwitch) {
                [_weightNotificationSwitch removeFromSuperview];
                _weightNotificationSwitch = nil;
            }
            
            _weightNotificationSwitch = [[UISwitch alloc] init];
            [_weightNotificationSwitch setCenter:CGPointMake(kSCREEN_WIDTH - _weightNotificationSwitch.frame.size.width / 2 - CGWidthFromIP6P(18), 44 / 2)];
            [_weightNotificationSwitch addTarget:self action:@selector(changeConfig:) forControlEvents:UIControlEventValueChanged];
            [cell.contentView addSubview:_weightNotificationSwitch];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        return cell;
    } else if (indexPath.row == 1) {
        NSString *cellId = [NSString stringWithFormat:@"Cell_%ld_%ld_%d_%d", (long)indexPath.section, (long)indexPath.row, [[[NSUserDefaults standardUserDefaults] objectForKey:@"weight_hour"] intValue], [[[NSUserDefaults standardUserDefaults] objectForKey:@"weight_min"] intValue]];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];

        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            [cell setBackgroundColor:[UIColor whiteColor]];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 11, CGWidthFromIP6P(300), 19)];
            [titleLabel setText:@"알림 시간"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:titleLabel];
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, CGWidthFromIP6P(300), 23)];
            [descLabel setText:[NSString stringWithFormat:@"매일 %02d:%02d에 몸무게를 재도록 알립니다.", [[[NSUserDefaults standardUserDefaults] objectForKey:@"weight_hour"] intValue], [[[NSUserDefaults standardUserDefaults] objectForKey:@"weight_min"] intValue]]];
            [descLabel setTextColor:[UIColor lightGrayColor]];
            [descLabel setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:descLabel];

        }
        return cell;
    } else {
        NSString *cellId = [NSString stringWithFormat:@"DEFAULT"];
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        return cell;
    }
    
    return nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", (int)row] forKey:@"weight_hour"];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", (int)row] forKey:@"weight_min"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[DataSingleton sharedSingletonClass] registWeightLocalNotification];
    
    [_mainTableView reloadData];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 90;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 50;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%02d       ", (int)row];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return 24;
    } else if(component == 1) {
        return 60;
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

@end
