//
//  SleepCalendarViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 12. 4..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNSTextAttachment.h"
#import "SleepCalendarGuideViewController.h"

@interface SleepCalendarViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate>
{
    BOOL isSleepTime;
}

@property (strong, nonatomic) id pClass;

@property (strong, nonatomic) UIScrollView *mainScrollView;

@property (strong, nonatomic) UIView *contentView;

@property (strong, nonatomic) UILabel *yearLabel;

@property (strong, nonatomic) NSDictionary *dataDictionary;
@property (strong, nonatomic) NSArray *sleepArray;
@property (strong, nonatomic) UIScrollView *bannerScrollView;

@property (strong, nonatomic) UIView *blackMaskView;
@property (strong, nonatomic) UIView *dateSelectView;
@property (strong, nonatomic) UIView *pickerResignView;
@property (strong, nonatomic) UIPickerView *datePickerView;

@property (strong, nonatomic) UIScrollView *monthScrollView;
@property (strong, nonatomic) NSMutableArray *monthItemArray;

@property (strong, nonatomic) UILabel *prevValueLabel;
@property (strong, nonatomic) UILabel *currValueLabel;

@property (strong, nonatomic) UIButton *sleepScoreButton;
@property (strong, nonatomic) UIButton *sleepTimeButton;

@property (strong, nonatomic) UILabel *avrSleepTimeLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass date:(NSDate *)date;

@end
