//
//  PreferenceSensorNotificaionViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 4. 16..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreferenceSensorNotificaionViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *mainTableView;
@property (strong, nonatomic) UISwitch *notificationSwitch;

@property (strong, nonatomic) UILabel *overHRLabel;
@property (strong, nonatomic) UILabel *underHRLabel;
@property (strong, nonatomic) UILabel *overRRLabel;
@property (strong, nonatomic) UILabel *underRRLabel;

@property (strong, nonatomic) UISlider *overHRSlider;
@property (strong, nonatomic) UISlider *underHRSlider;
@property (strong, nonatomic) UISlider *overRRSlider;
@property (strong, nonatomic) UISlider *underRRSlider;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@end
