//
//  EventDetailViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 14..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"
#import "EventSignViewController.h"
#import "AKSDWebImageManager.h"

@interface EventDetailViewController : UIViewController <UIWebViewDelegate>
{
    BOOL _flagged;
}

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) NSDictionary *dataDictionary;

@property (strong, nonatomic) UIWebView *webView;

@property (strong, nonatomic) UIView *loginView;
@property (strong, nonatomic) UIScrollView *imageScrollView;

@property (strong, nonatomic) UIActivityIndicatorView *loadingIndicatorView;

- (void)updateView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass eventNo:(NSString *)eventNo;

@end
