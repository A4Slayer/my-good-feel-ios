//
//  SleepDayInfoGuideViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 27..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "SleepDayInfoGuideViewController.h"

@interface SleepDayInfoGuideViewController ()

@end

@implementation SleepDayInfoGuideViewController

- (void)dismissSelfView
{
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_close_pink"].size.width, [UIImage imageNamed:@"common_button_close_pink"].size.height)];
        [closeButton setImage:[UIImage imageNamed:@"common_button_close_pink"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(dismissSelfView) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:closeButton]];

        [self setTitle:@"일간 주기 정보 읽는 법"];
        
        
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT)];
        [_mainScrollView setBackgroundColor:UIColorFromRGB(0xffffff)];
        [self.view addSubview:_mainScrollView];

        
        UILabel *sleepSpectrumGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(CGHeightFromIP6P(79 / 3)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [sleepSpectrumGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [sleepSpectrumGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [sleepSpectrumGuideLabel setClipsToBounds:YES];
        [sleepSpectrumGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [sleepSpectrumGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [sleepSpectrumGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [sleepSpectrumGuideLabel setText:@"수면 스펙트럼"];
        [_mainScrollView addSubview:sleepSpectrumGuideLabel];

        UILabel *sleepSpectrumDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepSpectrumGuideLabel.frame.origin.y + sleepSpectrumGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepSpectrumDescLabel setNumberOfLines:0];
        [sleepSpectrumDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepSpectrumDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepSpectrumDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepSpectrumDescLabel setText:@"사용자의 수면 시간 중의 수면 깊이를 색으로 나타냅니다. 렘 수면 구간이 넓거나 수면 스트레스가 많을수록 수면 점수가 낮아지며, 규칙적인 시간에 잠자리에 들면 수면 점수가 높아질 수 있습니다."];
        [sleepSpectrumDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepSpectrumDescLabel];

        
        UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(209 / 3), sleepSpectrumDescLabel.frame.origin.y + sleepSpectrumDescLabel.frame.size.height + CGHeightFromIP6P(26), CGWidthFromIP6P(28), CGHeightFromIP6P(28))];
        [colorView setBackgroundColor:UIColorFromRGB(0xffbdbc)];
        [colorView setClipsToBounds:YES];
        [colorView.layer setCornerRadius:CGWidthFromIP6P(6)];
        [_mainScrollView addSubview:colorView];
        
        UILabel *stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(colorView.frame.origin.x + colorView.frame.size.width + CGWidthFromIP6P(12), colorView.frame.origin.y - CGHeightFromIP6P(1), CGWidthFromIP6P(100), CGHeightFromIP6P(30))];
        [stateLabel setText:@"깨어남 상태"];
        [stateLabel setTextColor:UIColorFromRGB(0x434343)];
        [stateLabel setFont:MediumWithSize(CGHeightFromIP6P(18))];
        [stateLabel sizeToFit];
        [_mainScrollView addSubview:stateLabel];

        UILabel *stateDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(stateLabel.frame.origin.x, stateLabel.frame.origin.y + stateLabel.frame.size.height + CGHeightFromIP6P(1), CGWidthFromIP6P(100), CGHeightFromIP6P(30))];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"수면 센서 영역(매트리스) 밖에 있거나 잠에 들지 않은 상태"]
                                                                                       attributes:@{
                                                                                                    NSFontAttributeName : LightWithSize(CGHeightFromIP6P(13)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x888888),
                                                                                                    NSKernAttributeName : [NSNumber numberWithFloat:CGWidthFromIP6P(-0.8)]
                                                                                                    }];
        [stateDescLabel setAttributedText:attrString];
        [stateDescLabel sizeToFit];
        [_mainScrollView addSubview:stateDescLabel];

        
        colorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(209 / 3), colorView.frame.origin.y + colorView.frame.size.height + CGHeightFromIP6P(53 / 3), CGWidthFromIP6P(28), CGHeightFromIP6P(28))];
        [colorView setBackgroundColor:UIColorFromRGB(0xfcef66)];
        [colorView setClipsToBounds:YES];
        [colorView.layer setCornerRadius:CGWidthFromIP6P(6)];
        [_mainScrollView addSubview:colorView];
        
        stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(colorView.frame.origin.x + colorView.frame.size.width + CGWidthFromIP6P(12), colorView.frame.origin.y - CGHeightFromIP6P(1), CGWidthFromIP6P(100), CGHeightFromIP6P(30))];
        [stateLabel setText:@"뒤척임 상태"];
        [stateLabel setTextColor:UIColorFromRGB(0x434343)];
        [stateLabel setFont:MediumWithSize(CGHeightFromIP6P(18))];
        [stateLabel sizeToFit];
        [_mainScrollView addSubview:stateLabel];
        
        stateDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(stateLabel.frame.origin.x, stateLabel.frame.origin.y + stateLabel.frame.size.height + CGHeightFromIP6P(1), CGWidthFromIP6P(100), CGHeightFromIP6P(30))];
        attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"수면 센서 영역(매트리스)에서 얕은 잠에 빠지지 않은 상태"]
                                                                                       attributes:@{
                                                                                                    NSFontAttributeName : LightWithSize(CGHeightFromIP6P(13)),
                                                                                                    NSForegroundColorAttributeName : UIColorFromRGB(0x888888),
                                                                                                    NSKernAttributeName : [NSNumber numberWithFloat:CGWidthFromIP6P(-0.8)]
                                                                                                    }];
        [stateDescLabel setAttributedText:attrString];
        [stateDescLabel sizeToFit];
        [_mainScrollView addSubview:stateDescLabel];
        
        
        colorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(209 / 3), colorView.frame.origin.y + colorView.frame.size.height + CGHeightFromIP6P(53 / 3), CGWidthFromIP6P(28), CGHeightFromIP6P(28))];
        [colorView setBackgroundColor:UIColorFromRGB(0xa4e581)];
        [colorView setClipsToBounds:YES];
        [colorView.layer setCornerRadius:CGWidthFromIP6P(6)];
        [_mainScrollView addSubview:colorView];
        
        stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(colorView.frame.origin.x + colorView.frame.size.width + CGWidthFromIP6P(12), colorView.frame.origin.y - CGHeightFromIP6P(1), CGWidthFromIP6P(100), CGHeightFromIP6P(30))];
        [stateLabel setText:@"얕은 잠 상태"];
        [stateLabel setTextColor:UIColorFromRGB(0x434343)];
        [stateLabel setFont:MediumWithSize(CGHeightFromIP6P(18))];
        [stateLabel sizeToFit];
        [_mainScrollView addSubview:stateLabel];
        
        stateDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(stateLabel.frame.origin.x, stateLabel.frame.origin.y + stateLabel.frame.size.height + CGHeightFromIP6P(1), CGWidthFromIP6P(100), CGHeightFromIP6P(30))];
        attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"뇌가 잠들지 않은 얕은 잠으로 꿈을 꾸는 상태"]
                                                            attributes:@{
                                                                         NSFontAttributeName : LightWithSize(CGHeightFromIP6P(13)),
                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x888888),
                                                                         NSKernAttributeName : [NSNumber numberWithFloat:CGWidthFromIP6P(-0.8)]
                                                                         }];
        [stateDescLabel setAttributedText:attrString];
        [stateDescLabel sizeToFit];
        [_mainScrollView addSubview:stateDescLabel];
        
        
        colorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(209 / 3), colorView.frame.origin.y + colorView.frame.size.height + CGHeightFromIP6P(53 / 3), CGWidthFromIP6P(28), CGHeightFromIP6P(28))];
        [colorView setBackgroundColor:UIColorFromRGB(0xdaade9)];
        [colorView setClipsToBounds:YES];
        [colorView.layer setCornerRadius:CGWidthFromIP6P(6)];
        [_mainScrollView addSubview:colorView];
        
        stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(colorView.frame.origin.x + colorView.frame.size.width + CGWidthFromIP6P(12), colorView.frame.origin.y - CGHeightFromIP6P(1), CGWidthFromIP6P(100), CGHeightFromIP6P(30))];
        [stateLabel setText:@"깊은 잠 상태"];
        [stateLabel setTextColor:UIColorFromRGB(0x434343)];
        [stateLabel setFont:MediumWithSize(CGHeightFromIP6P(18))];
        [stateLabel sizeToFit];
        [_mainScrollView addSubview:stateLabel];
        
        stateDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(stateLabel.frame.origin.x, stateLabel.frame.origin.y + stateLabel.frame.size.height + CGHeightFromIP6P(1), CGWidthFromIP6P(100), CGHeightFromIP6P(30))];
        attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"뇌까지 잠든 깊은 잠으로 나의 몸이 휴식을 취하는 상태"]
                                                            attributes:@{
                                                                         NSFontAttributeName : LightWithSize(CGHeightFromIP6P(13)),
                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x888888),
                                                                         NSKernAttributeName : [NSNumber numberWithFloat:CGWidthFromIP6P(-0.8)]
                                                                         }];
        [stateDescLabel setAttributedText:attrString];
        [stateDescLabel sizeToFit];
        [_mainScrollView addSubview:stateDescLabel];

        sleepSpectrumDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(stateDescLabel.frame.origin.y + stateDescLabel.frame.size.height + CGHeightFromIP6P(23)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepSpectrumDescLabel setNumberOfLines:0];
        [sleepSpectrumDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepSpectrumDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepSpectrumDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepSpectrumDescLabel setText:@"수면 정보 센싱 영역은 센서 반경 40cm로서 뒤척임과 수면습관에 따라 수면 정보 측정이 부정확할 수 있습니다."];
        [sleepSpectrumDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepSpectrumDescLabel];
        
        
        UILabel *sleepHeartrateGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepSpectrumDescLabel.frame.origin.y + sleepSpectrumDescLabel.frame.size.height + CGHeightFromIP6P(28)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [sleepHeartrateGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [sleepHeartrateGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [sleepHeartrateGuideLabel setClipsToBounds:YES];
        [sleepHeartrateGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [sleepHeartrateGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [sleepHeartrateGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [sleepHeartrateGuideLabel setText:@"취침 중 심박수 / 호흡주기"];
        [_mainScrollView addSubview:sleepHeartrateGuideLabel];
        
        UILabel *sleepHeartrateDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepHeartrateGuideLabel.frame.origin.y + sleepHeartrateGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [sleepHeartrateDescLabel setNumberOfLines:0];
        [sleepHeartrateDescLabel setBackgroundColor:[UIColor clearColor]];
        [sleepHeartrateDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [sleepHeartrateDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [sleepHeartrateDescLabel setText:@"취침 중에 매트리스 밑 수면 센서가 사용자의 호흡주기와 심박수를 추적합니다. 일간 수면 정보 메뉴에서는 오늘 취침 중 가장 낮은 심박수/호흡주기와 가장 높은 것을 화면에서 확인하실 수 있습니다. 매일 아침 센서가 추적한 심박수와 호흡주기가 기준치를 미달/초과하였을 경우 PUSH 알림을 띄우며, \"설정\"에서 심박수와 호흡주기의 기준을 조정할 수 있습니다."];
        [sleepHeartrateDescLabel sizeToFit];
        [_mainScrollView addSubview:sleepHeartrateDescLabel];

        
        UILabel *weightGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(sleepHeartrateDescLabel.frame.origin.y + sleepHeartrateDescLabel.frame.size.height + CGHeightFromIP6P(28)), CGWidthFromIP6P(1146 / 3), ceilf(CGHeightFromIP6P(160 / 3)))];
        [weightGuideLabel setBackgroundColor:UIColorFromRGB(0xf7f9fa)];
        [weightGuideLabel.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [weightGuideLabel setClipsToBounds:YES];
        [weightGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [weightGuideLabel setTextColor:UIColorFromRGB(0x000000)];
        [weightGuideLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(20))];
        [weightGuideLabel setText:@"몸무게와 생리 주기의 관계"];
        [_mainScrollView addSubview:weightGuideLabel];
        
        UILabel *weightDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), ceilf(weightGuideLabel.frame.origin.y + weightGuideLabel.frame.size.height + CGHeightFromIP6P(16)), kSCREEN_WIDTH - CGWidthFromIP6P(50 / 3) * 2, ceilf(CGHeightFromIP6P(100)))];
        [weightDescLabel setNumberOfLines:0];
        [weightDescLabel setBackgroundColor:[UIColor clearColor]];
        [weightDescLabel setTextColor:UIColorFromRGB(0x646464)];
        [weightDescLabel setFont:RegularWithSize(CGHeightFromIP6P(16))];
        [weightDescLabel setText:@"배란 이후 체내에 프로게스테론(황체호르몬)의 분비가 증가함에 따라 임신을 대비하여 체내에 수분과 영양분을 축적합니다. 역시 생리 시작일로부터 2주 전부터 몸무게가 증가하며 이후 생리 시작일부터 다시 몸무게가 줄어듭니다."];
        [weightDescLabel sizeToFit];
        [_mainScrollView addSubview:weightDescLabel];

        [_mainScrollView setContentSize:CGSizeMake(_mainScrollView.frame.size.width, weightDescLabel.frame.origin.y + weightDescLabel.frame.size.height + CGHeightFromIP6P(16))];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
