//
//  PasswordChangeViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 23..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"

@interface PasswordChangeViewController : UIViewController
{
    int passwordIndex;
    int changeStep;
}

@property (strong, nonatomic) NSMutableArray *passwordArray;
@property (strong, nonatomic) NSMutableArray *passwordConfirmArray;

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UILabel *descLabel;

@property (strong, nonatomic) UIView *passwordCharacter1View;
@property (strong, nonatomic) UIView *passwordCharacter2View;
@property (strong, nonatomic) UIView *passwordCharacter3View;
@property (strong, nonatomic) UIView *passwordCharacter4View;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end

