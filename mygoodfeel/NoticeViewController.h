//
//  NoticeViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 4..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoticeDetailViewController.h"
#import "NoticeTableViewCell.h"

@interface NoticeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    BOOL isLoading;
    BOOL isMore;
}

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) UITableView *mainTableView;
@property (strong, nonatomic) NSMutableArray *noticeArray;

@property (strong, nonatomic) NSString *lastPageStr;

@property (strong, nonatomic) NSMutableArray *readBoardNoArray;

- (void)addReadBoardNo:(NSString *)boardNo;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
