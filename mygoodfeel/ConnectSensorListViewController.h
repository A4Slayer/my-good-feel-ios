//
//  ConnectSensorListViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"
#import "ConnectSensorDoneViewController.h"
#import <ESFramework/ESFramework.h>

@interface ConnectSensorListViewController : UIViewController
{
    int findingCount;
    BOOL found;
}

@property (strong, nonatomic) id pClass;

@property (strong, nonatomic) UIImageView *sensorImageView;
@property (strong, nonatomic) UILabel *processLabel;
@property (strong, nonatomic) UIActivityIndicatorView *indicatorView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
