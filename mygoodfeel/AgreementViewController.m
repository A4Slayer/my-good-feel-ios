//
//  AgreementViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 14..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "AgreementViewController.h"

@interface AgreementViewController ()

@end

@implementation AgreementViewController

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:UIColorFromRGB(0xfafafa)];
        _pClass = pClass;

        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_back"].size.width / 2)];
        [backButton setImage:[UIImage imageNamed:@"common_button_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
        
        UIWebView *agreementWebView = [[UIWebView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(41) / 3, CGHeightFromIP6P(41) / 3, CGWidthFromIP6P(1160) / 3, CGHeightFromIP6P(1988) / 3 + kIPHONE_X_BOTTOM_MARGIN)];
        [agreementWebView setBackgroundColor:[UIColor whiteColor]];
        [agreementWebView.layer setBorderColor:UIColorFromRGB(0xd7d7d7).CGColor];
        [agreementWebView.layer setBorderWidth:k1PX];
        [agreementWebView setScalesPageToFit:YES];
        [agreementWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://m.ykbrand.co.kr/Terms/TermsPop?type=yk3"]]];
        [self.view addSubview:agreementWebView];

        [self setTitle:@"MY좋은느낌 이용약관"];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
