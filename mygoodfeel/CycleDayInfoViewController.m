//
//  CycleDayInfoViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 18..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "CycleDayInfoViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface CycleDayInfoViewController ()

@end

@implementation CycleDayInfoViewController

- (void)getCycleDailyInfo
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CYCLEDAILY_GET_INFO]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@", _dateString];
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserStatus"]];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        //
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            self->_dataDictionary = responseData[@"data"];
            [self->_cardViewCarousel reloadData];
        } else {
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (void)becomeDatePicker
{
    if (![_blackMaskButton superview]) {
        [self.navigationController.view addSubview:_blackMaskButton];
        [self.navigationController.view addSubview:_datePicker];
    }
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self->_blackMaskButton setAlpha:0.7];
        [self->_datePicker setFrame:CGRectMake(0, kSCREEN_HEIGHT - 215, kSCREEN_WIDTH, 215)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)resignDatePicker
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self->_blackMaskButton setAlpha:0];
        [self->_datePicker setFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 215)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)changedDatePickerValue:(UIDatePicker *)datePicker
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    formatter.dateFormat = @"yyyy-MM-dd";
    _dateString = [formatter stringFromDate:datePicker.date];
    
    formatter.dateFormat = @"yyyy년 M월 d일";
    [_dateButton setTitle:[formatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
    
    [self getCycleDailyInfo];
}

- (void)presentCycleDayInfoGuide
{
    CycleDayInfoGuideViewController *cycleDayInfoGuideViewController = [[CycleDayInfoGuideViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *cycleDayInfoGuideNavigationController = [[UINavigationController alloc] initWithRootViewController:cycleDayInfoGuideViewController];
    [cycleDayInfoGuideNavigationController.navigationBar setTranslucent:NO];
    [cycleDayInfoGuideNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                             forBarPosition:UIBarPositionAny
                                                                 barMetrics:UIBarMetricsDefault];
    [cycleDayInfoGuideNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [cycleDayInfoGuideNavigationController.navigationBar setTitleTextAttributes:@{
                                                                                  NSFontAttributeName: SemiBoldWithSize(18),
                                                                                  NSForegroundColorAttributeName: UIColorFromRGB(0xf05971)
                                                                                  }];
    [cycleDayInfoGuideNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffeff1)] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController presentViewController:cycleDayInfoGuideNavigationController animated:YES completion:^{
        //
    }];
}

- (void)presentCycleDiaryViewController:(UIButton *)button
{
    NSString *dateString = [NSString stringWithFormat:@"%ld", (long)button.tag];
    dateString = [NSString stringWithFormat:@"%@-%@-%@", [dateString substringWithRange:NSMakeRange(0, 4)], [dateString substringWithRange:NSMakeRange(4, 2)], [dateString substringWithRange:NSMakeRange(6, 2)]];
    
    CycleDiaryViewController *cycleDiaryViewController = [[CycleDiaryViewController alloc] initWithNibName:nil bundle:nil pClass:self dateString:dateString];
    UINavigationController *cycleDiaryNavigationController = [[UINavigationController alloc] initWithRootViewController:cycleDiaryViewController];
    [cycleDiaryNavigationController.navigationBar setTranslucent:NO];
    [cycleDiaryNavigationController.navigationBar setTitleTextAttributes:@{
                                                                           NSFontAttributeName: SemiBoldWithSize(18),
                                                                           NSForegroundColorAttributeName: UIColorFromRGB(0x000000)
                                                                           }];
    [cycleDiaryNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffffff)] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController presentViewController:cycleDiaryNavigationController animated:YES completion:^{
        //
    }];
}

- (void)changeToMainViewController
{
    [(RootViewController *)_pClass changeToMainViewController:_dateString];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass dateString:(NSString *)dateString
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        _dateString = dateString;
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        
        NSMutableAttributedString *attrString;
        CNSTextAttachment *imageAttachment;
        
        UIButton *titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleButton setBackgroundColor:[UIColor clearColor]];
        [titleButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleButton addTarget:self action:@selector(presentCycleDayInfoGuide) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setTitleView:titleButton];
        
        if (@available(iOS 9.0, *)) {
            [titleButton setTitle:@"주기 정보 " forState:UIControlStateNormal];
            [titleButton.titleLabel setFont:SemiBoldWithSize(19)];
            [titleButton setTitleColor:UIColorFromRGB(0x3c3c3e) forState:UIControlStateNormal];
            [titleButton setImage:[UIImage imageNamed:@"cycledayinfo_button_qmark"] forState:UIControlStateNormal];
            [titleButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
            [titleButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 3.5 / 2, 0)];
        } else {
            attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"주기 정보 "]
                                                                attributes:@{ NSFontAttributeName : SemiBoldWithSize(19),
                                                                              NSForegroundColorAttributeName : [UIColor whiteColor] }];
            
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -3.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_button_qmark"]];
            [attrString appendAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            
            [titleButton setAttributedTitle:attrString forState:UIControlStateNormal];
        }

        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu_black"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu_black"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
        
        UIButton *cycleCalendarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cycledayinfo_button_calendar"].size.width, [UIImage imageNamed:@"cycledayinfo_button_calendar"].size.height)];
        [cycleCalendarButton setImage:[UIImage imageNamed:@"cycledayinfo_button_calendar"] forState:UIControlStateNormal];
        [cycleCalendarButton addTarget:self action:@selector(changeToMainViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cycleCalendarButton]];

        
        _dateButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(284 / 3), kTOP_HEIGHT + CGHeightFromIP6P(15), CGWidthFromIP6P(674 / 3), CGHeightFromIP6P(148 / 3))];
        [_dateButton.layer setCornerRadius:_dateButton.frame.size.height / 2];
        [_dateButton setClipsToBounds:YES];
        [_dateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_dateButton.titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(19))];
        [_dateButton setTitle:[NSString stringWithFormat:@"%@년 %d월 %d일", [dateString substringWithRange:NSMakeRange(0, 4)], [[dateString substringWithRange:NSMakeRange(5, 2)] intValue], [[dateString substringWithRange:NSMakeRange(8, 2)] intValue]] forState:UIControlStateNormal];
        [_dateButton setTitleEdgeInsets:UIEdgeInsetsMake(CGHeightFromIP6P(2), 0, 0, 0)];
        [_dateButton addTarget:self action:@selector(becomeDatePicker) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_dateButton];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = _dateButton.bounds;
        gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [_dateButton.layer insertSublayer:gradient atIndex:0];

        UIImageView *optionImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_option"]];
        [optionImageView setCenter:CGPointMake(_dateButton.frame.size.width - CGWidthFromIP6P(23), _dateButton.frame.size.height / 2)];
        [_dateButton addSubview:optionImageView];

        
        _cardViewCarousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, _dateButton.frame.origin.y + _dateButton.frame.size.height + CGHeightFromIP6P(10), kSCREEN_WIDTH, CGHeightFromIP6P(1654 / 3))];
        [_cardViewCarousel setType:iCarouselTypeRotary];
        [_cardViewCarousel setBackgroundColor:[UIColor clearColor]];
        [_cardViewCarousel setDelegate:self];
        [_cardViewCarousel setDataSource:self];
        [_cardViewCarousel setPagingEnabled:YES];
        [_cardViewCarousel setBounces:NO];
        [self.view addSubview:_cardViewCarousel];

        
        _cardPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, _cardViewCarousel.frame.origin.y + _cardViewCarousel.frame.size.height + CGHeightFromIP6P(18), kSCREEN_WIDTH, CGHeightFromIP6P(13))];
        [_cardPageControl setPageIndicatorTintColor:UIColorFromRGB(0xffd8dd)];
        [_cardPageControl setCurrentPageIndicatorTintColor:UIColorFromRGB(0xf27f8e)];
        [_cardPageControl setNumberOfPages:1];
        [_cardPageControl setUserInteractionEnabled:NO];
        [self.view addSubview:_cardPageControl];

        
        _blackMaskButton = [[UIButton alloc] initWithFrame:self.view.bounds];
        [_blackMaskButton setBackgroundColor:[UIColor blackColor]];
        [_blackMaskButton setAlpha:0];
        [_blackMaskButton addTarget:self action:@selector(resignDatePicker) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.view addSubview:_blackMaskButton];
        
        _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 215)];
        [_datePicker setBackgroundColor:[UIColor whiteColor]];
        [_datePicker setDatePickerMode:UIDatePickerModeDate];
        [_datePicker addTarget:self action:@selector(changedDatePickerValue:) forControlEvents:UIControlEventValueChanged];
        [_datePicker setLocale:[NSLocale localeWithLocaleIdentifier:@"ko"]];
        [self.navigationController.view addSubview:_datePicker];

        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                       fromDate:[NSDate date]];
        [components setTimeZone:[NSTimeZone localTimeZone]];
        [components setMonth:1];
        [components setDay:1];
        [components setYear:[components year] - 10];
        [_datePicker setMinimumDate:[[NSCalendar currentCalendar] dateFromComponents:components]];

        components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                     fromDate:[NSDate date]];
        [components setTimeZone:[NSTimeZone localTimeZone]];
        [components setMonth:12];
        [components setDay:31];
        [components setYear:[components year] + 10];
        [_datePicker setMaximumDate:[[NSCalendar currentCalendar] dateFromComponents:components]];
        
        NSDateFormatter *selectedDateFormatter = [[NSDateFormatter alloc] init];
        [selectedDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [selectedDateFormatter setDateFormat:@"yyyy-MM-dd"];
        [_datePicker setDate:[selectedDateFormatter dateFromString:dateString]];
        
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"CycleInfoDaily",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"CycleInfoDaily",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];

        [self getCycleDailyInfo];
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"cycleday_guide"] intValue] == 0) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"cycleday_guide"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            UIButton *guideButton = [[UIButton alloc] initWithFrame:[[UIApplication sharedApplication] keyWindow].bounds];

            if (IS_35INCH) {
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_4"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_4"] forState:UIControlStateHighlighted];
            } else if (IS_IPHONE5S) {
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_5s"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_5s"] forState:UIControlStateHighlighted];
            } else if (IS_IPHONE8) {
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_8"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_8"] forState:UIControlStateHighlighted];
            } else if (IS_IPHONE8P) {
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_8p"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_8p"] forState:UIControlStateHighlighted];
            } else if (IS_IPHONEX) {
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_x"] forState:UIControlStateNormal];
                [guideButton setImage:[UIImage imageNamed:@"cycledayinfo_image_guide_x"] forState:UIControlStateHighlighted];
            }
            
            UIImageView *cancelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cyclecalendar_image_close"]];
            [cancelImageView setFrame:CGRectMake(16, 24, cancelImageView.frame.size.width, cancelImageView.frame.size.height)];
            [guideButton addSubview:cancelImageView];


            [guideButton addTarget:self action:@selector(removeGuideView:) forControlEvents:UIControlEventTouchUpInside];
            [[[UIApplication sharedApplication] keyWindow] addSubview:guideButton];

        }

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCycleDailyInfo) name:@"REFRESH_AFTER_LOGIN" object:nil];

    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)removeGuideView:(UIButton *)guideButton
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [guideButton setAlpha:0];
    } completion:^(BOOL finished) {
        [guideButton removeFromSuperview];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma marㄷ -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel

    if (_dataDictionary) {
        if ([_dataDictionary[@"Banner"] isKindOfClass:[NSArray class]]) {
            [_cardPageControl setNumberOfPages:1 + [_dataDictionary[@"Banner"] count]];
            [_cardPageControl setCurrentPage:0];
            
            return 1 + [_dataDictionary[@"Banner"] count];
        } else {
            return 1;
        }
    } else {
        return 0;
    }

    return 0;
}

- (void)balloonAnimation:(UIImageView *)balloonImageView
{
    CGPoint balloonCenter = balloonImageView.center;
    [balloonImageView setFrame:CGRectMake(0, 0, 0, 0)];
    [balloonImageView setCenter:balloonCenter];
    [balloonImageView setAlpha:1];
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [balloonImageView setFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.width, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.height)];
        [balloonImageView setCenter:balloonCenter];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            [balloonImageView setFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.width * 0.7, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.height * 0.7)];
            [balloonImageView setCenter:balloonCenter];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                [balloonImageView setFrame:CGRectMake(0, 0, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.width, [UIImage imageNamed:@"cycledayinfo_image_balloon"].size.height)];
                [balloonImageView setCenter:balloonCenter];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5 delay:3 options:UIViewAnimationOptionCurveLinear animations:^{
                    [balloonImageView setAlpha:0];
                } completion:^(BOOL finished) {
                }];
            }];
        }];
    }];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (index == 0) {       // info view
        UIView *infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(1586 / 3))];

        [infoView setBackgroundColor:[UIColor whiteColor]];
        [infoView setClipsToBounds:YES];
        [infoView.layer setCornerRadius:CGWidthFromIP6P(38 / 3)];
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), CGHeightFromIP6P(81 / 3), CGWidthFromIP6P(500 / 3), CGHeightFromIP6P(200 / 3))];
        [dateLabel setNumberOfLines:0];
        [dateLabel setFont:SemiBoldWithSize(21)];
        [dateLabel setTextColor:UIColorFromRGB(0x424242)];

        NSString *dateString = _dataDictionary[@"CycleDaily"][@"TarGetDt"];
        
        NSDateFormatter *weekdayDateFormatter = [[NSDateFormatter alloc] init];
        [weekdayDateFormatter setDateFormat:@"yyyy-MM-dd"];
        [weekdayDateFormatter setTimeZone:[NSTimeZone localTimeZone]];

        NSCalendar *weekdayCalender = [NSCalendar currentCalendar];
        NSDateComponents *weekdayComponents = [weekdayCalender components:NSCalendarUnitWeekday fromDate:[weekdayDateFormatter dateFromString:dateString]];
        NSArray *weekdayArray = @[ @"", @"일", @"월", @"화", @"수", @"목", @"금", @"토" ];

        [dateLabel setText:[NSString stringWithFormat:@"%@년\n%d월 %d일 (%@)", [dateString substringWithRange:NSMakeRange(0, 4)], [[dateString substringWithRange:NSMakeRange(5, 2)] intValue], [[dateString substringWithRange:NSMakeRange(8, 2)] intValue], weekdayArray[[weekdayComponents weekday]]]];
        [dateLabel sizeToFit];
        [infoView addSubview:dateLabel];
        
        UIButton *diaryButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(993 / 3), CGHeightFromIP6P(113 / 3), [UIImage imageNamed:@"cycledayinfo_button_diary"].size.width, [UIImage imageNamed:@"cycledayinfo_button_diary"].size.height)];
        [diaryButton setImage:[UIImage imageNamed:@"cycledayinfo_button_diary"] forState:UIControlStateNormal];
        [diaryButton setTag:[[dateString stringByReplacingOccurrencesOfString:@"-" withString:@""] intValue]];
        [diaryButton addTarget:self action:@selector(presentCycleDiaryViewController:) forControlEvents:UIControlEventTouchUpInside];
        [infoView addSubview:diaryButton];
        
        if ([[weekdayDateFormatter dateFromString:dateString] compare:[NSDate date]] == NSOrderedDescending) {
            [diaryButton setAlpha:0];
        } else {
            UIImageView *balloonImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_balloon"]];
            [balloonImageView setFrame:CGRectMake(diaryButton.frame.origin.x + diaryButton.frame.size.width - balloonImageView.frame.size.width + CGWidthFromIP6P(14), diaryButton.frame.origin.y + diaryButton.frame.size.height + CGHeightFromIP6P(6), balloonImageView.frame.size.width, balloonImageView.frame.size.height)];
            [balloonImageView setAlpha:0];
            [infoView addSubview:balloonImageView];
            
            [self performSelector:@selector(balloonAnimation:) withObject:balloonImageView afterDelay:0.0f];
        }
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(81 / 3), CGHeightFromIP6P(312 / 3), CGWidthFromIP6P(986 / 3), k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xe9e9e9)];
        [infoView addSubview:separatorView];
        
        UIImageView *statusImageView;
        
        if ([_dataDictionary[@"CycleDaily"][@"IconCd"] intValue] == 101) {
            statusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_none"]];
        } else if ([_dataDictionary[@"CycleDaily"][@"IconCd"] intValue] == 102) {
            statusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_normal"]];
        } else if ([_dataDictionary[@"CycleDaily"][@"IconCd"] intValue] == 103) {
            statusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_much"]];
        } else if ([_dataDictionary[@"CycleDaily"][@"IconCd"] intValue] == 104) {
            statusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_pre"]];
        } else if ([_dataDictionary[@"CycleDaily"][@"IconCd"] intValue] == 105) {
            statusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_availablemenstruation"]];
        } else if ([_dataDictionary[@"CycleDaily"][@"IconCd"] intValue] == 201) {
            statusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_less"]];
        }
        
        [statusImageView setCenter:CGPointMake(infoView.frame.size.width / 2, CGHeightFromIP6P(657 / 3))];
        [infoView addSubview:statusImageView];

        
        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50), statusImageView.frame.origin.y + statusImageView.frame.size.height + CGHeightFromIP6P(35), infoView.frame.size.width - CGWidthFromIP6P(100), CGHeightFromIP6P(40))];
        [statusLabel setBackgroundColor:[UIColor clearColor]];
        [statusLabel setTextAlignment:NSTextAlignmentCenter];
        [statusLabel setFont:BoldWithSize(CGHeightFromIP6P(27))];
        [statusLabel setText:_dataDictionary[@"CycleDaily"][@"MainDescription"]];
        [statusLabel setAdjustsFontSizeToFitWidth:YES];
        [statusLabel sizeToFit];

        if (statusLabel.frame.size.width > infoView.frame.size.width - CGWidthFromIP6P(100)) {
            [statusLabel setFrame:CGRectMake(0, statusLabel.frame.origin.y, infoView.frame.size.width - CGWidthFromIP6P(100), statusLabel.frame.size.height)];
        }
        
        [statusLabel setCenter:CGPointMake(infoView.frame.size.width / 2, statusLabel.center.y)];
        [infoView addSubview:statusLabel];
        
        
        UILabel *statusDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50), statusLabel.frame.origin.y + statusLabel.frame.size.height + CGHeightFromIP6P(15), infoView.frame.size.width - CGWidthFromIP6P(100), CGHeightFromIP6P(40))];
        [statusDescLabel setNumberOfLines:0];
        [statusDescLabel setBackgroundColor:[UIColor clearColor]];
        [statusDescLabel setTextColor:UIColorFromRGB(0x868686)];
        [statusDescLabel setTextAlignment:NSTextAlignmentCenter];
        [statusDescLabel setFont:MediumWithSize(CGHeightFromIP6P(17))];
        [statusDescLabel setText:_dataDictionary[@"CycleDaily"][@"SubDescription"]];
        [statusDescLabel sizeToFit];
        
        if (statusDescLabel.frame.size.width > infoView.frame.size.width - CGWidthFromIP6P(100)) {
            [statusDescLabel setFrame:CGRectMake(0, statusDescLabel.frame.origin.y, infoView.frame.size.width - CGWidthFromIP6P(100), statusDescLabel.frame.size.height)];
        }
        
        [statusDescLabel setCenter:CGPointMake(infoView.frame.size.width / 2, statusDescLabel.center.y)];
        [infoView addSubview:statusDescLabel];

        UIImageView *leftQuotesImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_quotes_left"]];
        [leftQuotesImageView setFrame:CGRectMake(statusLabel.frame.origin.x - leftQuotesImageView.frame.size.width - CGWidthFromIP6P(10), 0, leftQuotesImageView.frame.size.width, leftQuotesImageView.frame.size.height)];
        [leftQuotesImageView setCenter:CGPointMake(leftQuotesImageView.center.x, (statusDescLabel.frame.origin.y + statusDescLabel.frame.size.height - statusLabel.frame.origin.y) / 2 + statusLabel.frame.origin.y)];
        [infoView addSubview:leftQuotesImageView];

        UIImageView *rightQuotesImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cycledayinfo_image_quotes_right"]];
        [rightQuotesImageView setFrame:CGRectMake(statusLabel.frame.origin.x + statusLabel.frame.size.width + CGWidthFromIP6P(10), leftQuotesImageView.frame.origin.y, rightQuotesImageView.frame.size.width, rightQuotesImageView.frame.size.height)];
        [infoView addSubview:rightQuotesImageView];


        if ([_dataDictionary[@"CycleDaily"][@"IconCd"] intValue] == 105) {
            [statusLabel setTextColor:UIColorFromRGB(0xf78abd)];
            [leftQuotesImageView setImage:[UIImage imageNamed:@"cycledayinfo_image_quotes_left_pink"]];
            [rightQuotesImageView setImage:[UIImage imageNamed:@"cycledayinfo_image_quotes_right_pink"]];
        } else {
            [statusLabel setTextColor:UIColorFromRGB(0xf27f8e)];
        }

        separatorView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(81 / 3), CGHeightFromIP6P(1286 / 3), CGWidthFromIP6P(986 / 3), k1PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xe9e9e9)];
        [infoView addSubview:separatorView];
        
        UILabel *pyrexiaLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(78 / 3), CGHeightFromIP6P(1366 / 3), CGWidthFromIP6P(160 / 3), CGHeightFromIP6P(30))];
        [pyrexiaLabel setFont:MediumWithSize(CGHeightFromIP6P(16))];
        [pyrexiaLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [pyrexiaLabel setBackgroundColor:[UIColor clearColor]];
        [pyrexiaLabel setText:@"발열"];
        [pyrexiaLabel sizeToFit];
        [infoView addSubview:pyrexiaLabel];
        
        UILabel *pyrexiaValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(pyrexiaLabel.frame.origin.x, CGHeightFromIP6P(1445 / 3), CGWidthFromIP6P(240 / 3), CGHeightFromIP6P(66 / 3))];
        CNSTextAttachment *imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -4)];
        NSMutableAttributedString *attrString;
        
        if ([_dataDictionary[@"CycleDaily"][@"FeverYN"] isEqualToString:@"Y"]) {
            [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_pyrexia_mini"]];
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  있음"]
                                                                               attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
        } else {
            [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_none_pyrexia_mini"]];
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  정상"]
                                                                               attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
        }
        
        [pyrexiaValueLabel setAttributedText:attrString];
        [pyrexiaValueLabel sizeToFit];
        [infoView addSubview:pyrexiaValueLabel];
        
        
        UILabel *sexLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(434 / 3), CGHeightFromIP6P(1366 / 3), CGWidthFromIP6P(160 / 3), CGHeightFromIP6P(30))];
        [sexLabel setFont:MediumWithSize(CGHeightFromIP6P(16))];
        [sexLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [sexLabel setBackgroundColor:[UIColor clearColor]];
        [sexLabel setText:@"사랑"];
        [sexLabel sizeToFit];
        [infoView addSubview:sexLabel];
        
        UILabel *sexValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(sexLabel.frame.origin.x, CGHeightFromIP6P(1445 / 3), CGWidthFromIP6P(240 / 3), CGHeightFromIP6P(66 / 3))];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -4)];

        if ([_dataDictionary[@"CycleDaily"][@"SexYN"] isEqualToString:@"Y"]) {
            [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_sex_mini"]];
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  있음"]
                                                                               attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
        } else {
            [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_none_sex_mini"]];
            attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  없음"]
                                                                               attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
        }
        [sexValueLabel setAttributedText:attrString];
        [sexValueLabel sizeToFit];
        [infoView addSubview:sexValueLabel];

        
        UILabel *weightLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(787 / 3), CGHeightFromIP6P(1366 / 3), CGWidthFromIP6P(160 / 3), CGHeightFromIP6P(30))];
        [weightLabel setFont:MediumWithSize(CGHeightFromIP6P(16))];
        [weightLabel setTextColor:UIColorFromRGB(0xa8a8a8)];
        [weightLabel setBackgroundColor:[UIColor clearColor]];
        [weightLabel setText:@"몸무게"];
        [weightLabel sizeToFit];
        [infoView addSubview:weightLabel];
        
        UILabel *weightValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(weightLabel.frame.origin.x, CGHeightFromIP6P(1445 / 3), CGWidthFromIP6P(240 / 3), CGHeightFromIP6P(66 / 3))];
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -4)];
        [imageAttachment setImage:[UIImage imageNamed:@"cycledayinfo_image_icon_weight_mini"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        
        if (![_dataDictionary[@"CycleDaily"][@"Weight"] isEqualToString:@"0"]) {
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %.1fkg", [_dataDictionary[@"CycleDaily"][@"Weight"] floatValue]]
                                                                               attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
        } else {
            [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  미입력"]
                                                                               attributes:@{ NSFontAttributeName : MediumWithSize(CGHeightFromIP6P(18)),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0x525252) }]];
        }
        
        [weightValueLabel setAttributedText:attrString];
        [weightValueLabel sizeToFit];
        [infoView addSubview:weightValueLabel];


        view = infoView;
    } else {
        
        UIImageView *bannerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(1586 / 3))];
        [bannerImageView setBackgroundColor:[UIColor grayColor]];
        [bannerImageView setUserInteractionEnabled:YES];
        [bannerImageView setTag:index - 1];
        [bannerImageView.layer setCornerRadius:CGWidthFromIP6P(38 / 3)];
        [bannerImageView setClipsToBounds:YES];
        
        UIActivityIndicatorView *loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [loadingIndicatorView setCenter:CGPointMake(bannerImageView.frame.size.width / 2, bannerImageView.frame.size.height / 2)];
        [bannerImageView addSubview:loadingIndicatorView];
        [loadingIndicatorView startAnimating];

        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchCardBanner:)];
        [bannerImageView addGestureRecognizer:tapGestureRecognizer];
        
        [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][index - 1][@"BannerPath"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                     options:0
                                                    progress:nil
                                                   completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                       if (!error) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [loadingIndicatorView stopAnimating];
                                                               [loadingIndicatorView removeFromSuperview];
                                                               [bannerImageView setImage:image];
                                                           });
                                                       } else {
                                                       }
                                                   }];

        view = bannerImageView;
    }
    
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(1146 / 3), CGHeightFromIP6P(1586 / 3))];
    [shadowView.layer setShadowPath:[UIBezierPath bezierPathWithRect:view.bounds].CGPath];
    [shadowView.layer setShadowColor:[UIColor blackColor].CGColor];
    [shadowView.layer setShadowOffset:CGSizeMake(2, 2)];
    [shadowView.layer setShadowRadius:14];
    [shadowView.layer setShadowOpacity:0.6];
    [shadowView setBackgroundColor:[UIColor clearColor]];
    [shadowView setClipsToBounds:NO];
    [shadowView addSubview:view];
    
    view = shadowView;
    
    return view;
}

- (void)touchCardBanner:(UITapGestureRecognizer *)gesture
{
    [MSAnalytics trackEvent:@"Banner clicked" withProperties:@{
                                                               @"PageId" : @"CycleInfoDaily",
                                                               @"BannerNo" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][gesture.view.tag][@"BannerNo"] intValue]],
                                                               @"BannerPublishGroupCd" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][gesture.view.tag][@"BannerPublishGroupCd"] intValue]],
                                                               @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                               @"All" : [NSString stringWithFormat:@"%@|%@|%@|%@",
                                                                         @"CycleInfoDaily",
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][gesture.view.tag][@"BannerNo"] intValue]],
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][gesture.view.tag][@"BannerPublishGroupCd"] intValue]],
                                                                         [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                         ]
                                                               }];

    if ([_dataDictionary[@"Banner"][gesture.view.tag][@"MoveType"] intValue] == 101) {
        [(RootViewController *)_pClass changeToEventViewController];
        EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_dataDictionary[@"Banner"][gesture.view.tag][@"IndexNo"]];
        UINavigationController *eventNavigationController = (UINavigationController *)[[(RootViewController *)_pClass drawerViewController] centerViewController];
        [eventNavigationController pushViewController:eventDetailViewController animated:YES];

//        EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_dataDictionary[@"Banner"][gesture.view.tag][@"IndexNo"]];
//        [self.navigationController pushViewController:eventDetailViewController animated:YES];
    } else if ([_dataDictionary[@"Banner"][gesture.view.tag][@"MoveType"] intValue] == 102) {
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][gesture.view.tag][@"MoveType"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] options:@{} completionHandler:^(BOOL success) {
                exit(0);
            }];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][gesture.view.tag][@"MoveType"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
        }
    }
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing) {
        return value * 1.28;
    } else if (option == iCarouselOptionWrap) {
        return NO;
    } else if (option == iCarouselOptionVisibleItems) {
        if (carousel.currentItemIndex == 0 || (carousel.currentItemIndex == carousel.numberOfItems - 1)) {
            return 2;
        } else {
            return 3;
        }
    }
    return value;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    [_cardPageControl setCurrentPage:carousel.currentItemIndex];
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
    [_cardPageControl setCurrentPage:carousel.currentItemIndex];
}


@end
