//
//  NoticeDetailViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 5..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "DataSingleton.h"
#import "CNSTextAttachment.h"

@interface NoticeDetailViewController : UIViewController

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) NSString *boardNo;

@property (strong, nonatomic) UIView *contentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass boardNo:(NSString *)boardNo;

@end
