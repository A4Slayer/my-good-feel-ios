//
//  ConnectSensorDoneViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 11. 22..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"
#import <ESFramework/ESFramework.h>

@interface ConnectSensorDoneViewController : UIViewController

@property (strong, nonatomic) id pClass;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
