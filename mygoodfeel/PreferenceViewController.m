//
//  PreferenceViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 10. 12..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "PreferenceViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface PreferenceViewController ()

@end

@implementation PreferenceViewController

- (void)logout
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SIGN_OUT]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
    
}
    

- (void)loadAppConfig
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CONFIG_GET_INFO]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            
            [self->_dayMarkSwitch setOn:NO];
            [self->_dayNotificationSwitch setOn:NO];
            [self->_weightNotificationSwitch setOn:NO];
            [self->_eventNotificationSwitch setOn:NO];


            if ([responseData[@"data"][@"ExpectedDateReminderYN"] isEqualToString:@"Y"]) {
                [self->_dayMarkSwitch setOn:YES];
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"day_mark"];
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"day_mark"];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if ([responseData[@"data"][@"BeforeCyclePushYN"] isEqualToString:@"Y"]) {
                [self->_dayNotificationSwitch setOn:YES];
            }
            if ([responseData[@"data"][@"WeightPushYN"] isEqualToString:@"Y"]) {
                [self->_weightNotificationSwitch setOn:YES];
            }
            if ([responseData[@"data"][@"EventPushYN"] isEqualToString:@"Y"]) {
                [self->_eventNotificationSwitch setOn:YES];
            }
            
        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (void)changeConfig:(UISwitch *)configSwitch
{
    NSString *infoTypeString;
    
    if (configSwitch == _dayMarkSwitch) {
        infoTypeString = @"101";
    } else if (configSwitch == _dayNotificationSwitch) {
        infoTypeString = @"102";
    } else if (configSwitch == _eventNotificationSwitch) {
        infoTypeString = @"103";
    } else if (configSwitch == _weightNotificationSwitch) {
        infoTypeString = @"104";
    }
    
    NSString *infoValueString = @"N";
    
    if ([configSwitch isOn]) {
        infoValueString = @"Y";
    }
    
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CONFIG_SET_INFO]];
    
    [urlString appendFormat:@"/%@", infoTypeString];
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@", infoValueString];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        if ([responseData[@"code"] intValue] == 0) {
            if (configSwitch == self->_dayMarkSwitch) {

                [self loadAppConfig];

            } else if (configSwitch == self->_weightNotificationSwitch) {
                if ([self->_weightNotificationSwitch isOn]) {
                    [[DataSingleton sharedSingletonClass] registWeightLocalNotification];
                } else {
                    [[DataSingleton sharedSingletonClass] removeWeightLocalNotification];
                }
                
            }
        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
        
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xefeff4)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:@"설정"];
        [titleLabel setFont:SemiBoldWithSize(19)];
        [titleLabel setTextColor:UIColorFromRGB(0x000000)];
        [self.navigationItem setTitleView:titleLabel];
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu_black"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu_black"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];

        UIButton *cycleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_cycle"].size.width, [UIImage imageNamed:@"common_button_cycle"].size.height)];
        [cycleButton setImage:[UIImage imageNamed:@"common_button_cycle"] forState:UIControlStateNormal];
        [cycleButton addTarget:(RootViewController *)_pClass action:@selector(changeToCycleDayInfoViewController) forControlEvents:UIControlEventTouchUpInside];
        [cycleButton setAlpha:0];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cycleButton]];
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - kTOP_HEIGHT) style:UITableViewStyleGrouped];
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setTableFooterView:[[UIView alloc] init]];
        [self.view addSubview:_mainTableView];
        
        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"Setting",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"Setting",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];

        [self loadAppConfig];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadAppConfig) name:@"REFRESH_AFTER_LOGIN" object:nil];

    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            ChangeStepAgeViewController *changeStepAgeViewController = [[ChangeStepAgeViewController alloc] initWithNibName:nil bundle:nil pClass:self];
            
            UINavigationController *changeStepLastAgeNavigationController = [[UINavigationController alloc] initWithRootViewController:changeStepAgeViewController];
            [changeStepLastAgeNavigationController.navigationBar setBackgroundImage:[UIImage new]
                                                                      forBarMetrics:UIBarMetricsDefault];
            changeStepLastAgeNavigationController.navigationBar.shadowImage = [UIImage new];
            changeStepLastAgeNavigationController.navigationBar.translucent = YES;
            
            [self presentViewController:changeStepLastAgeNavigationController animated:YES completion:^{
            }];

        } else if (indexPath.row == 2) {
            PasswordViewController *passwordViewController = [[PasswordViewController alloc] initWithNibName:nil bundle:nil pClass:self];
            [self.navigationController pushViewController:passwordViewController animated:YES];
        }
    } else if (indexPath.section == 1) {
        PreferenceWeightViewController *preferenceWeightViewController = [[PreferenceWeightViewController alloc] initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:preferenceWeightViewController animated:YES];
    } else if (indexPath.section == 2) {
        PreferenceSensorViewController *preferenceSensorViewController = [[PreferenceSensorViewController alloc] initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:preferenceSensorViewController animated:YES];

//        ConnectSensorPreviousViewController *connectSensorPreviousViewController = [[ConnectSensorPreviousViewController alloc] initWithNibName:nil bundle:nil pClass:self];
//        [self.navigationController pushViewController:connectSensorPreviousViewController animated:YES];
        // 센서 설정
    } else if (indexPath.section == 3) {
        if (indexPath.row == 1) {
            AgreementViewController *agreementViewController = [[AgreementViewController alloc] initWithNibName:nil bundle:nil pClass:self];
            [self.navigationController pushViewController:agreementViewController animated:YES];
        } else if (indexPath.row == 2) {
            MarketingAgreementViewController *marketingAgreementViewController = [[MarketingAgreementViewController alloc] initWithNibName:nil bundle:nil pClass:self];
            [self.navigationController pushViewController:marketingAgreementViewController animated:YES];
        } else if (indexPath.row == 3) {
            // 사용자 정보
            PreferenceUserInfoViewController *preferenceUserInfoViewController = [[PreferenceUserInfoViewController alloc] initWithNibName:nil bundle:nil];
            [self.navigationController pushViewController:preferenceUserInfoViewController animated:YES];
        } else if (indexPath.row == 4) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:ykdxdt.ecom@gmail.com"]];
        }
    }
    
    
//    else if (indexPath.section == 3) {
//        [self logout];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userid"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//
//        [(RootViewController *)_pClass attachSignViewControllerFromLogout];
//    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

#pragma mark -
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 44;
        } else if (indexPath.row == 1) {
            return 60;
        } else if (indexPath.row == 2) {
            return 44;
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 60;
        } else if (indexPath.row == 1) {
            return 44;
        } else if (indexPath.row == 2) {
            return 60;
        }
    } else if (indexPath.section == 2) {
        if (IS_HIDE_SLEEP_PART) {
            return 0;
        } else {
            return 44;
        }
    } else if (indexPath.section == 3) {
        return 44;
    }
    
    
//    else if (indexPath.section == 3) {
//        return 44;
//    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        if (IS_HIDE_SLEEP_PART) {
            return 0;
        }
    }
    return 38;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *rtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 38)];
    [rtnView setBackgroundColor:[UIColor clearColor]];

    if (section < 4) {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 15, 200, 13)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextColor:UIColorFromRGB(0x6d6d72)];
        [titleLabel setFont:[UIFont systemFontOfSize:12]];
        
        if (section == 0) {
            [titleLabel setText:@"개인설정"];
        } else if (section == 1) {
            [titleLabel setText:@"알림"];
        } else if (section == 2) {
            [titleLabel setText:@"수면 센서 설정"];
        } else if (section == 3) {
            [titleLabel setText:@"고객 지원"];
        }
        
        [rtnView addSubview:titleLabel];
    } else {
        [rtnView setFrame:CGRectMake(0, 0, 0, 0)];
    }
    
    return rtnView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 3;
    } else if (section == 1) {
        return 3;
    } else if (section == 2) {
        return 1;
    } else if (section == 3) {
        return 5;
    }
    
    
//    else if (section == 3) {
//        return 1;
//    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [NSString stringWithFormat:@"Cell_%ld_%ld", (long)indexPath.section, (long)indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell setClipsToBounds:YES];
        
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"사전 주기 정보 수정"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];

                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            } else if (indexPath.row == 1) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 11, CGWidthFromIP6P(300), 19)];
                [titleLabel setText:@"예상 날짜 표시"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];

                UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, CGWidthFromIP6P(300), 23)];
                [descLabel setText:@"예상 생리 기간을 화면에 표시합니다."];
                [descLabel setTextColor:[UIColor lightGrayColor]];
                [descLabel setFont:[UIFont systemFontOfSize:14]];
                [cell.contentView addSubview:descLabel];

                if (_dayMarkSwitch) {
                    [_dayMarkSwitch removeFromSuperview];
                    _dayMarkSwitch = nil;
                }

                _dayMarkSwitch = [[UISwitch alloc] init];
                [_dayMarkSwitch setCenter:CGPointMake(kSCREEN_WIDTH - _dayMarkSwitch.frame.size.width / 2 - CGWidthFromIP6P(18), 60 / 2)];
                [_dayMarkSwitch addTarget:self action:@selector(changeConfig:) forControlEvents:UIControlEventValueChanged];
                [cell.contentView addSubview:_dayMarkSwitch];
                
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            } else if (indexPath.row == 2) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"앱 잠금"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];

                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
        } else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 11, CGWidthFromIP6P(300), 19)];
                [titleLabel setText:@"예상 생리 주기 알림"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];
                
                UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, CGWidthFromIP6P(300), 23)];
                [descLabel setText:@"다음 예상 주기 시작 5일 전 19:00에 알립니다."];
                [descLabel setTextColor:[UIColor lightGrayColor]];
                [descLabel setFont:[UIFont systemFontOfSize:14]];
                [cell.contentView addSubview:descLabel];

                if (_dayNotificationSwitch) {
                    [_dayNotificationSwitch removeFromSuperview];
                    _dayNotificationSwitch = nil;
                }
                
                _dayNotificationSwitch = [[UISwitch alloc] init];
                [_dayNotificationSwitch setCenter:CGPointMake(kSCREEN_WIDTH - _dayNotificationSwitch.frame.size.width / 2 - CGWidthFromIP6P(18), 60 / 2)];
                [_dayNotificationSwitch addTarget:self action:@selector(changeConfig:) forControlEvents:UIControlEventValueChanged];
                [cell.contentView addSubview:_dayNotificationSwitch];

                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            } else if (indexPath.row == 1) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"몸무게 재기 알림 설정"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

//                UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, CGWidthFromIP6P(300), 23)];
//                [descLabel setText:@"매일 07:05에 몸무게를 재도록 알립니다."];
//                [descLabel setTextColor:[UIColor lightGrayColor]];
//                [descLabel setFont:[UIFont systemFontOfSize:14]];
//                [cell.contentView addSubview:descLabel];
//
//                if (_weightNotificationSwitch) {
//                    [_weightNotificationSwitch removeFromSuperview];
//                    _weightNotificationSwitch = nil;
//                }
//
//                _weightNotificationSwitch = [[UISwitch alloc] init];
//                [_weightNotificationSwitch setCenter:CGPointMake(kSCREEN_WIDTH - _weightNotificationSwitch.frame.size.width / 2 - CGWidthFromIP6P(18), 60 / 2)];
//                [_weightNotificationSwitch addTarget:self action:@selector(changeConfig:) forControlEvents:UIControlEventValueChanged];
//                [cell.contentView addSubview:_weightNotificationSwitch];
//
//                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            } else if (indexPath.row == 2) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 11, CGWidthFromIP6P(300), 19)];
                [titleLabel setText:@"마케팅 정보 알림"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];
                
                UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, CGWidthFromIP6P(300), 23)];
                [descLabel setText:@"마케팅 정보 알림을 받습니다."];
                [descLabel setTextColor:[UIColor lightGrayColor]];
                [descLabel setFont:[UIFont systemFontOfSize:14]];
                [cell.contentView addSubview:descLabel];

                if (_eventNotificationSwitch) {
                    [_eventNotificationSwitch removeFromSuperview];
                    _eventNotificationSwitch = nil;
                }

                _eventNotificationSwitch = [[UISwitch alloc] init];
                [_eventNotificationSwitch setCenter:CGPointMake(kSCREEN_WIDTH - _eventNotificationSwitch.frame.size.width / 2 - CGWidthFromIP6P(18), 60 / 2)];
                [_eventNotificationSwitch addTarget:self action:@selector(changeConfig:) forControlEvents:UIControlEventValueChanged];
                [cell.contentView addSubview:_eventNotificationSwitch];

                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
        } else if (indexPath.section == 2) {
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
            [titleLabel setText:@"센서 설정"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [cell.contentView addSubview:titleLabel];
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        } else if (indexPath.section == 3) {
            if (indexPath.row == 0) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"앱 버전 정보"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];
                
                
                UILabel *valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - 100 - 16, 0, 100, 44)];
                [valueLabel setText:[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]];
                [valueLabel setFont:[UIFont systemFontOfSize:15]];
                [valueLabel setTextColor:UIColorFromRGB(0x8e8e93)];
                [valueLabel setTextAlignment:NSTextAlignmentRight];
                [cell.contentView addSubview:valueLabel];

                
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            } else if (indexPath.row == 1) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"MY좋은느낌 이용약관"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];
                
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            } else if (indexPath.row == 2) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"MY좋은느낌 개인정보 처리 방침"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];
                
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            } else if (indexPath.row == 3) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"사용자 정보"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];

                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            } else if (indexPath.row == 4) {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGWidthFromIP6P(300), 44)];
                [titleLabel setText:@"이메일 문의"];
                [titleLabel setTextColor:[UIColor blackColor]];
                [cell.contentView addSubview:titleLabel];
            }
        }
        
//        else if (indexPath.section == 3) {
//            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 44)];
//            [titleLabel setText:@"로그아웃"];
//            [titleLabel setTextColor:[UIColor redColor]];
//            [titleLabel setTextAlignment:NSTextAlignmentCenter];
//            [cell.contentView addSubview:titleLabel];
//        }
    }
    
    return cell;
}

//- (void)viewDidLayoutSubviews
//{
//    [super viewDidLayoutSubviews];
//
//    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
//        [_tableView setSeparatorInset:UIEdgeInsetsZero];
//    }
//
//    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
//        [_tableView setLayoutMargins:UIEdgeInsetsZero];
//    }
//}
//
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Remove seperator inset
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//
//    // Prevent the cell from inheriting the Table View's margin settings
//    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
//        [cell setPreservesSuperviewLayoutMargins:NO];
//    }
//
//    // Explictly set your cell's layout margins
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

@end
