//
//  CUITextField.h
//  amorelecture
//
//  Created by Lim daewang on 2014. 4. 23..
//  Copyright (c) 2014년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CUITextField : UITextField

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

- (id)initWithFrame:(CGRect)frame withEdgeInsets:(UIEdgeInsets)withEdgeInsets;
- (id)initWithCoder:(NSCoder *)aDecoder withEdgeInsets:(UIEdgeInsets)withEdgeInsets;

@end
