//
//  CycleDayInfoViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 18..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CUITextField.h"
#import "iCarousel.h"
#import "CycleDayInfoGuideViewController.h"
#import "CycleDiaryViewController.h"

@interface CycleDayInfoViewController : UIViewController <iCarouselDelegate, iCarouselDataSource>

@property (strong, nonatomic) id pClass;
@property (strong, nonatomic) iCarousel *cardViewCarousel;

@property (strong, nonatomic) UIButton *dateButton;
@property (strong, nonatomic) UIButton *blackMaskButton;
@property (strong, nonatomic) UIDatePicker *datePicker;

@property (strong, nonatomic) NSString *dateString;

@property (strong, nonatomic) NSDictionary *dataDictionary;
@property (strong, nonatomic) UIPageControl *cardPageControl;

- (void)getCycleDailyInfo;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass dateString:(NSString *)dateString;

@end
