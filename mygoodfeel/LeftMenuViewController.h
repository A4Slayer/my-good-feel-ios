//
//  LeftMenuViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 24..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuViewController : UIViewController

@property (strong, nonatomic) id pClass;

@property (strong, nonatomic) UIView *topView;

@property (strong, nonatomic) UIButton *notificationButton;

@property (strong, nonatomic) UIButton *cycleInfoButton;
@property (strong, nonatomic) UIView *cycleUnderlineView;

@property (strong, nonatomic) UIButton *sleepInfoButton;
@property (strong, nonatomic) UIView *sleepUnderlineView;

@property (strong, nonatomic) UIButton *dailyReportButton;
@property (strong, nonatomic) UIView *dailyUnderlineView;

@property (strong, nonatomic) UIButton *menstruationingButton;
@property (strong, nonatomic) UIView *menstruationingUnderlineView;

@property (strong, nonatomic) UIButton *eventButton;
@property (strong, nonatomic) UIView *eventUnderlineView;

@property (strong, nonatomic) UIButton *momQButton;
@property (strong, nonatomic) UIView *momQUnderlineView;

@property (strong, nonatomic) UIButton *noticeButton;
@property (strong, nonatomic) UIView *noticeUnderlineView;


@property (strong, nonatomic) UIButton *preferenceButton;
@property (strong, nonatomic) UIView *preferenceUnderlineView;

@property (strong, nonatomic) UILabel *menstruationLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass;

@end
