//
//  NoticeTableViewCell.h
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 5..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeTableViewCell : UITableViewCell

@property (strong, nonatomic) UIImageView *nImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier dataDictionary:(NSDictionary *)dataDictionary;
- (void)hideNewImage;

@end
