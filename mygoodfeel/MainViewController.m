//
//  MainViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 8. 24..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import "MainViewController.h"
#import "RootViewController.h"

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

@interface MainViewController ()

@end

@implementation MainViewController

- (void)bannerRollingThread
{
    if (self) {
        if (_bannerScrollView.contentSize.width > _bannerScrollView.frame.size.width) {
            if (_bannerScrollView.contentOffset.x < _bannerScrollView.contentSize.width - _bannerScrollView.frame.size.width) {
                [_bannerScrollView setContentOffset:CGPointMake(_bannerScrollView.contentOffset.x + _bannerScrollView.frame.size.width, _bannerScrollView.contentOffset.y) animated:YES];
            } else {
                [_bannerScrollView setContentOffset:CGPointMake(0, _bannerScrollView.contentOffset.y) animated:YES];
            }
            
        }
        [self performSelector:@selector(bannerRollingThread) withObject:nil afterDelay:8.0f];
    }
}

- (void)touchBanner:(UIButton *)button
{
    [MSAnalytics trackEvent:@"Banner clicked" withProperties:@{
                                                               @"PageId" : @"CycleInfoMonthly",
                                                               @"BannerNo" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerNo"] intValue]],
                                                               @"BannerPublishGroupCd" : [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerPublishGroupCd"] intValue]],
                                                               @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                               @"All" : [NSString stringWithFormat:@"%@|%@|%@|%@",
                                                                         @"CycleInfoMonthly",
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerNo"] intValue]],
                                                                         [NSString stringWithFormat:@"%d", [_dataDictionary[@"Banner"][button.tag][@"BannerPublishGroupCd"] intValue]],
                                                                         [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                         ]
                                                               }];

    if ([_dataDictionary[@"Banner"][button.tag][@"MoveType"] intValue] == 101) {
        [(RootViewController *)_pClass changeToEventViewController];
        EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:_dataDictionary[@"Banner"][button.tag][@"IndexNo"]];
        UINavigationController *eventNavigationController = (UINavigationController *)[[(RootViewController *)_pClass drawerViewController] centerViewController];
        [eventNavigationController pushViewController:eventDetailViewController animated:YES];
    } else if ([_dataDictionary[@"Banner"][button.tag][@"MoveType"] intValue] == 102) {
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][button.tag][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] options:@{} completionHandler:^(BOOL success) {
                exit(0);
            }];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][button.tag][@"MoveUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
        }
    }
}

- (void)dayInfoWithDate:(UIButton *)button
{
    NSString *dateString = [NSString stringWithFormat:@"%ld", (long)button.tag];
    dateString = [NSString stringWithFormat:@"%@-%@-%@", [dateString substringWithRange:NSMakeRange(0, 4)], [dateString substringWithRange:NSMakeRange(4, 2)], [dateString substringWithRange:NSMakeRange(6, 2)]];
    
    [(RootViewController *)_pClass changeToCycleDayInfoViewController:dateString];
}

- (void)getMonthlyCycleDiary
{
    NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CYCLEDIARY_GET_MONTH]];
    
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
    [urlString appendFormat:@"/%@-%02lu-01", _yearLabel.text, (long)(_monthScrollView.contentOffset.x / _monthScrollView.frame.size.width) % 12 + 1];
    [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserStatus"]];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
    [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
    
    
    [[DataSingleton sharedSingletonClass] attachLoadingView];

    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseData;
        NSError *error = nil;
        if (responseObject != nil) {
            responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        }
        
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        
        [self->_datePickerView selectRow:(long)(self->_monthScrollView.contentOffset.x / self->_monthScrollView.frame.size.width / 12) inComponent:0 animated:NO];
        [self->_datePickerView selectRow:(long)(self->_monthScrollView.contentOffset.x / self->_monthScrollView.frame.size.width) % 12 inComponent:1 animated:NO];
        
        
        self->_dataDictionary = responseData[@"data"];
        [self drawCalendar];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[DataSingleton sharedSingletonClass] detachLoadingView];
        [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
    }];
}

- (void)drawCalendar
{
    float itemWidth = (kSCREEN_WIDTH - (k2PX * 6)) / 7;
    float itemHeight = (CGHeightFromIP6P(421 - 43) - (k2PX * 7)) / 6;
    
    [_beforeCycleValueLabel setText:[NSString stringWithFormat:@"%@일", _dataDictionary[@"MenstrualCycle"][@"BeforeMenstrualCycle"]]];
    [_currentCycleValueLabel setText:[NSString stringWithFormat:@"%@일", _dataDictionary[@"MenstrualCycle"][@"CurrentMenstrualCycle"]]];
    [_afterCycleValueLabel setText:[NSString stringWithFormat:@"%@일", _dataDictionary[@"MenstrualCycle"][@"NextMenstrualCycle"]]];
    
    [_beforePeriodValueLabel setText:[NSString stringWithFormat:@"%@일", _dataDictionary[@"MenstrualPeriod"][@"BeforeMenstrualPeriod"]]];
    [_currentPeriodValueLabel setText:[NSString stringWithFormat:@"%@일", _dataDictionary[@"MenstrualPeriod"][@"CurrentMenstrualPeriod"]]];
    [_afterPeriodValueLabel setText:[NSString stringWithFormat:@"%@일", _dataDictionary[@"MenstrualPeriod"][@"NextMenstrualPeriod"]]];

    
    for (UIButton *bannerButton in [_bannerScrollView subviews]) {
        if ([bannerButton isKindOfClass:[UIButton class]]) {
            [bannerButton removeFromSuperview];
        }
    }
    
    for (int i = 0; i < [_dataDictionary[@"Banner"] count]; i++) {
        UIButton *bannerButton = [[UIButton alloc] initWithFrame:CGRectMake(_bannerScrollView.frame.size.width * i, 0, _bannerScrollView.frame.size.width, _bannerScrollView.frame.size.height)];
        [bannerButton setTag:i];
        [bannerButton addTarget:self action:@selector(touchBanner:) forControlEvents:UIControlEventTouchUpInside];
        [[AKSDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[_dataDictionary[@"Banner"][i][@"BannerPath"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                                     options:0
                                                    progress:nil
                                                   completed:^(UIImage *image, NSError *error, AKSDImageCacheType cacheType, BOOL finished) {
                                                       if (!error) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [bannerButton setImage:image forState:UIControlStateNormal];
                                                           });
                                                       } else {
                                                       }
                                                   }];

        [_bannerScrollView addSubview:bannerButton];
    }
    [_bannerScrollView setContentSize:CGSizeMake(_bannerScrollView.frame.size.width * [_dataDictionary[@"Banner"] count], _bannerScrollView.frame.size.height)];

    
    int arrayIndex = 0;

    for (UIView *sView in [_calendarView subviews]) {
        [sView removeFromSuperview];
    }
    
    
    NSArray *weekArray = @[ @"일", @"월", @"화", @"수", @"목", @"금", @"토" ];
    
    for (int i = 0; i < 7; i++) {
        UILabel *weekLabel = [[UILabel alloc] initWithFrame:CGRectMake(itemWidth * i + k2PX * i, 0, itemWidth, CGHeightFromIP6P(43))];
        [weekLabel setBackgroundColor:[UIColor clearColor]];
        [weekLabel setTextAlignment:NSTextAlignmentCenter];
        [weekLabel setText:weekArray[i]];
        [weekLabel setTextColor:UIColorFromRGB(0xaeb0b2)];
        [weekLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(12))];
        [_calendarView addSubview:weekLabel];
    }
    
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 7; j++) {
            
            UIButton *dayItemButton = [[UIButton alloc] initWithFrame:CGRectMake(itemWidth * j + k2PX * j, CGHeightFromIP6P(43) + itemHeight * i + k2PX * i + k2PX, itemWidth, itemHeight)];
            [dayItemButton setTag:[[_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"CycleInfoDt"] stringByReplacingOccurrencesOfString:@"-" withString:@""] intValue]];
            [dayItemButton setBackgroundImage:[DataSingleton imageFromColor:[UIColor whiteColor]] forState:UIControlStateNormal];
            [dayItemButton addTarget:self action:@selector(dayInfoWithDate:) forControlEvents:UIControlEventTouchUpInside];
            [_calendarView addSubview:dayItemButton];
            
            if ([_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"OvulationDayYN"] isEqualToString:@"Y"]) {
                [dayItemButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xf0e3f6)] forState:UIControlStateNormal];
            }
            
            if (![_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"MenstrualDayStatus"] isEqualToString:@"N"]) {
                if ([_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"MenstrualDayStatus"] isEqualToString:@"S"]) {
                    [dayItemButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffb6b8)] forState:UIControlStateNormal];
                } else if ([_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"MenstrualDayStatus"] isEqualToString:@"Y"]) {
                    [dayItemButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffccce)] forState:UIControlStateNormal];
                } else if ([_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"MenstrualDayStatus"] isEqualToString:@"A"]) {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"day_mark"] intValue] == 1) {
                        [dayItemButton setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffe3e4)] forState:UIControlStateNormal];
                    }
                }
                
                UIImageView *menstrualIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cyclecalendar_image_icon_period_none"]];
                [menstrualIconImageView setFrame:CGRectMake(5, 5, menstrualIconImageView.frame.size.width, menstrualIconImageView.frame.size.height)];
                if ([_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"MenstrualCd"] intValue] == 102) {
                    [menstrualIconImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_period_normal"]];
                } else if ([_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"MenstrualCd"] intValue] == 103) {
                    [menstrualIconImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_period_much"]];
                } else if ([_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"MenstrualCd"] intValue] == 104) {
                    [menstrualIconImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_icon_period_low"]];
                }
                [dayItemButton addSubview:menstrualIconImageView];
                
            }
            
            
            UILabel *dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            [dayLabel setBackgroundColor:[UIColor clearColor]];
            [dayLabel setTextAlignment:NSTextAlignmentRight];
            [dayLabel setText:[NSString stringWithFormat:@"%d", [[_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"CycleInfoDt"] substringWithRange:NSMakeRange(8, 2)] intValue]]];
            [dayLabel setTextColor:UIColorFromRGB(0x747474)];
            
            if ((long)(_monthScrollView.contentOffset.x / _monthScrollView.frame.size.width) % 12 + 1 != [_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"MonthNm"] intValue]) {
                [dayLabel setTextColor:UIColorFromRGB(0xd9d9d9)];
            }
            
            [dayLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(13))];
            [dayLabel sizeToFit];
            [dayLabel setFrame:CGRectMake(5, 5, dayItemButton.frame.size.width - 5 - 5, dayLabel.frame.size.height)];
            [dayItemButton addSubview:dayLabel];
            
            
            float bottomIconX = 5;
            
            if ([_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"SexYN"] isEqualToString:@"Y"]) {
                UIImageView *sexIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cyclecalendar_image_icon_period_love"]];
                [sexIconImageView setFrame:CGRectMake(bottomIconX, dayItemButton.frame.size.height - [UIImage imageNamed:@"cyclecalendar_image_icon_period_love"].size.height - 5, sexIconImageView.frame.size.width, sexIconImageView.frame.size.height)];
                [dayItemButton addSubview:sexIconImageView];
                bottomIconX = sexIconImageView.frame.origin.x + sexIconImageView.frame.size.width + 5;
            }
            
            if ([_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"FeverYN"] isEqualToString:@"Y"]) {
                UIImageView *pyrexiaIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cyclecalendar_image_icon_period_pyrexia"]];
                [pyrexiaIconImageView setFrame:CGRectMake(bottomIconX, dayItemButton.frame.size.height - [UIImage imageNamed:@"cyclecalendar_image_icon_period_pyrexia"].size.height - 5, pyrexiaIconImageView.frame.size.width, pyrexiaIconImageView.frame.size.height)];
                [dayItemButton addSubview:pyrexiaIconImageView];
                bottomIconX = pyrexiaIconImageView.frame.origin.x + pyrexiaIconImageView.frame.size.width + 5;
            }


            NSDateFormatter *currentDateFormatter = [[NSDateFormatter alloc] init];
            [currentDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            [currentDateFormatter setDateFormat:@"yyyy-MM-dd"];
            
            if ([[currentDateFormatter stringFromDate:[NSDate date]] isEqualToString:_dataDictionary[@"MonthlyCycleDiary"][arrayIndex][@"CycleInfoDt"]]) {
                [dayItemButton.layer setBorderWidth:3];
                [dayItemButton.layer setBorderColor:UIColorFromRGB(0xd6d6d6).CGColor];
            }
            
            arrayIndex++;
        }
    }

    for (int i = 0; i < 7; i++) {
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, CGHeightFromIP6P(43) + i * itemHeight + (k2PX * i), kSCREEN_WIDTH, k2PX)];
        [separatorView setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        [_calendarView addSubview:separatorView];
    }
    
    for (int i = 0; i < 6; i++) {
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(itemWidth * (i + 1) + (k2PX * i), CGHeightFromIP6P(43), k2PX, CGHeightFromIP6P(1134 / 3))];
        [separatorView setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        [_calendarView addSubview:separatorView];
    }
    

}

- (void)presentCycleCalendarGuide
{
    CycleCalendarGuideViewController *cycleCalendarGuideViewController = [[CycleCalendarGuideViewController alloc] initWithNibName:nil bundle:nil pClass:self];
    UINavigationController *cycleCalendarGuideNavigationController = [[UINavigationController alloc] initWithRootViewController:cycleCalendarGuideViewController];
    [cycleCalendarGuideNavigationController.navigationBar setTranslucent:NO];
    [cycleCalendarGuideNavigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                              forBarPosition:UIBarPositionAny
                                                                  barMetrics:UIBarMetricsDefault];
    [cycleCalendarGuideNavigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [cycleCalendarGuideNavigationController.navigationBar setTitleTextAttributes:@{
                                                                                   NSFontAttributeName: SemiBoldWithSize(18),
                                                                                   NSForegroundColorAttributeName: UIColorFromRGB(0xf05971)
                                                                                   }];
    [cycleCalendarGuideNavigationController.navigationBar setBackgroundImage:[DataSingleton imageFromColor:UIColorFromRGB(0xffeff1)] forBarMetrics:UIBarMetricsDefault];

    [self.navigationController presentViewController:cycleCalendarGuideNavigationController animated:YES completion:^{
        //
    }];
}

- (void)becomePicker
{
    if (![_blackMaskView superview]) {
        [self.navigationController.view addSubview:_blackMaskView];
        [self.navigationController.view addSubview:_dateSelectView];
    }
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self->_blackMaskView setAlpha:0.7];
        [self->_dateSelectView setFrame:CGRectMake(0, kSCREEN_HEIGHT - 215 - 44, kSCREEN_WIDTH, 215 + 44)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)resignPicker
{
    [_monthScrollView setContentOffset:CGPointMake((_monthScrollView.frame.size.width * 12 * [_datePickerView selectedRowInComponent:0]) + _monthScrollView.frame.size.width * [_datePickerView selectedRowInComponent:1], 0) animated:YES];
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self->_blackMaskView setAlpha:0];
        [self->_dateSelectView setFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 215 + 44)];
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)prevMonth
{
    if ((int)_monthScrollView.contentOffset.x % (int)_monthScrollView.frame.size.width == 0) {
        if ((int)_monthScrollView.contentOffset.x > 0) {
            [_monthScrollView setContentOffset:CGPointMake((int)_monthScrollView.contentOffset.x - (int)_monthScrollView.frame.size.width, (int)_monthScrollView.contentOffset.y) animated:YES];
        }
    }
}

- (void)nextMonth
{
    if ((int)_monthScrollView.contentOffset.x % (int)_monthScrollView.frame.size.width == 0) {
        if ((int)_monthScrollView.contentOffset.x < (int)_monthScrollView.contentSize.width - (int)_monthScrollView.frame.size.width) {
            [_monthScrollView setContentOffset:CGPointMake((int)_monthScrollView.contentOffset.x + (int)_monthScrollView.frame.size.width, (int)_monthScrollView.contentOffset.y) animated:YES];
        }
    }
}

- (void)removeGuideView:(UIButton *)guideButton
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [guideButton setAlpha:0];
    } completion:^(BOOL finished) {
        [guideButton removeFromSuperview];
    }];
    
}

- (void)updateTermInfo
{
    if ([[DataSingleton sharedSingletonClass] needUpdateTerm]) {
        [[DataSingleton sharedSingletonClass] setNeedUpdateTerm:NO];
        
        NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_SET_TERM_INFO]];
        
        [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserId"]];           // ID
        [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] termAgreeStr]];           // ID
        [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] personalDataAgreeStr]];           // ID
        [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] marketingAgreeStr]];           // ID
        

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
        [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
        
        [[DataSingleton sharedSingletonClass] attachLoadingView];
        
        [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *responseData;
            NSError *error = nil;
            if (responseObject != nil) {
                responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                               options:NSJSONReadingMutableContainers
                                                                 error:&error];
            }
            
            [[DataSingleton sharedSingletonClass] detachLoadingView];
            
            switch ([responseData[@"code"] intValue]) {
                case 0:
                    break;
                    
                default:
                    [[[UIAlertView alloc] initWithTitle:kALERT_NETWORK_FAIL message:responseData[@"msg"][@"ko"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                    break;
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [[DataSingleton sharedSingletonClass] detachLoadingView];
        }];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass
{
    return [self initWithNibName:nibNameOrNil bundle:nibBundleOrNil pClass:pClass date:[NSDate date]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass date:(NSDate *)date
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _pClass = pClass;
        [self.view setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        [self updateTermInfo];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.view.bounds;
        gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [self.view.layer insertSublayer:gradient atIndex:0];


        NSMutableAttributedString *attrString;
        CNSTextAttachment *imageAttachment;
        
        UIButton *titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH / 2, kNAVIGATIONBAR_HEIGHT)];
        [titleButton setBackgroundColor:[UIColor clearColor]];
        [titleButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleButton addTarget:self action:@selector(presentCycleCalendarGuide) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setTitleView:titleButton];
        
        if (@available(iOS 9.0, *)) {
            [titleButton setTitle:@"주기 캘린더 " forState:UIControlStateNormal];
            [titleButton.titleLabel setFont:SemiBoldWithSize(19)];
            [titleButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
            [titleButton setImage:[UIImage imageNamed:@"cyclecalendar_button_qmark"] forState:UIControlStateNormal];
            [titleButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
            [titleButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 3.5 / 2, 0)];
        } else {
            attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"주기 캘린더 "]
                                                                                           attributes:@{ NSFontAttributeName : SemiBoldWithSize(19),
                                                                                                         NSForegroundColorAttributeName : [UIColor whiteColor] }];
            
            imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -3.5)];
            [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_button_qmark"]];
            [attrString appendAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
            
            [titleButton setAttributedTitle:attrString forState:UIControlStateNormal];
        }
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 44 / 2 - [UIImage imageNamed:@"common_button_menu"].size.width / 2)];
        [menuButton setImage:[UIImage imageNamed:@"common_button_menu"] forState:UIControlStateNormal];
        [menuButton addTarget:(RootViewController *)_pClass action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
        
        UIButton *cycleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:@"common_button_cycle"].size.width, [UIImage imageNamed:@"common_button_cycle"].size.height)];
        [cycleButton setImage:[UIImage imageNamed:@"common_button_cycle"] forState:UIControlStateNormal];
        [cycleButton addTarget:(RootViewController *)_pClass action:@selector(changeToCycleDayInfoViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cycleButton]];

        
        UIView *yearBGView = [[UIView alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, CGHeightFromIP6P(16))];
        [yearBGView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:yearBGView];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                       fromDate:date];

        _yearLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kTOP_HEIGHT, kSCREEN_WIDTH, CGHeightFromIP6P(16))];
        [_yearLabel setText:[NSString stringWithFormat:@"%ld", [components year]]];
        [_yearLabel setBackgroundColor:[UIColor clearColor]];
        [_yearLabel setTextColor:[UIColor whiteColor]];
        [_yearLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(17))];
        [_yearLabel setTextAlignment:NSTextAlignmentCenter];
        [self.view addSubview:_yearLabel];
        
        NSDateFormatter *monthDateFormatter = [[NSDateFormatter alloc] init];
        [monthDateFormatter setDateFormat:@"M"];
        
        
        _monthScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _yearLabel.frame.origin.y + _yearLabel.frame.size.height, (int)(kSCREEN_WIDTH / 2 - CGWidthFromIP6P(30)), CGHeightFromIP6P(71))];
        [_monthScrollView setDelegate:self];
        [_monthScrollView setClipsToBounds:NO];
        [_monthScrollView setCenter:CGPointMake(self.view.frame.size.width / 2, _monthScrollView.center.y)];
        [_monthScrollView setBackgroundColor:[UIColor clearColor]];
        [_monthScrollView setShowsVerticalScrollIndicator:NO];
        [_monthScrollView setShowsHorizontalScrollIndicator:NO];
        [_monthScrollView setPagingEnabled:YES];
        [_monthScrollView setContentSize:CGSizeMake(_monthScrollView.frame.size.width * 12 * 21, _monthScrollView.frame.size.height)];
        [self.view addSubview:_monthScrollView];
        
        UIButton *prevMonthButton = [[UIButton alloc] initWithFrame:CGRectMake(0, _yearLabel.frame.origin.y + _yearLabel.frame.size.height, kSCREEN_WIDTH / 4, _monthScrollView.frame.size.height)];
        [prevMonthButton setBackgroundColor:[UIColor clearColor]];
        [prevMonthButton addTarget:self action:@selector(prevMonth) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:prevMonthButton];
        
        UIButton *nextMonthButton = [[UIButton alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH / 4 * 3, _yearLabel.frame.origin.y + _yearLabel.frame.size.height, kSCREEN_WIDTH / 4, _monthScrollView.frame.size.height)];
        [nextMonthButton setBackgroundColor:[UIColor clearColor]];
        [nextMonthButton addTarget:self action:@selector(nextMonth) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextMonthButton];

        _monthItemArray = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < 12 * 21; i++) {
            
            UIView *monthBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(55), CGHeightFromIP6P(55))];
            [monthBGView setBackgroundColor:[UIColor whiteColor]];
            [monthBGView.layer setCornerRadius:monthBGView.frame.size.width / 2];
            [monthBGView setClipsToBounds:YES];
            [monthBGView setAlpha:0];
            [_monthScrollView addSubview:monthBGView];
            
            UIButton *monthButton = [[UIButton alloc] initWithFrame:CGRectMake(i * _monthScrollView.frame.size.width, 0, _monthScrollView.frame.size.width, CGHeightFromIP6P(70))];
            [monthButton.titleLabel setFont:MediumWithSize(CGHeightFromIP6P(20))];
            [monthButton setTitle:[NSString stringWithFormat:@"%d월", i % 12 + 1] forState:UIControlStateNormal];
            [monthButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [monthButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
            [monthButton setBackgroundColor:[UIColor clearColor]];
            [monthButton addTarget:self action:@selector(becomePicker) forControlEvents:UIControlEventTouchUpInside];
            [_monthScrollView addSubview:monthButton];
            
            [monthBGView setCenter:CGPointMake(monthButton.center.x, monthButton.center.y - 1)];
            
            [_monthItemArray addObject:@{
                                         @"view" : monthBGView,
                                         @"button" : monthButton
                                         }];
        }

        NSDate *currDate = [NSDate date];
        NSDateComponents *currComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                       fromDate:currDate];
        
        [_monthScrollView setContentOffset:CGPointMake(([[monthDateFormatter stringFromDate:date] intValue] + (12 * ([components year] + 10 - [currComponents year])) - 1) * _monthScrollView.frame.size.width, 0) animated:YES];

        
        gradient = [CAGradientLayer layer];
        gradient.frame = yearBGView.bounds;
        gradient.colors = @[(id)UIColorFromRGB(0xea979c).CGColor, (id)UIColorFromRGB(0xcda9d5).CGColor];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [yearBGView.layer insertSublayer:gradient atIndex:0];
        
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _monthScrollView.frame.origin.y + _monthScrollView.frame.size.height, kSCREEN_WIDTH, kSCREEN_HEIGHT - (_monthScrollView.frame.origin.y + _monthScrollView.frame.size.height))];
        [_mainScrollView setBackgroundColor:UIColorFromRGB(0xf1f1f5)];
        [self.view addSubview:_mainScrollView];
        
        
        _calendarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, CGHeightFromIP6P(1263 / 3))];
        [_calendarView setBackgroundColor:[UIColor whiteColor]];
        [_mainScrollView addSubview:_calendarView];

        
        UIView *iconGuideBGView = [[UIView alloc] initWithFrame:CGRectMake(0, _calendarView.frame.origin.y + _calendarView.frame.size.height, kSCREEN_WIDTH, CGHeightFromIP6P(130 / 3))];
        [iconGuideBGView setBackgroundColor:[UIColor whiteColor]];
        [_mainScrollView addSubview:iconGuideBGView];
        
        UILabel *cycleGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), CGHeightFromIP6P(25 / 3), CGWidthFromIP6P(192 / 3), CGHeightFromIP6P(81 / 3))];
        [cycleGuideLabel setBackgroundColor:UIColorFromRGB(0xffccce)];
        [cycleGuideLabel setTextColor:UIColorFromRGB(0x717171)];
        [cycleGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [cycleGuideLabel setText:@"생리 기간"];
        [cycleGuideLabel setFont:RegularWithSize(CGHeightFromIP6P(14))];
        [iconGuideBGView addSubview:cycleGuideLabel];
        
        UILabel *availablePregnancyGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(319 / 3), CGHeightFromIP6P(25 / 3), CGWidthFromIP6P(234 / 3), CGHeightFromIP6P(81 / 3))];
        [availablePregnancyGuideLabel setBackgroundColor:UIColorFromRGB(0xf0e3f6)];
        [availablePregnancyGuideLabel setTextColor:UIColorFromRGB(0x717171)];
        [availablePregnancyGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [availablePregnancyGuideLabel setText:@"예상 가임기"];
        [availablePregnancyGuideLabel setFont:RegularWithSize(CGHeightFromIP6P(14))];
        [iconGuideBGView addSubview:availablePregnancyGuideLabel];
        
        UILabel *pyrexiaGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(612 / 3), CGHeightFromIP6P(25 / 3), CGWidthFromIP6P(152 / 3), CGHeightFromIP6P(81 / 3))];
        [pyrexiaGuideLabel setBackgroundColor:[UIColor clearColor]];
        [pyrexiaGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [iconGuideBGView addSubview:pyrexiaGuideLabel];
        
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -1.0)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_pyrexia"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 발열"]
                                                                           attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(14)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x717171) }]];
        [pyrexiaGuideLabel setAttributedText:attrString];
        
        UILabel *loveGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(820 / 3), CGHeightFromIP6P(25 / 3), CGWidthFromIP6P(154 / 3), CGHeightFromIP6P(81 / 3))];
        [loveGuideLabel setBackgroundColor:[UIColor clearColor]];
        [loveGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [iconGuideBGView addSubview:loveGuideLabel];
        
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -1.0)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_love"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 사랑"]
                                                                           attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(14)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x717171) }]];
        [loveGuideLabel setAttributedText:attrString];
        [loveGuideLabel sizeToFit];
        [loveGuideLabel setFrame:CGRectMake(CGWidthFromIP6P(820 / 3), CGHeightFromIP6P(25 / 3), loveGuideLabel.frame.size.width, CGHeightFromIP6P(81 / 3))];

        UILabel *bloodGuideLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(1030 / 3), CGHeightFromIP6P(25 / 3), CGWidthFromIP6P(181 / 3), CGHeightFromIP6P(81 / 3))];
        [bloodGuideLabel setBackgroundColor:[UIColor clearColor]];
        [bloodGuideLabel setTextAlignment:NSTextAlignmentCenter];
        [iconGuideBGView addSubview:bloodGuideLabel];
        
        imageAttachment = [[CNSTextAttachment alloc] initWithMargin:CGPointMake(0, -1.0)];
        [imageAttachment setImage:[UIImage imageNamed:@"cyclecalendar_image_blood"]];
        attrString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" 생리 양"]
                                                                           attributes:@{ NSFontAttributeName : RegularWithSize(CGHeightFromIP6P(14)),
                                                                                         NSForegroundColorAttributeName : UIColorFromRGB(0x717171) }]];
        [bloodGuideLabel setAttributedText:attrString];

        _bannerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, iconGuideBGView.frame.origin.y + iconGuideBGView.frame.size.height, kSCREEN_WIDTH, CGHeightFromIP6P(260 / 3))];
        [_bannerScrollView setBackgroundColor:UIColorFromRGB(0xe1ded4)];
        [_bannerScrollView setPagingEnabled:YES];
        [_bannerScrollView setBounces:NO];
        [_bannerScrollView setShowsVerticalScrollIndicator:NO];
        [_bannerScrollView setShowsHorizontalScrollIndicator:NO];
        [_bannerScrollView setScrollEnabled:NO];
        [_mainScrollView addSubview:_bannerScrollView];
        
        [self performSelector:@selector(bannerRollingThread) withObject:nil afterDelay:8.0f];

        // 50 78
        
        UILabel *menstruationPeriodLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), _bannerScrollView.frame.origin.y + _bannerScrollView.frame.size.height + CGHeightFromIP6P(78 / 3), CGWidthFromIP6P(230 / 3), CGHeightFromIP6P(48 / 3))];
        [menstruationPeriodLabel setBackgroundColor:[UIColor clearColor]];
        [menstruationPeriodLabel setTextColor:[UIColor blackColor]];
        [menstruationPeriodLabel setText:@"생리 기간"];
        [menstruationPeriodLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(18))];
        [menstruationPeriodLabel sizeToFit];
        [_mainScrollView addSubview:menstruationPeriodLabel];
        
        
        UIView *whiteRoundBGView = [[UIView alloc] initWithFrame:CGRectMake(menstruationPeriodLabel.frame.origin.x, menstruationPeriodLabel.frame.origin.y + menstruationPeriodLabel.frame.size.height + CGHeightFromIP6P(29 /3), CGWidthFromIP6P(368 / 3), CGHeightFromIP6P(160 / 3))];
        [whiteRoundBGView setBackgroundColor:[UIColor whiteColor]];
        [whiteRoundBGView.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [whiteRoundBGView setClipsToBounds:YES];
        [_mainScrollView addSubview:whiteRoundBGView];
        
        UILabel *prevPeroidLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(31 / 3), CGWidthFromIP6P(170 / 3), CGHeightFromIP6P(38 / 3))];
        [prevPeroidLabel setBackgroundColor:[UIColor clearColor]];
        [prevPeroidLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
        [prevPeroidLabel setText:@"직전 주기"];
        [prevPeroidLabel setTextColor:[UIColor blackColor]];
        [whiteRoundBGView addSubview:prevPeroidLabel];
        
        _beforePeriodValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(192 / 3), CGHeightFromIP6P(81 / 3), CGWidthFromIP6P(153 / 3), CGHeightFromIP6P(58 / 3))];
        [_beforePeriodValueLabel setBackgroundColor:[UIColor clearColor]];
        [_beforePeriodValueLabel setFont:MediumWithSize(CGHeightFromIP6P(19))];
        [_beforePeriodValueLabel setText:@"-"];
        [_beforePeriodValueLabel setTextAlignment:NSTextAlignmentRight];
        [_beforePeriodValueLabel setTextColor:[UIColor blackColor]];
        [whiteRoundBGView addSubview:_beforePeriodValueLabel];

        
        // 192 81
        
        whiteRoundBGView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(439 / 3), menstruationPeriodLabel.frame.origin.y + menstruationPeriodLabel.frame.size.height + CGHeightFromIP6P(29 /3), CGWidthFromIP6P(368 / 3), CGHeightFromIP6P(160 / 3))];
        [whiteRoundBGView setBackgroundColor:[UIColor whiteColor]];
        [whiteRoundBGView.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [whiteRoundBGView setClipsToBounds:YES];
        [_mainScrollView addSubview:whiteRoundBGView];

        prevPeroidLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(31 / 3), CGWidthFromIP6P(170 / 3), CGHeightFromIP6P(38 / 3))];
        [prevPeroidLabel setBackgroundColor:[UIColor clearColor]];
        [prevPeroidLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(15))];
        [prevPeroidLabel setText:@"최근 주기"];
        [prevPeroidLabel setTextColor:UIColorFromRGB(0xfc4f74)];
        [whiteRoundBGView addSubview:prevPeroidLabel];
        
        _currentPeriodValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(192 / 3), CGHeightFromIP6P(81 / 3), CGWidthFromIP6P(153 / 3), CGHeightFromIP6P(58 / 3))];
        [_currentPeriodValueLabel setBackgroundColor:[UIColor clearColor]];
        [_currentPeriodValueLabel setFont:BoldWithSize(CGHeightFromIP6P(19))];
        [_currentPeriodValueLabel setText:@"-"];
        [_currentPeriodValueLabel setTextAlignment:NSTextAlignmentRight];
        [_currentPeriodValueLabel setTextColor:UIColorFromRGB(0xfc4f74)];
        [whiteRoundBGView addSubview:_currentPeriodValueLabel];

        whiteRoundBGView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(828 / 3), menstruationPeriodLabel.frame.origin.y + menstruationPeriodLabel.frame.size.height + CGHeightFromIP6P(29 /3), CGWidthFromIP6P(368 / 3), CGHeightFromIP6P(160 / 3))];
        [whiteRoundBGView setBackgroundColor:[UIColor whiteColor]];
        [whiteRoundBGView.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [whiteRoundBGView setClipsToBounds:YES];
        [_mainScrollView addSubview:whiteRoundBGView];
        
        
        prevPeroidLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(31 / 3), CGWidthFromIP6P(312 / 3), CGHeightFromIP6P(38 / 3))];
        [prevPeroidLabel setBackgroundColor:[UIColor clearColor]];
        [prevPeroidLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
        [prevPeroidLabel setText:@"다음 예상 주기"];
        [prevPeroidLabel setTextColor:UIColorFromRGB(0x868686)];
        [whiteRoundBGView addSubview:prevPeroidLabel];
        
        _afterPeriodValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(192 / 3), CGHeightFromIP6P(81 / 3), CGWidthFromIP6P(153 / 3), CGHeightFromIP6P(58 / 3))];
        [_afterPeriodValueLabel setBackgroundColor:[UIColor clearColor]];
        [_afterPeriodValueLabel setFont:MediumWithSize(CGHeightFromIP6P(19))];
        [_afterPeriodValueLabel setText:@"-"];
        [_afterPeriodValueLabel setTextAlignment:NSTextAlignmentRight];
        [_afterPeriodValueLabel setTextColor:UIColorFromRGB(0x868686)];
        [whiteRoundBGView addSubview:_afterPeriodValueLabel];

        
        UILabel *menstruationCycleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(50 / 3), whiteRoundBGView.frame.origin.y + whiteRoundBGView.frame.size.height + CGHeightFromIP6P(78 / 3), CGWidthFromIP6P(230 / 3), CGHeightFromIP6P(48 / 3))];
        [menstruationCycleLabel setBackgroundColor:[UIColor clearColor]];
        [menstruationCycleLabel setTextColor:[UIColor blackColor]];
        [menstruationCycleLabel setText:@"생리 시작 주기"];
        [menstruationCycleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(18))];
        [menstruationCycleLabel sizeToFit];
        [_mainScrollView addSubview:menstruationCycleLabel];

        
        whiteRoundBGView = [[UIView alloc] initWithFrame:CGRectMake(menstruationCycleLabel.frame.origin.x, menstruationCycleLabel.frame.origin.y + menstruationCycleLabel.frame.size.height + CGHeightFromIP6P(29 /3), CGWidthFromIP6P(368 / 3), CGHeightFromIP6P(160 / 3))];
        [whiteRoundBGView setBackgroundColor:[UIColor whiteColor]];
        [whiteRoundBGView.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [whiteRoundBGView setClipsToBounds:YES];
        [_mainScrollView addSubview:whiteRoundBGView];
        
        prevPeroidLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(31 / 3), CGWidthFromIP6P(170 / 3), CGHeightFromIP6P(38 / 3))];
        [prevPeroidLabel setBackgroundColor:[UIColor clearColor]];
        [prevPeroidLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
        [prevPeroidLabel setText:@"직전 주기"];
        [prevPeroidLabel setTextColor:[UIColor blackColor]];
        [whiteRoundBGView addSubview:prevPeroidLabel];
        
        _beforeCycleValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(192 / 3), CGHeightFromIP6P(81 / 3), CGWidthFromIP6P(153 / 3), CGHeightFromIP6P(58 / 3))];
        [_beforeCycleValueLabel setBackgroundColor:[UIColor clearColor]];
        [_beforeCycleValueLabel setFont:MediumWithSize(CGHeightFromIP6P(19))];
        [_beforeCycleValueLabel setText:@"-"];
        [_beforeCycleValueLabel setTextAlignment:NSTextAlignmentRight];
        [_beforeCycleValueLabel setTextColor:[UIColor blackColor]];
        [whiteRoundBGView addSubview:_beforeCycleValueLabel];
        
        
        // 192 81
        
        whiteRoundBGView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(439 / 3), menstruationCycleLabel.frame.origin.y + menstruationCycleLabel.frame.size.height + CGHeightFromIP6P(29 /3), CGWidthFromIP6P(368 / 3), CGHeightFromIP6P(160 / 3))];
        [whiteRoundBGView setBackgroundColor:[UIColor whiteColor]];
        [whiteRoundBGView.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [whiteRoundBGView setClipsToBounds:YES];
        [_mainScrollView addSubview:whiteRoundBGView];
        
        prevPeroidLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(31 / 3), CGWidthFromIP6P(170 / 3), CGHeightFromIP6P(38 / 3))];
        [prevPeroidLabel setBackgroundColor:[UIColor clearColor]];
        [prevPeroidLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(15))];
        [prevPeroidLabel setText:@"최근 주기"];
        [prevPeroidLabel setTextColor:UIColorFromRGB(0xfc4f74)];
        [whiteRoundBGView addSubview:prevPeroidLabel];
        
        _currentCycleValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(192 / 3), CGHeightFromIP6P(81 / 3), CGWidthFromIP6P(153 / 3), CGHeightFromIP6P(58 / 3))];
        [_currentCycleValueLabel setBackgroundColor:[UIColor clearColor]];
        [_currentCycleValueLabel setFont:BoldWithSize(CGHeightFromIP6P(19))];
        [_currentCycleValueLabel setText:@"-"];
        [_currentCycleValueLabel setTextAlignment:NSTextAlignmentRight];
        [_currentCycleValueLabel setTextColor:UIColorFromRGB(0xfc4f74)];
        [whiteRoundBGView addSubview:_currentCycleValueLabel];
        
        whiteRoundBGView = [[UIView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(828 / 3), menstruationCycleLabel.frame.origin.y + menstruationCycleLabel.frame.size.height + CGHeightFromIP6P(29 /3), CGWidthFromIP6P(368 / 3), CGHeightFromIP6P(160 / 3))];
        [whiteRoundBGView setBackgroundColor:[UIColor whiteColor]];
        [whiteRoundBGView.layer setCornerRadius:CGWidthFromIP6P(12 / 3)];
        [whiteRoundBGView setClipsToBounds:YES];
        [_mainScrollView addSubview:whiteRoundBGView];
        
        
        prevPeroidLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(28 / 3), CGHeightFromIP6P(31 / 3), CGWidthFromIP6P(312 / 3), CGHeightFromIP6P(38 / 3))];
        [prevPeroidLabel setBackgroundColor:[UIColor clearColor]];
        [prevPeroidLabel setFont:RegularWithSize(CGHeightFromIP6P(15))];
        [prevPeroidLabel setText:@"다음 예상 주기"];
        [prevPeroidLabel setTextColor:UIColorFromRGB(0x868686)];
        [whiteRoundBGView addSubview:prevPeroidLabel];
        
        _afterCycleValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(192 / 3), CGHeightFromIP6P(81 / 3), CGWidthFromIP6P(153 / 3), CGHeightFromIP6P(58 / 3))];
        [_afterCycleValueLabel setBackgroundColor:[UIColor clearColor]];
        [_afterCycleValueLabel setFont:MediumWithSize(CGHeightFromIP6P(19))];
        [_afterCycleValueLabel setText:@"-"];
        [_afterCycleValueLabel setTextAlignment:NSTextAlignmentRight];
        [_afterCycleValueLabel setTextColor:UIColorFromRGB(0x868686)];
        [whiteRoundBGView addSubview:_afterCycleValueLabel];

        [_mainScrollView setContentSize:CGSizeMake(kSCREEN_WIDTH, whiteRoundBGView.frame.origin.y + whiteRoundBGView.frame.size.height + CGHeightFromIP6P(10))];
        
        
        
        _blackMaskView = [[UIView alloc] initWithFrame:self.view.bounds];
        [_blackMaskView setBackgroundColor:[UIColor blackColor]];
        [_blackMaskView setAlpha:0];
        
        _dateSelectView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 44 + 215)];
        [_dateSelectView setBackgroundColor:[UIColor clearColor]];

        UIView *pickerResignView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGWidthFromIP6P(414), 44)];
        [pickerResignView setBackgroundColor:[UIColor clearColor]];
        
        UIView *pickerBlackMaskView = [[UIView alloc] initWithFrame:pickerResignView.bounds];
        [pickerBlackMaskView setBackgroundColor:[UIColor blackColor]];
        [pickerBlackMaskView setAlpha:0.8];
        [pickerResignView addSubview:pickerBlackMaskView];
        
        UIButton *pickerResignButton = [[UIButton alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(414) - CGWidthFromIP6P(40 + 4 + 4), 3, CGWidthFromIP6P(40), 44 - 3 - 3)];
        [pickerResignButton.titleLabel setFont:RegularWithSize(14)];
        [pickerResignButton setTitle:@"완료" forState:UIControlStateNormal];
        [pickerResignButton setBackgroundColor:[UIColor clearColor]];
        [pickerResignButton addTarget:self action:@selector(resignPicker) forControlEvents:UIControlEventTouchUpInside];
        [pickerResignView addSubview:pickerResignButton];
        
        [_dateSelectView addSubview:pickerResignView];

        
        
        
        _datePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, kSCREEN_WIDTH, 215)];
        [_datePickerView setBackgroundColor:[UIColor whiteColor]];
        [_datePickerView setShowsSelectionIndicator:YES];
        [_datePickerView setDelegate:self];
        [_datePickerView setDataSource:self];
        [_dateSelectView addSubview:_datePickerView];

        
        UILabel *yearLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [yearLabel setFont:RegularWithSize(21)];
        [yearLabel setText:@"년"];
        [yearLabel setTextColor:UIColorFromRGB(0x333333)];
        [yearLabel sizeToFit];
        [yearLabel setCenter:CGPointMake(_datePickerView.frame.size.width / 2 + CGWidthFromIP6P(5), _datePickerView.frame.size.height / 2 + 1)];
        [_datePickerView addSubview:yearLabel];
        
        
        UILabel *monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [monthLabel setFont:RegularWithSize(21)];
        [monthLabel setText:@"월"];
        [monthLabel setTextColor:UIColorFromRGB(0x333333)];
        [monthLabel sizeToFit];
        [monthLabel setCenter:CGPointMake(kSCREEN_WIDTH - CGWidthFromIP6P(118), _datePickerView.frame.size.height / 2 + 1)];
        [_datePickerView addSubview:monthLabel];
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"weight_hour"]) {
            [[NSUserDefaults standardUserDefaults] setObject:@"7" forKey:@"weight_hour"];
            [[NSUserDefaults standardUserDefaults] setObject:@"5" forKey:@"weight_min"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }

        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"first_launch"] intValue] == 1) {
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"first_guide"] intValue] == 0) {
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"first_guide"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                // 최초 실행
                UIButton *guideButton = [[UIButton alloc] initWithFrame:[[UIApplication sharedApplication] keyWindow].bounds];
                
                // 분기에 따라 이미지 세트
                if (IS_35INCH) {
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_4"] forState:UIControlStateNormal];
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_4"] forState:UIControlStateHighlighted];
                } else if (IS_IPHONE5S) {
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_5s"] forState:UIControlStateNormal];
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_5s"] forState:UIControlStateHighlighted];
                } else if (IS_IPHONE8) {
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_8"] forState:UIControlStateNormal];
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_8"] forState:UIControlStateHighlighted];
                } else if (IS_IPHONE8P) {
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_8p"] forState:UIControlStateNormal];
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_8p"] forState:UIControlStateHighlighted];
                } else if (IS_IPHONEX) {
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_iphonex"] forState:UIControlStateNormal];
                    [guideButton setImage:[UIImage imageNamed:@"cyclecalendar_image_guide_iphonex"] forState:UIControlStateHighlighted];
                }
                
                UIImageView *cancelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cyclecalendar_image_close"]];
                [cancelImageView setFrame:CGRectMake(16, 24, cancelImageView.frame.size.width, cancelImageView.frame.size.height)];
                [guideButton addSubview:cancelImageView];
                
                [guideButton addTarget:self action:@selector(removeGuideView:) forControlEvents:UIControlEventTouchUpInside];
                [[[UIApplication sharedApplication] keyWindow] addSubview:guideButton];
                
                [self performSelector:@selector(agreementAlert) withObject:nil afterDelay:0.0f];
            }
            
        } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"first_launch"] intValue] == 2) {
            // 두번째 실행
            [[NSUserDefaults standardUserDefaults] setObject:@"3" forKey:@"first_launch"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            // 사전 정보 입력 스킵 체크
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"skip_menstruation"] intValue] == 1) {
                // 얼럿
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"사전 생리 정보를 입력해주세요"
                                                                    message:@"사전 생리 정보를 입력해주시면 보다 정확히 예상 주기 정보를 받아보실 수 있습니다."
                                                                   delegate:self
                                                          cancelButtonTitle:@"닫기"
                                                          otherButtonTitles:@"입력하기", nil];
                [alertView show];
            }
        }

        [MSAnalytics trackEvent:@"Page clicked" withProperties:@{
                                                                 @"PageId" : @"CycleInfoMonthly",
                                                                 @"UserNo" : [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"],
                                                                 @"All" : [NSString stringWithFormat:@"%@|%@",
                                                                           @"CycleInfoMonthly",
                                                                           [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]
                                                                           ]
                                                                 }];

        if ([[DataSingleton sharedSingletonClass] notificationInfo]) {
            if ([[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageType" lowercaseString]] && [[[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageType" lowercaseString]] intValue] == 101) {          // 단순 메시지
            } else if ([[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageType" lowercaseString]] && [[[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageType" lowercaseString]] intValue] == 102) {          // 내부 이동
                
                if ([[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageCd" lowercaseString]] && [[[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageCd" lowercaseString]] intValue] == 102) {
                    
                    // 이벤트
                    
                    [(RootViewController *)_pClass changeToEventViewController];
                    EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:[[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"TargetIndexNo" lowercaseString]]];
                    UINavigationController *eventNavigationController = (UINavigationController *)[[(RootViewController *)_pClass drawerViewController] centerViewController];
                    [eventNavigationController pushViewController:eventDetailViewController animated:YES];

                    
//                    EventDetailViewController *eventDetailViewController = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self eventNo:[[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"TargetIndexNo" lowercaseString]]];
//                    [self.navigationController pushViewController:eventDetailViewController animated:YES];
                    
                } else if ([[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageCd" lowercaseString]] && [[[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageCd" lowercaseString]] intValue] == 103) {
                    
                    // 공지사항
                    NoticeDetailViewController *noticeDetailViewController = [[NoticeDetailViewController alloc] initWithNibName:nil bundle:nil pClass:self boardNo:[[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"TargetIndexNo" lowercaseString]]];
                    [self.navigationController pushViewController:noticeDetailViewController animated:YES];
                }
                
            } else if ([[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageType" lowercaseString]] && [[[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"MessageType" lowercaseString]] intValue] == 103) {          // 외부 이동
                
                WebViewNavigationController *webViewNavigationController = [[WebViewNavigationController alloc] initWithURL:[[[DataSingleton sharedSingletonClass] notificationInfo][@"aps"][[@"Message" lowercaseString]][[@"TargetUrl" lowercaseString]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] title:@""];
                [self.navigationController presentViewController:webViewNavigationController animated:YES completion:^{
                    //
                }];
            }
            [[DataSingleton sharedSingletonClass] setNotificationInfo:nil];
        }

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMonthlyCycleDiary) name:@"REFRESH_AFTER_LOGIN" object:nil];

    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)agreementAlert
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"My Alert"
                                                                   message:@"This is an alert."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [[alert view] setTintColor:UIColorFromRGB(0xff5575)];
    
    [alert setValue:[[NSAttributedString alloc] initWithString:@"푸시 알림 동의 (선택)" attributes:@{
                                                                                             NSFontAttributeName : BoldWithSize(18),
                                                                                             NSForegroundColorAttributeName : UIColorFromRGB(0xf16774)
                                                                                             }]
             forKey:@"attributedTitle"];

    
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"\n푸시 알림을 통해서 "
                                                                                   attributes:@{ NSFontAttributeName : MediumWithSize(14),
                                                                                                 NSForegroundColorAttributeName : [UIColor blackColor] }];
    
    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:@"MY좋은느낌의 다양한 서비스와 혜택"
                                                                       attributes:@{ NSFontAttributeName : BoldWithSize(14),
                                                                                     NSForegroundColorAttributeName : [UIColor blackColor] }]];
    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:@"을 받으실 수 있습니다. 이후 설정 메뉴를 통해 언제든 동의 여부를 변경하실 수 있습니다.\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                                                       attributes:@{ NSFontAttributeName : MediumWithSize(14),
                                                                                     NSForegroundColorAttributeName : [UIColor blackColor] }]];
    [alert setValue:attrString forKey:@"attributedMessage"];

    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:@"닫기" style:UIAlertActionStyleCancel
                                                        handler:^(UIAlertAction * action) {
                                                            
                                                            NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CONFIG_SET_All_INFO]];
                                                            
                                                            [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
                                                            [urlString appendFormat:@"/Y/N/N/N"];   // 닫기일경우엔 그냥 다 N
                                                            
                                                            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                                                            [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
                                                            [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
                                                            
                                                            [[DataSingleton sharedSingletonClass] attachLoadingView];
                                                            
                                                            [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
                                                            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                                                NSDictionary *responseData;
                                                                NSError *error = nil;
                                                                if (responseObject != nil) {
                                                                    responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                                                                   options:NSJSONReadingMutableContainers
                                                                                                                     error:&error];
                                                                }
                                                                
                                                                [[DataSingleton sharedSingletonClass] detachLoadingView];
                                                                
                                                            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                                                [[DataSingleton sharedSingletonClass] detachLoadingView];
                                                                [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                                                            }];

                                                            
                                                        }];
    UIAlertAction *agreeAction = [UIAlertAction actionWithTitle:@"선택사항 동의" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            
                                                            NSMutableString *urlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", [[DataSingleton sharedSingletonClass] hostString], kAPI_PATH, kAPI_CONFIG_SET_All_INFO]];
                                                            
                                                            [urlString appendFormat:@"/%@", [[DataSingleton sharedSingletonClass] userInfo][@"UserNo"]];
                                                            [urlString appendFormat:@"/Y"];
                                                            
                                                            if (self->isSelectedMenstruation) {
                                                                [urlString appendFormat:@"/Y"];
                                                            } else {
                                                                [urlString appendFormat:@"/N"];
                                                            }

                                                            if (self->isSelectedMarketing) {
                                                                [urlString appendFormat:@"/Y"];
                                                            } else {
                                                                [urlString appendFormat:@"/N"];
                                                            }

                                                            if (self->isSelectedWeight) {
                                                                [urlString appendFormat:@"/Y"];

                                                                [[DataSingleton sharedSingletonClass] registWeightLocalNotification];
                                                            } else {
                                                                [urlString appendFormat:@"/N"];
                                                            }

                                                            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                                                            [manager setResponseSerializer:[AFHTTPResponseSerializer new]];
                                                            [manager.requestSerializer setValue:[[DataSingleton sharedSingletonClass] keyString] forHTTPHeaderField:@"Ocp-Apim-Subscription-Key"];
                                                            
                                                            [[DataSingleton sharedSingletonClass] attachLoadingView];
                                                            
                                                            [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
                                                            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                                                NSDictionary *responseData;
                                                                NSError *error = nil;
                                                                if (responseObject != nil) {
                                                                    responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                                                                   options:NSJSONReadingMutableContainers
                                                                                                                     error:&error];
                                                                }
                                                                
                                                                [[DataSingleton sharedSingletonClass] detachLoadingView];
                                                                
                                                            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                                                [[DataSingleton sharedSingletonClass] detachLoadingView];
                                                                [[[UIAlertView alloc] initWithTitle:nil message:kALERT_NETWORK_FAIL delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
                                                            }];
                                                        }];
    [alert addAction:closeAction];
    [alert addAction:agreeAction];
    if (@available(iOS 9.0, *)) {
        [alert setPreferredAction:agreeAction];
    }
    
    [alert.view setClipsToBounds:YES];
    
    self->isShowAgreementAlert = NO;

    _agreementTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 140, alert.view.frame.size.width, 218) style:UITableViewStylePlain];
    [_agreementTableView setBackgroundColor:[UIColor clearColor]];
    [_agreementTableView setDelegate:self];
    [_agreementTableView setDataSource:self];
    [_agreementTableView setTableFooterView:[[UIView alloc] init]];
    [_agreementTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    [_agreementTableView setScrollEnabled:NO];
    [alert.view addSubview:_agreementTableView];

    [self presentViewController:alert animated:YES completion:^{
        self->isShowAgreementAlert = YES;
        
        self->isSelectedMenstruation = YES;
        self->isSelectedWeight = NO;
        self->isSelectedMarketing = YES;
        self->isOpenAgreement = NO;

        [self->_agreementTableView setFrame:CGRectMake(0, 140, alert.view.frame.size.width, 218)];
        [self->_agreementTableView reloadData];
    }];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        ChangeStepAgeViewController *changeStepAgeViewController = [[ChangeStepAgeViewController alloc] initWithNibName:nil bundle:nil pClass:self];
        
        UINavigationController *changeStepLastAgeNavigationController = [[UINavigationController alloc] initWithRootViewController:changeStepAgeViewController];
        [changeStepLastAgeNavigationController.navigationBar setBackgroundImage:[UIImage new]
                                                                  forBarMetrics:UIBarMetricsDefault];
        changeStepLastAgeNavigationController.navigationBar.shadowImage = [UIImage new];
        changeStepLastAgeNavigationController.navigationBar.translucent = YES;
        
        [self presentViewController:changeStepLastAgeNavigationController animated:YES completion:^{
        }];

//        ChangeStepLastMenstruationViewController *changeStepLastMenstruationViewController = [[ChangeStepLastMenstruationViewController alloc] initWithNibName:nil bundle:nil pClass:self];
//
//        UINavigationController *changeStepLastMenstruationNavigationController = [[UINavigationController alloc] initWithRootViewController:changeStepLastMenstruationViewController];
//        [changeStepLastMenstruationNavigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                                           forBarMetrics:UIBarMetricsDefault];
//        changeStepLastMenstruationNavigationController.navigationBar.shadowImage = [UIImage new];
//        changeStepLastMenstruationNavigationController.navigationBar.translucent = YES;
//
//        [self presentViewController:changeStepLastMenstruationNavigationController animated:YES completion:^{
//        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// UIPickerView Delegate & DataSource
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        if (component == 0) {
            return 100;
        } else if (component == 1) {
            return 80;
        }
    }
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        return 50;
    }
    
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        if (component == 0) {
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                           fromDate:[NSDate date]];
            
            return [NSString stringWithFormat:@"%ld", (long)row + ([components year] - 10)];
        } else if(component == 1) {
            return [NSString stringWithFormat:@"%ld", (long)row + 1];
        }
    }
    return nil;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _datePickerView) {
        if (component == 0) {
            return 21;
        } else if(component == 1) {
            return 12;
        }
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView == _datePickerView) {
        return 2;
    }
    return 0;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == _monthScrollView) {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay
                                                                       fromDate:[NSDate date]];
        [_yearLabel setText:[NSString stringWithFormat:@"%lu", (long)(scrollView.contentOffset.x / scrollView.frame.size.width / 12 + 0.04) + [components year] - 10]];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _monthScrollView) {
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            for (int i = 0; i < [self->_monthItemArray count]; i++) {
                UIView *bgView = self->_monthItemArray[i][@"view"];
                UIButton *itemButton = self->_monthItemArray[i][@"button"];
                if (i == (int)(scrollView.contentOffset.x / scrollView.frame.size.width)) {
                    [bgView setAlpha:1];
                    [itemButton setTitleColor:UIColorFromRGB(0xf9626e) forState:UIControlStateNormal];
                } else {
                    [bgView setAlpha:0];
                    [itemButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
                }
            }
        } completion:^(BOOL finished) {
            [self performSelectorOnMainThread:@selector(getMonthlyCycleDiary) withObject:nil waitUntilDone:NO];
        }];
    }

}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (scrollView == _monthScrollView) {
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            for (int i = 0; i < [self->_monthItemArray count]; i++) {
                UIView *bgView = self->_monthItemArray[i][@"view"];
                UIButton *itemButton = self->_monthItemArray[i][@"button"];
                if (i == (int)(scrollView.contentOffset.x / scrollView.frame.size.width)) {
                    [bgView setAlpha:1];
                    [itemButton setTitleColor:UIColorFromRGB(0xf9626e) forState:UIControlStateNormal];
                } else {
                    [bgView setAlpha:0];
                    [itemButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
                }
            }
        } completion:^(BOOL finished) {
            [self performSelectorOnMainThread:@selector(getMonthlyCycleDiary) withObject:nil waitUntilDone:NO];
        }];
    }
}


#pragma mark -
#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if (isSelectedMenstruation && isSelectedWeight && isSelectedMarketing) { // 다 선택 되어있으면 전체 체크 해제
            isSelectedMenstruation = isSelectedWeight = isSelectedMarketing = NO;
            [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            [_agreementMenstruationCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            [_agreementWeightCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            [_agreementMarketingCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
        } else {        // 그 외엔 전체 선택
            isSelectedMenstruation = isSelectedWeight = isSelectedMarketing = YES;
            [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            [_agreementMenstruationCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            [_agreementWeightCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            [_agreementMarketingCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
        }
    } else if (indexPath.row == 1) {
        isSelectedMenstruation = !isSelectedMenstruation;
        if (isSelectedMenstruation) {
            [_agreementMenstruationCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            if (isSelectedMenstruation && isSelectedWeight && isSelectedMarketing) {
                [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            }
        } else {
            [_agreementMenstruationCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
        }
    } else if (indexPath.row == 2) {
        isSelectedWeight = !isSelectedWeight;
        if (isSelectedWeight) {
            [_agreementWeightCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            if (isSelectedMenstruation && isSelectedWeight && isSelectedMarketing) {
                [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            }
        } else {
            [_agreementWeightCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
        }
    } else if (indexPath.row == 3) {
        isSelectedMarketing = !isSelectedMarketing;
        if (isSelectedMarketing) {
            [_agreementMarketingCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            if (isSelectedMenstruation && isSelectedWeight && isSelectedMarketing) {
                [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            }
        } else {
            [_agreementMarketingCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
        }
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark -
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 44;
    } else if (indexPath.row == 4) {

        UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16 + 4, 12, tableView.frame.size.width - 16 - 16 - 4 - 4, 19)];
        [descLabel setPreferredMaxLayoutWidth:descLabel.frame.size.width];
        [descLabel setNumberOfLines:0];
        [descLabel setText:@"회원 가입 시 이름, 생년월일, 휴대전화번호 등의 정보를 허위로 기재해서는 안 됩니다. 회원 계정에 등록된 정보는 항상 정확한 최신 정보가 유지될 수 있도록 관리해 주세요. 자신의 계정을 다른 사람에게 판매, 양도, 대여 또는 담보로 제공하거나 다른 사람에게 그 사용을 허락해서는 안 됩니다. 아울러 자신의 계정이 아닌 타인의 계정을 무단으로 사용해서는 안 됩니다. 이에 관한 상세한 내용은 계정 운영정책을 참고해 주시기 바랍니다. 타인에 대해 직접적이고 명백한 신체적 위협을 가하는 내용의 게시물, 타인의 자해 행위 또는 자살을 부추기거나 권장하는 내용의 게시물, 타인의 신상정보, 사생활 등 비공개 개인정보를 드러내는 내용의 게시물, 타인을 지속적으로 따돌리거나 괴롭히는 내용의 게시물, 성매매를 제안, 알선, 유인 또는 강요하는 내용의 게시물, 공공 안전에 대해 직접적이고 심각한 위협을 가하는 내용의 게시물은 제한될 수 있습니다. 관련 법령상 금지되거나 형사처벌의 대상이 되는 행위를 수행하거나 이를 교사 또는 방조하는 등의 범죄 관련 직접적인 위험이 확인된 게시물, 관련 법령에서 홍보, 광고, 판매 등을 금지하고 있는 물건 또는 서비스를 홍보, 광고, 판매하는 내용의 게시물, 타인의 지식재산권 등을 침해하거나 모욕, 사생활 침해 또는 명예훼손 등 타인의 권리를 침해하는 내용이 확인된 게시물은 제한될 수 있습니다. 자극적이고 노골적인 성행위를 묘사하는 등 타인에게 성적 수치심을 유발시키거나 왜곡된 성 의식 등을 야기할 수 있는 내용의 게시물, 타인에게 잔혹감 또는 혐오감을 일으킬 수 있는 폭력적이고 자극적인 내용의 게시물, 본인 이외의 자를 사칭하거나 허위사실을 주장하는 등 타인을 기만하는 내용의 게시물, 과도한 욕설, 비속어 등을 계속하여 반복적으로 사용하여 심한 혐오감 또는 불쾌감을 일으키는 내용의 게시물은 제한될 수 있습니다."];
        [descLabel setTextColor:UIColorFromRGB(0x999999)];
        [descLabel setFont:MediumWithSize(13)];
        [descLabel sizeToFit];

        return descLabel.frame.size.height + 12 + 12;
    } else {
        return 58;
    }
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isShowAgreementAlert) {
        return 4 + isOpenAgreement;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [NSString stringWithFormat:@"Cell_%ld_%ld", indexPath.section, indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (indexPath.row == 0) {
            [cell setBackgroundColor:UIColorFromRGB(0xfff2f3)];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, tableView.frame.size.width - 16 - 16, 44)];
            [titleLabel setText:@"전체 선택"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [titleLabel setFont:BoldWithSize(17)];
            [cell.contentView addSubview:titleLabel];
            
            _agreementAllCheckBoxImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 52 / 3, 52 / 3)];
            [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            if (isSelectedMenstruation && isSelectedWeight && isSelectedMarketing) {
                [_agreementAllCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            }
            [_agreementAllCheckBoxImageView setCenter:CGPointMake(titleLabel.frame.size.width + 10, 44 / 2)];
            [cell.contentView addSubview:_agreementAllCheckBoxImageView];
            
        } else if (indexPath.row == 1) {
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 12, tableView.frame.size.width - 16 - 16, 24)];
            [titleLabel setText:@"예상 생리 주기 알림"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [titleLabel setFont:MediumWithSize(17)];
            [cell.contentView addSubview:titleLabel];
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 36, tableView.frame.size.width - 16 - 16, 19)];
            [descLabel setText:@"예상 생리 주기 5일 전 시작일을 미리 알려드립니다."];
            [descLabel setTextColor:UIColorFromRGB(0x999999)];
            [descLabel setFont:MediumWithSize(11)];
            [cell.contentView addSubview:descLabel];

            _agreementMenstruationCheckBoxImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 52 / 3, 52 / 3)];
            [_agreementMenstruationCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            if (isSelectedMenstruation) {
                [_agreementMenstruationCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            }
            [_agreementMenstruationCheckBoxImageView setCenter:CGPointMake(titleLabel.frame.size.width + 10, 58 / 2)];
            [cell.contentView addSubview:_agreementMenstruationCheckBoxImageView];

        } else if (indexPath.row == 2) {
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 12, tableView.frame.size.width - 16 - 16, 24)];
            [titleLabel setText:@"몸무게 재기 알림"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [titleLabel setFont:MediumWithSize(17)];
            [cell.contentView addSubview:titleLabel];
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 36, tableView.frame.size.width - 16 - 16, 19)];
            [descLabel setText:@"매일 지정한 시간에 몸무게 잴 시간을 알려드립니다."];
            [descLabel setTextColor:UIColorFromRGB(0x999999)];
            [descLabel setFont:MediumWithSize(11)];
            [cell.contentView addSubview:descLabel];

            _agreementWeightCheckBoxImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 52 / 3, 52 / 3)];
            [_agreementWeightCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            if (isSelectedWeight) {
                [_agreementWeightCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            }
            [_agreementWeightCheckBoxImageView setCenter:CGPointMake(titleLabel.frame.size.width + 10, 58 / 2)];
            [cell.contentView addSubview:_agreementWeightCheckBoxImageView];
            
        } else if (indexPath.row == 3) {
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 12, tableView.frame.size.width - 16 - 16, 24)];
            [titleLabel setText:@"마케팅 정보 알림"];
            [titleLabel setTextColor:[UIColor blackColor]];
            [titleLabel setFont:MediumWithSize(17)];
            [cell.contentView addSubview:titleLabel];
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 36, tableView.frame.size.width - 16 - 16, 19)];
            [descLabel setText:@"다양한 이벤트와 기획전 정보를 알려드립니다."];
            [descLabel setTextColor:UIColorFromRGB(0x999999)];
            [descLabel setFont:MediumWithSize(11)];
            [cell.contentView addSubview:descLabel];

            _agreementMarketingCheckBoxImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 52 / 3, 52 / 3)];
            [_agreementMarketingCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox"]];
            if (isSelectedMarketing) {
                [_agreementMarketingCheckBoxImageView setImage:[UIImage imageNamed:@"cyclecalendar_image_checkbox_h"]];
            }
            [_agreementMarketingCheckBoxImageView setCenter:CGPointMake(titleLabel.frame.size.width + 10, 58 / 2)];
            [cell.contentView addSubview:_agreementMarketingCheckBoxImageView];
            [titleLabel sizeToFit];

//            UIButton *agreementFlipButton = [[UIButton alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x + titleLabel.frame.size.width + 10, 12 - 1.5, 169 / 3, 63 / 3)];
//            [agreementFlipButton setImage:[UIImage imageNamed:@"cyclecalendar_button_term"] forState:UIControlStateNormal];
//            [agreementFlipButton addTarget:self action:@selector(toggleFlipAgreement) forControlEvents:UIControlEventTouchUpInside];
//            [cell.contentView addSubview:agreementFlipButton];

        } else if (indexPath.row == 4) {
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(16 + 4, 12, tableView.frame.size.width - 16 - 16 - 4 - 4, 19)];
            [descLabel setPreferredMaxLayoutWidth:descLabel.frame.size.width];
            [descLabel setNumberOfLines:0];
            [descLabel setText:@"회원 가입 시 이름, 생년월일, 휴대전화번호 등의 정보를 허위로 기재해서는 안 됩니다. 회원 계정에 등록된 정보는 항상 정확한 최신 정보가 유지될 수 있도록 관리해 주세요. 자신의 계정을 다른 사람에게 판매, 양도, 대여 또는 담보로 제공하거나 다른 사람에게 그 사용을 허락해서는 안 됩니다. 아울러 자신의 계정이 아닌 타인의 계정을 무단으로 사용해서는 안 됩니다. 이에 관한 상세한 내용은 계정 운영정책을 참고해 주시기 바랍니다. 타인에 대해 직접적이고 명백한 신체적 위협을 가하는 내용의 게시물, 타인의 자해 행위 또는 자살을 부추기거나 권장하는 내용의 게시물, 타인의 신상정보, 사생활 등 비공개 개인정보를 드러내는 내용의 게시물, 타인을 지속적으로 따돌리거나 괴롭히는 내용의 게시물, 성매매를 제안, 알선, 유인 또는 강요하는 내용의 게시물, 공공 안전에 대해 직접적이고 심각한 위협을 가하는 내용의 게시물은 제한될 수 있습니다. 관련 법령상 금지되거나 형사처벌의 대상이 되는 행위를 수행하거나 이를 교사 또는 방조하는 등의 범죄 관련 직접적인 위험이 확인된 게시물, 관련 법령에서 홍보, 광고, 판매 등을 금지하고 있는 물건 또는 서비스를 홍보, 광고, 판매하는 내용의 게시물, 타인의 지식재산권 등을 침해하거나 모욕, 사생활 침해 또는 명예훼손 등 타인의 권리를 침해하는 내용이 확인된 게시물은 제한될 수 있습니다. 자극적이고 노골적인 성행위를 묘사하는 등 타인에게 성적 수치심을 유발시키거나 왜곡된 성 의식 등을 야기할 수 있는 내용의 게시물, 타인에게 잔혹감 또는 혐오감을 일으킬 수 있는 폭력적이고 자극적인 내용의 게시물, 본인 이외의 자를 사칭하거나 허위사실을 주장하는 등 타인을 기만하는 내용의 게시물, 과도한 욕설, 비속어 등을 계속하여 반복적으로 사용하여 심한 혐오감 또는 불쾌감을 일으키는 내용의 게시물은 제한될 수 있습니다."];
            [descLabel setTextColor:UIColorFromRGB(0xbcbcbc)];
            [descLabel setFont:MediumWithSize(13)];
            [descLabel sizeToFit];
            [cell.contentView addSubview:descLabel];
            
        }
    }
    return cell;
}

- (void)toggleFlipAgreement
{
    isOpenAgreement = !isOpenAgreement;
    
    if (isOpenAgreement) {
        [_agreementTableView insertRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:4 inSection:0] ] withRowAnimation:UITableViewRowAnimationAutomatic];
        [_agreementTableView setContentOffset:CGPointMake(0, 44 + 58 + 58) animated:YES];
    } else {
        [_agreementTableView deleteRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:4 inSection:0] ] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}


@end
