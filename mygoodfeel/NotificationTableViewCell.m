//
//  NotificationTableViewCell.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 3. 6..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "NotificationTableViewCell.h"

@implementation NotificationTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier dataDictionary:(NSDictionary *)dataDictionary
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, CGHeightFromIP6P(77))];
        [bgView setBackgroundColor:UIColorFromRGB(0xffdfe7)];
        [self setSelectedBackgroundView:bgView];
        
        // 49 82 88 88
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(49) / 3, CGHeightFromIP6P(82) / 3, CGWidthFromIP6P(88) / 3, CGHeightFromIP6P(88) / 3)];
        [iconImageView setImage:[UIImage imageNamed:@"notification_image_speaker"]];
        if ([dataDictionary[@"MessageCd"] intValue] == 102) {
            [iconImageView setImage:[UIImage imageNamed:@"notification_image_present"]];
        }
        [self.contentView addSubview:iconImageView];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGWidthFromIP6P(171) / 3, CGHeightFromIP6P(64) / 3, CGWidthFromIP6P(1035) / 3, CGHeightFromIP6P(45) / 3)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setFont:SemiBoldWithSize(CGHeightFromIP6P(15))];
        [titleLabel setTextColor:UIColorFromRGB(0x3c3c3e)];
        [titleLabel setText:dataDictionary[@"MessageTitle"]];
//        [titleLabel setText:@"공지사항에 새 게시물이 등록되었습니다."];
        [titleLabel sizeToFit];
        [titleLabel setFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y, CGWidthFromIP6P(1035) / 3, titleLabel.frame.size.height)];
        [self.contentView addSubview:titleLabel];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y + titleLabel.frame.size.height + CGHeightFromIP6P(7), titleLabel.frame.size.width, CGHeightFromIP6P(12))];
        [timeLabel setBackgroundColor:[UIColor clearColor]];
        [timeLabel setFont:MediumWithSize(CGHeightFromIP6P(12))];
        [timeLabel setTextColor:UIColorFromRGB(0x757575)];
        [timeLabel setText:dataDictionary[@"MessageSendDt"]];
//        [timeLabel setText:@"59분 전"];
        [timeLabel sizeToFit];
        [timeLabel setFrame:CGRectMake(timeLabel.frame.origin.x, timeLabel.frame.origin.y, titleLabel.frame.size.width, timeLabel.frame.size.height)];
        [self.contentView addSubview:timeLabel];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
