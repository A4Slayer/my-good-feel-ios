//
//  CMMDrawerViewController.m
//  mygoodfeel
//
//  Created by daewang Lim on 2019. 1. 25..
//  Copyright © 2019년 appknot. All rights reserved.
//

#import "CMMDrawerViewController.h"

@interface CMMDrawerViewController ()

@end

@implementation CMMDrawerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

// 이벤트 웹뷰에서 파일첨부를 하기 위해 ViewController의 본 메소드를 오버라이딩 해야함
// 따라서 Pod이라 직접 코드를 수정할 수 없는 MMDrawController를 상속받아 CMMDrawController를 생성하여 사용
- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if (self.presentedViewController) {
        if (![self.presentedViewController isKindOfClass:[UIDocumentMenuViewController class]] || ![self.presentedViewController isKindOfClass:[UIImagePickerController class]]) {
            [super dismissViewControllerAnimated:flag completion:completion];
        }
    }
}

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion
{
    [super presentViewController:viewControllerToPresent animated:flag completion:completion];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
