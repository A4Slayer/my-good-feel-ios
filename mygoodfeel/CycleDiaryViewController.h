//
//  CycleDiaryViewController.h
//  mygoodfeel
//
//  Created by daewang Lim on 2018. 9. 20..
//  Copyright © 2018년 appknot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "DataSingleton.h"

@interface CycleDiaryViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (strong, nonatomic) id pClass;

@property (strong, nonatomic) UIButton *menstruationNoneButton;
@property (strong, nonatomic) UIButton *menstruationLowButton;
@property (strong, nonatomic) UIButton *menstruationNormalButton;
@property (strong, nonatomic) UIButton *menstruationMuchButton;

@property (strong, nonatomic) UILabel *menstruationLabel;

@property (strong, nonatomic) UIButton *pyrexiaButton;

@property (strong, nonatomic) UIButton *sexButton;

@property (strong, nonatomic) UITextField *weightTextField;
@property (strong, nonatomic) UIPickerView *weightPickerView;

@property (strong, nonatomic) NSString *dateString;
@property (strong, nonatomic) NSDictionary *dataDictionary;

@property (strong, nonatomic) NSString *menstruationCdString;
@property (strong, nonatomic) NSString *pyrexiaString;
@property (strong, nonatomic) NSString *sexString;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pClass:(id)pClass dateString:(NSString *)dateString;

@end
