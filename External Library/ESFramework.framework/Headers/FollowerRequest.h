//
//  FollowerRequest.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 24/01/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
@class FollowerSettings;

/*
 * FollowerType enum defined in accordance with 'Get Peers (for side menu)'
 * [2/14/2017 11:59 AM], source:
 * https://gitlab.com/earlysense/earlysense/wikis/following-requests-api#response--3
 */
typedef enum : NSUInteger {
    // todo check if we need here anything related NOT to followeR
    // look at commit 4fc79516ec35f1dddb0246deca0607228d24dea8
    PendingFollowee,
    Followee,

    Follower,
    PendingFollower,
    UnknnownFollowerType,
} FollowerType;

@interface FollowerRequest : NSObject

// todo: consider to add the following properties:
//  "first_summary_id"
//  "last_summary_id"
//  [2/14/2017 12:30 PM], source:
//  https://gitlab.com/earlysense/earlysense/wikis/following-requests-api#response--3
@property (nonatomic) NSString* followerEmail;
@property (nonatomic) NSString* followerFullName;
@property (nonatomic) FollowerType followerType;
@property (nonatomic) FollowerSettings* sharedDataScheme;

- (id)initWithDataFrom:(NSDictionary*)followerData;
+ (FollowerType)getFollowerTypeFrom:(NSString*)followerTypeAsString;

@end
