//
//  HeadlessHubDataManager.h
//  EarlySnseFramework
//
//  Created by Rebecca Biaz on 30/10/2017.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ManagedUser.h"
#import "NetworkConfig.h"
#import "ManagedUserSettings.h"

@interface HeadlessHubDataManager : NSObject
{
    ManagedUser *_managedUser;
    NetworkConfig *_networkConfig;
    ManagedUserSettings *_managedUserSettings;
}

+ (HeadlessHubDataManager *_Nonnull)sharedInstance;

+ (int)numInBedDefault;     //get default people in bed.
+ (int)numInBedChoices;     //Num of Choices from 1 to...
+ (ManagedUser *_Nonnull)managedUserForManagedUserSettings:(ManagedUserSettings *_Nullable)managedUserSettings;     //Num of Choices from 1 to...

- (ManagedUser *_Nullable)managedUser;
- (NetworkConfig *_Nullable)networkConfig;
- (ManagedUserSettings *_Nullable)managedUserSettings;

- (void)updateManagedUser:(ManagedUser *_Nullable)managedUser;
- (void)updateNetworkConfig:(NetworkConfig *_Nullable)networkConfig;
- (void)updateManagedUserSettings:(ManagedUserSettings *_Nullable)managedUserSettings;

@end
