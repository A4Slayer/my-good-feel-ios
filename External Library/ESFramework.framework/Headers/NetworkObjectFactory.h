//
//  NetworkObjectFactory.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 13/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TipObject.h"
#import "Session.h"
#import "SleepSummaryGeneral.h"
#import "SleepSummaryComplete.h"
#import "SeniorCareSettings.h"
#import "Diary.h"
#import "FollowerSettings.h"
#import "NotificationThresholdsExtra.h"
#import "RealTimeAlert.h"
#import "HeadlessHubSensor.h"
#import "UserManager.h"
#import "FertilitySummaryData.h"
#import "FertilitySummary.h"
#import "MonthData.h"
#import "TodayData.h"
#import "DailyEvent.h"
#import "MonthlyEvents.h"
#import "AllDailyEvents.h"
#import "DailySleepSummary.h"
#import "PeerUser.h"

@interface NetworkObjectFactory : NSObject

+ (TipObject*)createTip:(NSDictionary*)dict;

+ (Session*)createSession:(NSDictionary*)dict;

+ (SleepSummaryGeneral*)createSleepSummaryGeneral:(NSDictionary*)dict;

+ (SleepSummaryComplete*)createSleepSummaryComplete:(NSDictionary*)dict;

+ (SeniorCareSettings*)createSeniorCareSettings:(NSDictionary*)dict;

+ (Diary*)createDiary:(NSDictionary*)dict;

+ (FollowerSettings*)createFollowerSettings:(NSDictionary*)dict;

+ (NotificationThresholdsExtra*)createNotificationThresholds:(NSDictionary*)dict;

+ (HeadlessHubSensor*)createHeadlessHubSensor:(NSDictionary*)dict;

//fertility

+ (MonthData*)createMonthData:(NSDictionary*)dict;

+ (TodayData*)createTodayData:(NSDictionary*)dict;

+ (FertilitySummary*)createFertilitySummary:(NSDictionary*)dict;

+ (DailyEvent*)createDailyEvent:(NSDictionary*)dict;

+ (MonthlyEvents *)createMonthlyEvents:(NSDictionary *)dict forMonth:(NSInteger)month andYear:(NSInteger)year;

+ (AllDailyEvents*)createAllDailyEvents:(NSDictionary*)dict;

+ (DailySleepSummary*)createDailySleepSummary:(NSDictionary*)dict;

+ (NSArray *)createPeerUsers:(NSDictionary *)dict;


@end
