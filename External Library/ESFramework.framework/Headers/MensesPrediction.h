//
//  FertilitySummaryData.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 19/04/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MensesPrediction : NSObject

@property BOOL isEvitalsDifferent;
@property long notificationTime;

@end
