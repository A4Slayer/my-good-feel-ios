//
// Created by Vlad Zamskoi on 5/14/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;
@class FollowerSettings;


typedef enum EditFollowerActionType {
    Add,
    Update,
    Remove
} FollowerEditingActionType;


@interface FollowerSettingsEditingScheme : NSObject

@property(nonatomic) User* follower;
@property(nonatomic) FollowerSettings *settingsToApply;
@property(nonatomic) FollowerEditingActionType editingActionType;

- (NSDictionary*)convertToDictionary;

@end
