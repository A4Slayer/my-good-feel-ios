//
//  SeniorCareSettings.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 16/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmailSeniorCare : NSObject

@property (nonatomic) NSString* name;
@property (nonatomic) NSString* email;

@end

@interface SeniorCareSettings : NSObject

@property (nonatomic) NSInteger timeInSecFromMidnight;
@property (nonatomic) BOOL isActive;
@property (nonatomic) NSArray* emailList;

@end
