//
//  NotificationEvent.h
//  EarlySnseFramework
//
//  Created by Yaron Karasik on 8/14/17.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LocalNotificationType) {
    NotificationLowUsage,
    NotificationLowEngagement,
    NotificationPeriodPredicted,
    NotificationPeriodStart,
    NotificationFertilityStart,
    NotificationFirstLongSleep,
    NotificationEvitalsPeriodChange,
    NotificationComplianceReminder,
    NotificationForceCloseAlert,
    NotificationBluetoothOffAlert
};

@interface NotificationEvent : NSObject

// Notification type
@property (nonatomic, assign) LocalNotificationType notificationType;

// Title and body presented in the notification
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *body;

//convenience initializers
- initWithType:(LocalNotificationType)type title:(NSString*)title body:(NSString*)body days:(NSUInteger)days hour:(NSUInteger)hour;
- initWithType:(LocalNotificationType)type title:(NSString*)title body:(NSString*)body days:(NSUInteger)days hour:(NSUInteger)hour minute:(NSUInteger)minute;

// Which hour of the day will the notification be displayed
@property (nonatomic, assign) NSUInteger hour;
//how many minutes after the hour (defaults to 0)
@property (nonatomic, assign) NSUInteger minute;

// How many days forward will the notification be displayed, or a specific date it will be displayed on
// Which one will be used depends on te type of the notification
@property (nonatomic, assign) NSUInteger days;
@property (nonatomic, strong) NSDate *date;

// Extra parameters - currently unused
@property (nonatomic, strong) NSMutableDictionary *params;

// String identifier for each notification type
+ (NSString *)identifierForType:(LocalNotificationType)type;

//determine notification type by string identifier
+ (LocalNotificationType)typeForIdentifier:(NSString*)identifier;

// Returns the time interval to the notification based on the notification's type and properties. Used by the SDK to schedule the notification.
- (NSTimeInterval)timeIntervalForScheduling;

@end
