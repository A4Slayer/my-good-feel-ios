//
//  FertilityStatus.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 20/04/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TodayData.h"

typedef enum FertilityPhase
{
    Unknown = 0,
    MenseUserInput = 1,
    MenseEstimated = 2,
    MenseVerified = 3,
    Follicular = 4,
    FertilityDayOne = 5,
    FertilityWindowHigh = 6,
    FertilityWindowPeak = 7,
    Ovulation = 8,
    Luteal = 9
} FertilityPhase;

typedef enum BleedingIndication
{
    BleedingIndicationUnknown = 0,
    BleedingIndicationMenseUserInput = 1,
    BleedingIndicationMenseVerified = 3,
    BleedingIndicationBleedingNotOnPeriod = 10
} BleedingIndication;


typedef enum Compliance
{
    NoDataRecorded,
    SufficientDataRecorded,
    LowSignal,
    InsufficientDataRecorded
} Compliance;

@interface FertilityStatus : NSObject

@property FertilityPhase fertilityPhase;
@property BleedingIndication bleedingIndication;
@property Compliance compliance;
@property NSInteger cycleDay;
@property ECycleStage nextEventType;
@property NSInteger daysToNextEvent;


@end
