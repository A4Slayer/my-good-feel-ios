//
//  ESAlarmManager.h
//  EarlySnseFramework
//
//  Created by Roman on 15/11/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAlarmObject.h"

enum ESAlarmManagerNotificationType
{
	ESAlarmManagerNotificationTypeInvokeAlarm,
	ESAlarmManagerNotificationTypeStopAlarming,
	ESAlarmManagerNotificationTypeOutOfRange,
	ESAlarmManagerNotificationTypeInRange,
	ESAlarmManagerNotificationTypeAlarmUpdated
};

@interface ESAlarmManager : NSObject

+ (ESAlarmManager*)sharedInstance;

#pragma mark - Alarm Management

- (void)createAlarmWithTitle:(NSString*)title startTime:(int)startTime endTime:(int)endTime snoozeType:(enum ESAlarmSnoozeType)snoozeType ringtone:(NSString*)ringtone repeat:(BOOL)repeat repeatingDays:(NSArray*)dayNumbers bedTimeReminder:(NSNumber*)hoursBefore bedTimeReminderState:(BOOL)isBedTimeReminderOn customData:(NSString*)customData andCompletion:(void (^)(ESAlarmObject* alarm))completion;
- (void)getAllAlarmsWithCompletion:(void (^)(NSArray* alarms))completion;
- (void)updateAlarm:(ESAlarmObject*)alarm withCompletion:(void (^)(ESAlarmObject* alarm))completion;
- (void)deleteAlarm:(ESAlarmObject*)alarm withCompletion:(void (^)(BOOL success))completion;
- (void)clearAllAlarmsWithCompletion:(void (^)(BOOL success))completion;

#pragma mark - Alarm Fetches

- (ESAlarmObject*)getClosestAlarm;

#pragma mark - Status Handling

- (void)setAlarmingStateActive:(BOOL)active;
- (void)stopAllAlarms;
- (BOOL)isAlarmingInProgress;
- (BOOL)isAlarming;
- (BOOL)getAlarmingState;
- (BOOL)isPendingSnooze;
- (BOOL)canInvokeAlarm;

#pragma mark - Alarm Actions

- (void)snoozeAlarm;
- (void)stopCurrentAlarm;

#pragma mark - Notifications

- (NSString*)notificationForType:(enum ESAlarmManagerNotificationType)type;

@end
