//
//  FertilitySummary.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 19/04/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MonthData.h"
#import "TodayData.h"
#import "FertilityStatus.h"
#import "MensesPrediction.h"

@interface FertilitySummary : NSObject

@property NSDictionary<NSString *, MonthData *>* calendaricData;
@property TodayData* todayData;
@property MensesPrediction* mensesPrediction;


/**
 * Get data regarding fertility status.
 *
 * @param day   the day
 * @param month the month
 * @param year  the year
 * @see FertilityStatus
 * @return the fertility status
 */
- (FertilityStatus*)getDataForDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;

@end

