//
// Created by Vlad Zamskoi on 5/3/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface InvitationToFollowSomebody : NSObject <NSCoding>

@property(nonatomic) NSString *emailOfUserToFollow;
@property(nonatomic) NSString *nameOfUserToFollow;

@end