//
//  SleepParams.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 14/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DashboardParams.h"

@interface SleepParams : NSObject

@property (nonatomic) NSInteger actualSleepTime;
@property (nonatomic) NSInteger barsAwake;
@property (nonatomic) NSInteger barsDeepSleep;
@property (nonatomic) NSInteger barsREM;
@property (nonatomic) NSInteger barsScoreLatency;
@property (nonatomic) NSInteger barsSleepLatency;
@property (nonatomic) NSInteger barsBedExit;
@property (nonatomic) NSInteger barsSleepScore;
@property (nonatomic) NSInteger bedExits;
@property (nonatomic) NSString* diaryPostSleep;
@property (nonatomic) NSInteger efficiency;
@property (nonatomic) NSInteger minutesAwake;
@property (nonatomic) NSInteger minutesAwakeAfterSleep;
@property (nonatomic) NSInteger minutesDeepSleep;
@property (nonatomic) NSInteger minutesLightSleep;
@property (nonatomic) NSInteger minutesOutOfBed;
@property (nonatomic) NSInteger minutesREM;
@property (nonatomic) NSInteger movementDensity;
@property (nonatomic) NSInteger percentageDeepSleep;
@property (nonatomic) NSInteger percentageLightSleep;
@property (nonatomic) NSInteger percentageOutOfBed;
@property (nonatomic) NSInteger percentageREM;
@property (nonatomic) NSInteger percentageWake;
@property (nonatomic) NSInteger scoreBedExits;
@property (nonatomic) NSInteger scoreBonus;
@property (nonatomic) NSInteger scoreDeepSleep;
@property (nonatomic) NSInteger scoreNumberOfAwakenings;
@property (nonatomic) NSInteger scoreREM;
@property (nonatomic) NSInteger scoreSleepEfficiency;
@property (nonatomic) NSInteger scoreTimeToFallAsleep;
@property (nonatomic) NSInteger scoreTotalSleepTime;
@property (nonatomic) NSInteger sleep;
@property (nonatomic) NSInteger sleepBriefAverageScore;
@property (nonatomic) NSInteger sleepBriefLowerBound;
@property (nonatomic) NSInteger sleepBriefUpperBound;
@property (nonatomic) NSInteger targetSleepScore;
@property (nonatomic) NSInteger timesAwakened;
@property (nonatomic) NSInteger timestamp;
@property (nonatomic) NSInteger timestampFrom;
@property (nonatomic) NSInteger timestampTo;
@property (nonatomic) NSInteger timeToFallAsleep;
@property (nonatomic) NSInteger tipCategory;
@property (nonatomic) NSInteger tipIndex;
@property (nonatomic) NSInteger totalInBed;
@property (nonatomic) NSInteger unusualBedExits;
@property (nonatomic) NSInteger unusualBedExitsChange;
@property (nonatomic) NSInteger unusualDeep;
@property (nonatomic) NSInteger unusualDeepChange;
@property (nonatomic) NSInteger unusualHR;
@property (nonatomic) NSInteger unusualHRChange;
@property (nonatomic) NSInteger unusualMovement;
@property (nonatomic) NSInteger unusualMovementChange;
@property (nonatomic) NSInteger unusualREM;
@property (nonatomic) NSInteger unusualREMChange;
@property (nonatomic) NSInteger unusualRR;
@property (nonatomic) NSInteger unusualRRChange;
@property (nonatomic) NSInteger unusualSleepEfficiency;
@property (nonatomic) NSInteger unusualSleepEfficiencyChange;
@property (nonatomic) NSInteger unusualSleepLatency;
@property (nonatomic) NSInteger unusualSleepLatencyChange;
@property (nonatomic) NSInteger unusualSleepScore;
@property (nonatomic) NSInteger unusualSleepScoreChange;
@property (nonatomic) NSInteger unusualTotalSleepTime;
@property (nonatomic) NSInteger unusualTotalSleepTimeChange;
@property (nonatomic) NSInteger unusualWake;
@property (nonatomic) NSInteger unusualWakeChange;
@property (nonatomic) NSInteger wentToBed;
@property (nonatomic) NSInteger avgHR;
@property (nonatomic) NSInteger avgHistoryLatency;
@property (nonatomic) NSInteger avgRR;
@property (nonatomic) NSInteger avgHistoryTimeInBed;
@property (nonatomic) NSInteger avgHistoryTimesAwake;
@property (nonatomic) NSInteger suggestSleepStudy;
@property (nonatomic) NSInteger percentageREMFromSleep;
@property (nonatomic) NSInteger percentageLightSleepFromSleep;
@property (nonatomic) NSInteger percentageDeepSleepFromSleep;
@property (nonatomic) NSInteger avgHistoryHR;
@property (nonatomic) NSInteger avgHistoryRR;
@property (nonatomic) NSInteger hypnographLength;
@property (nonatomic) NSInteger avgStressCategory;
@property (nonatomic) NSInteger hrvMin;
@property (nonatomic) NSInteger hrvMax;
@property (nonatomic) NSInteger hrvThreshold1;
@property (nonatomic) NSInteger hrvThreshold2;

- (NSDictionary*)getExtraParams;
- (void)setExtraParams:(NSMutableDictionary*)params;
- (void)addExtraParamWithKey:(NSNumber*)key andValue:(NSNumber*)value;
- (void)clearExtraParams;
- (void)createDashboardParams;
- (DashboardParams *)getDashboardParams;

@end




