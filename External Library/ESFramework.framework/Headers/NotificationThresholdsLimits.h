//
//  NotificationThresholdsLimits.h
//  EarlySnseFramework
//
//  Created by Mumen Shabaro on 7/25/17.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationThresholdsLimits : NSObject

- (id)initWithDataFrom:(NSDictionary*)thresholdsLimitDataDictionary;

-(NSInteger) getHrAboveThresholdHighLimit;
-(NSInteger) getHrAboveThresholdLowLimit;
-(NSInteger) getRrAboveThresholdHighLimit;
-(NSInteger) getRrAboveThresholdLowLimit;
-(NSInteger) getHrBelowThresholdHighLimit;
-(NSInteger) getHrBelowThresholdLowLimit;
-(NSInteger) getRrBelowThresholdHighLimit;
-(NSInteger) getRrBelowThresholdLowLimit;

@end
