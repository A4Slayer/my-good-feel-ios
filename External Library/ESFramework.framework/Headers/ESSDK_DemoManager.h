#ifndef ESSDK_DemoManager_h
#define ESSDK_DemoManager_h

enum DemoManagerNotification
{
    DemoManagerNotificationStarted,
	DemoManagerNotificationCompleted,
	DemoManagerNotificationFailure
};

enum DemoReadingMode
{
	DemoReadingModeRealTime,
	DemoReadingModeFast
};

@interface DemoManager : NSObject

+ (DemoManager*)sharedInstance;

#pragma mark - Client - Public Methods

// Demo File Handling
- (void)setDemoFile:(NSString*)demoFile;
- (NSString*)getSelectedDemoFile;
- (NSArray*)getAvailableDemoFiles;

// Demo Reading Mode
- (void)setDemoReadingMode:(enum DemoReadingMode)mode;
- (enum DemoReadingMode)getDemoReadingMode;

// Demo Running
- (BOOL)startDemo;
- (void)stopDemo;
- (BOOL)isDemoModeActive;
- (void)setDemoModeActive:(BOOL)active;

#pragma mark - Notifications

- (NSString*)notificationForType:(enum DemoManagerNotification)type;

@end

#endif /* ESSDK_DemoManager_h */
