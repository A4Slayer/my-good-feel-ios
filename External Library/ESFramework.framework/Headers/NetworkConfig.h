//
//  NetworkConfig.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 31/01/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkConfig : NSObject

@property (nonatomic) NSString* networkType;
@property (nonatomic) NSString* ssid;
@property (nonatomic) NSString* networkPass;
@property (nonatomic) NSString* configType;

@end
