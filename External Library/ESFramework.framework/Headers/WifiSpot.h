//
//  WifiSpot.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 25/01/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WifiSpot : NSObject

@property (nonatomic) NSString* SSID;
@property (nonatomic) NSString* BSSID;
@property (nonatomic) NSString* capabilities;
@property (nonatomic) BOOL hiddenSSID;
@property (nonatomic) NSInteger signalLevel;
@property (nonatomic) NSInteger signalLevelPerc;
@property (nonatomic) NSString* password;
@property (nonatomic) NSInteger networkId;

@end
