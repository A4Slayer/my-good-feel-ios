//
//  SleepSummaryGeneral.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 14/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Session.h"
#import "SleepParams.h"

@interface SleepSummaryGeneral : NSObject

@property (nonatomic) Session* session;

@property (nonatomic) BOOL isEmpty;

@property (nonatomic) NSDictionary* sleepParamsDict;

@property (nonatomic) SleepParams* sleepParams;

- (void)setSleepParams:(SleepParams*)params;
- (SleepParams*)getSleepParams;
- (DashboardParams *)getDashboardParams;


@end
