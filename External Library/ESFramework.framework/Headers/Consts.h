//
//  Consts.h
//  EarlySnseFramework
//
//  Created by Giora on 15/08/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString  * const kLaunchAppAutomaticly;
static const NSInteger kSEC_IN_DAY_PLUS_59 = 24*60*60 + 59;
static const NSInteger kSEC_IN_DAY = 24*60*60;
static const NSInteger kSEC_IN_MIN = 60;
static const NSInteger MIN_IN_HR = 60;
static const NSInteger MIN_GET_HYPNO_INTERVAL_SECONDS = 5 * 60;
static const NSInteger PERSISTENT_SNOOZE_LIMIT_TIME = 5400;
static const NSInteger KB_IN_BYTES = 1024;
static const NSInteger MB_IN_BYTES = 1024*1024;

static const NSInteger PERSON_IN_BED_DEF = 2;
static const NSInteger NOT_DEF = -1;

static const NSInteger kPerceptAppId = 15;
#define MILI_IN_SEC 1000.0

#define DEFINE_SHARED_INSTANCE_USING_BLOCK(block) \
static dispatch_once_t pred = 0; \
__strong static id _sharedObject = nil; \
dispatch_once(&pred, ^{ \
    _sharedObject = block(); \
}); \
return _sharedObject; \

//user defaults keys
#define USER_DEFAULTS_HUB_ITERATION_COUNTER @"es_hubIterationCounter"
#define USER_DEFAULTS_HUB_TIME_AT_INIT @"es_hubTimeAtInit"
#define USER_DEFAULTS_HUB_TIME_OF_SIGNAL_DETECTED @"es_hubTimeOfSignalDetected"
#define USER_DEFAULTS_HUB_TIME_OF_LAST_BED_EXIT @"es_hubTimeOfLastBedExit"

#define USER_DEFAULTS_LAST_UPLOADED_LOGFILE_NAME @"es_lastUploadedLogfileName"
#define USER_DEFAULTS_LAST_LOGFILE_UPLOAD_ATTEMPT_TIME @"es_lastLogfileUploadAttemptTime"
#define USER_DEFAULTS_LAST_ALGS_RESPONSE_TIMESTAMP @"es_lastAlgsResponseTimestamp"
#define USER_DEFAULTS_LAST_USER_UUID @"es_lastUserUUID"

#define USER_DEFAULTS_MIN_HRV @"es_minHRV"
#define USER_DEFAULTS_MAX_HRV @"es_maxHRV"
#define USER_DEFAULTS_THRESHOLD1 @"es_hrvThreshold1"
#define USER_DEFAULTS_THRESHOLD2 @"es_hrvThreshold2"

#define USER_DEFAULTS_LAST_TIME_IN_BED @"es_lastTimeInBed"
#define USER_DEFAULTS_LAST_SENSOR_DISCONNECTION @"es_lastSensorDisconnection"
#define USER_DEFAULTS_ALARM_SWITCH_ACTIVE @"es_globalAlarmClockSwitch"

#define FIRST_LAUNCH_ACTIONS_PERFORMED_KEY @"es_firstLaunchActions"
#define BLE_DEFAULTS_LAST_SENSOR @"bleDefaultsLastSensor"
#define BLE_DEFAULTS_SAVE_PIEZO @"bleDefaultsSavePiezo"

#define USER_DEFAULTS_LAST_PIEZO_FILE_CREATION @"lastPiezoFileCreation"

#define NETWORK_DEF_USERNAME @"networkDefUsername"
#define NETWORK_DEF_PASSWORD @"networkDefPassword"
#define NETWORK_DEF_LAST_LOGIN @"networkDefLastLogin"
#define NETWORK_DEF_SERVER_URL @"networkDefServerUrl"
#define NETWORK_DEF_APP_PREFIX @"networkDefAppPrefix"

static NSString* kAllDailyEvents = @"esf_all_daily_events";
static NSString* kFertilitySummary = @"esf_fertility_summary";
static NSString* kFertilitySleepSummaries = @"esf_fertility_sleep_summaries";
//end user defaults keys


//Headlesshub end points

#define SCAN_WIFI @"scan_wifi"
#define STOP_SCAN_WIFI @"stop_scan_wifi"
#define NETWORK_CONFIG_START @"network_config_start"
#define NETWORK_CONFIG_STATUS @"network_config_status"
#define SET_CLOUD_USER @"set_cloud_user"
#define SCAN_SENSORS @"scan_sensors"
#define STOP_SCAN_SENSORS @"stop_scan_sensors"
#define SENSOR_CONFIG @"sensor_config"
#define END_MANAGE @"end_manage"

//JSON keys for headless hub protocol
#define HH_OVERRIDE @"override"
#define HH_SENSOR_MAC @"sensormac"

#define HH_CONFIG_TYPE @"configtype"
#define HH_NETWORK_TYPE @"nettype"
#define HH_CONFIG_TYPE_AUTOMATIC @"AUTOMATIC"
#define HH_NETWORK_TYPE_WIFI @"WIFI"
#define HH_NETWORK_ID @"netid"
#define HH_PASSWORD @"password"

#define HH_SENSORS_RESPONSE @"sensors"
#define HH_NETWORKS_RESPONSE @"networks"

#define HH_WAIT_MILLISECONDS @"waitmillis"


//JSON keys for server methods

#define TIME_STAMP @"timestamp"
#define MOBILE_VERSION @"mobile_version"
#define DISCLAIMER_PATH @"confirmDisclaimer"

typedef enum {
    UNDEFINED = -1,
    HAPPY = 0,
    OK = 1,
    SAD = 2
} Happiness;

@interface Consts : NSObject

@end
