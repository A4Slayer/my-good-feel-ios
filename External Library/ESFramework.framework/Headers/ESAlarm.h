//
//  ESAlarm+CoreDataClass.h
//  
//
//  Created by Roman on 15/11/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class ESAlarmObject;

@interface ESAlarm : NSManagedObject

- (NSDictionary*)toJSON;
- (ESAlarmObject*)toAlarmObject;

- (void)updateFromAlarmObject:(ESAlarmObject*)alarm;
- (void)updateFromDictionary:(NSDictionary*)dict;

- (void)calculateNextAlarmTimeAndForce:(BOOL)force;

@end

NS_ASSUME_NONNULL_END

#import "ESAlarm+CoreDataProperties.h"
