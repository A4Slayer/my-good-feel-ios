//
//  ESSDK_AnalyticsManager.h
//  EarlySnseFramework
//
//  Created by Yaron Karasik on 8/9/17.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#ifndef ESSDK_AnalyticsManager_h
#define ESSDK_AnalyticsManager_h

@protocol AnalyticsManagerDelegate <NSObject>

- (void)lastSessionWasRestored;

@end


@interface AnalyticsManager : NSObject

@property (nonatomic, weak) id <AnalyticsManagerDelegate> delegate;

+ (AnalyticsManager *)sharedInstance;

- (void)reportLastSessionRestored;

@end


#endif /* ESSDK_AnalyticsManager_h */
