//
//  RealTimeAlert.h
//  EarlySnseFramework
//
//  Created by Giora on 24/08/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum {
    RealTimeHRUpperLimit,
    RealTimeRRUpperLimit,
    RealTimetLongOOB,
    RealTimeHRLowerLimit,
    RealTimeRRLowerLimit
} RealTime;

@interface RealTimeAlert : NSObject

@property (nonatomic,assign) RealTime alertType;
@property (nonatomic,assign) NSInteger timeStamp;
@property (nonatomic,assign) NSInteger alertValue;

-(RealTimeAlert*)initWith:(RealTime)alertType timeStamp:(NSInteger)timeStamp andAlertValue:(NSInteger)alertValue;

@end
