//
// Created by Vlad Zamskoi on 5/14/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FollowerSettingsEditingScheme;
@class User;


@interface FollowerSettingsEditingSchemeList : NSObject

@property(nonatomic) User *followee;
@property(nonatomic) NSArray<FollowerSettingsEditingScheme *> *editingSchemeList;

@end
