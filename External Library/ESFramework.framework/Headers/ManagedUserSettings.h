//
//  ManagedUserSettings.h
//  ESFramework
//
//  Created by Rebecca Biaz on 11/12/2017.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <ESFramework/ESFramework.h>

@interface ManagedUserSettings : JsonableElement

@property (nonatomic, strong) NotificationThresholdsExtra *notificationThresholdsExtra;
@property (nonatomic, strong) NSDictionary *algsConfiguration;
@property (nonatomic, unsafe_unretained) NSInteger peopleInBed;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *profilImg;

    
@end
