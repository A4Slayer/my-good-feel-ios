//
// Created by Vlad Zamskoi on 8/29/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

@class StartManageResult;
@class HeadlessHubError;
@class HeadlessHubStatus;
@class ServerSession;
@class User;
@class HubRequestSender;

typedef void(^StartManageForInitialSetupResultCallback)(StartManageResult *, HeadlessHubError *);

@interface StartManageHubForInitialSetupAsyncTask : NSObject

- (instancetype)initWithCompletion:(StartManageForInitialSetupResultCallback)completion
                     serverSession:(ServerSession *)serverSession
                  hubRequestSender:(HubRequestSender *)hubRequestSender;

- (void)start;

@end