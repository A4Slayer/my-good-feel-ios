//
//  FertilityManager.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 06/04/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DailyEvent.h"
#import "FertilitySummaryData.h"
#import "FertilitySummary.h"
#import "Consts.h"
#import "DailySleepSummary.h"

typedef enum FertilityStatusCode
{
    FertilityStatusOK,
    FertilityStatusValidationFailed,
    FertilityStatusUnknownError
    
} FertilityStatusCode;

enum FertilityNotificationType
{
    FertilityNotificationTypeFertilitySummaryChanged
};


@interface FertilityManager : NSObject

+ (FertilityManager*)sharedInstance;

+ (NSString*)predictionChangedKey;

/**
 * Create or update daily event.
 *
 * @param dailyEvent                      the daily event
 * @param ignore the ignore validation and send to server: if true and algorithms detect validation error, will ignore error and send to server.
 * @success success block
 * @failue failure block
  */

- (void)createUpdateDailyEvent:(DailyEvent*)dailyEvent ignoreValidationAndSendToServer:(BOOL)ignore
                       success:(void(^)(void))success
                       failure:(void(^)(NSInteger alertCode))failure;


/**
 * Create or update a group of daily events
 *
 * @param dailyEvents   an array of DailyEvent objects
 * @success success block
 * @failue failure block
 
 */

- (void)createUpdateDailyEvents:(NSArray<DailyEvent*>*)dailyEvents
                       success:(void(^)(void))success
                       failure:(void(^)(NSInteger alertCode))failure;

/**
 * Get a DailyEvent object for a give day/month/year combination
 *
 * @param day   the day
 * @param month the month
 * @param year  the year
 * @return the daily event
 */

- (DailyEvent*)getEventForDay:(NSInteger)day
                        month:(NSInteger)month
                         year:(NSInteger)year;


/**
 * Get an array of DailyEvent objecdts from the first day of the month/year provided to the last one 
 *
 * @param month the month
 * @param year  the year
 * @return the list of month events
 */

- (NSArray<DailyEvent*>*)getEventsForMonth:(NSInteger)month
                                      year:(NSInteger)year;

/**
 * Get an array of DailyEvent objecdts from the first day of the startMonth/startYear provided to the 
 * last day of the endMonth/endYear provided
 *
 * @param startMonth start month
 * @param startYear start year
 * @param endMonth end month
 * @param endYear end year
 * @return the list of month events
 */

- (NSArray<DailyEvent*>*)getEventsForRangeWithStartMonth:(NSInteger)startMonth
                                               startYear:(NSInteger)startYear
                                                endMonth:(NSInteger)endMonth
                                                 endYear:(NSInteger)endYear;


/**
 * Get fertility summary.
 *
 * @return FertilitySummary object. FertilitySummary contains the entire prediction for fertility plus some specific fields for the current day
 */
- (FertilitySummary*)getFertilitySummary;

/**
 * Get fertility summary - async version
 *
 */

- (void)getFertilitySummaryWithsuccess:(void(^)(FertilitySummary* summary))success
                               failure:(void(^)(NSInteger alertCode))failure forceFetch:(BOOL)force;

- (void)getFertilitySummaryWithsuccess:(void(^)(FertilitySummary* summary))success
                               failure:(void(^)(NSInteger alertCode))failure;


/**
 * Get the daily sleep summary for a given day/month/year combination
 *
 * @param day   the day
 * @param month the month
 * @param year  the year
 * @return the DailySleepSummary object
 */

- (DailySleepSummary*)getDailySleepSummaryForDay:(NSInteger)day
                        month:(NSInteger)month
                         year:(NSInteger)year;

- (void)clearAllFertilityData;

/**
 * recovers all of the logged in user's data from the server (daily events, fertility summary and daily sleep summaries)
 *
 */
- (void)recoverFertilityData;

- (void)notifyEvent:(enum FertilityNotificationType)notificationType;
- (NSString *)notificationForType:(enum FertilityNotificationType)type;



@end
