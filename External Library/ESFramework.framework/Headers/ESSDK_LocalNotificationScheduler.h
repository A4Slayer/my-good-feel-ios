//
//  ESSDK_LocalNotificationScheduler.h
//  EarlySnseFramework
//
//  Created by Yaron Karasik on 8/14/17.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#ifndef ESSDK_LocalNotificationScheduler_h
#define ESSDK_LocalNotificationScheduler_h

#import <Foundation/Foundation.h>
#import "NotificationEvent.h"

static const NSString* kNotificationId = @"notificationId";

@interface LocalNotificationScheduler : NSObject

+ (LocalNotificationScheduler *)sharedInstance;

// Set specific notification event types to active for this app; They will automatically be scheduled and presented
// Each event type has default values for title, body, date and time; These can be overridden per object before being enabled
- (void)enableNotificationEvents:(NSArray<NotificationEvent *> *)notificationEvents;

// Disable all notification event types for this app
- (void)disableAllNotificationEvents;

// Set a specific notification event type to disabled for this app
- (void)disableNotificationWithType:(LocalNotificationType)notificationType;

// Method used by the SDK itself to schedule or reschedule a notificaton type when a new trigger date has been delivered
// For example, if the SDK now knows that a period will begin in two days, it would call this method to schedule it
- (void)scheduleNotificationTypeIfEnabled:(LocalNotificationType)notificationType atDate:(NSDate *)date;

- (void)unscheduleLocalNotificationWithType:(LocalNotificationType)notificationType;

@end


#endif /* ESSDK_LocalNotificationScheduler_h */
