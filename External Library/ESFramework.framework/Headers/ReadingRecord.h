//
//  ReadingRecord.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 20/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReadingRecord : NSObject

@property (nonatomic) NSInteger timestamp;
@property (nonatomic) NSInteger heartRate;
@property (nonatomic) NSInteger respiratoryRate;
@property (nonatomic) NSInteger movementDensity;
@property (nonatomic) NSInteger timeInBed;
@property (nonatomic) NSInteger signalQuality;
@property (nonatomic) BOOL isInBed;
@property (nonatomic) NSInteger hrv;

@property (nonatomic) BOOL isHRAboveAlert;
@property (nonatomic) BOOL isHRBelowAlert;
@property (nonatomic) BOOL isRRAboveAlert;
@property (nonatomic) BOOL isRRBelowAlert;
@property (nonatomic) NSInteger heartRateAlert;
@property (nonatomic) NSInteger respiratoryRateAlert;

@property (nonatomic) BOOL isHistory;
@property (nonatomic) BOOL isHistoryHeartRate;
@property (nonatomic) BOOL isHistoryRespiratoryRate;

@property (nonatomic) NSInteger hrvMax;
@property (nonatomic) NSInteger hrvMin;
@property (nonatomic) NSInteger hrvThreshold1;
@property (nonatomic) NSInteger hrvThreshold2;

@property (nonatomic,assign) NSInteger stressCategory;
@property (nonatomic) NSInteger longOOBAlertTimestamp;
@property (nonatomic,assign) BOOL isLongOOBAlert;

@property (nonatomic,strong) NSString *deviceId;
@end
