//
//  DashboardParams.h
//  EarlySnseFramework
//
//  Created by Mumen Shabaro on 4/12/17.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Consts.h"

static int HEALTH_SUMMARY_DASHBOARD_COLOR = 120;
static int HEALTH_SUMMARY_DASHBOARD_REASON = 121;

static int NO_REASON = 1;
static int UNUSUAL_NIGHT_HR = 2;
static int UNUSUAL_NIGHT_RR = 4;
static int HIGH_HR_ALERT = 8;
static int HIGH_RR_ALERT = 16;
static int UNUSUAL_AHI = 32;
static int TWO_UNUSUAL_HR_NIGHTS = 64;
static int TWO_UNUSUAL_RR_NIGHTS = 128;
static int TWO_HIGH_HR_NIGHTS = 256;
static int TWO_HIGH_RR_NIGHTS = 512;
static int TWO_UNUSUAL_AHI = 1024;
static int TWO_UNUSUAL_TST = 2048;
static int LOW_HR_ALERT = 4096;
static int LOW_RR_ALERT = 8192;
static int TWO_LOW_HR_NIGHTS = 16384;
static int TWO_LOW_RR_NIGHTS = 32768;

@interface DashboardParams : NSObject {
    
}

+(DashboardParams *) initWithValues:(NSDictionary *)extraValues;
+(DashboardParams *) initWithEmptyValues;

/**
 * Gets happiness.
 *
 * @return the happiness level {@link Happiness#HAPPY}, {@link Happiness#OK}, {@link Happiness#SAD} or {@link Happiness#UNDEFINED}.
 */
-(Happiness) getHappiness;

/**
 * @return true if has unusual night heart rate, false otherwise.
 */
-(BOOL) hasUnusualNightHR;

/**
 * @return true if has unusual night respiration rate, false otherwise.
 */
-(BOOL) hasUnusualNightRR;

/**
 * @return true if has high heart rate, false otherwise.
 */
-(BOOL) hasHighHrAlert;

/**
 * @return true if has low heart rate, false otherwise.
 */
-(BOOL) hasLowHrAlert;

/**
 * @return true if has high respiration rate, false otherwise.
 */
-(BOOL) hasHighRrAlert;

/**
 * @return true if has low respiration rate, false otherwise.
 */
-(BOOL) hasLowRrAlert;

/**
 * @return true if has unusual Apnea–hypopnea index, false otherwise.
 */
-(BOOL) hasUnusualAHI;

/**
 * @return true if has two consecutive high heart rate nights, false otherwise.
 */
-(BOOL) hasTwoHighHrNights;

/**
 * @return true if has two consecutive low heart rate nights, false otherwise.
 */
-(BOOL) hasTwoLowHrNights;

/**
 * @return true if has two consecutive high respiration rate nights, false otherwise.
 */
-(BOOL) hasTwoHighRrNights;

/**
 * @return true if has two consecutive low respiration rate nights, false otherwise.
 */
-(BOOL) hasTwoLowRrNights;

/**
 * @return true if has two consecutive unusual respiration rate nights, false otherwise.
 */
-(BOOL) hasTwoUnusualNightRrNights;

/**
 * @return true if has two consecutive unusual heart rate nights, false otherwise.
 */
-(BOOL) hasTwoUnusualNightHrNights;

/**
 * @return true if has two consecutive unusual Apnea–hypopnea index, false otherwise.
 */
-(BOOL) hasTwoUnusualAHI;

/**
 * Has two unusual total sleep time nights.
 *
 * @return the boolean
 */
-(BOOL) hasTwoUnusualTST;

/**
 * @return true if Dashboard parameters contains reasons, false otherwise.
 */
-(BOOL) hasReason;

@end
