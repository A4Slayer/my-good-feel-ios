//
//  ESRequest.h
//  ESFramework
//
//  Created by Rebecca Biaz on 23/11/2017.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnSuccessResponse : NSObject

- (id)initWithRequest:(NSURLRequest *)request
             response:(NSURLResponse *)response
              andData:(id)data;

@property (nonatomic, strong, readonly) NSURLRequest *request;
@property (nonatomic, strong, readonly) NSURLResponse *response;
@property (nonatomic, strong, readonly) id data;

@end

@interface OnFailureResponse : NSObject

@property (nonatomic, strong, readonly) NSURLRequest *request;
@property (nonatomic, strong, readonly) NSURLResponse *response;
@property (nonatomic, strong, readonly) NSError *error;

- (id)initWithRequest:(NSURLRequest *)request
             response:(NSURLResponse *)response
             andError:(NSError *)error;

@end

typedef NS_ENUM(NSInteger, ESRequestMethodType)
{
    ESRequestMethodTypeGet = 0,
    /*1*/ ESRequestMethodTypePost,
    /*2*/ ESRequestMethodTypePut,
    /*3*/ ESRequestMethodTypeDelete,
    /*4*/ ESRequestMethodTypePatch,
};

typedef NS_ENUM(NSInteger, ESRequestRequestType)
{
    ESRequestRequestTypeHttp = 0,
    ESRequestRequestTypeJson,
    ESRequestRequestTypePrevious, //set the same serializer of previous request
};

typedef NS_ENUM(NSInteger, ESRequestResponseType)
{
    ESRequestResponseTypeHttp = 0,
    ESRequestResponseTypeJson,
    ESRequestResponseTypePrevious, //set the same serializer of previous request

};

typedef NS_ENUM(NSInteger, ESAcceptableContentType)
{
    ESAcceptableContentTypeAPPJSON = 0,
    ESAcceptableContentTypeTEXTJSON,
    ESAcceptableContentTypeTEXTJAVASCRIPT,
    ESAcceptableContentTypeTEXTHTML,
    ESAcceptableContentTypeAPPHALJSON,
    ESAcceptableContentTypeOctetStream,
};

typedef void (^ESSuccessResponse)(OnSuccessResponse *);
typedef void (^ESFailureResponse)(OnFailureResponse *);
typedef void (^ESUploadProgress)(NSProgress *);
typedef void (^ESDownloadProgress)(NSProgress *);


@interface ESRequest : NSObject
{
}

+ (NSString *)methodForMethodType:(ESRequestMethodType)method;
+ (ESRequestMethodType)methodTypeForMethod:(NSString *)method;
+ (NSString *)contentTypeForAcceptableContentType:(ESAcceptableContentType)acceptableContentType;

- (id)initWithRequest:(NSString *)request
           withHeaders:(NSDictionary *)headers
           params:(id)params
           body:(NSData *)body
           acceptableContentTypes:(NSArray *)acceptableContentTypes
           methodType:(ESRequestMethodType)methodType
         requestType:(ESRequestRequestType)requestType
         responseType:(ESRequestResponseType)responseType
      successCallBack:(ESSuccessResponse)successCallBack
      failureCallBack:(ESFailureResponse)failureCallBack;

- (id)initWithRequest:(NSString *)request
          withHeaders:(NSDictionary *)headers
               params:(id)params
                 body:(NSData *)body
acceptableContentTypes:(NSArray *)acceptableContentTypes
           methodType:(ESRequestMethodType)methodType
          requestType:(ESRequestRequestType)requestType
         responseType:(ESRequestResponseType)responseType
      successCallBack:(ESSuccessResponse)successCallBack
      failureCallBack:(ESFailureResponse)failureCallBack
       uploadProgress:(ESUploadProgress)uploadProgressCallBack
     downloadProgress:(ESDownloadProgress)downloadProgressCallBack;


@property (nonatomic, strong, readonly) NSString *request;
@property (nonatomic, strong, readonly) NSDictionary *headers;
@property (nonatomic, strong, readonly) id params;
@property (nonatomic, strong, readonly) NSData *body;
@property (nonatomic, strong, readonly) NSArray *acceptableContentTypes;
@property (nonatomic, unsafe_unretained, readonly) ESRequestMethodType methodType;
@property (nonatomic, unsafe_unretained, readonly) ESRequestRequestType requestType;
@property (nonatomic, unsafe_unretained, readonly) ESRequestResponseType responseType;

@property (nonatomic, strong, readonly) ESSuccessResponse successCompletionBlock;
@property (nonatomic, strong, readonly) ESFailureResponse failureCompletionBlock;
@property (nonatomic, strong, readonly) ESUploadProgress uploadProgressBlock;
@property (nonatomic, strong, readonly) ESDownloadProgress downloadProgressBlock;

@end
