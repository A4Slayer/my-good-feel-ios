//
//  ErrorCodes.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 21/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#ifndef ErrorCodes_h
#define ErrorCodes_h

/*!
 @enum NetworkAPIStatusCode
 
 @discussion contains error codes
 
 @constant NetworkAPIStatusCodeSuccess global success error code
 
 @constant NetworkAPIStatusCodeWrongUserPass wrong user name or password
 
 
 
 
 
 @constant NetworkAPIStatusCodeAccountInUse The account is logged in to from a different device
 
 @constant NetworkAPIStatusCodeUnknownError General error code
 
 @constant NetworkAPIStatusCodeInvalidSensor The sensor mac address is not recognized by the server. This sensor is not allowed to be used with the application
 
 @constant NetworkAPIStatusCodeBrokerFailed Failed to get a URL from the broker
 
 @constant NetworkAPIStatusCodeBadParameters Server error on bad parameters
 
 @constant NetworkAPIStatusCodeNoNetworking cannot perform action - no network connection
 
 @constant NetworkAPIStatusCodeNoAccountNetworking cannot perform action - not logged in
 
 @constant NetworkAPIStatusCodeUserAlreadyExists This user name already exists
 
 @constant NetworkAPIStatusCodeUserCannotFollowHimself User cannot follow himself
 
 @constant NetworkAPIStatusCodeFollowRequestPending There is already a pending following request for this user
 
 @constant NetworkAPIStatusCodeUserNotFound  no such user name exists
 
 
 */

typedef enum NetworkAPIStatusCode
{
    NetworkAPIStatusCodeStart = -1,
    /*0*/ NetworkAPIStatusCodeSuccess,
    /*1*/ NetworkAPIStatusCodeWrongUserPass,
    /*2*/ NetworkAPIStatusCodeAccountInUse,
    /*3*/ NetworkAPIStatusCodeUnknownError,
    /*4*/ NetworkAPIStatusCodeInvalidSensor,
    /*5*/ NetworkAPIStatusCodeBrokerFailed,
    /*6*/ NetworkAPIStatusCodeBadParameters,
    /*7*/ NetworkAPIStatusCodeNoNetworking,
    /*8*/ NetworkAPIStatusCodeNoAccountNetworking,
    /*9*/ NetworkAPIStatusCodeUserAlreadyExists,
    /*10*/ NetworkAPIStatusCodeUserCannotFollowHimself,
    /*11*/ NetworkAPIStatusCodeFollowRequestPending,
    /*12*/ NetworkAPIStatusCodeUserNotFound,
    /*13*/ NetworkAPIStatusCodeEND,
    /*14*/ NetworkAPIStatusResourcesDosentExist,
    /*15*/ NetworkAPIStatusResourcesIsUpToDate,
    /*16*/ NetworkAPIStatusFolloweeReachedMax,
    /*17*/ NetworkAPIStatusNoPermission,
    /*18*/ NetworkAPIStatusUserIsNotManager,
    /*19*/ NetworkAPIStatusCannotDeleteLastManager,
    /*20*/ NetworkAPIStatusNetworkError,
    /*21*/ NetworkAPIStatusFollowingInviteExists,
    /*22*/ NetworkAPIStatusRelationExists,
    /*23*/ NetworkAPIStatusRequestExistedRelationCreated,
    /*24*/ NetworkAPIStatusUploadLogFileTooLarge,
    /*25*/ NetworkAPIStatusNoPermissionToAccessData
} NetworkAPIStatusCode;


#endif /* ErrorCodes_h */
