#ifndef ESSDK_h
#define ESSDK_h

@interface Earlysense : NSObject

/**
 initialize the sdk
 
 @param appName - a string provided by EarlySense
 @param version - free version string meaningful to the app
 @param suffix - a string provided by EarlySense
 @param appId - a number provided by EarlySense
 @param urlBroker - optional (can be left nil) - provided by EarlySense
 */
+ (void)init:(NSString*)appName
  appVersion:(NSString*)version
      suffix:(NSString*)suffix
       appId:(NSInteger)appId
   urlBroker:(NSString*)urlBroker;

/**
 query for current sdk version

 @return sdk version
 */

+ (NSString*)sdkVersion;

@end

#endif
