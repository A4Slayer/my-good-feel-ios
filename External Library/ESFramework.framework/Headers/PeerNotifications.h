//
// Created by Vlad Zamskoi on 5/3/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RequestToFollowMe;
@class ResponseToMyFollowRequest;
@class InvitationToFollowSomebody;

@interface PeerNotifications : NSObject

@property(nonatomic, readonly) NSMutableArray<RequestToFollowMe *> *requestsToFollowMe;
@property(nonatomic, readonly) NSMutableArray<ResponseToMyFollowRequest *> *responsesToMyFollowRequests;
@property(nonatomic, readonly) NSMutableArray<InvitationToFollowSomebody *> *invitationsToFollowSomebody;
@property(nonatomic, readonly) BOOL isEmpty;

- (void)addRequestToFollowMe:(RequestToFollowMe *)request;
- (void)addResponseToMyFollowRequest:(ResponseToMyFollowRequest *)response;
- (void)addInvitationToFollowSomebody:(InvitationToFollowSomebody *)invitation;
@end
