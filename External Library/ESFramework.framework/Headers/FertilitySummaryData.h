//
//  FertilitySummaryData.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 19/04/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FertilitySummary.h"
#import "FertilityDailySleepSummary.h"

@interface FertilitySummaryData : NSObject

@property FertilitySummary* fertilitySummary;
@property FertilityDailySleepSummary* fertilityDailySleepSummary;

@end
