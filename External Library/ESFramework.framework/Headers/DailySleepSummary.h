//
//  DailySleepSummary.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 30/04/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum ComplianceData
{
    ComplianceDataNoDataRecorded,
    ComplianceDataSufficientDataRecorded,
    ComplianceDataLowSignal,
    ComplianceDataInsufficientDataRecorded
} ComplianceData;

typedef enum SleepQuality
{
    noSleep,
    poor,
    good,
    great
} SleepQuality;

static const NSString* sleep_compliance_data = @"compliance_data";
static const NSString* sleep_relaxation = @"relaxation";
static const NSString* sleep_day = @"sleep_summary_day";
static const NSString* sleep_year = @"sleep_summary_year";
static const NSString* sleep_month = @"sleep_summary_month";
static const NSString* sleep_time = @"total_sleep_time";
static const NSString* sleep_quality = @"sleep_quality";
static const NSString* average_hr = @"avg_HR";
static const NSString* average_rr = @"avg_RR";
static const NSString* tip_category = @"tip_category";
static const NSString* tip_index = @"tip_index";

@interface DailySleepSummary : NSObject

@property NSInteger day;
@property NSInteger month;
@property NSInteger year;

@property SleepQuality sleepQuality;
@property float relaxation;
@property NSInteger totalSleepTime;
@property ComplianceData complianceData;
@property NSInteger averageHR;
@property NSInteger averageRR;
@property NSInteger tipCategory;
@property NSInteger tipIndex;

@end


