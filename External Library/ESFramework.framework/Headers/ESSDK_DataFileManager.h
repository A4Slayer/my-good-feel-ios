#ifndef ESSDK_DataFileManager_h
#define ESSDK_DataFileManager_h

@interface DataFileManager : NSObject

+ (DataFileManager * _Nonnull)sharedInstance;

#pragma mark - Client - Public Methods

- (void)writeDebugDataToLog:(NSString * _Nonnull)key andDebugData:(NSString * _Nonnull)debugData;

- (nonnull NSArray*)getLogFiles;

- (float)getDeviceFreeStorageSpaceMB;
- (NSString * _Nonnull)getMemoryUsageStatsString;

@end

#endif /* ESSDK_DataFileManager_h */
