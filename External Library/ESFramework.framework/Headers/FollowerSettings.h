//
//  FollowerSettings.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 05/07/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FollowerSettings : NSObject

@property (nonatomic) BOOL shareBreathingInterruptionsNotifications;
@property (nonatomic) BOOL shareOutOfBedNotifications;
@property (nonatomic) BOOL shareRRNotifications;
@property (nonatomic) BOOL shareHRNotifications;
@property (nonatomic) BOOL shareSleepMonitoringData;
@property (nonatomic) BOOL shareSleepSummaryData;

- (instancetype)initWithDataFrom:(NSDictionary*)sleepShareData;
- (NSDictionary*)convertToDictionary;

@end
