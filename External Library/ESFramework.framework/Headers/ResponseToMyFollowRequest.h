//
// Created by Vlad Zamskoi on 5/3/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ResponseToMyFollowRequest : NSObject

@property(nonatomic) NSString *emailOfSender;
@property(nonatomic) NSString *nameOfSender;
@property(nonatomic) BOOL isRequestAccepted;

@end