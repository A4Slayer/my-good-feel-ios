//
//  FolloweeManager.h
//  EarlySnseFramework
//
//  Created by Rebecca Biaz on 20/11/2017.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FollowerSettings.h"
#import "ErrorCodes.h"
/**
 * class FolloweeManager
 *
 * This singleton class is manager for followee user.
 *
 */

#define  FolloweeManagerNotificationSettingsUpdated         @"FolloweeManagerNotificationSettingsUpdated"

@interface FolloweeManager : NSObject

+ (FolloweeManager*)sharedInstance;

/**
 * startFollowUser. start to follow user, ask for permision to get data and start polling.
 *
 * @param userEmail user email of witch we want to follow
 * @param completion block to receive the call back, if we have permission to follow the user
 *
 */
- (void)startFollowUser:(NSString*)userEmail withCompletion:(void (^)(enum NetworkAPIStatusCode, FollowerSettings *))completion;

/**
 *
 * stopFollow. stop to follow the current user followed, unsubscribe and stop polling.
 *
 */
- (void)stopFollow;

/**
 *
 * @return if we are polling the current user (every 5 seconds).
 *
 */
- (BOOL)isPolling;

/**
 *
 * @return the followee settings of the current user that we follow.
 *
 */
- (FollowerSettings *)currentFolloweeSettings;

@end
