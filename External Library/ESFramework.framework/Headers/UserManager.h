#import <Foundation/Foundation.h>
#import "ErrorCodes.h"

@interface User : NSObject <NSCoding, NSCopying>

@property (nonatomic) NSString* email;
@property (nonatomic) NSString* uuid;
@property (nonatomic) NSString* password;
@property (nonatomic) NSString* name;
@property (nonatomic) NSInteger birthdateUnixtime;
@property (nonatomic) NSInteger height;
@property (nonatomic) NSInteger weight;
@property (nonatomic) NSInteger peopleInBed;
@property (nonatomic) NSInteger gender;
@property (nonatomic) NSInteger maxHrv;
@property (nonatomic) NSInteger minHrv;
@property (nonatomic) NSInteger hrvThreshold1;
@property (nonatomic) NSInteger hrvThreshold2;
@property (nonatomic) NSInteger firstSummaryIdSaved;
@property (nonatomic) NSInteger lastSummaryIdSaved;
@property (nonatomic) BOOL hasSensor;
@property (nonatomic) NSInteger packageLevel;
@property (nonatomic) NSInteger lastLoginFromApp;
@property (nonatomic) BOOL use24HoursFormat;
@property (nonatomic) BOOL useImperialUnits;

@property (nonatomic, readonly) BOOL hasPremiumAccount;
@property (nonatomic, readonly) BOOL wasLoggedInEverBefore;


//fertility params
@property (nonatomic) NSInteger periodLength;
@property (nonatomic) NSInteger cycleLength;
@property (nonatomic) BOOL pregnancyMode;

/**
 set a key/value pair (both key and value must be numbers) asociated with the user
 
 @param key the key to set
 @parm value the value to set for the key
*/
- (void)setCustomKey:(NSInteger)key withValue:(NSInteger)value;
- (void)removeCustomKey:(NSInteger)key;

/**
get value asociated with a custom key

@param key the key to get

@return an int with the value or nil if no such key exists
*/
- (NSInteger)getValueForCustomKey:(NSInteger)key;

- (NSDictionary*)getCustomKeys;

@end

@interface UserManager : NSObject
+ (UserManager*)sharedInstance;

@property (nonatomic) User* loggedInUser;

/**
 get the currently logged in user
 
 @return User object containing the logged in user, or nil if we are not logged in
 */
- (nullable User*) getUser;

/**
 update the currently logged in user
 
 @param user a User object to use for the update
 @param performConfigure - update the algorithm with the changes
 @param completion the completion block - if the completion called is not called with success it means the changes could not be saved to the server and will not take place
 */
- (void)updateUser:(User *)user shouldConfigure:(BOOL)performConfigure withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;

/**
 update the currently logged in user
 
 @param user a User object to use for the update
 
 @param completion the completion block - if the completion called is not called with success it means the changes could not be saved to the server and will not take place
 */
- (void)updateUser:(User *)user withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;

/**
 register a new user
 
 @param email will be used as the user name, must be unique in the server
 @param password password for the new user
 @param completion returnCode
 
 */
- (void)registerWithEmail:(NSString*)email
              andPassword:(NSString*)password
           withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 log in
 
 @param email the user's email
 @param password password of the user
 @customKeys optional dictionary of custom keys supported by the server for this application
 @param completion the completion block
 
 */
- (void)loginWithEmail:(NSString*)email
           andPassword:(NSString*)password
            customKeys:(NSDictionary*)customKeys
                 force:(BOOL)force withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode, User* userData))completion;

/**
 check if we are currently logged in
 
 @return true if we are logged in
 */
- (BOOL)isLoggedIn;

/**
 request to change the logged in user's password
 
 @param currentPassword the current password
 @param updatedPassword the new password
 @param completion the completion block
 
 */
- (void)changePassword:(NSString*)currentPassword
            toPassword:(NSString*)updatedPassword
        withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 request password restore
 
 @param email the user's email - the server will send a password restore link to this mail if it is recognized
 @param completion the completion block
 
 */
- (void)forgotPasswordWithEmail:(NSString*)email
                 withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 logs out the currently logged in user
 
 @param completion the completion block
 
 */
- (void)logoutWithCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

- (void)clearUserData;


@end
