//
//  NotificationThresholdsExtra.h
//  EarlySnseFramework
//
//  Created by Mumen Shabaro on 7/25/17.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationThresholds.h"
#import "NotificationThresholdsLimits.h"

@interface NotificationThresholdsExtra : NotificationThresholds

- (id)initWithDataFrom:(NSDictionary*)thresholdsDataDictionary;

-(NotificationThresholdsLimits *) getNotificationThresholdsLimits;
-(void) updateThresholdsHighHR:(NSInteger)highHR andhighRR:(NSInteger)highRR andLowRR:(NSInteger)lowRR andLowHR:(NSInteger)lowHR andHourFromMinutes:(NSInteger)hourFromMinutes andHourToMinutes:(NSInteger)hourToMinutes andIsOOBEnabled:(BOOL)isOOBEnabled;
-(void) setNotificationThresholdsLimits:(NotificationThresholdsLimits *)notificationThresholdsLimits;
@end
