//
//  HeadlessHubSensor.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 25/01/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HeadlessHubSensor : NSObject

@property (nonatomic) NSString* name;
@property (nonatomic) NSString* macAdress;
@property (nonatomic) NSInteger signallevel;
@property (nonatomic) NSString* alias;

@end
