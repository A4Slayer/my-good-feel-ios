#ifndef ESSDK_NetworkAPIManager_h
#define ESSDK_NetworkAPIManager_h

#import "TipObject.h"
#import "SeniorCareSettings.h"
#import "Diary.h"
#import "FollowerSettings.h"
#import "NotificationThresholdsExtra.h"
#import "ErrorCodes.h"
#import "PeerUser.h"
#import "RequestExecutor.h"
#import "RequestFactory.h"
#import "ExportSleepSummary.h"

@class User;


/*!
 @enum NetworkAPINotificationType
 
 @discussion contains constant string with the names of the different NSNotifications
 which can be posted by the NetworkAPIManager class
 
 @constant NetworkAPINotificationTypeInvalidSensor fired when the server indicates that the connected
 sensor is not recognized or is already asociated with a different account
 
 @constant NetworkAPINotificationTypeAccountInUse fired when a login response from the server indicates the account 
 is already in use from a different device
 
 @constant NetworkAPINotificationSleepSummaryAvailable fired to notify that a new sleep summary for the current sleep session is available
 
 @constant NetworkAPINotificationFolloweeRealtimeDataAvailable fired whenever up to date real time parameters about the current sleep (heart rate, respiration rate, etc) are available
 
 @constant NetworkAPINotificationFolloweeAddedSensor fired to notify the app that the user has just registered a sensor
 
 @constant NetworkAPINotificationOfflinePacketsAvailable fired to indicate that the database contains unsent packets from a previous sleep session
 
 @constant NetworkAPINotificationOfflinePacketsSent fired when the sdk is done uploading unsent packets to the server
 
 @constant NetworkAPINotificationOfflinePacketsRestoreError fired when uploading unsent packets failed
 
 @constant NetworkAPINotificationTypeForceLogout fired when as a result of trying to perform a network action the sdk detects that the same account is now signed in
 from a different location and the current user should be logged out
 
 @constant NetworkAPINotificationOfflinePacketsRestoreComplete fired to indicate offline packets have been uploaded succesfully
 
 @constant NetworkAPINotificationTypeNetworkLost fired when lose of network connection is detected
 
 @constant NetworkAPINotificationTypeNetworkOnline fired when recovery of network connection is detected
 
 @constant NetworkAPINotificationTypeNetworkLost fired when lose of network connection is detected
 
 @constant NetworkAPINotificationSensorNoData fired when a connected sensor is not sending us data for 15 seconds
 
 @constant NetworkAPINotificationTypeNetworkLost fired when lose of network connection is detected
 
 */

enum NetworkAPINotificationType
{
	NetworkAPINotificationTypeInvalidSensor,
	NetworkAPINotificationTypeAccountInUse,
    NetworkAPINotificationSleepSummaryAvailable,
    NetworkAPINotificationFolloweeRealtimeDataAvailable,
    NetworkAPINotificationFolloweeAddedSensor,
    NetworkAPINotificationOfflinePacketsAvailable,
    NetworkAPINotificationOfflinePacketsSent,
	NetworkAPINotificationOfflinePacketsRestoreCompleteWithHypno,
	NetworkAPINotificationOfflinePacketsRestoreCompleteWithoutHypno,
	NetworkAPINotificationOfflinePacketsRestoreError,
	NetworkAPINotificationTypeForceLogout,
	NetworkAPINotificationOfflinePacketsRestoreComplete,
	NetworkAPINotificationTypeNetworkLost,
	NetworkAPINotificationTypeNetworkOnline,
    NetworkAPINotificationActionPerformedWhenThereNoInternetConnection,
    NetworkAPINotificationSensorNoData,
    NetworkAPINotificationSensorHasData,
    NetworkAPINotificationSensorNoDataReset,
    NetworkAPINotificationConfigureDone,
    NetworkAPINotificationPendingSummaryDone
};

extern NSString* const NetworkAPINotificationSleepSummaryAvailableSessionKey;
extern NSString* const NetworkAPINotificationSleepSummaryAvailableUserKey;

/*!
 @discussion This class contains methods for:<p>
 Managing user related actions with the server (register, login, recover a password)<p>
 Managing follower/followee relationships (sending a follow request, querying for a list of
 the logged in user's followers and followees<p>
 Managing the list of followers to whom the user wants to send a daily report to<p>
 Checking if there is any sensor data stored in the local database that was not uploaded to the server (and requesting to send it)<p>
 
 
 */


@interface NetworkAPIManager : NSObject
@property (nonatomic) BOOL loggedInUserHasSensor;
@property (nonatomic) BOOL uploadOfflineDataAutomatically;
@property (nonatomic) BOOL pendingSummaryRefresh;
+ (NetworkAPIManager*)sharedInstance;
+ (void)setCustomServer:(NSString*)url;
- (BOOL)executeRequestIfNetworkAllowed:(ESRequest *)request;

#pragma mark - Client - Public Methods

// Sensor Handling

/**
 register the sensor with the server
 
 @param sensorMAC MAC address of the sensor
 
 */
- (void)registerSensor:(NSString*)sensorMAC withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

//followee related


/**
 Prepare to start monitoring a followee's real time measurements

 The completion block will be called with a status code (success or an error) and a FollowerSettings object.
 Refer to the contents of the FollowerSettings object to see which information this followee is willing to share
 
 @warning This method declares the intent to monitor a user and returns the FollowerSetting indicating what this user is sharing, but
 it does not start the actual monitoring. To start monitoring call startPollingRealtimeDataForUser

 @param userEmail registered email of the user we want to monitor
 @param completion completion block 
 
 */
- (void)startRealtimeMonitoringForUser:(NSString*)userEmail
                        withCompletion:(void (^)(enum NetworkAPIStatusCode code, FollowerSettings* settings))completion;


/**
 Start monitoring a followee's real time measurements

 @param userEmail registered email of the user we want to monitor
 */
- (void)startPollingRealtimeDataForUser:(NSString*)userEmail;


/**
 Stop monitoring a followee's real time measurements
 
 Call this method when real time measurements of a followee are no longer required.
 */
- (void)stopRealtimeMonitoringForUser:(NSString *)userEmail withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;

- (void)getRealimeDataForUser:(NSString *)userEmail
                   OnComplete:(void(^)(enum NetworkAPIStatusCode returnCode, id data, NSError *networkError))completion; //without polling
/**
 Send a following request to a user

 @param email registered email of the user we want to follow
 @param completion completion block ( will be called with the result of the action )
 */
- (void)sendFollowRequest:(NSString*)email withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;


/**
 Reply (accept or decline) to a following request

 @param email registered email of the user who sent the follow request
 @param requestAccepted boolean, send YES to accept or NO to decline
 @param completion completion block ( will be called with the result of the action )
 */
- (void)replyToFollowRequest:(NSString*)email requestAccepted:(BOOL)requestAccepted withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;


/**
 Use this method to request a list of peers. Peers are users you are following, users who are following you, <p>or pending followees (users who have been
 sent a follow request but have not yet replied to it)

 @param completion completion block ( will be called with the result of the action )
 */
- (void)getPeersWithCompletion:(void (^)(enum NetworkAPIStatusCode returnCode, NSArray* peerUsers))completion;


/**
 Use this method to request current list of peer notifications. Those can be either replies to follow requests <p>
 sent by you or incoming following requests sent to you by other users.

 @param completion block ( will be called with the result of the action )
 */
- (void)getPeerNotificationsWithCompletion:(void (^)(enum NetworkAPIStatusCode returnCode, NSDictionary* userData))completion;


/**
 Get the follower settings for one of the logged in user's followers

 @param followerEmail registered email of the follower
 @param completion completion block ( will be called with the result of the action )
 */
- (void)getFollowerSettings:(NSString*)followerEmail withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode, FollowerSettings* settings))completion;


/**
 Set specific follower settings for one of the logged in user's followers

 @param followerEmail registered email of the follower
 @param setting FollowerSett
 @param completion completion block ( will be called with the result of the action )
 */
- (void)setFollowerSetting:(NSString*)followerEmail
                   settings:(FollowerSettings*)setting
            withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;


/**
 Get the logged in user's notification thresholds<p>
 Notifictation thresholds indicates the threshold heart rate and respiration rate values above which to alert the user and his followers<br>
 As well as the time period (start and end hour) between which an alert should be sent to the follower if the user is found to be<br>
 out of bed for more than 25 minutes

 @param completion completion block ( will be called with the result of the action )
 */
- (void)getNotificationThresholdsWithCompletion:(void (^)(enum NetworkAPIStatusCode returnCode, NotificationThresholdsExtra* settings))completion;

/**
 Get the logged in user's default notification thresholds<p>
 Notifictation thresholds indicates the threshold heart rate and respiration rate values above which to alert the user and his followers<br>
 As well as the time period (start and end hour) between which an alert should be sent to the follower if the user is found to be<br>
 out of bed for more than 25 minutes
 
 @param completion completion block ( will be called with the result of the action )
 */
- (void)getDefaultShareDataThresholdsLimitsWithCompletion:(void (^)(enum NetworkAPIStatusCode returnCode, NotificationThresholdsExtra* settings))completion;

/**
 Set the logged in user's notification thresholds<p>

 @param settings NotificationThresholds object filled with the desired values
 @param completion completion block ( will be called with the result of the action )
 */
- (void)setNotificationThresholds:(NotificationThresholdsExtra*)settings
                   withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 *  Remove a follower from the logged in users followers list
 *
 *  @param email registered email of the follower
 *  @param completion completion block ( will be called with the result of the action )
 */
- (void)removeFollower:(NSString *)email
        withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;


/**
 *  Remove a followee from the list of user the logged in user is following
 *
 *  @param email registered email of the followee
 *  @param completion completion block ( will be called with the result of the action )
 */
- (void)removeFollowee:(NSString *)email
        withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;


/**
 get the user name (email) of the currently logged in user
 
 @return user name (email) of the currently logged in user
 */
- (NSString*)getUsername;

// Network state


/**
 check if there is a network connection

 @return true if connected to the internet
 */
- (BOOL)networkState;


/**
 checks if there is an active wifi connection

 @return true if there is an active wifi connection
 */
- (BOOL)networkStateWifi;


/**
 checks if there is an active cellular data connection

 @return true if there is an active cellular data connection
 */
- (BOOL)networkStateCellular;

// Account Handling

/**
 register a new user
 
 @param email will be used as the user name, must be unique in the server
 @param name full name of the user
 @param birthDayTimeStamp the user's birthday in unix time
 @param height height of the new user
 @param weight weight of the new user
 @param peopleInBed number of people in bed
 @param gender gender of the new user
 @param targetSleepScore targetSleepScore of the new user
 @param targetSleepTime targetSleepTime of the new user
 @param newsletter newsletter of the new user
 @param recommendGotoSleep recommendGotoSleep of the new user
 @param hasSensor true if registering a user with a sensor, otherwise the user is only a follower
 @param completion the completion block
 */
- (void)registerWithEmail:(NSString*)email andPassword:(NSString*)password
                     name:(NSString*)name
        birthDayTimeStamp:(NSInteger)birthDayTimeStamp
                   height:(NSInteger)height
                   weight:(NSInteger)weight
             peopleInBedd:(NSInteger)peopleInBed
                   gender:(NSInteger)gender
         targetSleepScore:(NSInteger)targetSleepScore
          targetSleepTime:(NSInteger)targetSleepTime
               newsletter:(NSInteger)newsletter
       recommendGotoSleep:(NSInteger)recommendGotoSleep
                hasSensor:(BOOL)hasSensor
           withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

- (void)registerWithEmail:(NSString*)email
              andPassword:(NSString*)password
           withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

- (void)registerWithEmail:(NSString *)email
              andPassword:(NSString *)password
                 fullName:(NSString *)fullName
           withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;


- (void)isEmailExist:(NSString*)email
      withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;


/**
 log in
 
 @param email the user's email
 @param password password of the user
 @customKeys optional dictionary of custom keys supported by the server for this application
 @param completion the completion block
 
 */
- (void)loginWithEmail:(NSString*)email andPassword:(NSString*)password customKeys:(NSDictionary*)customKeys force:(BOOL)force withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode, User* userData))completion;
- (void)loginWithEmail:(NSString*)email andPassword:(NSString*)password customHeaderKeys:(NSDictionary*)customHeaderKeys customKeys:(NSDictionary*)customKeys force:(BOOL)force withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode, User* userData))completion;

/**
 check if we are currently logged in
 
 @return true if we are logged in
 */
- (BOOL)isLoggedIn;

/**
 request to change the logged in user's password
 
 @param currentPassword the current password
 @param updatedPassword the new password
 @param completion the completion block
 
 */
- (void)changePassword:(NSString*)currentPassword toPassword:(NSString*)updatedPassword withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 request password restore
 
 @param email the user's email - the server will send a password restore link to this mail if it is recognized
 @param completion the completion block
 
 */
- (void)forgotPasswordWithEmail:(NSString*)email withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 logs out the currently logged in user
 
 @param completion the completion block
 
 */
- (void)logoutWithCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

//data recovery

/**
 checks if there are any unsent sleep data packets stored on the device returns the size of the total unsent packets in bytes (zero if no unsent data)
 
 @return number of bytes of data available for restore
 */
- (NSInteger)checkForUnsentRealTimeData;


/**
 deletes all unsent sleep data packets stored on the device
 
 */
- (void)deleteUnsentRealTimeData;


/**
 Check if app send offline data to server
 
 @return true if sending data to server, otherwhise false. Default is false
 */

- (BOOL)isSendingOfflineData;

// Summary Data

- (void)getTip:(int)tipIndex withCategory:(int)tipCategory withCompletion:(void (^)( enum NetworkAPIStatusCode code, TipObject *tipObject))completion;

- (void)setDayStartTime:(NSInteger)startTime;
-(NSInteger)getDayStartTime;

- (void)getSummaryForUserId:(NSString*)userId forYear:(NSInteger)year forMonth:(NSInteger)month forDay:(NSInteger)day withCompletion:(void (^)(enum NetworkAPIStatusCode code,NSArray* sleepSessions))completion;

- (void)getGeneralSummaryForUserId:(NSString*)userId forYear:(NSInteger)year forMonth:(NSInteger)month forDay:(NSInteger)day numberOfDays:(NSInteger)numberOfDays withCompletion:(void (^)(NSArray* sleepSessions))completion;

//diary

- (void)getDiaryForUserId:(NSString*)userId  fetchFromServer:(BOOL)fetchFromServer forYear:(NSInteger)year forMonth:(NSInteger)month forDay:(NSInteger)day withCompletion:(void (^)(Diary* diary))completion;

- (void)setDiaryForUserId:(NSString*)userid diaryId:(NSString*)diaryId alcohol:(NSInteger)alcohol caffeine:(NSInteger)caffeine fitness:(NSInteger)fitness meal:(NSInteger)meal stress:(NSInteger)stress mood:(NSInteger)mood comments:(NSString*)comments withCompletion:(void (^)(Diary* diary))completion;


//emailed reports

/**
 activate / deactivate the senior care feature
 
 @param isActive bool
 @param completion completion block ( will be called with the result of the action )
 
 */
- (void)activateSeniorCare:(BOOL)isActive withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 add an email to the senior care mailing list
 
 @param email email to add
 @param name name asociated with this email
 @param completion completion block ( will be called with the result of the action )
 
 */
- (void)addContactToSeniorEmailList:(NSString*)email name:(NSString*)name withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 remove an email from the senior care mailing list
 
 @param email email to remove
 @param completion completion block ( will be called with the result of the action )
 
 */
- (void)removeContactFromEmailList:(NSString*)email withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 add set the time for senior care email to be sent
 
 @param timeToSend seconds from midnight ,If the user chose to send the email at 8 AM, in seconds = 8*3600
 @param completion completion block ( will be called with the result of the action )
 
 */
- (void)setTimeToSendSeniorCareEmail:(NSInteger)timeToSend withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 Gets email report settings

 @param completion completion block ( will be called with the result of the action )
 */
- (void)getSeniorCareSettingsWithCompletion:(void (^)(enum NetworkAPIStatusCode returnCode, SeniorCareSettings* settings))completion;


/**
 Sets email report settings

 @param settings SeniorCareSettings object filled with the desired values
 @param completion completion block ( will be called with the result of the action )
 */
- (void)setSeniorCareSettings:(SeniorCareSettings*)settings withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

/**
 set all settings of the senior care feature
 
 @param isActive bool
 @param timeToSend seconds from midnight ,If the user chose to send the email at 8 AM, in seconds = 8*3600
 @param contactList array of strings containing list of emails
 @param completion the completion block
 
 */
- (void)setSeniorCareSettings:(BOOL)isActive timeToSend:(NSInteger)timeToSend contactList:(NSString*)contactList withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

//push notifications (fcm)

- (void)emailFertilitySummary:(void (^)(enum NetworkAPIStatusCode, NSDictionary *))completion;

/**
 Provide the sdk with a Google Firebase FCM token that will be used by the server to send push notifications

 @param token the FCM token
 @param completion completion block ( will be called with the result of the action )
 */
- (void)setFcmToken:(NSString*)token withCompletion:(void (^)(enum NetworkAPIStatusCode returnCode))completion;

//Images


/**
 Upload profile image for the logged in user

 @param base64Img base 64 encoding of the image
 @param completion completion block ( will be called with the result of the action )
 */
- (void)uploadImage:(NSString *)base64Img withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;

/**
 Download profile image for user

 @param user registered email of the user
 @param timeStamp the timestamp  sec
 @param isFallowing is following
 @param completion completion block ( will be called with the result of the action )
 */
- (void)downloadImage:(NSString*)user timeStamp:(long)timeStamp isFollowing:(BOOL)isFallowing completion:(void (^)(NSDictionary *, enum NetworkAPIStatusCode))completion;

/**
 Upload log
 
 @param logPath path file
 @param checkSum the checksum of the log file
 @param completion completion block ( will be called with the result of the action )
 */
- (void)uploadLog:(NSString *)logPath checkSum:(int)checkSum withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;

/*get sleep session with a specific id - public for QA use in Developer screen
 @pararm sessionId - the id tapped by the tester*/

- (void)getSleepSessionWithId:(NSInteger)sessionId;
- (void)getSleepSessionWithId:(NSInteger)sessionId forUsername:(NSString *)username withCompletion:(void (^) (NetworkAPIStatusCode))completion;

- (void)refreshSleepSummary;

/*get sleep session with a specific id - public for QA use in Developer screen
 @pararm sessionId - the id tapped by the tester
 @param completion - completion handler for server resposnse code
 */
-(void)getSleepSessionForDeveloper:(NSInteger)sessionId withCompletion:(void (^) (enum NetworkAPIStatusCode))completion;

#pragma mark - Notifications


/**
 This method is the correct way to obtain the NSNotification name (NSString) for a given type of notification<br>
 that can be posted by the SDK

 @param type The notification type
 @return The NSNotification name string to use
 */
- (NSString*)notificationForType:(enum NetworkAPINotificationType)type;

#pragma mark - export

/**
 exportSleepSummaryDataWithExportSleepSummary - email a report to the user with his sleep data
 @param ExportSleepSummary - object that hold SessionReportData, email to send the export mail, datesList - list of dates to be included in the report, and report type.
 @return
 */
- (void)exportSleepSummaryDataWithExportSleepSummary:(ExportSleepSummary *)exportSleepSummary withCompletion:(void (^)(enum NetworkAPIStatusCode))completion;

/**
 exportSleepSummaryData - email a report to the user with his sleep data
 @param datesList - list of dates to be included in the report
 @return
 */
-(void)exportSleepSummaryData:(NSArray *_Nonnull)datesList withCompletion:(void(^_Nullable)(enum NetworkAPIStatusCode))completion ;

/**
 exportSleepSummaryData - email a report to the user with a followee's  sleep data
 @param datesList - list of dates to be included in the report
 @param followeeName - the  followee userName
 @return
 */
-(void)exportFolloweeSleepSummaryData:(NSArray *_Nonnull)datesList followeeName:(NSString *_Nonnull)user withCompletion:(void(^_Nullable)(enum NetworkAPIStatusCode))completion;

#pragma mark - Confirm Disclaimer

- (void)confirmDisclaimer:(NSTimeInterval)timestamp deviceVersion:(NSString *_Nonnull)version withCompletion:(void(^_Nullable)(enum NetworkAPIStatusCode))completion;

@end

#endif /* ESSDK_NetworkAPIManager_h */
