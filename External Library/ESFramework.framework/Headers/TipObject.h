//
//  TipObject.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 13/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TipObject : NSObject

@property (nonatomic) NSString* tipBody;
@property (nonatomic) NSString* categoryBody;
@property (nonatomic) NSInteger tipIndex;
@property (nonatomic) NSInteger categoryIndex;

@end
