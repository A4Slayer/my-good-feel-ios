#import <Foundation/Foundation.h>
#import "ESSDK_NetworkAPIManager.h"

#import "TipObject.h"
#import "Diary.h"
#import "SleepSummaryComplete.h"

@interface HistoryManager: NSObject
+ (HistoryManager*)sharedInstance;


//diary

/**
 get a user's diary for a specific date
 
 @param userId user ID of the user for whom we want to fetch a diary
 @param year --
 @param month --
 @param day year/month/day combination of the date we want to fetch a diary for
 @param completion the completion block
 
 */
- (void)getDiaryForUserId:(NSString*)userId
                  forYear:(NSInteger)year
                 forMonth:(NSInteger)month
                   forDay:(NSInteger)day
           withCompletion:(void (^)(Diary* diary))completion;

/**
 udpate the a user's diary for current date
 
 @param userid user ID of the user for whom we want to update the diary
 @param diaryId if this is the first update being made for the current day's diary, pass nil for diaryId. The diary will be created on the server and the dictionary passed to the completion block will contain the diaryId. If this is not the first update and the app has a diaryId, pass this diaryId
 @param alcohol --
 @param caffeine --
 @param fitness --
 @param meal --
 @param stress --
 @param comments --
 @param completion the completion block
 
 */
- (void)setDiaryForUserId:(NSString *)userid diaryId:(NSString *)diaryId alcohol:(NSInteger)alcohol caffeine:(NSInteger)caffeine fitness:(NSInteger)fitness meal:(NSInteger)meal stress:(NSInteger)stress mood:(NSInteger)mood comments:(NSString *)comments withCompletion:(void (^)(Diary*))completion;
- (void)setDiaryForUserId:(NSString *)userid diaryTimestamp:(NSNumber *)diaryTimestamp alcohol:(NSInteger)alcohol caffeine:(NSInteger)caffeine fitness:(NSInteger)fitness meal:(NSInteger)meal stress:(NSInteger)stress mood:(NSInteger)mood comments:(NSString *)comments withCompletion:(void (^)(Diary*))completion;

//set the time (hours between 0 and 23) that will be used to decide which sleep sessions belong to a date.

/**
 set the time (hours between 0 and 23) that will be used to decide which sleep sessions belong to a date
 
 @param startTime example: if this is set to 20, when the sdk is requested to return all sleep sessions in a given date it will returns sessions that started betwen 8PM of the day before the date to 7.59PM in the date itselfthe sdk default for this is 20 (8 pm)whenever this param is set by the app, the local sleep history (all saved daily detailed sleep sessions and the shorted sleep summaries) is deleted. it is only deleted locally and will be fetched from the server again when needed
 
 
 */
- (void)setDayStartTime:(NSInteger)startTime;


- (NSInteger)getDayStartTime;

/**
 get all sleep sessions logged for this user in a give date
 
 @param userId user ID of the user for whom we want to fetch
 @param year --
 @param month --
 @param day year/month/day combination of the date we want to fetch sleep sessions for
 @param completion the completion block
 
 */
- (void)getSummaryForUserId:(NSString*)userId
                    forYear:(NSInteger)year
                   forMonth:(NSInteger)month
                     forDay:(NSInteger)day
             withCompletion:(void (^)(enum NetworkAPIStatusCode code, NSArray* sleepSessions))completion;

/**
 get reduced sleep summaries for a range of dates for this user
 
 @param userId user ID of the user for whom we want to fetch sleep summaries for
 @param year --
 @param month --
 @param day year/month/day combination of the date that marks the start of the range
 @param numberOfDays together with the previous day/month/year params defines the range requested
 @param completion the completion block
 
 */
- (void)getGeneralSummaryForUserId:(NSString*)userId
                           forYear:(NSInteger)year
                          forMonth:(NSInteger)month
                            forDay:(NSInteger)day
                      numberOfDays:(NSInteger)numberOfDays
                    withCompletion:(void (^)(enum NetworkAPIStatusCode code, NSArray* sleepSessions))completion;

/**
 get the display texts asociated with a given tip index and tip category
 
 @param tipIndex tip index number
 @param tipCategory tip category number
 @param completion the completion block
 
 */
- (void)getTip:(int)tipIndex withCategory:(int)tipCategory withCompletion:(void (^)( enum NetworkAPIStatusCode code, TipObject *tipObject))completion;

#pragma mark - History & Sleep Session Management

#pragma mark - Utility Methods

+ (NSDate*)getBeginningOfSleepDay:(NSDate*)date;
+ (NSDate*)getEndOfSleepDay:(NSDate*)date;
+ (NSDate*)getBeginningOfWeekForDate:(NSDate*)date andFirstWeekDay:(int)firstDayOfWeek;
+ (NSDate*)getEndOfWeekForDate:(NSDate*)date andFirstWeekDay:(int)firstDayOfWeek;
+ (NSDate*)getBeginningOfMonthForDate:(NSDate*)date;
+ (NSDate*)getEndOfMonthForDate:(NSDate*)date;
+ (NSDate*)createUTCTimestampForDate:(NSDate*)date;
+ (NSDate*)createUTCTimestampForYear:(int)year month:(int)month day:(int)day;
+ (NSDate*)createUTCTimestampForYear:(int)year month:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second;

#pragma mark - Public Methods

- (void)loadAllDiariesWithProgress:(void (^)(double downloadProgress))progress andCompletion:(void (^)(enum NetworkAPIStatusCode code))completion;
- (void)loadAllDiariesForUser:(NSString*)userId withProgress:(void (^)(double downloadProgress))progress andCompletion:(void (^)(enum NetworkAPIStatusCode code))completion;
- (void)loadAllDiariesFrom:(NSNumber*)minTimestamp forUser:(NSString*)userId withProgress:(void (^)(double downloadProgress))progress andCompletion:(void (^)(enum NetworkAPIStatusCode code))completion;

- (NSArray*)getDiariesForDate:(NSDate*)date forUser:(NSString*)userId;
- (NSArray*)getWeekDiariesForDate:(NSDate*)date withWeekStart:(int)weekStart forUser:(NSString*)userId;
- (NSArray*)getMonthDiariesForDate:(NSDate*)date forUser:(NSString*)userId;
- (NSArray*)getDiariesInRangeFrom:(NSDate*)from to:(NSDate*)to forUser:(NSString*)userId;

- (void)loadAllSummariesWithProgress:(void (^)(double downloadProgress))progress andCompletion:(void (^)(enum NetworkAPIStatusCode code))completion;
- (void)loadAllSummariesForUser:(NSString*)userId withProgress:(void (^)(double downloadProgress))progress andCompletion:(void (^)(enum NetworkAPIStatusCode code))completion;
- (void)loadAllSummariesFrom:(NSNumber*)minTimestamp forUser:(NSString*)userId withProgress:(void (^)(double downloadProgress))progress andCompletion:(void (^)(enum NetworkAPIStatusCode code))completion;

- (BOOL)hasSummaryForUser:(NSString *)userId;

- (SleepSummaryComplete*)getNextSummaryFrom:(NSDate*)from forUser:(NSString*)userId;
- (SleepSummaryComplete*)getPrevSummaryFrom:(NSDate*)from forUser:(NSString*)userId;
- (SleepSummaryComplete*)getDetailedHistoryForId:(NSInteger)summaryId forUser:(NSString*)userId;
- (NSArray*)getSleepSegmentsForDate:(NSDate*)date forUser:(NSString*)userId;
- (NSArray*)getWeekSummariesForDate:(NSDate*)date withWeekStart:(int)weekStart forUser:(NSString*)userId  filterShortSessions:(BOOL)filterShortSessions;
- (NSArray*)getMonthSummariesForDate:(NSDate*)date forUser:(NSString*)userId filterShortSessions:(BOOL)filterShortSessions;
- (NSArray*)getNextWeekForDate:(NSDate*)date withWeekStart:(int)weekStart forUser:(NSString*)userId;
- (NSArray*)getPrevWeekForDate:(NSDate*)date withWeekStart:(int)weekStart forUser:(NSString*)userId;
- (NSArray*)getNextMonthForDate:(NSDate*)date forUser:(NSString*)userId;
- (NSArray*)getPrevMonthForDate:(NSDate*)date forUser:(NSString*)userId;
- (NSArray*)getSummariesInRangeFrom:(NSDate*)from to:(NSDate*)to forUser:(NSString*)userId filterShortSessions:(BOOL)filterShortSessions;
- (NSArray*)getSummariesForYear:(int)year month:(int)month day:(int)day forDayRange:(int)dayRange forUser:(NSString*)userId;

@end
