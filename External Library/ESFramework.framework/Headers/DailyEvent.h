//
//  DailyEvent.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 19/04/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

static const NSInteger NotSelected = -99;

static const NSInteger BleedingNone = 0;
static const NSInteger BleedingSpotting = 1;
static const NSInteger BleedingYes = 2;

static const NSInteger NotSick = 0;
static const NSInteger IsSick = 1;

static const NSInteger NoIntercourse = 0;
static const NSInteger HadIntercourse = 1;

//json keys
static const NSString* bleed_level = @"bleed_level";
static const NSString* intercourse = @"intercourse";
static const NSString* illness = @"illness";
static const NSString* timestamp = @"date_timestamp";

@interface DailyEvent : NSObject

typedef NSInteger BleedingLevel;
typedef NSInteger Sickness;
typedef NSInteger Intercourse;


@property NSInteger day;
@property NSInteger month;
@property NSInteger year;

@property BleedingLevel bleedingLevel;
@property Intercourse intercourse;
@property Sickness sickness;



- (NSDictionary*)toDictionary;

@end
