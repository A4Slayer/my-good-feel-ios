//
//  TodayData.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 19/04/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TodayData : NSObject

typedef enum ECycleStage {
    StartPeriod,
    EndPeriod,
    StartFertilityWindow,
    EndFertilityWindow,
    UnknownStage
} ECycleStage;

@property NSString* currentDate;
@property NSInteger startPeriod;
@property NSInteger endPeriod;
@property NSInteger startFertilityWindow;
@property NSInteger endFertilityWindow;
@property NSString* presentCode;
@property NSInteger currentCycleDay;
@property NSString* currentFertilityPhase;
@property NSInteger sickness;
@property NSInteger pregnant;


@property (nonatomic) ECycleStage cycleStage;

- (NSDate*)effectiveDate;
- (BOOL)isUpdateToDate;

@end
