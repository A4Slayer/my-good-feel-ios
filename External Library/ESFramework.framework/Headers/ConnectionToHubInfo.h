//
// Created by Vlad Zamskoi on 7/2/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionToHubInfo : NSObject

+ (BOOL)isPhoneConnectedToHeadlessHub;
+ (NSString *)SSIdOfCurrentlyConnectedHub;

@end
