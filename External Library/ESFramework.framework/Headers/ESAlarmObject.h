//
//  ESAlarmObject.h
//  EarlySnseFramework
//
//  Created by Roman on 15/11/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

enum ESAlarmSnoozeType
{
	ESAlarmSnoozeTypeClassic,
	ESAlarmSnoozeTypePersistent
};

@interface ESAlarmObject : NSObject

@property (nonatomic, copy) NSNumber *alarmId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) int timeStart;
@property (nonatomic, assign) int timeEnd;
@property (nonatomic, assign) enum ESAlarmSnoozeType snoozeType;
@property (nonatomic, copy) NSString *ringtone;
@property (nonatomic, copy) NSArray *repeatingDays;
@property (nonatomic, copy) NSDate *nextAlarmTime;
@property (nonatomic, assign) bool isOn;
@property (nonatomic, assign) bool repeatAlarm;
@property (nonatomic, assign) int bedTimeReminder;
@property (nonatomic, assign) bool bedTimeReminderActive;
@property (nonatomic, copy) NSString *customData;

@end
