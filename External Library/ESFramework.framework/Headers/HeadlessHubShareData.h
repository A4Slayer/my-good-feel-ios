//
//  HeadlessHubShareData.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 24/01/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FollowerSettings.h"

@interface HeadlessHubShareData : NSObject

@property (nonatomic) NSString* managedEmail;
@property (nonatomic) NSString* managerEmail;
@property (nonatomic) FollowerSettings* sharedDataScheme;

@end
