//
//  JsonableElement.h
//  ESFramework
//
//  Created by Rebecca Biaz on 13/11/2017.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * class JsonableElement
 *
 * This class should be inherited,  it usable to parse json data to model object.
 *
 */

@interface JsonableElement : NSObject

/**
 * deserializeJSONRoot. deserialize the given jsonRoot to JsonableElement array.
 *
 * @param jsonRoot NSArray or NSDictionary  the root of json data received from server
 * @param elementKey the key to retrieve the data from jsonRoot
 * @param classHandler the class than inherit from JsonableElement, expected to receive array of this class.
 *
 * @return JsonableElement array parsed
 */

+ (NSArray *)deserializeJSONRoot:(id)jsonRoot elementKey:(NSString *)elementKey classHandler:(Class)classHandler;

/**
 * fillWithDictionaryElement. desirialize the given element to own properties.
 *
 * @param element NSDictionary the root of json data received from server for the current object
 */

- (void)fillWithDictionaryElement:(id)element; // override this in subclasses

/**
 * getObjectFromKey. get the value for given key in element object
 *
 * @param element json data received from server for the current object
 * @param key the key that we need to retreive his value
 * @param defaultValue the value that we need to return if no value or NSNull in the element json.
 *
 * @return the value for the key
 */

- (id)getObjectFromKey:(id)element key:(NSString *)key defaultValue:(id)defaultValue;

/**
 * getObjectFromKey. get the value for given key in element object
 *
 * @param element json data received from server for the current object
 * @param key the key that we need to retreive his value
 *
 * @return the value for the key or nil if there is no value.
 */

- (id)getObjectFromKey:(id)element key:(NSString *)key;

@end
