//
//  HeadlessHubError.h
//  EarlySnseFramework
//
//  Created by Vlad Zamskoi on 4/7/17.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ErrorCodes.h"
#import "HeadlessHubErrorCode.h"

@interface HeadlessHubError : NSObject

@property (nonatomic, readonly) enum HeadlessHubErrorCode code;
@property (nonatomic, readonly) NSError* underlyingError;
@property (nonatomic, readonly) NSString* errorDescription;

- (id)initWithHubConnectionError:(NSError*)underlyingError;
- (id)initWithErrorByCode:(enum HeadlessHubErrorCode)hubErrorCode;
+ (enum HeadlessHubErrorCode)getHubErrorCodeFrom:(NSDictionary*)hubResponse;

@end
