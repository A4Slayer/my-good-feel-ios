//
//  Session.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 14/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Session : NSObject

@property (nonatomic) NSInteger offsetInSec;

@property (nonatomic) NSString* sensorUUID;

@property (nonatomic) NSInteger timeStamp;

@property (nonatomic) long globalTimestamp;

@property (nonatomic) NSInteger utcTimestamp;

@end

