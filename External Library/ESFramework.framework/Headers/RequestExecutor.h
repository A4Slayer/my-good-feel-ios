//
//  RequestExecutor.h
//  ESFramework
//
//  Created by Rebecca Biaz on 23/11/2017.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESRequest.h"

#define TIMEOUT_INTERVAL        30

@interface RequestExecutor : NSObject

+ (RequestExecutor *)sharedInstance;

- (void)addKnowHttpError:(int)knowHttpError;
- (void)executeRequest:(ESRequest *)request;
- (NSSet *)setOfAcceptableContentTypes:(NSArray *)acceptableContentTypes;

@end
