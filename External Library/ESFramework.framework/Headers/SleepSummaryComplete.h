//
//  SleepSummaryComplete.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 15/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SleepSummaryGeneral.h"

@interface SleepSummaryComplete : SleepSummaryGeneral

@property (nonatomic) NSArray* hrGraph;
@property (nonatomic) NSArray* rrGraph;
@property (nonatomic) NSArray* hypnoDepthGraph;
@property (nonatomic) NSArray* hypnoTypeGraph;
@property (nonatomic) NSArray* movementGraph;
@property (nonatomic) NSArray* stressGraph;
@property (nonatomic) NSArray* hrvBalanceGraph;
@property (nonatomic) NSArray* realTimeAlerts;
@property (nonatomic) NSNumber* targetSleepScore;


-(SleepSummaryComplete*)initWith:(BOOL)isEmpty;

@end
