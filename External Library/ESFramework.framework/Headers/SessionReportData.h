//
//  SessionReportData.h
//  ESFramework
//
//  Created by Rebecca Biaz on 23/07/2018.
//  Copyright © 2018 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SessionReportData : NSObject

@property (nonatomic, unsafe_unretained) NSInteger summaryId;
@property (nonatomic, unsafe_unretained) NSString *base64encodedSleepImg;
@property (nonatomic, unsafe_unretained) NSString *base64encodedHrRrImg;

- (instancetype)initWithSummaryId:(NSInteger)summaryId sleepImg:(NSString *)sleepImg HrRrImg:(NSString *)HrRrImg;
- (NSDictionary *)toDictionary;
@end
