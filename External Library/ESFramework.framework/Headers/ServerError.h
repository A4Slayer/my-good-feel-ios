//
// Created by Vlad Zamskoi on 5/10/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ErrorCodes.h"

@interface ServerError : NSObject

@property (nonatomic, readonly) enum NetworkAPIStatusCode code;
@property (nonatomic, readonly) NSError* underlyingError;
@property (nonatomic, readonly) NSString* errorDescription;

- (instancetype)initWithCode:(NetworkAPIStatusCode)code;
- (instancetype)initWithNetworkError:(NSError*)underlyingError;

+ (enum NetworkAPIStatusCode)errorCodeFromServerResponse:(NSDictionary*)response;

@end
