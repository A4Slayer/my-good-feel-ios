//
// Created by Vlad Zamskoi on 6/4/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum HeadlessHubErrorCode {
    // Mapped from HeadlessHub Responses
    HubErrorCodeSuccess,
    HubErrorCodeAuthenticationError,
    /*2*/HubErrorCodeSessionInProgress,
    HubErrorCodeInternalHubError,
    HubErrorCodeInvalidConfig,
    /*5*/ HubErrorCodeInvalidPassword,
    HubErrorCodeNoInternet,
    HubErrorCodeSensorInUse,
    /*8*/HubErrorCodeIncompatibleSensor,
    HubErrorCodeStartManageNotCalled,
    HubErrorCodeNetworkError,
    /*11*/HubErrorCodeConnectionFailed,
    HubErrorCodeSetWifiInProgress,
    HubErrorCodeInvalidToken,

    //Additional (logical)
    /*14*/HubErrorCodeConnectionError,
    HubErrorCodeSetupUnavailable,
    /* deprecated, todo update all usages to `HubErrorCodeUnexpected` */ HubErrorCodeUnknownError,
    HubErrorCodeUnexpected,

    // deprecated
    HubErrorCodeRegisteredForOtherUser,
    HubErrorCodeNoSensorsFound,
    HubErrorCodeInvalidWifiKeys
} HeadlessHubErrorCode;
