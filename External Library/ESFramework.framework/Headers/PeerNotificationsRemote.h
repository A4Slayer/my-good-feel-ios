//
// Created by Vlad Zamskoi on 5/4/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PeerNotifications;
@class InvitationToFollowSomebody;

/**
 * This class is responsible for fetching and sending peer_notifications over network.
 * It is created to abstract UIApplication logic from `NetworkAPIManager` class which is hard to maintain and test
 */
@interface PeerNotificationsRemote : NSObject

typedef void (^VoidCallback)();

- (void)fetchNotifications:(nonnull void(^)(PeerNotifications * _Nonnull peerNotifications))completion;

- (void)acceptInvitationToFollowSomebody:(InvitationToFollowSomebody * _Nonnull)invitation
                              onComplete:(VoidCallback _Nullable)onCompleteCallback;

- (void)declineInvitationToFollowSomebody:(InvitationToFollowSomebody * _Nonnull)invitation;

@end
