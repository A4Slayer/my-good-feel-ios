//
//  AnalyticsManager.h
//  EarlySnseFramework
//
//  Created by Yaron Karasik on 8/9/17.
//  Copyright © 2017 EarlySense. All rights reserved.
//

@protocol AnalyticsManagerDelegate <NSObject>

@optional
- (void)lastSessionWasRestored;

@end


@interface AnalyticsManager : NSObject

@property (nonatomic, weak) id <AnalyticsManagerDelegate> delegate;

+ (AnalyticsManager *)sharedInstance;

- (void)reportLastSessionRestored;

@end
