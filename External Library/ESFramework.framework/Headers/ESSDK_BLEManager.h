#import <CoreBluetooth/CoreBluetooth.h>

#ifndef ESSDK_BLEManager_h
#define ESSDK_BLEManager_h

/**
 * General class for communication with Sensor.
 * All communication with Sensor must be proceeded through this class.
 * <p><b>Registering to notifications from BLEManager</b></p>
 * <pre class="code">
 * <p></p>
 * <b>1) Registering for a notification (in this code example we choose the arbitrary selector name sensorConnected) - </b>
 * &nbsp;[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorConnected) name:[[BLEManager sharedInstance] notificationForType:BLENotificationTypeConnectedToSensor] object:nil];
 * <b>2) Available notification types - see BLENotificationType in this document</b>
 * <p><b>2) Sensor discovery and connection procedure:</b></p>
 * <b>1) Begin by requesting a scan - call [[BLEManager sharedInstance] startScanForSensors]; </b>
 * <b>2) Receive notification BLENotificationTypeDiscoveredSensor</b>
 * <b>3) call [[BLEManager sharedInstance] getAvailableSensors] - populate the sensor list in the UI with the returned array</b>
 * <b>4) when the user selects a sensor to connect to, call [[BLEManager sharedInstance] connectToSensor:sensorName] - pass the value of the </b>
 * <b> BLE_KEY_PERIPHERAL_UUID key in the dictionary of the selected sensor </b>
 * <b>5) when the sensor is connected,the BLENotificationTypeConnectedToSensor will fire - the next step is to read the mac address from the connected sensor  </b>
 * <b>6) and register it with the server using [[NetworkAPIManager sharedInstance] registerSensor], to which we will pass the sensor's mac address  </b>
 * <b> if the registeredSensor call returned success, it is OK to stay connected to this sensor and use it  </b>
 *   </pre>
 */

enum BLENotificationType
{
	BLENotificationTypeNotAvailable,
	BLENotificationTypeConnectedToSensor,
	BLENotificationTypeRestored,
	BLENotificationTypeDisconnectedFromSensor,
	BLENotificationTypeUserDisconnectedFromSensor,
    BLENotificationTypeSensorNotConnectedEnoughTime,
	BLENotificationTypeConnectionError,
	BLENotificationTypeDiscoveredSensor,
    BLENotificationTypeBluetoothTurnedOn,
    
    BLENotificationTypeRSSIReadingRecieved,
    
    BLENotificationTypeBLEVersionReceived,
    BLENotificationTypeRemoveLastFailedTimeStamp,
    BLENotificationTypeUpgradeFinished,
    BLENotificationTypeUpgradeFailed,
    BLENotificationTypeUpgradeSuccess,
    BLENotificationTypeUpgradeInProgress
};

#define BLE_KEY_PERIPHERAL @"keyBLEPeripheral"
#define BLE_KEY_PERIPHERAL_UUID @"keyBLEPeripheralUUID"
#define BLE_KEY_PERIPHERAL_NAME @"keyBLEPeripheralName"
#define BLE_KEY_MAC_ADDR @"keyBLEMACAddress"
#define BLE_KEY_SW_REVISION @"keyBLESoftwareRevision"
#define BLE_KEY_RSSI @"keyBLERSSI"

@interface BLEManager : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>

@property BOOL rssiOn;

+ (BLEManager*)sharedInstance;

#pragma mark - Client - Public Methods

// Client app configuration methods

/**
 configure the prefix of the bt name of the sensors with which the app will work
 
 @param sensorPrefix the sensor name prefix
 
 */
- (void)setSensorNamePrefix:(NSString*)sensorPrefix;

/**
 enable/disable logging of raw sensor data. This should only be enabled for debug purpose as constantly saving raw sensor data will significantly increate the storage used by the app
 
 @param save raw sensor data will be saved if true is passed
 
 */
- (void)setSavePiezoData:(BOOL)save;

/**
 @return logging of raw sensor data enable/disable
 */
- (BOOL)isSavingPiezoData;

// Scanning and result methods

/**
 start scanning for sensors. After the scan is done a notification of type BLENotificationTypeDiscoveredSensor is fired. At this point the app can use the getAvailableSensors method to get a list of sensors
 */
- (void)startScanForSensors;

/**
 Stop scanning for sensors.
 */
- (void)stopScanForSensors;

/**
 get a list of available sensors we can connect to
 
 @return the result is delivered as a dictionary with an entry for each available sensor. the key is the sensor UUID and the value is a dictionary with sensor information
 */
- (NSDictionary*)getAvailableSensors;

/**
 clear the list of available sensors, leaving the currently connected sensor in it (if a sensor is connected)
  */
- (void)clearSensorList;

// Sensor connection handling

/**
 connect to a sensor
 
 @param sensorUUID UDID for the sensor want to connect with
 
 */
- (void)connectToSensor:(NSString*)sensorUUID;

/**
 disconnect from a sensor
 */
- (void)disconnectFromSensor;

/**
 the last selected sensor, even if not connected at this moment.
 
 @return The connected sensor information as NSDictionary
 */
- (NSDictionary*)getConnectedSensor;

/**
 the last selected sensor, even if not connected at this moment.
 
 @return The last connected sensor information as NSDictionary
 */
- (NSDictionary*)getLastConnectedSensor;

- (NSDictionary*)getTargetSensor;

// Sensor state methods

/**
 check if we are connected to a sensor
 @return true if connected
 */
- (BOOL)isConnectedToSensor;

/**
 check if the app was connected to at least one sensor since it was installed
 
 @return true if was connected
 */
- (BOOL)wasConnectedToSensor;

/**
 --
 */
- (void)forgetCurrentSensor;

/**
 --
 
 @param sensor last connected sensor information as NSDictionary
 
 */
- (void)setLastConnectedSensor:(NSDictionary*)sensor;

/**
 Check if there is an upgrade in progress
 
 @return true if there is an upgrade in progress, return false if there is no upgrade in progress.
 
 */
- (BOOL)isUpgradeInProgress;
- (BOOL)isDeviceBluetoothOn;

// BLE Manager Methods

/**
 Stop any active scans and disconnect from the sensor if connected
 */
- (void)stopBLEManager;

- (CBCentralManagerState) getBluetoothStatus;
- (BOOL)isSensorUpgradeAvailable;
- (BOOL)upgradeSensorIfPossible;
- (NSArray *)getAvailableImages;
- (void)setSelectedImage:(int)selectedImage;
- (NSString *)getSelectedImage;

#pragma mark - Notifications

- (NSString*)notificationForType:(enum BLENotificationType)type;

@end

#endif /* ESSDK_BLEManager_h */
