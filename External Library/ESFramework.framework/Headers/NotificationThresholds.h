//
//  NotificationThresholds.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 09/07/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationThresholds : NSObject

- (id)initWithDataFrom:(NSDictionary*)thresholdsDataDictionary;

-(void) setHrAboveNotificationThresholds:(NSInteger)hrAbove;
-(void) setHrBelowNotificationThresholds:(NSInteger)hrBelow;
-(void) setRrAboveNotificationThresholds:(NSInteger)rrAbove;
-(void) setRrBelowNotificationThresholds:(NSInteger)rrBelow;
-(void) setOnOffBedNotifications:(BOOL)onOffBed;
-(void) setOutOfBedFromHour:(NSInteger)outOfBedFrom;
-(void) setOutOfBedToHour:(NSInteger)outOfBedTo;

-(NSInteger) getHrAboveNotificationThresholds;
-(NSInteger) getHrBelowNotificationThresholds;
-(NSInteger) getRrAboveNotificationThresholds;
-(NSInteger) getRrBelowNotificationThresholds;
-(BOOL) getOnOffBedNotifications;
-(NSInteger) getOutOfBedFromHour;
-(NSInteger) getOutOfBedToHour;

@end

