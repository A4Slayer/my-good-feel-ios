//
//  HeadlessHubManager.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 22/01/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import "ErrorCodes.h"
#import "HeadlessHubErrorCode.h"

@class ManagedUser;
@class HeadlessHubStatus;
@class ServerError;
@class FollowerSettingsEditingSchemeList;
@class NotificationThresholdsExtra;
@class HeadlessHubShareData;
@class FollowerRequest;
@class NetworkConfig;
@class HeadlessHubSensor;
@class HeadlessHubError;
@class WifiSpot;
@class HeadlessHubCloudManager;
@class HeadlessHubDeviceManager;
@class HubRequestSender;
@class ConnectionToHubInfo;
@class StartManageResult;
@class ManagedUserSettings;

typedef NS_ENUM(NSInteger, ConnectionHubToServerProgressType)
{
    ConnectionHubToServerProgressTypeStart = 0,
    /*1*/ ConnectionHubToServerProgressTypeSubscribe,
    /*2*/ ConnectionHubToServerProgressTypeGetRealTimeData,
    /*3*/ ConnectionHubToServerProgressTypeSuccess,
    /*4*/ ConnectionHubToServerProgressTypeFailed,
};

/**
 * Is implemented according to API documentation:
 *
 * "https://earlysense.atlassian.net/wiki/spaces/LiveFree/pages/2644975/Technical+Specification+-+High+Level+Design#TechnicalSpecification-HighLevelDesign-_Toc475291082"
 *
 * However, several new decision were made after that documentation was updated last time.
 * As well, some methods mentioned in documentation are not used anymore in the application,
 * therefore they have been removed from SDK.
 *
 * */
@interface HeadlessHubManager : NSObject

+ (HeadlessHubManager*)sharedInstance;


/**
 setter to be used in tests
 */
- (void)setHeadlessHubCloudManager:(HeadlessHubCloudManager*)newValue;

/**
 setter to be used in tests
 */
-(void)setHeadlessHubDeviceManager:(HeadlessHubDeviceManager *)newValue;

/**
 setter to be used in tests
 */
- (void)setHubRequestSender:(HubRequestSender *)newValue;

/**
 setter to be used in tests
 */
- (void)setConnectionToHubInfo:(ConnectionToHubInfo *)newValue;

/**
 Registers the user in the cloud, register the session owner as a manager(follower) of the registered user(managed user)

 @param user HeadlessHub user name
 @param success will be called on success
 @param failure will be called on failure

 */
- (void)registerManagedUser:(ManagedUser*)user
                    success:(void(^)())success
                    failure:(void (^)(enum NetworkAPIStatusCode returnCode, NSError* networkError))failure;

/**
 Updates details of a manager(follower) user(managed user) details
 
 @param user a User object containing the username of the user to update along with all of the up to date values for the different user fields
 @param success will be called on success
 @param failure will be called on failure
 
 
 */
- (void)updateManagedUserProfile:(ManagedUser*)user
                         success:(void(^)())success
                         failure:(void(^)(enum NetworkAPIStatusCode returnCode, NSError* networkError))failure;

/**
 Get details of a manager(follower) user(managed user) details
 
 @param userEmail a User object containing the username of the user to update along with all of the up to date values for the different user fields
 @param success will be called on success
 @param failure will be called on failure
 
 */

- (void)getManagedUserSettingsAndConfiguration:(NSString *)userEmail
                                       success:(void(^)(ManagedUserSettings *))success
                                       failure:(void(^)(enum NetworkAPIStatusCode returnCode, NSError* networkError))failure;
/**
 Sets Managed User's Notifications Thresholds

 @param thresholds The HeadlessHub user thresholds
 @param success will be called on success
 @param failure will be called on failure

 */
- (void)setManagedUserThresholds:(NotificationThresholdsExtra*)thresholds
                         forUser:(NSString*)managedUserName
                         success:(void(^)())success
                         failure:(void(^)(enum NetworkAPIStatusCode returnCode, NSError* networkError))failure;

/**
 * Edit followers of target user (Managed user) specified within
 *  @param editedFollowerSchemeList --
 * @param successCallback --
 * @param failCallback --
 */
- (void)editFollowersSettings:(FollowerSettingsEditingSchemeList *)editedFollowerSchemeList
                    onSuccess:(void (^)())successCallback
                       onFail:(void(^)(ServerError *))failCallback;

- (void)getHubStatusForManagedUser:(ManagedUser *)managedUser
                         onSuccess:(void (^)(HeadlessHubStatus *))successCallback
                            onFail:(void(^)(HeadlessHubError*))failCallback;

- (void)getStatusHeadlessHub:(void(^)(HeadlessHubStatus *, HeadlessHubError *))completion;

- (void)startManageHeadlessHub:(void(^)(StartManageResult *, HeadlessHubError *))completion;

- (void)registerHeadlessHubWithCurrentUserWithCompletion:(void(^)(ManagedUser *, HeadlessHubError *))completion;

- (void)setHubConfigWithCompletion:(void(^)(ManagedUser *, HeadlessHubError *))completion;

/**
 Starts the headless hub configuration process – connects to the device management interface and authenticates.
 Use this version of `startManage...` method if you haven't successfully passed neither `registerManagedUser` nor `updateManagedUser`, but you know
    1. what `managedUser` has been assigned as HeadlessHub_user before AND
    2. LAN_adapter_macAddress of the headlessHub

 @param successCallback will be called on success
 @param failCallback will be called on failure

 */
- (void)startManageHeadlessHubWithMacAddress:(NSString *)macAddress
                              forManagedUser:(ManagedUser *)managedUser
                                   onSuccess:(void (^)())successCallback
                                      onFail:(void(^)(HeadlessHubError*))failCallback;

/**
 Instructs the HeadlessHub to start searching for WiFi networks.
 Networks discovered by HeadlessHub are passed as arguments to the provided callback.

 @param completion callback is called with fetched data passed to it. If argument `HeadlessHubError` is not nil, it means that something went wrong during performing the operation and in such case you may not want to use `NSArray<WifiSpot *> *` which can be empty (nil) or contain invalid data.

 */
- (void)startScanWifiNetworksFromHeadlessHubWithCompletion:(void (^)(NSArray<WifiSpot *> *, HeadlessHubError *))completion;

/**
 Configure the headless hub network

 @param networkConfig Network configuration object
 @param success will be called on success
 @param failure will be called on failure

 */
- (void)configureHeadlessHubNetwork:(NetworkConfig *)networkConfig
                            success:(void (^)())success
                            failure:(void(^)(enum HeadlessHubErrorCode returnCode, NSError* networkError))failure;

/**
 Configures the hub to be owned by the current managed user

 @param success will be called on success
 @param failure will be called on failure

 */
- (void)configureHeadlessHubUserWithSuccess:(void (^)())success
                                    failure:(void(^)(enum HeadlessHubErrorCode returnCode, NSError* networkError))failure;

/**
 Fetches list of sensors that are visible for HeadlessHub.

 @param success callback will be called ONLY if there is at least one found sensor, so you do NOT get empty array passed in to the callback. Otherwise, @param failure callback is called.
 */
- (void)startScanSensorsFromHeadlessHubWithSuccess:(void(^)(NSArray<HeadlessHubSensor*>* sensors))success
                                           failure:(void (^)(enum HeadlessHubErrorCode returnCode, NSError* networkError))failure;

/**
 Configure the specified sensor to be used by the Headless Hub and its user.
 Should be called with takeover=false first, and if {@code SensorAlreadyInUse} error is returned,
 this method can be called again with takeover=true only if specifically approved by the end user

 @param macAddress The ID returned via headlessHubFoundSensorslistener
 @param takeOver Take over a sensor which is already assigned to another user
 @param success will be called on success
 @param failure will be called on failure

 @return
 */
- (void)configureHeadlessHubSensorWithMacAddress:(NSString *)macAddress
                                        takeOver:(BOOL)takeOver
                                         success:(void (^)())success
                                         failure:(void(^)(enum HeadlessHubErrorCode returnCode, NSError* networkError))failure;

/**
 * @deprecated
 * Stops the management session of the hub.
 * The hub will disconnect the management hot spot function and start a new network with different identification
 */
// todo remove from SDK
- (void)stopManageHeadlessHubWithSuccess:(void (^)())success
                                 failure:(void(^)(enum HeadlessHubErrorCode returnCode, NSError* error))failure;

/**
 * Stops the management session of the hub.
 * The hub will disconnect the management hot spot function and start a new network with different identification
 *
 * @param completion invoked in background thread because disconnecting from HeadlessHub can take few seconds. If you need to update UI when disconnection has happened, make sure you dispatch to MainThread explicitly in your completion block:
 */
// todo rename to `stopManageHeadlessHubWithCompletion` after current implementation of `stopManageHeadlessHubWithSuccess` will be removed
- (void)disconnectFromHubOnBackgroundThread:
        (void (^)(HeadlessHubError *))completion;

/**
 * Permanently deletes the headless hub local data – user information and saved signals, sensor settings, and unless requested to save - network settings
 */
- (void)clearHeadlessHubDataWithSuccess:(void (^)())successCallback
                                failure:(void (^)(HeadlessHubError *))failCallback;

/**
 Gets Managed User's Notifications Thresholds
 
 @param managedUserEmail user name of the managed user to get a response for
 @param success will be called on success with a NotificationThresholds object
 @param failure will be called on failure

 */
- (void)getManagedUserThresholds:(NSString*)managedUserEmail
                         success:(void(^)(NotificationThresholdsExtra* userThresholds))success
                         failure:(void(^)(enum NetworkAPIStatusCode returnCode, NSError* networkError))failure;

/**
 Gets list of managers(followers)
 
 @param managedUserEmail user name of the managed user to get a response for
 @param success will be called on success with an array of FollowerRequest objects, one for each follower of the managed user
 @param failure will be called on failure

 */
- (void)getManagedUserFollowers:(NSString*)managedUserEmail
                        success:(void(^)(NSArray<FollowerRequest*>* followers))success
                        failure:(void(^)(enum NetworkAPIStatusCode returnCode, NSError* networkError))failure;

- (void)setManagedUserPasswordAsync:(NSString *)newPassword
                          onSuccess:(void(^)())successCallback
                             onFail:(void(^)(HeadlessHubError *))failCallback;

// Waiting for connection between Hub to network (with delay to let the Hub time to connect)
- (void)waitForConnectionHubToNetwork:(ManagedUser *)managedUser
                        userOnSuccess:(void(^)(id data))successCallback
                               onFail:(void (^)(enum NetworkAPIStatusCode returnCode, NSError* networkError))failure
                           onProgress:(void (^)(enum ConnectionHubToServerProgressType progressType, CGFloat percent))progressBlock;

// we check with the server if the Hub is connected, registered and send data.

- (void)isHUBConnectedToServerWithManagedUser:(ManagedUser *)managedUser
                                userOnSuccess:(void(^)(id data))successCallback
                                       onFail:(void (^)(enum NetworkAPIStatusCode returnCode, NSError* networkError))failure
                                   onProgress:(void (^)(enum ConnectionHubToServerProgressType progressType, CGFloat percent))progressBlock;

@end
