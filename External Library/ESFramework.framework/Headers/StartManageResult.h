//
// Created by Vlad Zamskoi on 8/29/17.
// Copyright (c) 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface StartManageResult : NSObject

@property(nonatomic) BOOL hasHubInternetAccess;
@property(nonatomic) BOOL isConnectionWired;

@end