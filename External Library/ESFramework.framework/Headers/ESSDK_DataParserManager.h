#ifndef ESSDK_DataParserManager_h
#define ESSDK_DataParserManager_h
@protocol DataParser<NSObject>

- (void)parseKey:(double)dataKey andValue:(double)dataValue;

@end

enum SensorDataNotificationType
{
    SensorDataNotificationTypeRealTimeReadingReceived,
    SensorDataNotificationTypeLocalAlarmClockInvoked,
    SensorDataNotificationTypeCloudAlarmClockInvoked,
    SensorDataNotificationTypeInBedStatusChanged,
    SensorDataNotificationTypeAlarmInRange
};

@interface DataParserManager : NSObject

+ (DataParserManager*)sharedInstance;

#pragma mark - Infrastructure - Internal methods

- (void)handleAlgDataOutput:(unsigned char*)outputTable withCloudSignalData:(unsigned char*)cloudSignalData;
- (void)handleCloudRealtimeData:(NSData*)cloudData;
- (NSDictionary*)processSleepSessionResponse:(NSDictionary*)dict;

#pragma mark - Client - Public Methods

- (void)addCustomCloudParser:(id<DataParser>)customParser;
- (void)addCustomRealtimeParser:(id<DataParser>)customParser;
- (void)restartAlgorithms;

- (BOOL)isUserInBed;
- (BOOL)didUserEnteredBed;
- (NSString*)isUserInBedToString;
- (NSString*)getAlgVersion;
- (BOOL)checkForHypnoRequest:(NSData*)cloudResponse;

#pragma mark - Notifications

- (NSString *)notificationForType:(enum SensorDataNotificationType)type;

@end


#endif /* ESSDK_DataParserManager_h */
