//
//  MonthData.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 19/04/2017.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MonthData : NSObject

@property NSString* cycleDays;
@property NSString* fertilityPhases;
@property NSString* bleedingIndications;
@property NSString* complianceData;
@property NSString* nextEventData;

@end
