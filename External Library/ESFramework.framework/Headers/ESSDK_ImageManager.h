#ifndef ESSDK_ImageManager_h
#define ESSDK_ImageManager_h


#import "ErrorCodes.h"

@interface ImageManager : NSObject

+(ImageManager*)sharedInstance;
-(void)saveUserImage:(NSString*)user withImageData:(NSString *)imgString64 withCompletion:(void (^)(NSString *string64encoded))completion;
-(void)getUserImage:(NSString*)user isForFollowingRequest:(BOOL)isForFollowingRequest withCompletion:(void (^)(NSString *string64encoded, enum NetworkAPIStatusCode))completion;
-(void)clearCashForUser:(NSString*)user;
@end

#endif /* Header_h */
