//
//  ResponseHandler.h
//  EarlySnseFramework
//
//  Created by Mumen Shabaro on 6/12/17.
//  Copyright © 2017 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESServerResponseHandler : NSObject

+ (id)sharedInstance;
- (void)checkEvacuationLevel:(NSDictionary *)response;

@end
