//
//  Diary.h
//  EarlySnseFramework
//
//  Created by Tal Shahar on 16/06/2016.
//  Copyright © 2016 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Session.h"

@interface Diary : NSObject

@property (nonatomic) NSString* diaryId;
@property (nonatomic) NSInteger meal;
@property (nonatomic) NSInteger fitness;
@property (nonatomic) NSInteger caffeine;
@property (nonatomic) NSInteger mood;
@property (nonatomic) NSInteger alcohol;
@property (nonatomic) NSInteger stress;
@property (nonatomic) NSString* other;
@property (nonatomic) Session* session;

@end


