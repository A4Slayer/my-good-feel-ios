//
//  PeerUser.h
//  ESFramework
//
//  Created by Rebecca Biaz on 13/11/2017.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import "JsonableElement.h"

typedef NS_ENUM(NSInteger, PeerUserType)
{
    PeerUserTypeUnkwon = 0,
    PeerUserTypePendingFollowee,/*1*/
    PeerUserTypeFollowee,/*2*/
    PeerUserTypeFollower,/*3*/
    PeerUserTypeManagedFollowee,/*4*/
};

/**
 * class PeerUser
 *
 * This class hold all values for peerUser received from server.
 *
 */

@interface PeerUser : JsonableElement

@property (nonatomic, strong, nullable) NSString *fullName;
@property (nonatomic, strong, nullable) NSString *email;
@property (nonatomic, strong, nullable) NSString *firstSummaryId;
@property (nonatomic, strong, nullable) NSString *lastSummaryId;
@property (nonatomic, unsafe_unretained) PeerUserType type;

@end
