//
//  HubStatus.h
//  EarlySnseFramework
//
//  Created by Vlad Zamskoi on 4/5/17.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HeadlessHubStatus : NSObject

typedef NS_ENUM(NSInteger, HeadlessHubNetworkStatus) {
    HubNetworkStatusInternetAccess = 0,
    HubNetworkStatusLocalOnly,
    HubNetworkStatusNone,
    HubNetworkStatusExceptional,
    HubNetworkStatusWrongPassword
};

typedef NS_ENUM(NSInteger, HeadlessHubNetworkConnectionType) {
    HubNetworkTypeWifi = 0,
    HubNetworkTypeLan,
    HubNetworkTypeNotConnected,
    HubNetworkTypeNotExceptional
};

typedef NS_ENUM(NSInteger, HeadlessHubSensorConnectionStatus) {
    HubSensorStatusInRange,
    HubSensorStatusNotInRange,
    HubSensorStatusConnecting,
    HubSensorStatusConnected,
    HubSensorStatusBLEError,
    HubSensorStatusExceptional
};

typedef NS_ENUM(NSInteger, HeadlessHubNetworkConfigType) {
    HubNetworkConfigTypeNone,
    HubNetworkConfigTypeManual,
    HubNetworkConfigTypeAutomatic,
    HubNetworkConfigTypeExceptional
};

@property (nonatomic, readonly) HeadlessHubNetworkStatus networkStatus;
@property (nonatomic, readonly) HeadlessHubNetworkConnectionType networkType;
@property (nonatomic, readonly, nullable) NSString* networkName;
@property (nonatomic, readonly) HeadlessHubNetworkConfigType networkConfigType;
@property (nonatomic, readonly) BOOL isSensorConfigured;
@property(nonatomic, readonly) HeadlessHubSensorConnectionStatus sensorConnectionStatus;
@property(nonatomic, readonly, nullable) NSString *sensorMacAddress;
@property(nonatomic, readonly) BOOL isHubUserConfigured;
@property(nonatomic, readonly) BOOL isManagerUserAssigned;

- (id _Nullable )initWithDataFrom:(NSDictionary* _Nullable)response;

@end
