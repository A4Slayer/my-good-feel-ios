//
//  RequestFactory.h
//  ESFramework
//
//  Created by Rebecca Biaz on 26/11/2017.
//  Copyright © 2017 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESRequest.h"

@interface RequestFactory : NSObject

+ (ESRequest *)requestForNSUrlRequest:(NSURLRequest *)urlRequest
                   updateHeaderFields:(NSDictionary *)newHeaders
                      successCallBack:(ESSuccessResponse)successCallBack
                      failureCallBack:(ESFailureResponse)failureCallBack;

# pragma mark - Url Broker

+ (ESRequest *)requestForGetUrlFromUrlBroker:(NSString *)urlRequest
                                      params:(NSDictionary *)params
                             successCallBack:(ESSuccessResponse)successCallBack
                             failureCallBack:(ESFailureResponse)failureCallBack;

# pragma mark - Authentification

+ (ESRequest *)requestForLogin:(NSString *)urlRequest
                       headers:(NSDictionary *)headers
               successCallBack:(ESSuccessResponse)successCallBack
               failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForInternalRegisterWithEmail:(NSString *)urlRequest
                                           headers:(NSDictionary *)headers
                                   successCallBack:(ESSuccessResponse)successCallBack
                                   failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForLogout:(NSString *)urlRequest
           userAuthentification:(NSString *)userToken
                successCallBack:(ESSuccessResponse)successCallBack
                failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForInternalIsEmailExist:(NSString *)urlRequest
                                      headers:(NSDictionary *)headers
                              successCallBack:(ESSuccessResponse)successCallBack
                              failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForInternalForgotPassword:(NSString *)urlRequest
                                        headers:(NSDictionary *)headers
                                successCallBack:(ESSuccessResponse)successCallBack
                                failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForChangePassword:(NSString *)urlRequest
                                headers:(NSDictionary *)headers
                        successCallBack:(ESSuccessResponse)successCallBack
                        failureCallBack:(ESFailureResponse)failureCallBack;


+ (ESRequest *)requestForGetShareDataThresholds:(NSString *)urlRequest
                           userAuthentification:(NSString *)userToken
                                successCallBack:(ESSuccessResponse)successCallBack
                                failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForSetNotificationThresholds:(NSString *)urlRequest
                              userAuthentification:(NSString *)userToken
                                            params:(NSMutableDictionary *)params
                                   successCallBack:(ESSuccessResponse)successCallBack
                                   failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetDefaultShareDataThresholdsLimits:(NSString *)urlRequest
                                        userAuthentification:(NSString *)userToken
                                             successCallBack:(ESSuccessResponse)successCallBack
                                             failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForSetFcmToken:(NSString *)urlRequest
                fcmToken:(NSString *)fcmToken
                userAuthentification:(NSString *)userToken
                     successCallBack:(ESSuccessResponse)successCallBack
                     failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForConfigureAlgsWithBuffer:(NSString *)urlRequest
                           userAuthentification:(NSString *)userToken
                                           name:(NSString *)name
                                      hasSensor:(BOOL)hasSensor
                                   bufferLength:(NSString *)bufferLength
                                           body:(NSData *)body
                                successCallBack:(ESSuccessResponse)successCallBack
                                failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetTip:(NSString *)urlRequest
                        headers:(NSDictionary *)headers
                successCallBack:(ESSuccessResponse)successCallBack
                failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForConfirmDisclaimer:(NSString *)urlRequest
                      userAuthentification:(NSString *)userToken
                                    params:(NSDictionary *)params
                           successCallBack:(ESSuccessResponse)successCallBack
                           failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForUploadImage:(NSString *)urlRequest
                userAuthentification:(NSString *)userToken
                              params:(NSDictionary *)params
                     successCallBack:(ESSuccessResponse)successCallBack
                     failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForDownloadImage:(NSString *)urlRequest
                  userAuthentification:(NSString *)userToken
                                params:(NSDictionary *)params
                      progressCallBack:(ESDownloadProgress)progressCallBack
                       successCallBack:(ESSuccessResponse)successCallBack
                       failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForRegisterSensor:(NSString *)urlRequest
                                headers:(NSDictionary *)headers
                        successCallBack:(ESSuccessResponse)successCallBack
                        failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForActivateSeniorCare:(NSString *)urlRequest
                                    headers:(NSDictionary *)headers
                            successCallBack:(ESSuccessResponse)successCallBack
                            failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetSeniorCareSettings:(NSString *)urlRequest
                          userAuthentification:(NSString *)userToken
                               successCallBack:(ESSuccessResponse)successCallBack
                               failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForSetSeniorCareSettings:(NSString *)urlRequest
                                        headers:(NSDictionary *)headers
                                successCallBack:(ESSuccessResponse)successCallBack
                                failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForSetTimeToSendSeniorCareEmail:(NSString *)urlRequest
                                              headers:(NSDictionary *)headers
                                      successCallBack:(ESSuccessResponse)successCallBack
                                      failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForAddContactToSeniorEmailList:(NSString *)urlRequest
                                             headers:(NSDictionary *)headers
                                     successCallBack:(ESSuccessResponse)successCallBack
                                     failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForRemoveContactFromEmailList:(NSString *)urlRequest
                                            headers:(NSDictionary *)headers
                                    successCallBack:(ESSuccessResponse)successCallBack
                                    failureCallBack:(ESFailureResponse)failureCallBack;

# pragma mark - Summaries

+ (ESRequest *)requestForLoadAllSummaries:(NSString *)urlRequest
                     userAuthentification:(NSString *)userToken
                             minTimestamp:(NSNumber *)minTimestamp
                                   userId:(NSString *)userId
                         progressCallBack:(ESDownloadProgress)progressCallBack
                          successCallBack:(ESSuccessResponse)successCallBack
                          failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetSummaryForUserId:(NSString *)urlRequest
                        userAuthentification:(NSString *)userToken
                                      params:(NSDictionary *)params
                             successCallBack:(ESSuccessResponse)successCallBack
                             failureCallBack:(ESFailureResponse)failureCallBack;

# pragma mark - Sleep session

+ (ESRequest *)requestForGetSleepSessionWithId:(NSString *)urlRequest
                          userAuthentification:(NSString *)userToken
                                        params:(NSMutableDictionary *)params
                               successCallBack:(ESSuccessResponse)successCallBack
                               failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetSleepSummariesFromServer:(NSString *)urlRequest
                                userAuthentification:(NSString *)userToken
                                              params:(NSMutableDictionary *)params
                                     successCallBack:(ESSuccessResponse)successCallBack
                                     failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForExportSleepSummaryData:(NSString *)urlRequest
                           userAuthentification:(NSString *)userToken
                                         params:(NSMutableDictionary *)params
                                successCallBack:(ESSuccessResponse)successCallBack
                                failureCallBack:(ESFailureResponse)failureCallBack;

# pragma mark - Diary

+ (ESRequest *)requestForSetDiaryForUserId:(NSString *)urlRequest
                      userAuthentification:(NSString *)userToken
                                    params:(NSMutableDictionary *)params
                           successCallBack:(ESSuccessResponse)successCallBack
                           failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetDiaryForUserId:(NSString *)urlRequest
                      userAuthentification:(NSString *)userToken
                                    params:(NSMutableDictionary *)params
                          progressCallBack:(ESDownloadProgress)progressCallBack
                           successCallBack:(ESSuccessResponse)successCallBack
                           failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForLoadAllDiaries:(NSString *)urlRequest
                   userAuthentification:(NSString *)userToken
                                 params:(NSMutableDictionary *)params
                       progressCallBack:(ESDownloadProgress)progressCallBack
                        successCallBack:(ESSuccessResponse)successCallBack
                        failureCallBack:(ESFailureResponse)failureCallBack;

# pragma mark - Record

+ (ESRequest *)requestForSetCustomRecordForEmail:(NSString *)urlRequest
                            userAuthentification:(NSString *)userToken
                                          params:(NSMutableDictionary *)params
                                            body:(NSData *)body
                                progressCallBack:(ESDownloadProgress)progressCallBack
                                 successCallBack:(ESSuccessResponse)successCallBack
                                 failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetCustomRecordForEmail:(NSString *)urlRequest
                            userAuthentification:(NSString *)userToken
                                          params:(NSMutableDictionary *)params
                                progressCallBack:(ESDownloadProgress)progressCallBack
                                 successCallBack:(ESSuccessResponse)successCallBack
                                 failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForDeleteCustomRecordForEmail:(NSString *)urlRequest
                               userAuthentification:(NSString *)userToken
                                             params:(NSMutableDictionary *)params
                                   progressCallBack:(ESDownloadProgress)progressCallBack
                                    successCallBack:(ESSuccessResponse)successCallBack
                                    failureCallBack:(ESFailureResponse)failureCallBack;

# pragma mark - Follower/Followee

+ (ESRequest *)requestForGetPeers:(NSString *)urlRequest
             userAuthentification:(NSString *)userToken
                  successCallBack:(ESSuccessResponse)successCallBack
                  failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetPeerNotificationsWithCompletion:(NSString *)urlRequest
                                       userAuthentification:(NSString *)userToken
                                            successCallBack:(ESSuccessResponse)successCallBack
                                            failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForFolloweeRealtimeData:(NSString *)urlRequest
                         userAuthentification:(NSString *)userToken
                                       params:(NSDictionary *)userParams
                              successCallBack:(ESSuccessResponse)successCallBack
                              failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetFollowerSettings:(NSString *)urlRequest
                        userAuthentification:(NSString *)userToken
                                      params:(NSDictionary *)userParams
                             successCallBack:(ESSuccessResponse)successCallBack
                             failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForSetFollowerSettings:(NSString *)urlRequest
                        userAuthentification:(NSString *)userToken
                                      params:(NSDictionary *)userParams
                             successCallBack:(ESSuccessResponse)successCallBack
                             failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForSendFollowRequest:(NSString *)urlRequest
                      userAuthentification:(NSString *)userToken
                                    params:(NSDictionary *)userParams
                           successCallBack:(ESSuccessResponse)successCallBack
                           failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForReplyToFollowRequest:(NSString *)urlRequest
                         userAuthentification:(NSString *)userToken
                                       params:(NSDictionary *)userParams
                              successCallBack:(ESSuccessResponse)successCallBack
                              failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForRespondToFollowingInvite:(NSString *)urlRequest
                             userAuthentification:(NSString *)userToken
                                           params:(NSDictionary *)userParams
                                  successCallBack:(ESSuccessResponse)successCallBack
                                  failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForRemoveFollower:(NSString *)urlRequest
                   userAuthentification:(NSString *)userToken
                                 params:(NSDictionary *)userParams
                        successCallBack:(ESSuccessResponse)successCallBack
                        failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForRemoveFollowee:(NSString *)urlRequest
                   userAuthentification:(NSString *)userToken
                                 params:(NSDictionary *)userParams
                        successCallBack:(ESSuccessResponse)successCallBack
                        failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForEditFollowers:(NSString *)urlRequest
                  userAuthentification:(NSString *)userToken
                               headers:(NSDictionary *)headers
                                params:(id)userParams
                       successCallBack:(ESSuccessResponse)successCallBack
                       failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForStartRealtimeMonitoring:(NSString *)urlRequest
                            userAuthentification:(NSString *)userToken
                                          params:(NSDictionary *)params
                                 successCallBack:(ESSuccessResponse)successCallBack
                                 failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForStopRealtimeMonitoring:(NSString *)urlRequest
                            userAuthentification:(NSString *)userToken
                                          params:(NSDictionary *)params
                                 successCallBack:(ESSuccessResponse)successCallBack
                                 failureCallBack:(ESFailureResponse)failureCallBack;

# pragma mark - RC

+ (ESRequest *)requestForRegisterManagedUser:(NSString *)urlRequest
                           userAuthentification:(NSString *)userToken
                                         params:(NSDictionary *)params
                                successCallBack:(ESSuccessResponse)successCallBack
                                failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForUpdateManagedUser:(NSString *)urlRequest
                      userAuthentification:(NSString *)userToken
                                    params:(NSDictionary *)params
                           successCallBack:(ESSuccessResponse)successCallBack
                           failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForSetManagedUserThresholds:(NSString *)urlRequest
                             userAuthentification:(NSString *)userToken
                                           params:(NSDictionary *)params
                                  successCallBack:(ESSuccessResponse)successCallBack
                                  failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetManagedUserThresholds:(NSString *)urlRequest
                             userAuthentification:(NSString *)userToken
                                           params:(NSDictionary *)params
                                  successCallBack:(ESSuccessResponse)successCallBack
                                  failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetManagedUserFollowers:(NSString *)urlRequest
                            userAuthentification:(NSString *)userToken
                                          params:(NSDictionary *)params
                                 successCallBack:(ESSuccessResponse)successCallBack
                                 failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetManagedUserSettingsAndConfiguration:(NSString *)urlRequest
                                           userAuthentification:(NSString *)userToken
                                                         params:(NSDictionary *)params
                                                successCallBack:(ESSuccessResponse)successCallBack
                                                failureCallBack:(ESFailureResponse)failureCallBack;

# pragma mark - Fertility
+ (ESRequest *)requestForRecoverAllFertilityData:(NSString *)urlRequest
                            userAuthentification:(NSString *)userToken
                                          params:(NSDictionary *)params
                                 successCallBack:(ESSuccessResponse)successCallBack
                                 failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForGetFertilitySummary:(NSString *)urlRequest
                        userAuthentification:(NSString *)userToken
                                      params:(NSDictionary *)params
                             successCallBack:(ESSuccessResponse)successCallBack
                             failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForEmailFertilitySummary:(NSString *)urlRequest
                          userAuthentification:(NSString *)userToken
                               successCallBack:(ESSuccessResponse)successCallBack
                               failureCallBack:(ESFailureResponse)failureCallBack;

+ (ESRequest *)requestForUpdateServerWithDailyEvents:(NSString *)urlRequest
                                userAuthentification:(NSString *)userToken
                                                body:(NSData *)body
                                     successCallBack:(ESSuccessResponse)successCallBack
                                     failureCallBack:(ESFailureResponse)failureCallBack;

@end
