//
//  ExportSleepSummary.h
//  ESFramework
//
//  Created by Rebecca Biaz on 23/07/2018.
//  Copyright © 2018 EarlySense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionReportData.h"

typedef enum : NSUInteger {
    dailyReportType,
    continiousPeriodReportType,
    sparsePeriodReportType,
} ReportType;

@interface ExportSleepSummary : NSObject

@property (nonatomic, strong) NSArray<SessionReportData *> *dailySessions;
@property (nonatomic, strong) NSArray *dates;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, unsafe_unretained) ReportType reportType;

- (instancetype)initWithDailySessions:(NSArray<SessionReportData *> *)dailySessions dates:(NSArray *)dates email:(NSString *)email reportType:(ReportType)reportType;
- (NSArray *)toDailySessionDictionary;
- (NSString *)reportTypeStr;

@end
