
#import <Foundation/Foundation.h>
#import "UserManager.h"

@interface ManagedUser : User

@property (nonatomic) NSString* profileImage;

- (id)initWithDataFrom:(NSDictionary*)userDataDictionary;

@end
