# ESFramework

## Build version 3.3.34.6
## last version release 3.3.28

######  Oct 23 2018

## API changes of IOS Earlysense SDK:
### UserManager
#### - (User*) getUser --> - (nullable User*) getUser
mean you should add ? if you develop in swift language.

### NetworkAPIManager
### offlineDataCheckPassed variable removed. 
all offline data handled inside the SDK.

### BLEManager
### BLENotificationTypeUpgradeFaild --> BLENotificationTypeUpgradeFailed
Notification renamed.
