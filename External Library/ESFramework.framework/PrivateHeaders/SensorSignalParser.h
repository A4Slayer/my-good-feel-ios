/*
 * SensorSignalParser.h
 *
 */

#ifndef SENSORSIGNALPARSER_H_
#define SENSORSIGNALPARSER_H_

// Sensor block parsing states
enum eState
{
	START,
	SCAN_LEN,
	CALC_CHECKSUM,
	READ_CHECKSUM_FIRST,
	COMPLETE,
	READ_CHECKSUM_SECOND
};
// Data parser's returned codes
enum eReturnedCode
{
	VALID_RESPONSE,
	NOT_ENOUGH_DATA,
	INVALID_SIGNAL,
	ALGS_ERROR
};

typedef unsigned char byte;

#define MAX_PACKET_SIZE 102
#define DATA_PACKET_SIZE 100
#define DATA_SIZE 93
#define COUNTER_OFFSET 4
#define HEADER_OFFSET 4
#define CHECK_SUM_OFFSET 97
#define PIEZO_ELEMENTS 360
#define PACKET_ELEMENTS 90
#define NOT_IN_USE_INT 67108864		// 2^26
#define ALG_RESULTS_LEN 500
#define OTHER_SENS_LEN 3*6
#define MAX_CLOUD_DATA_LEN 2048
#define MAX_PARM_LEN 2048
#define CONFIG_ARRAY_SIZE 120

// Method signatures.
extern byte *processSensorSignal(const byte *rawData, int bufLength, byte *params, int isDemoMode);
extern void init();
extern int configure(const int* inputMat, int length);
extern void getAlgVersion(char ver[20]);

extern void gottawork();

void validate(int b_time1970, const int configTable[180],
              double paramCount, int calendric_start_date_unixtime, const int
              monthly_days_list[16], const int fertility_phases_data[], const int
              fertility_phases_size[2], const int bleeding_indications_data[], const int
              bleeding_indications_size[2],  const int all_daily_events_data[], const int
              all_daily_events_size[2], int all_daily_events_rows, const int new_event[5],
              boolean_t *b_is_valid_input, boolean_t *call_process_fertility_summary, int
              *fertility_alertCode);


#endif /* SENSORSIGNALPARSER_H_ */
