#pragma once
/*
 * Alert codes enum common to the Algs, the CU and the GUI.
 */
#define ALERT_ID_VALUE_SEPERATOR ';'
#define ALERT_PAIR_SEPERATOR ','

// Alert Type - must be synchronized with the GUI constants!!!
enum eAlertType
{
    NO_ALERT=0,
    HR_VALUE,
    RR_VALUE,
    MOVEMENT_VALUE,
    TIME_IN_BED,
    MOVEMENT_INDEX,
    TURN_COUNTER,
    HR_ABOVE,
    HR_BELOW,
    HR_HOME_ABOVE,
    HR_HOME_BELOW, //10
    RR_ABOVE,
    RR_BELOW ,
    RR_HOME_ABOVE,
    RR_HOME_BELOW,
    RESTLESSNESS, //15
    NO_MOTION,
    BED_TECHNICAL,
    CHAIR_TECHNICAL,
    BED_SENSOR_UPSIDE_DOWN,
    CHAIR_SENSOR_UPSIDE_DOWN, //20
    LONG_CEX,
    IMMEDIATE_BEX_LOW_SENS,
    LONG_BEX,
    IMMEDIATE_BEX_HIGH_SENS,
    VERY_LOW_BATTERY,
    LOW_BATTERY,
    NO_BATTERY,
    IMMEDIATE_CEX_LOW_SENS,
    IMMEDIATE_CEX_HIGH_SENS,
    HIGH_DEV_TEMPERATURE, //30
    IOC_RESET,
    HARD_DRIVE_FULL,
    LOW_MOVEMENT,
    // 33
    EXTREME_MOVEMENT,
    POSTURE_COUNTUP,
    POSTURE_RESET_FAIL,
    POSTURE_RESET_SUCCEEDED,
    PATIENT_SELF_TURN,
    RESTLESS_SUMMARY,
    MESSAGE_TO_USER, //40
    LOW_SIGNAL_LEVEL,
    UNSTABLE_SIGNAL_LEVEL,
    UNIT_MALFUNCTION,
    SPO2_VALUE,
    SPO2_ABOVE,
    // 45
    SPO2_BELOW,
    SPO2_DEV_PROBLEM,
    NO_MEASUREMENT,
    BED_CHAIR_CONFLICT,
    PT_ON_NO_AIR_MAT_DETECTED, //50
    PT_OFF_AIR_MAT_DETECTED,
    REASSESS_BRADEN,
    HR_RANGE,
    RR_RANGE,
    TREND_INDICATION,
    // 55
    NO_ADMIT,
    BED_SENSOR_CONNECT,
    BED_SENSOR_DISCONNECT,
    CHAIR_SENSOR_CONNECT,
    CHAIR_SENSOR_DISCONNECT, //60
    BED_SENSOR_DEFECTIVE,
    CHAIR_SENSOR_DEFECTIVE,
    BED_SENSOR_PROBLEM,
    CHAIR_SENSOR_PROBLEM,
    BED_BEFORE_SENSOR_EXPIRATION,
    BED_SENSOR_EXPIRED,
    CHAIR_BEFORE_SENSOR_EXPIRATION,
    CHAIR_SENSOR_EXPIRED,
    INVALID_SENSOR_CONNECTED,
    IS_IN_BED,
    // 70
    SIGNAL_QUALITY,
    ALGS_STATE,
    AIR_MATTRESS_BIT,
    HOME_RR_IMD_ABOVE,
    HOME_RR_IMD_BELOW,
    HOME_HR_IMD_ABOVE,
    HOME_HR_IMD_BELOW,
    HOME_TREND_POS_RR,
    HOME_TREND_POS_HR,
    HOME_TREND_LOW_RR, //80
    HOME_TREND_HIGH_RR,
    HOME_TREND_LOW_HR,
    HOME_TREND_HIGH_HR,
    HOME_TREND_SFR,
    HOME_TREND_NIGHT_SLEEP,
    HOME_TREND_DAY_SLEEP,
    HOME_TREND_NIGHT_BEX,
    HOME_TREND_SLEEP_IRR,
    HOME_GROUP_COLOR,
    HOME_TOTAL_SCORE, //90
    ITERATION_COUNTER,
    CURRENT_SENSOR_DATA,
    CALL_HC,
    // 93 Bose - request to call HC with the call type
    HEART_BEAT,
    // Bose - heart beat time in seconds from 1970 - followed by difference in milli and confidence level (3 entries value)
    RESP_CYCLE,
    // Bose - respiration time in seconds from 1970, followed by duration in seconds and an empty cell (3 entries value)
    AVG_HR,
    // 96 - consumer
    AVG_RR,
    AVG_SLEEP,
    
    // Internal codes - not to be used by the Algs
    BEX_WITHOUT_ALERT, //used by GUI
    COMMUNICATION_LOST, //used by GUI
    
    WAKE_SLEEP_STATE,
    //101  (0=SLEEP, 1=WAKE, 2=Out of bed)
    WENT_TO_BED,
    // (time in format of seconds from 1970)
    TIME_TO_FALL_ASLEEP,
    // (time in minutes)
    TIME_AWAENED,
    // (number of awakenings)
    IN_BED_TOTAL,
    // (time in minutes)
    ACTUAL_SLEEP_TIME,
    // 106 - Consumer (time in minutes)
    
    HR_VAR, // 107 Bose - HR varaibility
    RR_VAR, // Bose - RR varaibility
    RELAX_IND,
    // Bose - relaxation index
    SLEEP_PROB,
    // 110 -Bose - Sleep probability
    SLEEP_DEPTH,
    // Bose - Sleep depth
    SMART_WAKEUP_ALERT, // Bose - wake up alert
    MOVE_CLASS,
    // Bose - how significant the movement were
    
    SLEEP_SUMMARY_START,
    // Bose - start of Hypnogram
    SLEEP_SUMMARY_STOP,
    // Bose - End of Hypnogram
    BEX_CHARGING,
    CHAIR_REMINDER, // Indication for a patient in chair for a long time
    HYPNOGRAM_LENGTH, //118
    HYPNOGRAM_SLEEP_EFFICENCY,
    NOT_USED1,
    // 120
    // Samsung air-condition temperature for 4 sleep stages
    NOT_USED2,
    // 121
    // Samsung air-condition fan level for 4 sleep stages
    INTERNAL_ALGS_DATA,
    // 122 - Algs data to be uploaded to the Cloud.
    TIP_CATEGORY,
    TIP_INDEX,
    PCT_WAKE,
    // 125
    PC_REM,
    PCT_LS,
    PCT_SWS,
    PCT_OOB,
    SLEEP_SCORE_LATENCY, //130
    SLEEP_SCORE_BEX,
    SLEEP_SCORE_AWAKE,
    SLEEP_SCORE_SWS,
    SLEEP_SCORE_REM,
    SLEEP_SCORE_SE,
    // 135
    SLEEP_SCORE_TST,
    SLEEP_SCORE_BONUS,
    HR_STD,
    RR_STD,
    UNUSUAL_NIGHT_HR,
    // 140
    UNUSUAL_NIGHT_RR,
    UNUSUAL_NIGHT_MOV,
    UNUSUAL_NIGHT_SLEEP_SCORE,
    UNUSUAL_NIGHT_SL,
    UNUSUAL_NIGHT_BEX,
    UNUSUAL_NIGHT_WK,
    UNUSUAL_NIGHT_SWS,
    UNUSUAL_NIGHT_REM,
    UNUSUAL_NIGHT_SE,
    UNUSUAL_NIGHT_TST,
    // 150
    SUGGEST_SLEEP_STUDY,
    MOV_DENSITY,
    MIN_WAKE,
    MIN_REM,
    MIN_LS,
    MIN_SWS,
    MIN_OOB,
    NUM_BARS_SLEEP_SCORE_LATENCY,
    NUM_BARS_SLEEP_SCORE_BEX,
    NUM_BARS_SLEEP_SCORE_AWAKE,
    // 160
    NUM_BARS_SLEEP_SCORE_SWS,
    NUM_BARS_SLEEP_SCORE_REM,
    NUM_BARS_SLEEP_SCORE_SE,
    NUM_BARS_SLEEP_SCORE_TST,
    UNUSUAL_NIGHT_CLASS_HR,
    UNUSUAL_NIGHT_CLASS_RR,
    UNUSUAL_NIGHT_CLASS_MOV,
    UNUSUAL_NIGHT_CLASS_SLEEP_SCORE,
    UNUSUAL_NIGHT_CLASS_SL,
    UNUSUAL_NIGHT_CLASS_BEX,
    // 170
    UNUSUAL_NIGHT_CLASS_WK,
    UNUSUAL_NIGHT_CLASS_SWS,
    UNUSUAL_NIGHT_CLASS_REM,
    UNUSUAL_NIGHT_CLASS_SE,
    UNUSUAL_NIGHT_CLASS_TST,
    SWITCH_TO_DAY,
    AC_ROOMTEMP,
    AC_TEMP,
    AC_MODE,
    AC_POWERMODE,
    // 180
    AC_FANMODE,
    AC_WINDLEVEL,
    STATUS_FOR_AC_MODE,
    INSTANTANEOUS_SLEEPWAKE,
    STEADYSTATE_SLEEPWAKE,
    NUM_OUT_OF_BEDS,
    PCT_REM_FROM_SLEEP,
    PCT_LS_FROM_SLEEP,
    PCT_SWS_FROM_SLEEP,
    WAKE_AFTER_SO,
    // 190
    AVG_HISTORY_HR,
    AVG_HISTORY_RR,
    AVG_HISTORY_LATENCY,
    AVG_HISTORY_TIMES_AWAKE,
    AVG_HISTORY_TIME_IN_BED,
    LOWER_SCORE_RANGE_FOR_AGE,
    UPPER_SCORE_RANGE_FOR_AGE,
    AVG_HISTORY_SCORE,
    AC_SLEEPING_TIME,
    TURN_OFF_TV,
    // 200
    CALIBRATION_INDICATION,
    ZPULSEAMP_CALIBRATED_THRESHOLD,
    P373_CALIBRATED_THRESHOLD,
    P2P_3SEC_CALIBRATED_THRESHOLD,
    P_SIRS,
    REALTIME_SLEEP_STAGE,
    TS_REALTIME_SLEEP_STAGE, //Bose
    DELAYED_SLEEP_STAGE, //Bose
    TS_DELAYED_SLEEP_STAGE, //Bose
    RESP_STAB_NO_MOV, //Bose 210
    ALG_INIT, //Bose
    HYPNOGRAM_STATUS,
    HYPNOGRAM_ID,
    HRV,
    HRV_MIN, //215
    HRV_THRESH1,
    HRV_THRESH2,
    HRV_MAX,
    LONG_OOB_CONSUMER,
    TIME_AT_INIT,//220
    TIME_OF_SIGNAL_DETECTED,
    TIME_OF_LAST_BED_EXIT,
    STRESS_CATEGORY,
    AVG_STRESS,
    AVG_STRESS_CATEGORY, //225
    ALL_ALERTS
};
// Algs configuration parameter codes
enum eConfigType
{
    NoParam,
    ParamACTemp_1,
    ParamACTemp_2,
    ParamACTemp_3,
    ParamACTemp_4,
    ParamACFan_1,
    ParamACFan_2,
    ParamACFan_3,
    ParamACFan_4,
    ParamPeople_InBed,
    ParamAge, // 10 - Age in months default 480
    ParamWeight, // default 180
    Param_Height, // default 180
    ParamGender, // 0 male, 1 female, -99 unknown
    ParamIsMainSens,
    ParamUpHRLimit,
    ParamLowHRLimit,
    ParamUpRRLimit,
    ParamLowRRLimit,
    ParamUpO2Limit,
    ParamLowO2Limit, // 20
    ParamBEXDuration,
    ParamBexOn,
    ParamChairDuration,
    ParamTurnDuration,
    ParamTurnOn,
    ParamTurnReset,
    ParamNoAdmitDelay,
    ParamNoMeasure,
    ParamLowMotionTime,
    ParamSmartType, // 30 For recovery
    ParamBirthDate, // Sec from 1970
    ParamHospital_Type,
    ParamWorkingModule, // 0-safety 1-vitals 2-all
    ParamAdmitted, // 0-discharge 1-admit
    ParamScope,
    ParamTime2Alert_HRHi,
    ParamTime2Alert_HRLo,
    ParamTime2Alert_RRHi,
    ParamTime2Alert_RRLo,
    ParamSmartWake_Time, // 40
    ParamSmartWake_Window,
    ParamBex_Alg,
    ParamCurrentTemp,
    ParamAdmitTime,
    ParamTargetSleepTime, // 45
    ParamGenHypno,
    ParamAC_CurrentRoomTemp,
    ParamAC_CurrentACTemp,
    ParamAC_CurrentMode,
    ParamAC_CurrentPowerMode, // 50
    ParamAC_CurrentFanMode,
    ParamAC_CurrentWindLevel,
    ParamACSetting,
    ParamAC_SleepingTime,
    ParamStartNewSession,
    // 55
    ParamsManualNewSleep,
    ParamNewsletter,
    ParamTargetScore,
    ParamRecommendGoToSleep,
    ParamTvMode,
    // 60
    ParamBEX_Calibration,
    ParamHeightUnit,
    ParamWeightUnit,
    ParamTemperatureUnit,
    ParamUse24HourFormat,
    // 65
    ParamHR_RR_Alert_Enabled,
    ParamHR_RR_Alert_Hr_Threshold,
    ParamHR_RR_Alert_Rr_Threshold,
    ParamHR_RR_Alert_Delay,
    ParamHR_RR_Alert_Night_Only,
    // 70
    Param_Hr_Baseline_Gap,
    Param_Rr_Baseline_Gap,
    Param_Enter_Night_Time,
    Param_Exit_Night_Time,
    Param_Sirs_Timeline,
    // 75
    Param_Algs_Alerts_Demo,
    Param_Diary_Meal,
    Param_Diary_Caffeine,
    Param_Diary_fitness,
    Param_Diary_Alcohol,
    // 80
    Param_HRV_min,
    Param_HRV_THRESH1,
    Param_HRV_THRESH2,
    Param_HRV_MAX,
    Params_Local_Offset = 96
};

// Parameter codes to expose in the Consumer hub
enum eHubParamType
{
    HUB_NO_ALERT,
    HUB_HR,
    HUB_RR,
    HUB_MOVEMENT,
    HUB_IN_BED,
    HUB_SLEEP_WAKE,
    // 5
    HUB_RELAXATION,
    HUB_SMART_WAKEUP,
    HUB_CALL_HYPNO,
    HUB_AVG_HR,
    HUB_AVG_RR,
    // 10
    HUB_AVG_SLEEP,
    HUB_WENT_TO_BED,
    HUB_TIME_AWAKENED,
    HUB_TIME_TO_FALL_ASLEEP,
    HUB_ACTUAL_SLEEP_TIME,//15
    HUB_SLEEP_SUMMARY_START,
    HUB_IN_BED_TOTAL,
    HUB_HEART_BEAT,
    HUB_RESP_CYCLE,
    HUB_MOV_DENSITY,// 20
    HUB_COMMAND_1,
    HUB_COMMAND_2,
    HUB_COMMAND_3,
    HUB_COMMAND_4,
    HUB_COMMAND_5,
    HUB_COMMAND_6,
    HUB_HYPNOGRAM_LENGTH,
    HUB_HYPNOGRAM_SLEEP_EFFICENCY,
    HUB_TIP_CATEGORY,
    HUB_TIP_INDEX,
    // 30
    HUB_PCT_WAKE,
    HUB_PC_REM,
    HUB_PCT_LS,
    HUB_PCT_SWS,
    HUB_PCT_OOB,
    HUB_SLEEP_SCORE_LATENCY,
    HUB_SLEEP_SCORE_BEX,
    HUB_SLEEP_SCORE_AWAKE,
    HUB_SLEEP_SCORE_SWS,
    HUB_SLEEP_SCORE_REM,
    // 40
    HUB_SLEEP_SCORE_SE,
    HUB_SLEEP_SCORE_TST,
    HUB_SLEEP_SCORE_BONUS,
    HUB_HR_STD,
    HUB_RR_STD,
    HUB_UNUSUAL_NIGHT_HR,
    HUB_UNUSUAL_NIGHT_RR,
    HUB_UNUSUAL_NIGHT_MOV,
    HUB_UNUSUAL_NIGHT_SLEEP_SCORE,
    HUB_UNUSUAL_NIGHT_SL,
    // 50
    HUB_UNUSUAL_NIGHT_BEX,
    HUB_UNUSUAL_NIGHT_WK,
    HUB_UNUSUAL_NIGHT_SWS,
    HUB_UNUSUAL_NIGHT_REM,
    HUB_UNUSUAL_NIGHT_SE,
    HUB_UNUSUAL_NIGHT_TST,
    HUB_SUGGEST_SLEEP_STUDY,
    HUB_MIN_WAKE,
    HUB_MIN_REM,
    HUB_MIN_LS,
    //60
    HUB_MIN_SWS,
    HUB_MIN_OOB,
    HUB_NUM_BARS_SLEEP_SCORE_LATENCY,
    HUB_NUM_BARS_SLEEP_SCORE_BEX,
    HUB_NUM_BARS_SLEEP_SCORE_AWAKE,
    HUB_NUM_BARS_SLEEP_SCORE_SWS,
    HUB_NUM_BARS_SLEEP_SCORE_REM,
    HUB_NUM_BARS_SLEEP_SCORE_SE,
    HUB_NUM_BARS_SLEEP_SCORE_TST,
    HUB_UNUSUAL_NIGHT_CLASS_HR,
    // 70
    HUB_UNUSUAL_NIGHT_CLASS_RR,
    HUB_UNUSUAL_NIGHT_CLASS_MOV,
    HUB_UNUSUAL_NIGHT_CLASS_SLEEP_SCORE,
    HUB_UNUSUAL_NIGHT_CLASS_SL,
    HUB_UNUSUAL_NIGHT_CLASS_BEX,
    HUB_UNUSUAL_NIGHT_CLASS_WK,
    HUB_UNUSUAL_NIGHT_CLASS_SWS,
    HUB_UNUSUAL_NIGHT_CLASS_REM,
    HUB_UNUSUAL_NIGHT_CLASS_SE,
    HUB_UNUSUAL_NIGHT_CLASS_TST,//80
    HUB_SWITCH_TO_DAY,
    HUB_STATUS_FOR_AC,
    HUB_NUM_OUT_OF_BEDS,
    HUB_PCT_REM_FROM_SLEEP,
    HUB_PCT_LS_FROM_SLEEP,
    HUB_PCT_SWS_FROM_SLEEP,
    HUB_WAKE_AFTER_SO,
    // 87
    HUB_TARGET_SCORE,
    HUB_AVG_HISTORY_HR,
    HUB_AVG_HISTORY_RR,
    // 90
    HUB_AVG_HISTORY_LATENCY,
    HUB_AVG_HISTORY_TIMES_AWAKE,
    HUB_AVG_HISTORY_TIME_IN_BED,
    HUB_LOWER_SCORE_RANGE_FOR_AGE,
    HUB_UPPER_SCORE_RANGE_FOR_AGE,
    HUB_AVG_HISTORY_SCORE,
    HUB_AC_SLEEPING_TIME,
    HUB_TURN_OFF_TV,
    // 98
    HUB_HYPNOGRAM_ID,
    HUB_HRV,// 100
    HUB_HRV_MIN,
    HUB_HRV_THRESHOLD_1,
    HUB_HRV_THRESHOLD_2,
    HUB_HRV_MAX,
    HUB_HR_HOME_ABOVE,
    HUB_RR_HOME_ABOVE,
    HUB_SIGNAL_QUALITY,
    HUB_LONG_OOB,
    HUB_TIME_AT_INIT,
    HUB_TIME_OF_SIGNAL_DETECTED,//110
    HUB_TIME_OF_LAST_BED_EXIT,
    HUB_STRESS_CATEGORY,
    HUB_AVG_STRESS,
    HUB_AVG_STRESS_CATEGORY,
    HUB_HR_HOME_BELOW = 123,
    HUB_RR_HOME_BELOW = 124
    
};



