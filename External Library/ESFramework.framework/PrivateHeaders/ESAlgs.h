/*
 * ESAlgs.h
 *
 *  Created on: Feb 15, 2015
 *      Author: matan.reinstein
 */

#ifndef ESALGS_H_
#define ESALGS_H_

#define AlgoTableSize		64
//[0] 0-this is main sensor 1-this is not the main sensor ; [1] 1
#define ParamIsMainSensor	0
//[0] low limit ; [1] high limit
#define ParamHRLimit		1
//[0] low limit ; [1] high limit
#define ParamRRLimit		2
//[1] low motion time in seconds
#define ParamMOVLimit 		3
//[0] bed/chair duration ; [1] 0-off 1-on
#define ParamBEXLimit 		4
//[0] not admit delay ; [1] not measured delay
#define ParamSignalDetection 5
//[0] turn counter value in minutes ; [1] 0-not currently pressing reset, 1-turn reset performed
#define ParamPSCHLimit		6
//[0] 0-off 1-on ;
#define ParamPSCHAlarmOn	7
//[0] low limit ; [1] high limit
#define ParamSPO2Limit 		8
//[0] smart alert type report at startup
#define ParamSmartAlertType	9
//[0] age in months ; [1] birthdate - seconds since epoch
#define ParamPatientinfo	10
//[0] hospital type ; [1] working module: 0-safety 1-vitals 2-all
#define ParamHospitalType	12
//[0] 0-discharge 1-admit ; [1] SCOP
#define ParamAdmitPatient	12
//[0] time to alert low ; [1] time to alert high
#define ParamTime2AlertHR	13
//[0] time to alert low ; [1] time to alert high
#define ParamTime2AlertRR	14
//[0] height; [1] weight
#define ParamHeight			15
//[0] People in bed; [1] gender (0 male, 1 female, -99 unknown)
#define ParamPeopleInBed	16
#define ParamSmartWakeTime  17
#define ParamBexAlg         18
#define ParamLastData		19

#if !defined(__cplusplus)
#define MONExternC extern
#else
#define MONExternC extern "C"
#endif

#import "SensorSignalParser.h"

//extern void algs_function_initialize();
extern void algs_hub_Consumer_initialize();

extern void algs_function_init(double sigType, const double recoveryMat[200]);

extern void algs_version(char ver[6]);

//extern double algs_config(const double input_mat[38]);
extern double algs_config(const double configTable[90], double paramCount);

extern void algs_function(const unsigned char piezo_data[360], const unsigned
						  char film_data[360], const double accX_data[2], const double accY_data[2],
						  const double accZ_data[2], double time1970, const double sensors_info[18],
						  double OutputTable[148]);

extern void getCurrentSummary(double t0, double t1, double OutputTable[128]);

extern void algs_trend(double type, double Output[600]);

extern void fertility_validator(int b_time1970, const double configTable[180],
                                double paramCount, int calendric_start_date_unixtime, const int
                                monthly_days_list[16], const int fertility_phases_data[], const int
                                fertility_phases_size[2], const int c_last_calendric_bleeding_indic[], const int d_last_calendric_bleeding_indic[2], const int all_daily_events_data[], const int
                                all_daily_events_size[2], int all_daily_events_rows, const int new_event[5],
                                unsigned char *b_is_valid_input, unsigned char *call_process_fertility_summary, int
                                *fertility_alertCode);

/*extern void process_HC_summary(const signed char HC_input[5760], double HC_error,
 double HC_start_time, double ES_key_value_output[64], double ES_SleepStage
 [2880], double ES_SleepDepth[2880], double ES_SleepSimplified[2880], double
 ES_SleepSimplified2[2880]);*/




#endif /* ESALGS_H_ */
