//
//  UIImage+GIF.h
//  LBGIFImage
//
//  Created by Laurin Brandner on 06.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSData+AKGIF.h"
#import <UIKit/UIKit.h>

@interface UIImage (GIF)

+ (UIImage *)aksd_animatedGIFNamed:(NSString *)name;
+ (UIImage *)aksd_animatedGIFWithData:(NSData *)data;

- (UIImage *)aksd_animatedImageByScalingAndCroppingToSize:(CGSize)size;

@end
