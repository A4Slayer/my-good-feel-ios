/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import <Foundation/Foundation.h>
#import "AKSDWebImageDownloader.h"
#import "AKSDWebImageOperation.h"

@interface AKSDWebImageDownloaderOperation : NSOperation <AKSDWebImageOperation>

@property (strong, nonatomic, readonly) NSURLRequest *request;
@property (assign, nonatomic, readonly) AKSDWebImageDownloaderOptions options;

- (id)initWithRequest:(NSURLRequest *)request
              options:(AKSDWebImageDownloaderOptions)options
             progress:(AKSDWebImageDownloaderProgressBlock)progressBlock
            completed:(AKSDWebImageDownloaderCompletedBlock)completedBlock
            cancelled:(void (^)())cancelBlock;

@end
