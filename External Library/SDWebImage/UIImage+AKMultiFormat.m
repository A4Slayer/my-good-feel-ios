//
//  UIImage+MultiFormat.m
//  SDWebImage
//
//  Created by Olivier Poitrey on 07/06/13.
//  Copyright (c) 2013 Dailymotion. All rights reserved.
//

#import "UIImage+AKMultiFormat.h"
#import "UIImage+AKGIF.h"

#ifdef AKSD_WEBP
#import "UIImage+AKWebP.h"
#endif

@implementation UIImage (MultiFormat)

+ (UIImage *)aksd_imageWithData:(NSData *)data
{
    UIImage *image;

    if ([data aksd_isGIF])
    {
        image = [UIImage aksd_animatedGIFWithData:data];
    }
    else
    {
        image = [[UIImage alloc] initWithData:data];
    }

#ifdef AKSD_WEBP
    if (!image) // TODO: detect webp signature
    {
        image = [UIImage aksd_imageWithWebPData:data];
    }
#endif

    return image;
}

@end
